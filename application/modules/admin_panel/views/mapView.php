<!-- <!DOCTYPE html>
<html>
<body>

<h1>Please follow this location </h1>


<div id="map" style="width:100%;height:500px"></div>

<!--<script>
function myMap() {
  var mapCanvas = document.getElementById("map");
  var mapOptions = {
    center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
    zoom: 10
  }
  
  var map = new google.maps.Map(mapCanvas, mapOptions);
}   
</script>
<script>
function myMap() {
  var mapCanvas = document.getElementById("map");
  var myCenter = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>); 
  var mapOptions = {center: myCenter, zoom: 12};
  var map = new google.maps.Map(mapCanvas,mapOptions);
  var marker = new google.maps.Marker({
    position: myCenter,
    animation: google.maps.Animation.BOUNCE
  });
  marker.setMap(map);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?callback=myMap&key=AIzaSyB21_tNUz6ehR3DAqEcM68u_7ZgasLLZH8"></script>

</body>
</html> -->

<!DOCTYPE html>
<html>
<head>
    <!-- Header -->
    <?php   $this->load->view('segments/header');   
            $this->load->view('segments/leftMenu');  ?>
	
    <!-- //Left side column. contains the logo and sidebar -->
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        
      <h1>
	  
       <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?>
        <small></small>
      </h1>
        <h1>Please follow this location </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?></li>
        <!--<li class="active">Dashboard</li>-->
      </ol>
    </section>
	<div class="row">
	 <div class="col-md-12">
	 <?php if(!empty($this->session->flashdata('success_message'))){?>
	  <div class="alert alert-success">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('success_message'); ?>
		</div>
	  <?php }?>
	  </div>
	  </div>
<div class="box box-info well" style="overflow-x:auto;">
	<div class="row">
		<div class="col-md-12">
		     <table id="settings" class="table table-bordered table-hover">
				<div id="map" style="width:100%;height:500px"></div>

<
			</table>
		</div>
	</div>
	</div>
	
	
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Footer -->
 <?php include('segments/footer.php'); ?> 
<!--// Footer -->
  <!-- Control Sidebar -->
  <?php include('segments/controlSidebar.php'); ?>
  <!-- /.control-sidebar -->
  
</div>
<!-- ./wrapper -->
  

<!-- jQuery 2.2.0 -->
<?php include('segments/jquery.php'); ?>
<script>
  $(function () {
    $('#settings').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>
<script>
function myMap() {
  var mapCanvas = document.getElementById("map");
  var myCenter = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>); 
  var mapOptions = {center: myCenter, zoom: 12};
  var map = new google.maps.Map(mapCanvas,mapOptions);
  var marker = new google.maps.Marker({
    position: myCenter,
    animation: google.maps.Animation.BOUNCE
  });
  marker.setMap(map);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?callback=myMap&key=AIzaSyB21_tNUz6ehR3DAqEcM68u_7ZgasLLZH8"></script>

<!-- //jQuery 2.2.0 -->
</body>
</html>
