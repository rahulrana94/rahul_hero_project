<!DOCTYPE html>
<html>
<head>
    <!-- Header -->
    <?php   $this->load->view('segments/header');   
	    $this->load->view('segments/leftMenu');  ?>
        <!-- //Left side column. contains the logo and sidebar -->
<style>
.imageround{
	border-radius:50%;
	height:0px;
	width:0px;
}
</style> 

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>	 
       <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?></li>
        <!--<li class="active">Dashboard</li>-->
      </ol>
    </section>
    
   
	<div class="row">
	 <div class="col-md-12">
	 <?php if(!empty($this->session->flashdata('success_message'))){?>
	  <div class="alert alert-success">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('success_message'); ?>
            </div>
          <?php }else if(!empty($this->session->flashdata('error_message'))){?>
              <div class="alert alert-error">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('error_message'); ?>
            </div>
              
         <?php }?>
	  </div>
	
	  </div>
  
	<form method="post" action="" autocomplete="off" enctype="multipart/form-data">
         <div class="box box-info">
	  <div class="row">
		<div class="col-md-12">
		<div class="form-group">
                  <label class="col-sm-2 control-label" for="inputEmail3"></label>
                   <div class="col-sm-6">
                
                  </div>
                </div>
		</div>
	</div>

	
        <div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1">Upload PDF</div>
			<div class="col-md-4"><input type="file" name="pdf"  value="<?php echo @$user->profileImage;?>"   data-input="false"> 
			<span class="error"><?php echo form_error('pdf');?></span>
			 <span id="lblError" style="color: red;"></span>
			</div>
			<div class="col-md-4"><?php if(!empty($user->profileImage)){ ?> <img src="<?php echo base_url();?><?php echo 'profileImages/'.$user->profileImage;?>" style="height:20%; width:30%; border-radius:50%;"><?php } ?></div>
		</div>
	</div>
        
        
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1"> </div>
                          <input type="hidden" name="old_image" value="<?php echo @$setting->icon_img; ?>">
			<div class="col-md-4"><button type="submit" class="btn btn-success" id="btnUpload" >submit</button> <a href="<?php echo base_url('index.php/admin/dashboard');?>" class="btn btn-primary"> Cancel</a></div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<br>
	</div>
	</form>
	<?php if(isset($setting)) {
            
          //  pre($setting);
            ?>
    
   
    <div class="box box-info well" style="overflow-x:auto;">
	<div class="row">
		<div class="col-md-12">
		     <table id="settings" class="table table-bordered table-hover">
				<thead>
				<tr>
						<th>S.N</th>
						<th>PDF</th>
						<th>Version</th>
                                                <th>Registration Date</th>
						<th>View</th>
						
				</tr>
				</thead>
				<tbody>
				<?php if(!empty($setting)){$i=1; 
                                foreach($setting as $settingg ){
                                    $id = $settingg['id'];
                                    
                                    ?>
					
				<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $settingg['pdf']; ?></td>
				<td><?php echo $settingg['version']; ?></td>
				<td><?php echo $settingg['created_on']; ?></td>
                                <td><?php if(empty($settingg['pdf']))   { ?> <a href="<?php echo base_url() . "uploads/" . $settingg['pdf']; ?>">VIEW</a> <?php }else { ?> <a href="<?php echo base_url() . "uploads/" . $settingg['pdf']; ?>"   target="_blank">View </a><?php } ?></td>
<!--                                <td><a href="">Download</a></td>-->
			
				</tr>
				
                                <?php $i++; } } ?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
     <?php } ?>
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Footer -->
 <?php include('segments/footer.php'); ?> 
<!--// Footer -->
  <!-- Control Sidebar -->
  <?php include('segments/controlSidebar.php'); ?>
  <!-- /.control-sidebar -->
  
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<?php include('segments/jquery.php'); ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/timePicker/kendo.all.min.js" ></script>

<!-- jQuery 2.2.0 -->
<?php include('segments/jquery.php'); ?>
<script>
  $(function () {
    $('#settings').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

</body>
</html>
