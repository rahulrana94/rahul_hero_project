<!DOCTYPE html>
<html>
<head>
    <!-- Header -->
    <?php   $this->load->view('segments/header');   
      $this->load->view('segments/leftMenu');  ?>
	
    <!-- //Left side column. contains the logo and sidebar -->
<style>
.imageround{
	border-radius:50%;
	height:0px;
	width:0px;
}
</style>  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
	  
       <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">   <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?></li>
      </ol>
    </section>
	<div class="row">
	 <div class="col-md-12">
	 <?php if(!empty($this->session->flashdata('success_message'))){?>
	  <div class="alert alert-success">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('success_message'); ?>
		</div>
	  <?php }?>
	  </div>
	  </div>
	<div class="box box-info well" style="overflow-x:auto;">
	<div class="row">
		
	</div>
	</div>
	
	
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Footer -->
 <?php include('segments/footer.php'); ?> 
<!--// Footer -->
  <!-- Control Sidebar -->
  <?php include('segments/controlSidebar.php'); ?>
  <!-- /.control-sidebar -->
  
</div>
<!-- ./wrapper -->
  

<!-- jQuery 2.2.0 -->
<?php include('segments/jquery.php'); ?>
<!-- //jQuery 2.2.0 -->
</body>
</html>
