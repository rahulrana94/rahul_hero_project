<!DOCTYPE html>
<html>
<head>
    <!-- Header -->
    <?php   $this->load->view('segments/header');   
	    $this->load->view('segments/leftMenu');  ?>
	
    <!-- //Left side column. contains the logo and sidebar -->
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
	  
       <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?></li>
        <!--<li class="active">Dashboard</li>-->
      </ol>
    </section>
    
   
      <div class="row">
	 <div class="col-md-12">
	 <?php if(!empty($this->session->flashdata('success_message'))){?>
	  <div class="alert alert-success">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?php echo $this->session->flashdata('success_message'); ?>
		</div>
          <?php }?>
               
	  </div>
	
	  </div>
  
	<form method="post" action="" autocomplete="off" enctype="multipart/form-data">
<div class="box box-info">
	<div class="row">
		<div class="col-md-12">
		<div class="form-group">
                  <label class="col-sm-2 control-label" for="inputEmail3"></label>
                   <div class="col-sm-6">
                
                  </div>
                </div>
		</div>
	</div>
  
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1"> Callout charge </div>
			<div class="col-md-4"><input type="text" name="callout_charge" id="callout_charge"  class="form-control" placeholder="Enter callout charge" value="<?php if(isset($setting->callout_charge)) { echo $setting->callout_charge; }else{ echo set_value('callout_charge'); } ?>"> 
			<span class="error"><?php echo form_error('callout_charge');?></span>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1"> Vat </div>
			<div class="col-md-4"><input type="text" name="vat" id="vat"  class="form-control" placeholder="Enter rate of vat" value="<?php if(isset($setting->vat)) { echo $setting->vat; }else{ echo set_value('vat'); } ?>"> 
			<span class="error"><?php echo form_error('vat');?></span>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1"> Commission </div>
			<div class="col-md-4"><input type="text" name="commission" id="commission"  class="form-control" placeholder="Enter rate of commission" value="<?php if(isset($setting->commision)) { echo $setting->commision; }else{ echo set_value('commission'); } ?>"> 
			<span class="error"><?php echo form_error('commission');?></span>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
        
	<br>
        
        <div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1"> Maximum Quote </div>
			<div class="col-md-4"><input type="text" name="maximum_quote" id="maximum_quote"  class="form-control" placeholder="Enter maximum number of quote" value="<?php if(isset($setting->maximum_quote)) { echo $setting->maximum_quote; }else{ echo set_value('maximum_quote'); } ?>"> 
			<span class="error"><?php echo form_error('maximum_quote');?></span>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
        <br>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1"> </div>
			<div class="col-md-4"><button type="submit" class="btn btn-success" id="btnUpload" >submit</button> <a href="<?php echo base_url('index.php/admin/dashboard');?>" class="btn btn-primary"> Cancel</a></div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<br>
	</div>
	</form>
	
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Footer -->
 <?php include('segments/footer.php'); ?> 
<!--// Footer -->
  <!-- Control Sidebar -->
  <?php include('segments/controlSidebar.php'); ?>
  <!-- /.control-sidebar -->
  
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<?php include('segments/jquery.php'); ?>
<script>
$(document).ready(function(){
	$('#datepicker').datepicker({
		  format: 'yyyy-mm-dd',
		//startDate: '-3d',
		//endDate: '-2d',
		 autoclose: true
	});
	$('#date').datepicker({
		  format: 'yyyy-mm-dd',
		//startDate: '-3d',
		//endDate: '-2d',
		 autoclose: true
	});
});

</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/timePicker/kendo.all.min.js" ></script>

<script type="text/javascript">
			 
           $(document).ready(function(){
			   
			 $("#time").timepicker({
					showInputs: false,
                                       format: 'HH:MM'
				});
				 
				  $(".startTime").timepicker({
					showInputs: false,
                                       format: 'HH:mm'
				});
		   });
		   
</script>

</body>
</html>
