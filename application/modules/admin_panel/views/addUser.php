<!DOCTYPE html>
    <!-- Header -->
<?php   $this->load->view('segments/header');   
	$this->load->view('segments/leftMenu'); 
        ?>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
       #role1,#role2
       {
           display:none;
       }
       
    </style>
   <script>
$(document).ready(function(){
    $('#role').on('change', function() {
      if ( this.value == '1' || this.value == '2' || this.value == '3'|| this.value == '6')
      //.....................^.......
      {
           $("#role1").hide();
        $("#role2").hide();
      }
      else  if ( this.value == '4' || this.value == '5')
      {
          $("#role1").show();
          $("#role2").hide();
      }
      else  if ( this.value == '7')
      {
          $("#role1").show();
          $("#role2").show();
      }
       else  
      {
        $("#role1").hide();
        $("#role2").hide();
      }
    });
});
</script>


<script type="text/javascript">


    $(document).ready(function() {

        $('select[name="state"]').on('change', function() {

            var stateID = $(this).val();
            
      // alert(stateID);

            if(stateID) {

                $.ajax({

                    url: '/hero/index.php/admin/myformAjax/'+stateID,
                    type: "GET",
                    dataType: "json",

                    success:function(data) {
                        $('select[name="city"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="city"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');
                         //   $('select[name="city"]').append('<option value="'+ value.name +'">'+ value.name +'</option>');

                        });

                    }

                });

            }else{

                $('select[name="city"]').empty();

            }

        });

    });

</script>
   
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
	  
       <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?></li>
        <!--<li class="active">Dashboard</li>-->
      </ol>
    </section>
    
   
      <div class="row">
	 <div class="col-md-12">
	 <?php if(!empty($this->session->flashdata('success_message'))){?>
	  <div class="alert alert-success">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?php echo $this->session->flashdata('success_message'); ?>
		</div>
          <?php }?>
               
	  </div>
	
	  </div>
  
	<form method="post" action="" autocomplete="off" enctype="multipart/form-data">
<div class="box box-info">
	<div class="row">
		<div class="col-md-12">
		<div class="form-group">
                  <label class="col-sm-2 control-label" for="inputEmail3"></label>
                   <div class="col-sm-6">
                
                  </div>
                </div>
		</div>
	</div>
 <!--Role -->
 <div class="row">
		<div class="col-md-12">

                <div class="col-md-3 col-md-push-1">Role</div>

               <div class="col-md-4"> 
                <select name="role" id="role" class="form-control" style="width:350px">
                    <option value="">--- Select Role ---</option>
                    <?php
                        foreach ($user_roles as $key => $value) {
                          echo "<option value='".$value->id."'>".$value->role_name."</option>";
                       }
                   ?>
                </select>
                   <span class="error"><?php echo form_error('role');?></span>
               </div>
                    </div>
	</div>
 <br>
  <div class="row" id="role1">
		<div class="col-md-12">
                <div class="col-md-3 col-md-push-1">RSM</div>
               <div class="col-md-4"> 
                  <select name="state" id="state" class="form-control" style="width:350px">
                    <option value="">--- Select RSM ---</option>
                    <!--<option value="">abc</option>-->
                    <!--<option value="">xyz</option>-->
                     <?php
                        foreach ($states as $key => $value) {
                           // echo "<option value='".$value->id."'>".$value->name."</option>";
                            echo "<option value='".$value->userid."'>".$value->first_name."</option>";
                        }
                    ?>

                </select>
               </div>
                    </div>
	</div>
 <br>
  <div class="row" id="role2">
		<div class="col-md-12">
                <div class="col-md-3 col-md-push-1">Dealer</div>
               <div class="col-md-4"> 
                <select name="city" class="form-control" style="width:350px">
                    <option value="0">--- Select Dealer ---</option>
                </select>
               </div>
                    </div>
	</div>
 <!-- Role End -->
        <br>
        <div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1">User Name</div>
                        <div class="col-md-4"><input type="text" name="username"  id="username" class="form-control" placeholder="Enter your username" value="<?php if(isset($student->username)) { echo $student->username; }else{ echo set_value('username'); } ?>" >
				<span class="error"><?php echo form_error('username');?></span>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
        
                <br>
        <div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1">Password</div>
			<div class="col-md-4"><input type="password" name="password"  id="password" class="form-control" placeholder="Enter your password" value="<?php if(isset($student->password)) { echo $student->password; }else{ echo set_value('password'); } ?>" >
				<span class="error"><?php echo form_error('password');?></span>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
        <br>
        <div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1">E-mail</div>
                        <div class="col-md-4"><input type="email" name="email"  id="email" class="form-control"  placeholder="Enter your email address" value="<?php if(isset($student->email)) { echo $student->email; }else{ echo set_value('email'); } ?>" >
				<span class="error"><?php echo form_error('email');?></span>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
        <br>
        
   
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1">Mobile </div>
                        <div class="col-md-4"><input type="tel" name="mobile" id="mobile" maxlength="15"  accept="numeric" placeholder="Enter your moblie number" class="form-control"  value="<?php if(isset($student->mobile)) { echo $student->mobile; }else{ echo set_value('mobile'); } ?>"> 
			<span class="error"><?php echo form_error('mobile');?></span>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
        <br>
        
        
        <div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1">First Name</div>
			<div class="col-md-4"><input type="text" name="first_name"  id="first_name" class="form-control" placeholder="Enter your First name" value="<?php if(isset($student->first_name)) { echo $student->first_name; }else{ echo set_value('first_name'); } ?>" >
				<span class="error"><?php echo form_error('first_name');?></span>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
        <br>
      
        <div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1">Last Name</div>
			<div class="col-md-4"><input type="text" name="last_name"  id="last_name" class="form-control" placeholder="Enter your Last name" value="<?php if(isset($student->last_name)) { echo $student->last_name; }else{ echo set_value('last_name'); } ?>" >
				<span class="error"><?php echo form_error('last_name');?></span>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

        
       	
       
       
<!--            <div class="form-group">
                    <label for="title">Select State:</label>
                <select name="state" id="state" class="form-control" style="width:350px">
                    <option value="">--- Select State ---</option>
                    <?php
                        foreach ($states as $key => $value) {
                            echo "<option value='".$value->id."'>".$value->name."</option>";
                        }
                    ?>
                </select>
            </div>


            <div class="form-group">
                <label for="title">Select City:</label>
                <select name="city" class="form-control" style="width:350px">
                </select>
            </div>
        -->
        
        
        
        
        
        
        
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1"> </div>
			<div class="col-md-4"><button type="submit" class="btn btn-success" id="btnUpload" >Submit</button> <a href="<?php echo base_url('index.php/admin/dashboard');?>" class="btn btn-primary"> Cancel</a></div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<br>
	</div>
	</form>
	
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Footer -->
 <?php include('segments/footer.php'); ?> 
<!--// Footer -->
  <!-- Control Sidebar -->
  <?php include('segments/controlSidebar.php'); ?>
  <!-- /.control-sidebar -->
  
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->





 <?php include('segments/jquery.php'); ?>


</body>
</html>
