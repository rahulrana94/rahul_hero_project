<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php if(isset($title)){ echo $title; }?></title>
  <link rel="shortcut icon" href="<?php echo base_url("assets/topMenuLogo.gif");?>"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css"/>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.css"/>

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"/>
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"/>
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css"/>
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css"/>
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/flat/blue.css"/>
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/morris/morris.css"/>
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css"/>
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datepicker/datepicker3.css"/>
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker-bs3.css"/>
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"/>
 

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  
  <![endif]-->
<style>
.error{
	color:red;
}
</style>
  </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    
    <!-- Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('index.php/admin/dashboard'); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>B</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>Bazinga</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
      
	  <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
	
          <?php /*  ?>   
          
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo base_url()?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo base_url()?>assets/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo base_url()?>assets/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo base_url()?>assets/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo base_url()?>assets/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
 */		?>
		  
<!-- Notifications: style can be found in dropdown.less -->
<?php 
$customerList = customer();
//pre($customer);die;
$newcust= "";
$days = "";
foreach($customerList as $cust) {
//if($cust['created_on'] == date('Y-m-d')) {
$newcust = $newcust+1;

$date1 = strtotime($cust['created_on']);
$date2 = strtotime(date('Y-m-d'));
$days = $date2 - $date1;
$days = round($days / 86400);
if ($days < 1) {
$days = 'today';
} else if ($days <= 31) {
$days = $days . ' days ago';
} else if ($days > 32) {
$m = $days / 30;
$m = intval($m);
$days = $m . ' month ago';
}
//}
}
?>


<?php 
$CustomerRating = customerRating();
//pre($CustomerRating);die;                        
$Rate= "";
$days2 = "";
foreach($CustomerRating as $CustomerRate) {
    
$date = explode(" ",$CustomerRate['create_date']);
//if($date['0'] == date('Y-m-d')) {
$Rate = $Rate+1;

//pre($Rate);die('jai');

$date1 = strtotime($CustomerRate['create_date']);
$date2 = strtotime(date('Y-m-d'));

//pre($date2);die;
$days2 = $date2 - $date1;
$days2 = round($days2 / 86400);
if ($days2 < 1) {
$days2 = 'today';
} else if ($days2 <= 31) {
$days2 = $days2 . ' days ago';
} else if ($days2 > 32) {
$m = $days2 / 30;
$m = intval($m);
$days2 = $m . ' month ago';
}
//}
}

?>


<?php 
$CustomerQuote = customerQuote();
//pre($CustomerRating);die;                        
$quote= "";
$days3 = "";
foreach($CustomerQuote as $Quote) {
    
$date = explode(" ",$Quote['create_date']);
//if($date['0'] == date('Y-m-d')) {
$quote = $quote+1;

//pre($quote);die('jai');

$date1 = strtotime($Quote['create_date']);
$date2 = strtotime(date('Y-m-d'));

//pre($date2);die;
$days3 = $date2 - $date1;
$days3 = round($days3 / 86400);
if ($days3 < 1) {
$days3 = 'today';
} else if ($days3 <= 31) {
$days3 = $days3 . ' days ago';
} else if ($days3 > 32) {
$m = $days3 / 30;
$m = intval($m);
$days3 = $m . ' month ago';
}
//}
}

?>





<?php 
$cancelled= cancelled();
//pre($cancelled);die;                        
$cancled= "";
$days4 = "";
foreach($cancelled as $cancle) {
    
$date = explode(" ",$cancle['created_on']);
//if($date['0'] == date('Y-m-d')) {
$cancled = $cancled+1;

//pre($quote);die('jai');

$date1 = strtotime($cancle['created_on']);
$date2 = strtotime(date('Y-m-d'));

//pre($date2);die;
$days4 = $date2 - $date1;
$days4 = round($days4 / 86400);
if ($days4 < 1) {
$days4 = 'today';
} else if ($days4 <= 31) {
$days4 = $days4 . ' days ago';
} else if ($days4 > 32) {
$m = $days4 / 30;
$m = intval($m);
$days4 = $m . ' month ago';
}
//}
}

?>


<?php 
$reschedules = reschedules();
//pre($reschedules);die;                        
$reschedule= "";
$days5 = "";
foreach($reschedules as $resche) {
    
$date = explode(" ",$resche['created_on']);
//if($date['0'] == date('Y-m-d')) {
$reschedule = $reschedule+1;

//pre($quote);die('jai');

$date1 = strtotime($resche['created_on']);
$date2 = strtotime(date('Y-m-d'));

//pre($date2);die;
$days5 = $date2 - $date1;
$days5 = round($days5 / 86400);
if ($days5 < 1) {
$days5 = 'today';
} else if ($days5 <= 31) {
$days5 = $days5 . ' days ago';
} else if ($days5 > 32) {
$m = $days5 / 30;
$m = intval($m);
$days5 = $m . ' month ago';
}
//}
}

?>



<?php 
$expiredJobs = expiredJobs();
//pre($expiredJobs);die;                        
$expire= "";
$days6 = "";
foreach($expiredJobs as $expiredJob) {
    
//$date = explode(" ",$expiredJob['create_date']);
//if($date['0'] == date('Y-m-d')) {
$expire = $expire+1;

//pre($quote);die('jai');

$date1 = strtotime($expiredJob['date']);
$date2 = strtotime(date('Y-m-d'));

//pre($date2);die;
$days6 = $date2 - $date1;
$days6 = round($days6 / 86400);
if ($days6 < 1) {
$days6 = 'today';
} else if ($days6 <= 31) {
$days6 = $days6 . ' days ago';
} else if ($days6 > 32) {
$m = $days6 / 30;
$m = intval($m);
$days6 = $m . ' month ago';
}
//}
}

?>

<?php 
$missedAppointment = missedAppointment();
//pre($missedAppointment);die;                        
$missed= "";
$days7 = "";
foreach($missedAppointment as $missedAppoint) {
    
//$date = explode(" ",$missedAppoint['date']);
//if($date['0'] == date('Y-m-d')) {
$missed = $missed+1;

//pre($quote);die('jai');

$date1 = strtotime($missedAppoint['date']);
$date2 = strtotime(date('Y-m-d'));

//pre($date2);die;
$days7 = $date2 - $date1;
$days7 = round($days7 / 86400);
if ($days7 < 1) {
$days7 = 'today';
} else if ($days7 <= 31) {
$days7 = $days7 . ' days ago';
} else if ($days7 > 32) {
$m = $days7 / 30;
$m = intval($m);
$days7 = $m . ' month ago';
}
//}
}

?>

<?php 

$lateAppointment = lateAppointment();
//  pre($lateAppointment);die('Headred');                        
$late= "";
$days8 = "";
foreach($lateAppointment as $lateAppoint) {
    
//$date = explode(" ",$lateAppoint['date']);
//if($date['0'] == date('Y-m-d')) {
$late = $late+1;

//pre($quote);die('jai');

$date1 = strtotime($lateAppoint['date']);
$date2 = strtotime(date('Y-m-d'));

//pre($date2);die;
$days8 = $date2 - $date1;
$days8 = round($days8 / 86400);
if ($days8 < 1) {
$days8 = 'today';
} else if ($days8 <= 31) {
$days8 = $days8 . ' days ago';
} else if ($days8 > 32) {
$m = $days8 / 30;
$m = intval($m);
$days8 = $m . ' month ago';
}
//}
}

?>


<?php 
$providerRating = providerRating();
//pre($providerRating);die;                        
$ProRate= "";
$days9 = "";
foreach($providerRating as $providerRate) {
    
//$date = explode(" ",$providerRate['create_date']);
//if($date['0'] == date('Y-m-d')) {
$ProRate = $ProRate+1;

//pre($Rate);die('jai');

$date1 = strtotime($providerRate['create_date']);
$date2 = strtotime(date('Y-m-d'));

//pre($date2);die;
$days9 = $date2 - $date1;
$days9 = round($days9 / 86400);
if ($days9 < 1) {
$days9 = 'today';
} else if ($days9 <= 31) {
$days9 = $days9 . ' days ago';
} else if ($days9 > 32) {
$m = $days9 / 30;
$m = intval($m);
$days9 = $m . ' month ago';
}
//}
}

?>




          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?php echo $newcust + $Rate + $quote + $cancled + $reschedule + $expire + $missed  + $late + $ProRate; ?></span>

              
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php echo $newcust + $Rate + $quote + $cancled + $reschedule + $expire + $missed  + $late + $ProRate; ?> notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="<?php echo base_url()?>index.php/admin/customerList">
                      <i class="fa fa-anchor"></i> <?php echo $newcust ;?> new customer  <?php echo $days; ?>
                    </a>
                  </li>
                   <li>
                    <a href="<?php echo base_url()?>index.php/admin/customerRating">
                      <i class="fa fa-microphone"></i> <?php echo $Rate ;?> new customer rating <?php echo $days2; ?>
                    </a>
                  </li>
                   <li>
                    <a href="<?php echo base_url()?>index.php/admin/quoteRecieved">
                      <i class="fa fa-list"></i> <?php echo $quote ;?>  new high quote value <?php echo $days3; ?>
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url()?>index.php/admin/cancelled">
                      <i class="fa fa-warning text-yellow"></i> <?php echo $cancled ;?>  new canceled job <?php echo $days4; ?>
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url()?>index.php/admin/reschedules">
                      <i class="fa fa-user text-red"></i> <?php echo $reschedule;?>  new reschedules job <?php echo $days5; ?>
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url()?>index.php/admin/expiredJobs">
                      <i class="fa fa-user text-red"></i> <?php echo $expire;?>  new expired job <?php echo $days6; ?>
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url()?>index.php/admin/missedAppointment">
                      <i class="fa fa-user text-red"></i> <?php echo $missed;?>  new missed Appointment <?php echo $days7; ?>
                    </a>
                  </li>
                <li>
                    <a href="<?php echo base_url()?>index.php/admin/lateAppointment">
                      <i class="fa fa-user text-red"></i> <?php echo $late;?>  new late Appointment <?php echo $days8; ?>
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url()?>index.php/admin/providerRating">
                      <i class="fa fa-user text-red"></i> <?php echo $ProRate;?>  new  provider rating <?php echo $days9; ?>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer" id="show"><a href="#">View all</a></li>
            </ul>
          </li>

   <?php /*  ?>       
          
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
				 
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
				     
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
         
          
          <!-- User Account: style can be found in dropdown.less -->
          
            
            */ ?>
            
            
 <li class="dropdown user user-menu">
		 
	
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="<?php echo base_url()?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
        <!--<img src="<?php echo base_url()?>assets/dist/img/Profile_picture.jpg" class="user-image" alt="User Image">-->
        <span class="hidden-xs">Admin</span>
        </a>
		 
           
			
				
		
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url()?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                <!--<img src="<?php echo base_url()?>assets/dist/img/Profile_picture.jpg" class="img-circle" alt="User Image">-->

                <p>
              
                </p>
              </li>
              <!-- Menu Body -->
<?php /*			  
			<li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
				
                <!-- /.row -->
              </li>
			  */?>
              <!-- Menu Footer-->
              <li class="user-footer">
		
	        <div class="pull-left">
	        <!--<a href="#" class="btn btn-default btn-flat">Profile</a>-->
                </div>
			
			           
			  <div class="pull-right">
                  <a href="<?php echo base_url('index.php/admin/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
<?php /*         
		 <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
		  */?>
        </ul>
      </div>
    </nav>
  </header>
