<aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
            <?php            
             		$session = $this->session->userdata('session_name');
			$username = $session->username;
			$password = $session->password;
		        $userData = $this->db->select('*')->from('admin')->where(array('username'=>$username,'password'=>$password,'status'=>'1'))->get()->row_array();                              
                    
                    $type = $this->db->select('*')->from('role')->where(array('id'=>$userData['user_type']))->get()->row_array();           
                    $module_name =   $type['module_name'];
                    $exp = explode(',',$module_name);
                    
                  //  pre($exp);     // Taking All assisn module value into array ,After this i am using in_array function for accessiability of modules.
             ?>
            
            
            
          <a href="<?php echo base_url('index.php/admin/dashboard');?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
          </a>
        </li>

           <?php  if (in_array("1", $exp)){ ?>
        
        <li class="treeview" value="1">
          <a href="#">
            <i class="fa fa-anchor"></i> <span>Admin Management</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">        
            <li class="active"><a href="<?php echo base_url('index.php/admin/addRole'); ?>"><i class="fa fa-circle-o"></i> Add Role</a></li>
            <li class="active"><a href="<?php echo base_url('index.php/admin/listRole'); ?>"><i class="fa fa-circle-o"></i> List Role</a></li>
          </ul>
        </li>
           <?php } ?> 
        
        <?php  if (in_array("2", $exp)){ ?>
        <li class="treeview" value="2">
          <a href="#">
            <i class="fa fa-anchor"></i> <span>User Management</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">        
               <li class="active"><a href="<?php echo base_url('index.php/admin/addUser'); ?>"><i class="fa fa-circle-o"></i> Add User</a></li>
          </ul>
        </li>
              <?php } ?>  
        
        
        <?php  if (in_array("3", $exp)){ ?>
         <li class="treeview" value="3">
          <a href="#">
            <i class="fa fa-anchor"></i> <span>Sales Management</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">        
            <li class="active"><a href="<?php echo base_url('index.php/admin/uploadPDF'); ?>"><i class="fa fa-circle-o"></i> Upload Catalogue</a></li>
          </ul>
        </li>
        <?php } ?> 
        
        
        <?php  if (in_array("4", $exp)){ ?>
         <li class="treeview" value="4">
          <a href="#">
            <i class="fa fa-anchor"></i> <span>Complaint Management</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">        
            <li class="active"><a href="<?php echo base_url('index.php/admin/uploadPDF'); ?>"><i class="fa fa-circle-o"></i> Upload Catalogue</a></li>
          </ul>
        </li>
        
        <?php } ?>  
        
         <?php  if (in_array("5", $exp)){ ?>
         <li class="treeview" value="5">
          <a href="#">
            <i class="fa fa-anchor"></i> <span>Performance Management</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">        
            <li class="active"><a href="<?php echo base_url('index.php/admin/uploadPDF'); ?>"><i class="fa fa-circle-o"></i> Upload Catalogue</a></li>
          </ul>
        </li>
        
	  <?php } ?> 
        
        <?php  if (in_array("6", $exp)){ ?>
         <li class="treeview" value="6">
          <a href="#">
            <i class="fa fa-anchor"></i> <span>Catalogue Management</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">        
            <li class="active"><a href="<?php echo base_url('index.php/admin/uploadPDF'); ?>"><i class="fa fa-circle-o"></i> Upload Catalogue</a></li>
          </ul>
        </li>
        
	  <?php } ?> 
        
        
         <?php  if (in_array("7", $exp)){ ?>
         <li class="treeview" value="7">
          <a href="#">
            <i class="fa fa-anchor"></i> <span>Master  Management</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">        
            <li class="active"><a href="<?php echo base_url('index.php/admin/uploadPDF'); ?>"><i class="fa fa-circle-o"></i> Upload Catalogue</a></li>
          </ul>
        </li>
        
	  <?php } ?> 
        
        
        
      </ul>
    </section>
  </aside>
