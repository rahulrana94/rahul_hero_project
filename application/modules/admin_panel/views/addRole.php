<!DOCTYPE html>
<html>
<head>
    <!-- Header -->
    <?php   $this->load->view('segments/header');   
	    $this->load->view('segments/leftMenu');  ?>
	

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
	  
       <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?></li>
        <!--<li class="active">Dashboard</li>-->
      </ol>
    </section>
    
   
      <div class="row">
	 <div class="col-md-12">
	 <?php if(!empty($this->session->flashdata('success_message'))){?>
	  <div class="alert alert-success">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?php echo $this->session->flashdata('success_message'); ?>
		</div>
          <?php }?>
               
	  </div>
	
	  </div>
  
	<form method="post" action="" autocomplete="off" enctype="multipart/form-data">
<div class="box box-info">
	<div class="row">
		<div class="col-md-12">
		<div class="form-group">
                  <label class="col-sm-2 control-label" for="inputEmail3"></label>
                   <div class="col-sm-6">
                
                  </div>
                </div>
		</div>
	</div>
 
        <br>
        <div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1">Role Name</div>
                        <div class="col-md-4"><input type="text" name="role_name"  id="role_name" class="form-control" placeholder="Enter the role name" value="<?php if(isset($student->name)) { echo $student->name; }else{ echo set_value('name'); } ?>" >
				<span class="error"><?php echo form_error('role_name');?></span>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
        
         
        <br>
        
<div>  
  <div class="row" >
    <div class="col-md-12">
        <div class="col-md-3 col-md-push-1">Modules</div>
        <div class="col-md-8">
      <label class="checkbox-inline">
        <input type="checkbox" name="modules[]" value="1">Role Management
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" name="modules[]" value="2">User Management
      </label>
      <label class="checkbox-inline">
        <!--<input type="checkbox" name="modules[]" value="3" checked="checked">Catalogue Management-->
        <input type="checkbox" name="modules[]" value="3" >Catalogue Management
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" name="modules[]" value="4">Complaint Management
      </label>
            <label class="checkbox-inline">
        <input type="checkbox" name="modules[]" value="5">Sales Management
      </label>
    </div>
  </div> 
     
  </div>
         <div class="row">
    <div class="col-md-12">
        <div class="col-md-3 col-md-push-1">  </div>
        <div class="col-md-8">
      <label class="checkbox-inline">
        <input type="checkbox" name="modules[]" value="6">Performance Management
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" name="modules[]" value="7">Master Management
      </label>
    </div>
  </div> 
     
  </div>
</div>
        
 	
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-push-1"> </div>
			<div class="col-md-4"><button type="submit" class="btn btn-success" id="btnUpload" >Submit</button> <a href="<?php echo base_url('index.php/admin/dashboard');?>" class="btn btn-primary"> Cancel</a></div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<br>
	</div>
	</form>
	
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Footer -->
 <?php include('segments/footer.php'); ?> 
<!--// Footer -->
  <!-- Control Sidebar -->
  <?php include('segments/controlSidebar.php'); ?>
  <!-- /.control-sidebar -->
  
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<?php include('segments/jquery.php'); ?>
<script>
$(document).ready(function(){
	$('#datepicker').datepicker({
		  format: 'yyyy-mm-dd',
		//startDate: '-3d',
		//endDate: '-2d',
		 autoclose: true
	});
	$('#date').datepicker({
		  format: 'yyyy-mm-dd',
		  startDate: '0d',
		//endDate: '-2d',
		 autoclose: true
	});
});

</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/timePicker/kendo.all.min.js" ></script>

<script type="text/javascript">
			 
           $(document).ready(function(){
			   
			 $("#time").timepicker({
                                        showInputs: false,
                                        format: 'HH:MM'
				});
				 
				  $(".startTime").timepicker({
					showInputs: false,
                                        format: 'HH:mm'
				});
		   });
		   
</script>

</body>
</html>
