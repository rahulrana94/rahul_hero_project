<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php if(isset($title)) { echo $title; }?></title>
    <link href="<?php echo base_url('assets/css/'); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/'); ?>/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/'); ?>/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/'); ?>/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
    .error{
		color:red;
		}
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
					<div class="row">
					 <div class="col-md-12">
					 <?php if(!empty($this->session->flashdata('error_message'))){?>
						 
					  <div class="alert alert-danger">
						  
								<?php echo $this->session->flashdata('error_message'); ?>
						</div>
					  <?php }?>
					  </div>
					</div>
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
					<form method="post" action="" autocomplete="off">
                    <div class="panel-body">
                        <div style="color: red"></div>
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username" type="text" value="">
									<span class="error"><?php echo form_error('username');?></span>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id='password' placeholder="Password" name="password" type="password" value="">
									<span class="error"><?php echo form_error('password');?></span>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type='submit'  class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                  
                    </div>
					</form>
                </div>
				
            </div>
        </div>
    </div>
<?php //$this->load->view('footer'); ?>
<script>
	function close()
	{
		$( "div" ).remove( ".alert" );
		//$( ".alert" ).remove();
	}
</script>
