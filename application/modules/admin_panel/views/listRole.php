<!DOCTYPE html>
<html>
<head>
    <!-- Header -->
    <?php   $this->load->view('segments/header');   
            $this->load->view('segments/leftMenu');  ?>
	
    <!-- //Left side column. contains the logo and sidebar -->
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
	  
       <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> <?php if(isset($pageTitle) && !empty($pageTitle)){ echo $pageTitle; }?></li>
        <!--<li class="active">Dashboard</li>-->
      </ol>
    </section>
	<div class="row">
	 <div class="col-md-12">
	 <?php if(!empty($this->session->flashdata('success_message'))){?>
	  <div class="alert alert-success">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('success_message'); ?>
		</div>
	  <?php }?>
	  </div>
	  </div>
<div class="box box-info well" style="overflow-x:auto;">
	<div class="row">
		<div class="col-md-10">
		     <table id="settings" class="table table-bordered table-hover">
				<thead>
				<tr>
						<th>S.N</th>
						<th>Role</th>
                                                <th>Assign Modules</th>
						<th>Upload On</th>
						
				</tr>
				</thead>
				<tbody>
				<?php if(!empty($setting)){$i=1; foreach($setting as $settingg ){ $id = $settingg['id']; ?>
					
				<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $settingg['role_name']; ?></td>                               
				<td><?php echo $settingg['seprated']; ?></td>
				<td><?php echo $settingg['created_on']; ?></td>
			
				
				</tr>
				
                                <?php $i++; } } ?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
	
	
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Footer -->
 <?php include('segments/footer.php'); ?> 
<!--// Footer -->
  <!-- Control Sidebar -->
  <?php include('segments/controlSidebar.php'); ?>
  <!-- /.control-sidebar -->
  
</div>
<!-- ./wrapper -->
  

<!-- jQuery 2.2.0 -->
<?php include('segments/jquery.php'); ?>
<script>
  $(function () {
    $('#settings').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>
<!-- //jQuery 2.2.0 -->
</body>
</html>
