<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends MX_Controller
{
	
    function __construct()
    {
                parent::__construct();
                $this->load->library('session');
                $this->load->library('form_validation','uploads');
                $this->load->helper(array('form','url'));
                $this->load->helper('html');
                $this->load->model('Admin_model');
                $this->load->helper('cookie');
           
    }
    	public  function index(){
		$obj = new admin_model();
		if($_POST){
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if($this->form_validation->run() == TRUE){
				$postedArr = $this->security->xss_clean($_POST);
				$username=$obj->escape_str($postedArr['username']);
				$password=$obj->escape_str($postedArr['password']);
				$row = $obj->isUserExist($username,$password);
                              //  pre($row);die;
				if(count($row)>0){		
                               //  $mySession = array('userid'=>$row->userid,'status'=>$row->status,'username'=>$row->username,'first_name'=>$row->first_name,'last_name'=>$row->last_name,'email'=>$row->email);
                               //pre($mySession);die;
                               //  $this->session->set_userdata($mySession);
                               //  pre($_SESSION);die;
                                  $this->session->set_userdata('session_name',$row);
                                //    pre($_SESSION);die;
                                   redirect(base_url('index.php/admin/dashboard'));		
				}else {
					$this->session->set_flashdata('error_message','Please login with your correct username and password!');
					redirect('admin');
				}			
			} 	
		}
		$data = array('title'=>'Admin Login','pageTitle'=>'Admin Login');
		$this->load->view('userLogin',$data);
		
	} 
       
    
	public  function index29July17()
	{
		if(!empty($this->session->userdata('session_name')))
		{
			$session = $this->session->userdata('session_name');
                     //  pre($session);die;
			$userId = $session->userid;
                      // pre($userId);die;
			if($userId !='')
			{
				 redirect('admin/dashboard');
			}
		}
		
		$obj = new admin_model();
		if($_POST)
		{
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if($this->form_validation->run() == TRUE)
			{
				$postedArr = $this->security->xss_clean($_POST);
				$username=$obj->escape_str($postedArr['username']);
				$password=$obj->escape_str($postedArr['password']);
				$row=$obj->userLogin('admin',$username,$password);
                               // pre(count($row));die;
				if(count($row)>0)
				{	
					
					if($row->user_type == '1') 
					{
						if($row->status == '1') 
						{	
							if($row->email_status == '1') 
							{
								$mySession = array('userid'=>$row->userid,'status'=>$row->status,'username'=>$row->username,'first_name'=>$row->first_name,'last_name'=>$row->last_name,'email'=>$row->email);
								$this->session->set_userdata($mySession);
								$this->session->set_userdata('session_name',$row);
                                                                
								redirect(base_url('index.php/admin/dashboard'));
							}else{
								$this->session->set_flashdata('error_message','Your email not verified,Please verified your email address!');
								redirect('admin/adminLogin');
							}
						
						}else{
							$this->session->set_flashdata('error_message','Your account is not activated,Please contact to administrator  !');
							redirect('admin');
						}
					}else{
					
						$this->session->set_flashdata('error_message','wrong username and password !');
						redirect('admin');
					}
				}else {
					$this->session->set_flashdata('error_message','Please login with your correct username and password!');
					redirect('admin');
				}			
			} 	
		}
		$data = array('title'=>'Admin Login','pageTitle'=>'Admin Login');
		$this->load->view('userLogin',$data);
		
	} 
	
        
   
        
	public function dashboard()
	{
			
		$data = array('title'=>'Dashboard','pageTitle'=>'Dashboard');
		$this->load->view('dashboard',$data);
	}
    public function logout()
    {
       $this->session->unset_userdata('some_name');
       $this->session->sess_destroy();
       redirect('admin','refresh');
    }	
	public function providersDetails()
	{
		if(!empty($this->session->userdata('session_name')))
		{
			$session = $this->session->userdata('session_name');
			$userId = $session->userid;
			if($userId !='')
			{
				$data = array('title'=>'Provider Details','pageTitle'=>'Provider Details');
				$data['providers'] 	= $this->db->select('*')->from('providers')->get()->result();
				//pre($data);die("rana");
                              //  foreach ($data['providers'] as $providerdata){
                                    
                              //  }
                                $this->load->view('providers',$data);
			}
		}else{
			 redirect('admin','refresh');
		}
	}
        
        public function customerList()
	{
		if(!empty($this->session->userdata('session_name')))
		{
			$session = $this->session->userdata('session_name');
			$userId = $session->userid;
			if($userId !='')
			{
                               // $this->db->update('isRead',"1");
                                $this->db->set("isRead",1);
                                $this->db->update("customers");
				$data = array('title'=>'Customer Details','pageTitle'=>'Customer Details');
				$data['customerList'] 	= $this->db->select('*')->from('customers')->get()->result();
				//pre($data);die("rana");
                                $this->load->view('customerList',$data);
			}
		}else{
			 redirect('admin','refresh');
		}
	}
        
        
        
        public function businessDetails()
	{
		if(!empty($this->session->userdata('session_name')))
		{
			$session = $this->session->userdata('session_name');
			$userId = $session->userid;
			if($userId !='')
			{
				$data = array('title'=>'Business Details List','pageTitle'=>'Business Details List');
				$data['providers'] 	= $this->db->select('*')->from('providers')->get()->result();
                                //pre($data);die("businessDetails");
				$this->load->view('businessDetails',$data);
			}
		}else{
			 redirect('admin','refresh');
		}
	}
        
        public function documentDetails()
	{
		if(!empty($this->session->userdata('session_name')))
		{
			$session = $this->session->userdata('session_name');
			$userId = $session->userid;
			if($userId !='')
			{
				$data = array('title'=>'Document Details List','pageTitle'=>'Document Details List');
				$data['providers'] 	= $this->db->select('*')->from('providers')->get()->result();
                                //pre($data);die("businessDetails");
				$this->load->view('documentDetails',$data);
			}
		}else{
			 redirect('admin','refresh');
		}
	}
    public function deleteProviderList()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        //echo $id; die('delete Provider');
                        $table = 'providers';
                        $obj            = new admin_model;
                        $result         = $obj->deleteProviderList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Provider has been deleted successfully');
				redirect('admin/providersDetails');
                        }
	
	      }
        
	}   
        
        
       public function deleteCustomerList()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        //echo $id; die('delete Provider');
                        $table = 'customers';
                        $obj            = new admin_model;
                        $result         = $obj->deleteCustomerList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Customer has been deleted successfully');
				redirect('admin/customerList');
                        }
	
	      }
        
	}    
        
        
        public function deleteBusinessDetails()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        //echo $id; die('delete Provider');
                        $table = 'providers';
                        $obj            = new admin_model;
                        $result         = $obj->deleteProviderList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Business details has been deleted successfully');
				redirect('admin/businessDetails');
                        }
	
	      }
        
	}   
        
            public function deleteDocumentDetails()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        //echo $id; die('delete Provider');
                        $table = 'providers';
                        $obj            = new admin_model;
                        $result         = $obj->deleteProviderList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Document details has been deleted successfully');
				redirect('admin/documentDetails');
                        }
	
	      }
        
	}   
      
        
	public function activation($id=null)
	{
		$data=array();
		if(isset($_GET['providerId']) != "")
		{
			$id =base64_decode($_GET['providerId']);
			$where = array('prov_id'=>$id);
			$userInfo	= $this->db->select('*')->from('providers')->where(array('prov_id'=>$id))->get()->row();
			if($userInfo->status == '0'){
				$updateStatus = array('status'=>'1');
				$flag_message = 'Documents has been verified successfully.';
			}
			if($userInfo->status == '1'){
				$updateStatus = array('status'=>'0');
				$flag_message = ' Documents has been rejected';
			}
			$this->db->update('providers',$updateStatus,$where);
			$this->session->set_flashdata("success_message",$flag_message);
			redirect('admin/providersDetails');
		}
	}
        
        
        	
	public function household()
	{
            if(!empty($this->session->userdata('session_name')))
		{
                //$sessionName = $this->session->userdata('session_name');
                //$adminId = $sessionName->id;                
                //pre($adminId);die;
                $data=array('title'=>'Household','pageTitle'=>'Add Household');    
                $data['household'] = $this->db->select('*')->get_where('sub_category',array('main_category'=>1))->result();
                
                //pre($data);die;
                
                
		
//		if(isset($_GET['zxcvbnm']))
//		{
//			$userId =  base64_decode($_GET['zxcvbnm']);
//			$data=array('title'=>'Edit Teacher','pageTitle'=>'Edit Teacher');
//			$data['teacher'] = $this->db->select('*')->from($table)->where(array('id'=>$userId))->get()->row();
//		}
//		else
//		{
//			$data=array('title'=>'Add Teacher','pageTitle'=>'Add Teacher');
//                        $data['routes'] = $this->db->select('*')->from('schl_routes')->where(array('schoolId'=>$schoolId))->get()->result();
//		}
                
                $table='customer_job';
		if($_POST)
		{
			$this->form_validation->set_rules('budget','Budget','trim|required');
			$this->form_validation->set_rules('job_description','Job description','trim|required');
			$this->form_validation->set_rules('location','Address','trim|required');
			
			
			if($this->form_validation->run()==TRUE)
			{
				$main_categoryId 		= $this->input->post('main_categoryId');
				$imidiate_callout 		= $this->input->post('imidiate_callout');
				$date 	                        = $this->input->post('date');
				$time       	                = $this->input->post('time');
				$promocode	                = $this->input->post('promocode');
				$budget                         = $this->input->post('budget');
				$job_description                = $this->input->post('job_description');
				$i_will_suply_all_material      = $this->input->post('i_will_suply_all_material');
				$location                       = $this->input->post('location');	
                                $created                        = date('Y-m-d H:i:s');
                                $prepAddr = str_replace(' ','+',$location);
                                $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
                                $output= json_decode($geocode);
                                $lat = $output->results[0]->geometry->location->lat;
                                $long = $output->results[0]->geometry->location->lng;
				if(empty($imidiate_callout))
                                {
                                    $imidiate_callout = 0;
                                }
                                if(empty($i_will_suply_all_material))
                                {
                                    $i_will_suply_all_material = '0';
                                }
				
                                $timeExplode =  explode(" ", $time) ;
                                //pre($timeExplode[1]);die;
                                
                                if(!empty($time) and  $timeExplode[1]!="AM")
                                {
                                $timeExplode =  explode(" ", $time) ;
                                $timeExplode1 = explode(":", $timeExplode[0]) ;
                                $timeExplode2 = $timeExplode1[0] + 12;
                                $time = $timeExplode2.':'.$timeExplode1[0].':00';
                                }
                               else
                                {
                                $time = $timeExplode[0].':00';  
                                }
				//pre($time);die;
                                
					$insertArray = array(
									'main_cat_id'      =>1,
									'sub_cat_id'       =>$main_categoryId,
									'user_id'          =>3,
									'imidiate_callout' =>$imidiate_callout,
									'date'             =>$date,
									'time'             =>$time,
									'promocode'        =>$promocode,			                          
                                                                        'create_date'      =>$created,
                                                                        'status'           =>0,
                                                                     //   'modified'=>$modified 
                                            
                                            
									);
                                        //pre($insertArray);die;
					$result = $this->db->insert($table,$insertArray);
                                        //pre($result); die;
                                        $lastinsertedId = $this->db->insert_id();
                                        $table2 ='customer_job_household_relation';
                                        
                                        
                               
					$insertArray2 = array(
                                                                        'job_id'=>$lastinsertedId,
									'budget'=>$budget,
									'job_description'=>$job_description,
									'i_will_suply_all_material'=>$i_will_suply_all_material,
									'location'=>$location,
									'location_lat'=>$lat,
									'location_long'=>$long,
 
									);
                                       // pre($insertArray2); die();
					$this->db->insert($table2,$insertArray2);
				
					if($result ==TRUE)
					{
						$this->session->set_flashdata('success_message','Job added sucessfully');
						redirect(base_url('index.php/admin/household'));
						
					}
				
							
			}
		}	
		$this->load->view('household',$data);
                }else{
			 redirect('admin','refresh');
		}
	}
 
          
          	
	public function outdoor()
	{
            if(!empty($this->session->userdata('session_name')))
		{
            $data=array('title'=>'Outdoor','pageTitle'=>'Add Outdoor');
            $data['outdoor'] = $this->db->select('*')->get_where('sub_category',array('main_category'=>2))->result();
             
            //pre($data);die;

            $table='customer_job';
            if($_POST)
            { 
            $this->form_validation->set_rules('budget','Budget','trim|required');
            $this->form_validation->set_rules('job_description','Job description','trim|required');
            $this->form_validation->set_rules('location','Address','trim|required');


            if($this->form_validation->run()==TRUE)  
            {
            $main_categoryId 		    = $this->input->post('main_categoryId');
            $imidiate_callout 		    = $this->input->post('imidiate_callout');
            $date 	                    = $this->input->post('date');
            $time       	            = $this->input->post('time');
            $promocode	                    = $this->input->post('promocode');
            $budget                         = $this->input->post('budget');
            $job_description                = $this->input->post('job_description');
            $i_will_suply_all_material      = $this->input->post('i_will_suply_all_material');
            $location                       = $this->input->post('location');	
            $created                        = date('Y-m-d H:i:s');
            $prepAddr = str_replace(' ','+',$location);
            $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
            $output= json_decode($geocode);
            $lat = $output->results[0]->geometry->location->lat;
            $long = $output->results[0]->geometry->location->lng;
           // $modified = date('Y-m-d H:i:s');
            if(empty($imidiate_callout))
            {
                $imidiate_callout = 0;
            }
            if(empty($i_will_suply_all_material))
            {
                $i_will_suply_all_material = '0';
            }

            /////////////////////////
            
            $timeExplode =  explode(" ", $time) ;
                                //pre($timeExplode[1]);die;
                                
                                if(!empty($time) and  $timeExplode[1]!="AM")
                                {
                                $timeExplode =  explode(" ", $time) ;
                                $timeExplode1 = explode(":", $timeExplode[0]) ;
                                $timeExplode2 = $timeExplode1[0] + 12;
                                $time = $timeExplode2.':'.$timeExplode1[0].':00';
                                }
                               else
                                {
                                $time = $timeExplode[0].':00';  
                                }
                                
            ////////////////////////////
            $insertArray = array(
                                'main_cat_id'      =>2,
                                'sub_cat_id'       =>$main_categoryId,
                                'user_id'          =>3,
                                'imidiate_callout' =>$imidiate_callout,
                                'date'             =>$date,
                                'time'             =>$time,
                                'promocode'        =>$promocode,			                          
                                'create_date'      =>$created,
                                'status'           =>0,
                             //   'modified'=>$modified   
                                );
                    $result = $this->db->insert($table,$insertArray);
                    //pre($result); die;
                    $lastinsertedId = $this->db->insert_id();
                    
                    $table2 ='customer_job_outdoor_relation';
                    $insertArray2 = array(
                                'job_id'=>$lastinsertedId,
                                'budget'=>$budget,
                                'job_description'=>$job_description,
                                'i_will_suply_all_material'=>$i_will_suply_all_material,
                                'location'=>$location,
                                'location_lat'=>$lat,
                                'location_long'=>$long,
                                );
                    // pre($insertArray2); die();
                    $this->db->insert($table2,$insertArray2);
                            
            if($result ==TRUE) {
            $this->session->set_flashdata('success_message','Job added sucessfully');
            redirect(base_url('index.php/admin/outdoor'));
            }
				
							
			}
		}	
		$this->load->view('outdoor',$data);
                }else{
			 redirect('admin','refresh');
		}
	}
       
        public function personal()
	{
            
if(!empty($this->session->userdata('session_name')))
		{
            $data=array('title'=>'Personal','pageTitle'=>'Add Personal');
            $data['personal'] = $this->db->select('*')->get_where('sub_category',array('main_category'=>3))->result();
             
            //pre($data);die;

            $table='customer_job';
            if($_POST)
            {           
            $this->form_validation->set_rules('job_description','Job description','trim|required');
            $this->form_validation->set_rules('location','Address','trim|required');
            $this->form_validation->set_rules('number_of_people','number of people','required|numeric');
            $this->form_validation->set_rules('parking','parking','required');


            if($this->form_validation->run()==TRUE)  
            {
            $main_categoryId 		    = $this->input->post('main_categoryId');
            $imidiate_callout 		    = $this->input->post('imidiate_callout');
            $date 	                    = $this->input->post('date');
            $time       	            = $this->input->post('time');
            $promocode	                    = $this->input->post('promocode');
            $budget                         = $this->input->post('budget');
            $job_description                = $this->input->post('job_description');
            $number_of_people               = $this->input->post('number_of_people');
            $parking                        = $this->input->post('parking');
            $location                       = $this->input->post('location');	
            $created                        = date('Y-m-d H:i:s');
            $prepAddr = str_replace(' ','+',$location);
            $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
            $output= json_decode($geocode);
            $lat = $output->results[0]->geometry->location->lat;
            $long = $output->results[0]->geometry->location->lng;
           // $modified = date('Y-m-d H:i:s');
            if(empty($imidiate_callout))
            {
                $imidiate_callout = 0;
            }
          ///////////////////////////////////
            $timeExplode =  explode(" ", $time) ;
                                //pre($timeExplode[1]);die;
                                
                                if(!empty($time) and  $timeExplode[1]!="AM")
                                {
                                $timeExplode =  explode(" ", $time) ;
                                $timeExplode1 = explode(":", $timeExplode[0]) ;
                                $timeExplode2 = $timeExplode1[0] + 12;
                                $time = $timeExplode2.':'.$timeExplode1[0].':00';
                                }
                               else
                                {
                                $time = $timeExplode[0].':00';  
                                }
          //////////////////////////////////
            $insertArray = array(
            'main_cat_id'      =>3,
            'sub_cat_id'       =>$main_categoryId,
            'user_id'          =>3,
            'imidiate_callout' =>$imidiate_callout,
            'date'             =>$date,
            'time'             =>$time,
            'promocode'        =>$promocode,			                          
            'create_date'      =>$created,
            'status'           =>0,
                             //   'modified'=>$modified   
                                );
            $result = $this->db->insert($table,$insertArray);
            //pre($result); die;
            $lastinsertedId = $this->db->insert_id();

            $table2 ='customer_job_personal_relation';
            $insertArray2 = array(
            'job_id'                   =>$lastinsertedId,
            'job_description'          =>$job_description,
            'number_of_people'         =>$number_of_people,
            'parking'                  =>$parking,                              
            'location'                 =>$location,
            'location_lat'             =>$lat,
            'location_long'            =>$long,
                                );
                    // pre($insertArray2); die();
                    $this->db->insert($table2,$insertArray2);
                            
            if($result ==TRUE) {
            $this->session->set_flashdata('success_message','Job added sucessfully');
            redirect(base_url('index.php/admin/personal'));
            }
				
							
			}
		}	
		$this->load->view('personal',$data);
                }else{
			 redirect('admin','refresh');
		}
	}
        
         public function transport()
	{
             
if(!empty($this->session->userdata('session_name')))
		{
            $data=array('title'=>'Transport','pageTitle'=>'Add Transport');
            $data['transport'] = $this->db->select('*')->get_where('sub_category',array('main_category'=>4))->result();
             
            //pre($data);die;

            $table='customer_job';
            if($_POST)
            {           
            $this->form_validation->set_rules('job_description','Job description','trim|required');
            $this->form_validation->set_rules('pickup_flights','Pick up flight','required|numeric');
            $this->form_validation->set_rules('dropoff_location','Drop off flights','required|numeric');
            //$this->form_validation->set_rules('items_to_move','Move item','required|numeric');
            $this->form_validation->set_rules('pickup_flights','pick up flight','required|numeric');
            $this->form_validation->set_rules('dropoff_flights','Drop off flight','required|numeric');
            $this->form_validation->set_rules('pickup_location','Pick up location','required');
            $this->form_validation->set_rules('dropoff_location','Drop off location','required');         
            $this->form_validation->set_rules('estimated_value','Estimated Value','required');
           // $this->form_validation->set_rules('image','image','required');
            
            
            if($this->form_validation->run()==TRUE)  
            {
            $main_categoryId 		    = $this->input->post('main_categoryId');
            $imidiate_callout 		    = $this->input->post('imidiate_callout');
            $date 	                    = $this->input->post('date');
            $time       	            = $this->input->post('time');
            $items_to_move      	    = $this->input->post('items_to_move');          
            $i_can_assist_the_movers        = $this->input->post('i_can_assist_the_movers');
            $insure_item      	            = $this->input->post('insure_item');
            $promocode	                    = $this->input->post('promocode');
            $estimated_value                = $this->input->post('estimated_value');
            $job_description                = $this->input->post('job_description');
            $pickup_flights                 = $this->input->post('pickup_flights');
            $dropoff_flights                = $this->input->post('dropoff_flights');
            $pickup_location                = $this->input->post('pickup_location');
            $dropoff_location               = $this->input->post('dropoff_location');
            $created                        = date('Y-m-d H:i:s');
            $prepAddr = str_replace(' ','+',$pickup_location);
            $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
            $output= json_decode($geocode);
            $lat = $output->results[0]->geometry->location->lat;
            $long = $output->results[0]->geometry->location->lng;
            
            
            $prepAddr1 = str_replace(' ','+',$dropoff_location);
            $geocode1=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr1.'&sensor=false');
            $output1= json_decode($geocode1);
            $lat1 = $output1->results[0]->geometry->location->lat;
            $long1 = $output1->results[0]->geometry->location->lng;
            
            if(empty($imidiate_callout))
            {
                $imidiate_callout = 0;
            }
            
            
            $count = count($_FILES);
            if($count>20){
            $this->session->set_flashdata('success_message','over twenty item can not be moved');
            redirect(base_url('index.php/admin/transport'));
            }
            //pre($count);die; 
            for($i=0; $i<$count; $i++)
            {
            $filename                       = $_FILES['image'.$i]['name'];
	    $ext	                    = pathinfo($filename, PATHINFO_EXTENSION);          
            $image = '';
            if (in_array($ext, array('png', 'jpeg', 'jpg', 'PNG', 'JPEG', 'JPG', 'gif', 'GIF'))) 
                    {                
                    $image = rand(999,9999).$filename;
                    $final[] = $image;                
                    move_uploaded_file($_FILES['image'.$i]['tmp_name'], getcwd().'/transportImages/'.$final[$i]);           
                    }	            
            }
            
            if(!empty($final)){
                   $implode = implode(",",$final);
            }
            else{
                $implode = "";
            }
                   
                   //echo $implode;die;
            /////////////////////////////////////////
             $timeExplode =  explode(" ", $time) ;
                                //pre($timeExplode[1]);die;
                                
                                if(!empty($time) and  $timeExplode[1]!="AM")
                                {
                                $timeExplode =  explode(" ", $time) ;
                                $timeExplode1 = explode(":", $timeExplode[0]) ;
                                $timeExplode2 = $timeExplode1[0] + 12;
                                $time = $timeExplode2.':'.$timeExplode1[0].':00';
                                }
                               else
                                {
                                $time = $timeExplode[0].':00';  
                                }      
             /////////////////////////////////////////      
                   
            $insertArray = array(
                                'main_cat_id'           =>4,
                                'sub_cat_id'            =>0,
                                'user_id'               =>3,
                                'imidiate_callout'      =>$imidiate_callout,
                                'date'                  =>$date,
                                'time'                  =>$time,
                                'promocode'             =>$promocode,			                          
                                'create_date'           =>$created,
                                'status'                =>0,
                             //   'modified'=>$modified   
                                );
                    $result = $this->db->insert($table,$insertArray);
                    //pre($result); die;
                    $lastinsertedId = $this->db->insert_id();
                    
                    $table2 ='customer_job_transport_relation';
                    $insertArray2 = array(
                                'job_id'                                   =>$lastinsertedId,
                                'items_to_move'                            =>$items_to_move,
                                'image'                                    =>$implode,
                                'i_can_assist_the_movers'                  =>$i_can_assist_the_movers,
                                'insure_item'                              =>$insure_item,
                                'estimated_value'                          =>$estimated_value,
                                'pickup_flights'                           =>$pickup_flights,
                                'dropoff_flights'                          =>$dropoff_flights,
                                'pickup_location'                          =>$pickup_location,
                                'dropoff_location'                         =>$dropoff_location,
                                'job_description'                          =>$job_description,                        
                                'pickup_location_lat'                      =>$lat,
                                'pickup_location_long'                     =>$long,
                                'dropoff_location_lat'                     =>$lat1,
                                'dropoff_location_long'                    =>$long1,
                                );
                    // pre($insertArray2); die();
            $this->db->insert($table2,$insertArray2);
                            
            if($result ==TRUE) {
            $this->session->set_flashdata('success_message','Job added sucessfully');
            redirect(base_url('index.php/admin/transport'));
            }
				
							
			}
		}	
		$this->load->view('transport',$data);
                 }else{
			 redirect('admin','refresh');
		}
	}
        
        public function setting()
	{
            
        
if(!empty($this->session->userdata('session_name')))
		{ 
		$table='setting';
		if(isset($_GET['zxcvbnm']))
		{
			$userId =  base64_decode($_GET['zxcvbnm']);
			$data=array('title'=>'Edit Setting Details','pageTitle'=>'Edit Setting Details');
			$data['setting'] = $this->db->select('*')->from($table)->where(array('id'=>1))->get()->row();
		}else{
			$data=array('title'=>'Add Setting Details','pageTitle'=>'Add Setting Details');
		}
		if($_POST)
		{
                $this->form_validation->set_rules('callout_charge','Callout charge','required|numeric');    
                $this->form_validation->set_rules('vat','vat','required|numeric');    
                $this->form_validation->set_rules('commission','Commision','required|numeric');    
                $this->form_validation->set_rules('maximum_quote','Maximum Quote','required|numeric');    
		if($this->form_validation->run()==TRUE)
		{
			$callout_charge          = $this->input->post('callout_charge');
			$vat                     = $this->input->post('vat');
			$commission              = $this->input->post('commission');
			$maximum_quote           = $this->input->post('maximum_quote');
			
                        
                        
                        if(!isset($_GET['zxcvbnm']))
			{
				$insertArray = array(
                                                    'callout_charge'=>$callout_charge,
                                                    'vat'=>$vat,
                                                    'commision'=>$commission,
                                                    'maximum_quote'=>$maximum_quote,
                                                  
                                                    );
                                    $result = $this->db->insert($table,$insertArray);
                                  
                                    if($result ==TRUE)
                                    {
                                            $this->session->set_flashdata('success_message','Setting  has been added successfully');
                                            redirect(base_url('index.php/admin/setting'));
                                            
                                    }
			}
                        elseif(isset($_GET['zxcvbnm']))
			{
				$updateArray = array(
						 'callout_charge'   =>$callout_charge,
                                                 'vat'              =>$vat,
                                                 'commision'       =>$commission,
                                                 'maximum_quote'    =>$maximum_quote,
						
														);
				$this->db->where(array('id'=>1));
				$result = $this->db->update($table,$updateArray);
				if($result ==TRUE)
				{
					$this->session->set_flashdata('success_message','Setting has been updated  successfully');
					redirect(base_url('index.php/admin/settingList'));
				}
			}									
		}
	}
	$this->load->view('setting',$data);
         }else{
			 redirect('admin','refresh');
		}
	}
        
        
        public function settingList()
	{	
            if(!empty($this->session->userdata('session_name')))
		{
            $obj = new admin_model();                
            $data=array('title'=>'Setting Details List','pageTitle'=>'Setting Details List');
            $data['setting'] = $obj->getSettingList();
            //pre($data['setting']);die;
            $this->load->view('settingList',$data);	
            }else{
			 redirect('admin','refresh');
		}
	}
        
        
       	public function addProviders()
	{
            if(!empty($this->session->userdata('session_name')))
		{
                $data=array('title'=>'Add Providers Reletion','pageTitle'=>'Add Providers Reletion');    
                $data['household'] = $this->db->select('*')->get_where('sub_category')->result();
                $data['providers'] = $this->db->select('prov_id,firstName')->get_where('providers')->result();
                //echo $this->db->last_query(); die;
                //pre($data);die;
               // pre($data['providers']);die;
                
                $table='providers_subcat_relation';
                $obj = new admin_model();
		if($_POST)
		{
			$this->form_validation->set_rules('providers','Providers','trim|required');
			$this->form_validation->set_rules('subcategoryId[]','Category','required');
			
			if($this->form_validation->run()==TRUE)
			{
                                $res = false;
				$SubcategoryIds		= $this->input->post('subcategoryId');
				$providerId             = $this->input->post('providers');				
                               
                                foreach($SubcategoryIds as $SubcategoryId){
                                    $alrdy = $obj->isIdExist($SubcategoryId,$providerId);
                                    if(!empty($alrdy)){
                                    $getSubCatName = $obj->getSubCatRecords("sub_category",$SubcategoryId);
                                    $insertArray = array(
                                            'business_subtype'        =>$SubcategoryId,
                                            'prov_id'                 =>$providerId,
                                            'business_subtype_name'   =>$getSubCatName,									
                                         );                       
                                        $result = $obj->insertData($table,$insertArray);
                                        if($result){ $res = TRUE;}
                                    }
                                }
                                 
                                        if($res ==TRUE)
                                        {
                                        $this->session->set_flashdata('success_message','Provider Relation added sucessfully');
                                        redirect(base_url('index.php/admin/addProviders'));						
                                        }else {
                                           $this->session->set_flashdata('success_message','Provider Relation already Exist');
                                            redirect(base_url('index.php/admin/addProviders'));
                                }
                                
                                
                                        }                                        
                                

			
		}	
		$this->load->view('addProviders',$data);
                }else{
			 redirect('admin','refresh');
		}
	}
  
        public function subHousehold()
	{
            if(!empty($this->session->userdata('session_name')))
		{
                $table='sub_category';     
                if(isset($_GET['zxcvbnm']))
		{
                $id =  base64_decode($_GET['zxcvbnm']);
                $data=array('title'=>'Edit Sub Cetegory','pageTitle'=>'Edit Sub Cetegory','key'=>'1');
                $data['setting'] = $this->db->select('*')->from($table)->where(array('sb_id'=>$id))->get()->row();
		}else{
                 $data=array('title'=>'Add Sub Cetegory','pageTitle'=>'Add Sub Cetegory','key'=>'0');   
                }
            if($_POST)
            {           
            $this->form_validation->set_rules('subHousehold','Household Cetegory','trim|required|max_length[20]');

            if($this->form_validation->run()==TRUE)  
            {
            $obj                            = new Admin_model;   
            $subHousehold 		    = $this->input->post('subHousehold');
            $filename                       = $_FILES['icon_img']['name'];
	    $ext                            = pathinfo($filename, PATHINFO_EXTENSION);
			
            $icon_img = '';
            if($_FILES['icon_img']['error'] == 0) {
            if (in_array($ext, array('png', 'jpeg', 'jpg', 'PNG', 'JPEG', 'JPG', 'gif', 'GIF'))) 
            {
            $icon_img = time().'.'.$ext;
            move_uploaded_file($_FILES['icon_img']['tmp_name'], getcwd().'/subcategoryimages/'.$icon_img);
            }		
            }
            else{
                   
             $icon_img = $this->input->post('old_image'); 
            }
            
            $obj                            = new Admin_model;
      /*      $insertArray = array(
                                'main_category'     =>1,
                                'name'              =>$subHousehold,
                                'icon_img'          =>$icon_img
                                );
            $result = $this->db->insert($table,$insertArray);
       
       
                                      
            if($result ==TRUE) {
            $this->session->set_flashdata('success_message','Household Cetegory added sucessfully');
            redirect(base_url('index.php/admin/subHousehold'));
            }
		*/	
            
            if(!isset($_GET['zxcvbnm']))
                         {
                                 $insertArray = array(
                                 'main_category'     =>1,
                                 'name'              =>$subHousehold,
                                 'icon_img'          =>$icon_img
                                 );
                         $result = $this->db->insert($table,$insertArray);

                         if($result ==TRUE) {
                         $this->session->set_flashdata('success_message','Sub Cetegory added sucessfully');
                         redirect(base_url('index.php/admin/SubCategoryList'));
                         }
			}
                        
                        elseif(isset($_GET['zxcvbnm']))
			{
				$updateArray = array(
                                'main_category'     =>1,
                                'name'              =>$subHousehold,
                                'icon_img'          =>$icon_img
                                );
				$this->db->where(array('sb_id'=>$id));
				$result = $this->db->update($table,$updateArray);
				if($result ==TRUE)
				{
					$this->session->set_flashdata('success_message','Sub Cetegory has been updated  successfully');
					redirect(base_url('index.php/admin/SubCategoryList'));
				}
			}
            
							
	  }
		}	
		$this->load->view('subHousehold',$data);
                }else{
			 redirect('admin','refresh');
		}
	}
        
         public function subOutdoor()
	{
             if(!empty($this->session->userdata('session_name')))
		{
                $table='sub_category';     
                if(isset($_GET['zxcvbnm']))
		{
                $id =  base64_decode($_GET['zxcvbnm']);
                $data=array('title'=>'Edit Sub Cetegory','pageTitle'=>'Edit Sub Cetegory','key'=>'1');
                $data['setting'] = $this->db->select('*')->from($table)->where(array('sb_id'=>$id))->get()->row();
		}else{
                 $data=array('title'=>'Add Sub Cetegory','pageTitle'=>'Add Sub Cetegory','key'=>'0');   
                }
            if($_POST)
            {           
            $this->form_validation->set_rules('subOutdoor','Outdoor Category','trim|required|max_length[20]');

            if($this->form_validation->run()==TRUE)  
            {
            $obj                            = new Admin_model;   
            $subOutdoor		    = $this->input->post('subOutdoor');
            $filename                       = $_FILES['icon_img']['name'];
	    $ext                            = pathinfo($filename, PATHINFO_EXTENSION);
			
            $icon_img = '';
             if($_FILES['icon_img']['error'] == 0) {
            if (in_array($ext, array('png', 'jpeg', 'jpg', 'PNG', 'JPEG', 'JPG', 'gif', 'GIF'))) 
            {
            $icon_img = time().'.'.$ext;
            move_uploaded_file($_FILES['icon_img']['tmp_name'], getcwd().'/subcategoryimages/'.$icon_img);
            }	
             }
        else{
                   
             $icon_img = $this->input->post('old_image'); 
            }
            
            $obj                            = new Admin_model;
         /*   $insertArray = array(
                                'main_category'     =>2,
                                'name'              =>$subOutdoor,
                                'icon_img'          =>$icon_img
                                );
            $result = $this->db->insert($table,$insertArray);
                                      
            if($result ==TRUE) {
            $this->session->set_flashdata('success_message','Outdoor Cetegory added sucessfully');
            redirect(base_url('index.php/admin/subOutdoor'));
            }*/
            
            if(!isset($_GET['zxcvbnm']))
                         {
                                 $insertArray = array(
                                 'main_category'     =>2,
                                 'name'              =>$subOutdoor,
                                 'icon_img'          =>$icon_img
                                 );
                         $result = $this->db->insert($table,$insertArray);

                         if($result ==TRUE) {
                         $this->session->set_flashdata('success_message','Sub Cetegory added sucessfully');
                         redirect(base_url('index.php/admin/SubCategoryList'));
                         }
			}
                        
                        elseif(isset($_GET['zxcvbnm']))
			{
				$updateArray = array(
                                'main_category'     =>2,
                                'name'              =>$subOutdoor,
                                'icon_img'          =>$icon_img
                                );
				$this->db->where(array('sb_id'=>$id));
				$result = $this->db->update($table,$updateArray);
				if($result ==TRUE)
				{
					$this->session->set_flashdata('success_message','Sub Cetegory has been updated  successfully');
					redirect(base_url('index.php/admin/SubCategoryList'));
				}
			}
							
	  }
		}	
		$this->load->view('subOutdoor',$data);
                 }else{
			 redirect('admin','refresh');
		}
	}
        
                
        public function subPersonal()
	{
            if(!empty($this->session->userdata('session_name')))
		{
                $table='sub_category';     
                if(isset($_GET['zxcvbnm']))
		{
                $id =  base64_decode($_GET['zxcvbnm']);
                $data=array('title'=>'Edit Sub Cetegory','pageTitle'=>'Edit Sub Cetegory','key'=>'1');
                $data['setting'] = $this->db->select('*')->from($table)->where(array('sb_id'=>$id))->get()->row();
		}else{
                 $data=array('title'=>'Add Sub Cetegory','pageTitle'=>'Add Sub Cetegory','key'=>'0');   
                }
                
            if($_POST)
            {           
            $this->form_validation->set_rules('subPersonal','Personal Category','trim|required|max_length[20]');

            if($this->form_validation->run()==TRUE)  
            {
            $obj                            = new Admin_model;   
            $subPersonal 		    = $this->input->post('subPersonal');
            $filename                       = $_FILES['icon_img']['name'];
	    $ext                            = pathinfo($filename, PATHINFO_EXTENSION);
	    
            if($_FILES['icon_img']['error'] == 0) {
            $icon_img = '';
            if (in_array($ext, array('png', 'jpeg', 'jpg', 'PNG', 'JPEG', 'JPG', 'gif', 'GIF'))) 
            {
            $icon_img = time().'.'.$ext;
            move_uploaded_file($_FILES['icon_img']['tmp_name'], getcwd().'/subcategoryimages/'.$icon_img);
            }	
            } else {
                
             $icon_img = $this->input->post('old_image');   
            }
            
     
            $obj                            = new Admin_model;
           
                        if(!isset($_GET['zxcvbnm']))
                         {
                                 $insertArray = array(
                                 'main_category'     =>3,
                                 'name'              =>$subPersonal,
                                 'icon_img'          =>$icon_img
                                 );
                         $result = $this->db->insert($table,$insertArray);

                         if($result ==TRUE) {
                         $this->session->set_flashdata('success_message','Sub Cetegory added sucessfully');
                         redirect(base_url('index.php/admin/SubCategoryList'));
                         }
			}
                        
                        elseif(isset($_GET['zxcvbnm']))
			{
				$updateArray = array(
                                'main_category'     =>3,
                                'name'              =>$subPersonal,
                                'icon_img'          =>$icon_img
                                );
				$this->db->where(array('sb_id'=>$id));
				$result = $this->db->update($table,$updateArray);
				if($result ==TRUE)
				{
					$this->session->set_flashdata('success_message','Sub Cetegory has been updated  successfully');
					redirect(base_url('index.php/admin/SubCategoryList'));
				}
			}
							
	  }
		}	
		$this->load->view('subPersonal',$data);
                 }else{
			 redirect('admin','refresh');
		}

                }
        
        
        
        public function subPersonal04April()
	{
            $data=array('title'=>'Personal Cetegory','pageTitle'=>'Personal Cetegory');
           //$data['personal'] = $this->db->select('*')->get_where('sub_category',array('main_category'=>1))->result();
            //pre($data);die;

            $table='sub_category';
            if($_POST)
            {           
            $this->form_validation->set_rules('subPersonal','Personal Category','trim|required|max_length[20]');

            if($this->form_validation->run()==TRUE)  
            {
            $obj                            = new Admin_model;   
            $subPersonal 		    = $this->input->post('subPersonal');
            $filename                       = $_FILES['icon_img']['name'];
	    $ext                            = pathinfo($filename, PATHINFO_EXTENSION);
			
            $icon_img = '';
            if (in_array($ext, array('png', 'jpeg', 'jpg', 'PNG', 'JPEG', 'JPG', 'gif', 'GIF'))) 
            {
            $icon_img = time().'.'.$ext;
            move_uploaded_file($_FILES['icon_img']['tmp_name'], getcwd().'/subcategoryimages/'.$icon_img);
            }		
     
            $obj                            = new Admin_model;
            $insertArray = array(
                                'main_category'     =>3,
                                'name'              =>$subPersonal,
                                'icon_img'          =>$icon_img
                                );
            $result = $this->db->insert($table,$insertArray);
                                      
            if($result ==TRUE) {
            $this->session->set_flashdata('success_message','Personal Cetegory added sucessfully');
            redirect(base_url('index.php/admin/subPersonal'));
            }
							
	  }
		}	
		$this->load->view('subPersonal',$data);

                }
                

        public function subTransport()
	{
            if(!empty($this->session->userdata('session_name')))
		{
            $data=array('title'=>'Transport Cetegory','pageTitle'=>'Transport Cetegory');
           //$data['personal'] = $this->db->select('*')->get_where('sub_category',array('main_category'=>1))->result();
            //pre($data);die;

            $table='sub_category';
            if($_POST)
            {           
            $this->form_validation->set_rules('subTransport','Transport Category','trim|required|max_length[20]');

            if($this->form_validation->run()==TRUE)  
            {
            $obj                            = new Admin_model;   
            $subTransport 		    = $this->input->post('subTransport');
            $filename                       = $_FILES['icon_img']['name'];
	    $ext                            = pathinfo($filename, PATHINFO_EXTENSION);
			
            $icon_img = '';
            if (in_array($ext, array('png', 'jpeg', 'jpg', 'PNG', 'JPEG', 'JPG', 'gif', 'GIF'))) 
            {
            $icon_img = time().'.'.$ext;
            move_uploaded_file($_FILES['icon_img']['tmp_name'], getcwd().'/subcategoryimages/'.$icon_img);
            }		
     
            $obj                            = new Admin_model;
            $insertArray = array(
                                'main_category'     =>4,
                                'name'              =>$subTransport,
                                'icon_img'          =>$icon_img
                                );
            $result = $this->db->insert($table,$insertArray);
                                      
            if($result ==TRUE) {
            $this->session->set_flashdata('success_message','Transport Cetegory added sucessfully');
            redirect(base_url('index.php/admin/subTransport'));
            }
							
	  }
		}	
		$this->load->view('subTransport',$data);
  }else{
			 redirect('admin','refresh');
		}
                }        
                
       public function jobquoted()
	{			
           if(!empty($this->session->userdata('session_name')))
            {
            $obj = new admin_model();
            $data=array('title'=>'Job Quoted List','pageTitle'=>'Job Quoted List');
            
            $document['date']            = date("Y-m-d"); 
            
            $data['today']               = $obj->getTodayData($document);    
           // pre($data);die();
            if ($data)    {
                $data['week']                = array();
                $week                        = $obj->getWeekData($document);
                if(!empty($week)) {
                    $data['week']             = $week; 
                }
               // pre($week);die();                
                $month                       = $obj->getMonthData($document);
                if(!empty($month)){
                    $data['month']            = $month; 
                }

                $year                       = $obj->getYearData($document);
                if(!empty($year)){
                    $data['year']              = $year; 
                }
            
            }
            //pre($data['setting']);die;
            $this->load->view('jobquoted',$data);
              }else{
			 redirect('admin','refresh');
		}
        }
        
    public function jobsCompleted()
	{			
            $obj = new admin_model();
            $data=array('title'=>'Jobs Completed List','pageTitle'=>'Jobs Completed List');
            
            $document['date']            = date("Y-m-d"); 
            
            $data['today']               = $obj->getJobsTodayData($document);    
           // pre($data);die();
            if ($data)    {
            $data['week']                = array();
            $week                        = $obj->getJobsWeekData($document);
            if(!empty($week)) {
            $data['week']             = $week; 
            }
           // pre($week);die();                
            $month                       = $obj->getJobsMonthData($document);
            if(!empty($month)){
            $data['month']            = $month; 
            }
            
            $year                       = $obj->getJobsYearData($document);
            if(!empty($year)){
            $data['year']              = $year; 
            }
            
            }
            //pre($data['setting']);die;
            $this->load->view('jobsCompleted',$data);		  
        }
        
        public function paymentList()
	{	
            if(!empty($this->session->userdata('session_name')))
            {
            $obj = new admin_model();                
            $data=array('title'=>'Provider value List','pageTitle'=>'Provider value List');
            $data['setting'] = $obj->getPaymentList();
            //pre($data['setting']);die;
            $this->load->view('paymentList',$data);	
            }else{
			 redirect('admin','refresh');
		}
	}
        
        
        public function jobsPosted()
	{	
            if(!empty($this->session->userdata('session_name')))
            {
            $obj = new admin_model();
            $data=array('title'=>'Job Posted List','pageTitle'=>'Job Posted List');
            
            $document['date']            = date("Y-m-d"); 
            
            $data['today']               = $obj->getTodayjobsPosted($document);    
           // pre($data);die();
            if ($data)    {
            $data['week']                = array();
            $week                        = $obj->getWeekjobsPosted($document);
            if(!empty($week)) {
            $data['week']             = $week; 
            }
           // pre($week);die();                
            $month                       = $obj->getMonthjobsPosted($document);
            if(!empty($month)){
            $data['month']            = $month; 
            }
            
            $year                       = $obj->getYearjobsPosted($document);
            if(!empty($year)){
            $data['year']              = $year; 
            }
            
            }
            //pre($data['setting']);die;
            $this->load->view('jobsPosted',$data);	
            }else{
			 redirect('admin','refresh');
		}
	        
            
        }
        
        
        
        public function quoteRecieved()
	{			
            if(!empty($this->session->userdata('session_name')))
            {
            $obj = new admin_model();                
            $data=array('title'=>'Quote Received List','pageTitle'=>'Quote Received List');
            $this->db->set("isRead",1);
            $this->db->update("quotations");
            $data['setting'] = $obj->getQuoteRecievedList();
           // pre($data['setting']);die;
            $this->load->view('quoteRecieved',$data);	
            }else{
			 redirect('admin','refresh');
		}
	}
        
        public function customerPayment()
	{	
            if(!empty($this->session->userdata('session_name')))
            {
            $obj = new admin_model();                
            $data=array('title'=>'Customer Value List','pageTitle'=>'Customer Value List');
            $data['setting'] = $obj->getCustomerPaymentList();
            //pre($data['setting']);die;
            $this->load->view('customerPayment',$data);	
            }else{
			 redirect('admin','refresh');
		}
	}
        
        
        public function available()
	{	
            if(!empty($this->session->userdata('session_name')))
            {
            $obj = new admin_model();                
            $data=array('title'=>'Available jobs List','pageTitle'=>'Available jobs List');
            $data['setting'] = $obj->getAvailableJobsList();
            //pre($data['setting']);die;
            $this->load->view('available',$data);
              }else{
			 redirect('admin','refresh');
		}
	}
        
        public function accepted()
	{	
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Accepted jobs List','pageTitle'=>'Accepted jobs List');
            $data['setting'] = $obj->getAcceptedJobsList();
            //pre($data['setting']);die;
            $this->load->view('accepted',$data);	
              }else{
			 redirect('admin','refresh');
		}
	}
        
        
        public function requested()
	{		
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Requested jobs List','pageTitle'=>'Requested jobs List');
            $data['setting'] = $obj->getRequestedJobsList();
            //pre($data['setting']);die;
            $this->load->view('requested',$data);
              }else{
			 redirect('admin','refresh');
		}
	}
        
        public function completed()
	{		
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Completed jobs List','pageTitle'=>'Completed jobs List');
            $data['setting'] = $obj->getCompletedJobsList();
            //pre($data['setting']);die;
            $this->load->view('completed',$data);	
              }else{
			 redirect('admin','refresh');
		}
	}
        
        public function pending()
	{		
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Pending jobs List','pageTitle'=>'Pending jobs List');
            $data['setting'] = $obj->getPendingJobsList();
           // pre($data['setting']);die;
            $this->load->view('pending',$data);		
              }else{
			 redirect('admin','refresh');
		}
	}
        
        
        
        public function cancelled()
	{		
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Cancelled jobs List','pageTitle'=>'Cancelled jobs List');
            $this->db->set("isRead",1);
            $this->db->update("cancel_job_relation");
            $data['setting'] = $obj->getCancelledJobsList();
          //  pre($data['setting']);die;
            $this->load->view('cancelled',$data);
              }else{
			 redirect('admin','refresh');
		}
	}
        
        public function reschedules()
	{		
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Reschedules jobs List','pageTitle'=>'Reschedules jobs List');
            $this->db->set("isRead",1);
            $this->db->update("reschedule_job_relation");
            $data['setting'] = $obj->getReschedulesJobsList();
            //  pre($data['setting']);die('Ranaaaaaaaaaaaa');
            $this->load->view('reschedules',$data);	
              }else{
			 redirect('admin','refresh');
		}
	}
        
        public function expiredJobs()
	{	
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Expired jobs List','pageTitle'=>'Expired Jobs List');
            $this->db->set("isRead",1);
            $this->db->where("customer_job.status", "0");
            $this->db->update("customer_job");
           // echo $this->db->last_query();die;
            $data['setting'] = $obj->getExpiredJobsList();
            //pre($data['setting']);die;
            $this->load->view('expiredJobs',$data);
             }else{
			 redirect('admin','refresh');
		}
	}
        
        public function missedAppointment()
	{	
            if(!empty($this->session->userdata('session_name')))
{
        	
            $obj = new admin_model();                
            $data=array('title'=>'Missed Appointment List','pageTitle'=>'Missed Appointment List');
            $this->db->set("isRead",1);
            $this->db->where("customer_job.status", "3");
            $this->db->where("customer_job.date<", date('Y-m-d'));
            $this->db->update("customer_job");
            $data['setting'] = $obj->getMissedCheckDatetime();
            
         // pre($data['setting']);die;
            $this->load->view('missedAppointment',$data);	
             }else{
			 redirect('admin','refresh');
		}
	}
        
        public function lateAppointment()
	{	
            if(!empty($this->session->userdata('session_name')))
            {
            $obj = new admin_model();                
            $data=array('title'=>'Late Appointment List','pageTitle'=>'Late Appointment List');
            
            $this->db->select("TIMESTAMPDIFF( MINUTE ,CONCAT( date,  ' ', time ), '" . date('Y-m-d H:i:s') . "'  ) AS difference");
            $this->db->set("isRead",1);
            $this->db->where("date",date('Y-m-d'));                  
            $this->db->where("customer_job.status",'3') ;
            $this->db->having("difference > ",30);
            $this->db->update("customer_job");   
            
            $data['setting'] = $obj->get_check_datetime();
            //pre($data['setting']);die;
            $this->load->view('lateAppointment',$data);	
             }else{
			 redirect('admin','refresh');
		}
	}
   
        public function householdList()
	{	
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Household Job List','pageTitle'=>'Household Job  List');
            $data['setting'] = $obj->gethouseholdLocation();
          //  pre($data['setting']);die;
            $this->load->view('householdList',$data);	
             }else{
			 redirect('admin','refresh');
		}
	}
        
        public function outdoorList()
	{		
           // die('udi');
            if(!empty($this->session->userdata('session_name')))
{
        
            $obj = new admin_model();                
            $data=array('title'=>'Outdoor Job  List','pageTitle'=>'Outdoor Job  List');
            $data['outdoor'] = $obj->getoutdoorLocation();
         //   pre($data['outdoor']);die;
            $this->load->view('outdoorList',$data);	
             }else{
			 redirect('admin','refresh');
		}
	}
        public function personalList()
	{	
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Personal Job  List','pageTitle'=>'Personal Job  List');
            $data['setting'] = $obj->getpersonalLocation();
           // pre($data['setting']);die;
            $this->load->view('personalList',$data);
             }else{
			 redirect('admin','refresh');
		}
	}
        public function transportList()
	{	
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Transport Job List','pageTitle'=>'Transport Job  List');
            $data['setting'] = $obj->gettransportLocation();
            //pre($data['setting']);die('none');
            $this->load->view('transportList',$data);	
             }else{
			 redirect('admin','refresh');
		}
	}      
        
      
        public function householdLocation()
	{
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Household Job Location List','pageTitle'=>'Household Job Location List');
            $data['setting'] = $obj->gethouseholdLocation();
            //pre($data['setting']);die;
            $this->load->view('householdLocation',$data);	
             }else{
			 redirect('admin','refresh');
		}
	}
        
        public function outdoorLocation()
	{
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Outdoor Job Location List','pageTitle'=>'Outdoor Job Location List');
            $data['setting'] = $obj->getoutdoorLocation();
            //pre($data['setting']);die;
            $this->load->view('outdoorLocation',$data);	
             }else{
			 redirect('admin','refresh');
		}
	}
        public function personalLocation()
	{
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Personal Job Location List','pageTitle'=>'Personal Job Location List');
            $data['setting'] = $obj->getpersonalLocation();
            //pre($data['setting']);die;
            $this->load->view('personalLocation',$data);
             }else{
			 redirect('admin','refresh');
		}
	}
        public function transportLocation()
	{
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Transport Job Location List','pageTitle'=>'Transport Job Location List');
            $data['setting'] = $obj->gettransportLocation();
            //pre($data['setting']);die;
            $this->load->view('transportLocation',$data);	
             }else{
			 redirect('admin','refresh');
		}
	}
        public function findMap()
	{		//echo 	$_GET['lat']."<br>";
        //echo $_GET['long'];
        //die();
            $data['lat']        = $_GET['lat'];
            $data['long']       = $_GET['long'];
            $this->load->view('mapView',$data);
	}
        
        public function findProviderMap()
	{		//echo 	$_GET['lat']."<br>";
        //echo $_GET['long'];
        //die();
            $data['lat']        = $_GET['lat'];
            $data['long']       = $_GET['long'];
            $this->load->view('mapViewProvider',$data);
	}
        
        
        public function deleteHouseholdList()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        $table = 'customer_job_household_relation';
                        $obj            = new admin_model;
                        $result         = $obj->deleteList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Job has been deleted successfully');
				redirect('admin/householdList');
                        }
	
	      }
        
	}
        
        
        public function deleteOutdoorList()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        $table = 'customer_job_outdoor_relation';
                        $obj            = new admin_model;
                        $result         = $obj->deleteList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Job has been deleted successfully');
				redirect('admin/outdoorList');
                        }
	
	      }
        
	}
        public function deletePersonalList()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        $table = 'customer_job_personal_relation';
                        $obj            = new admin_model;
                        $result         = $obj->deleteList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Job has been deleted successfully');
				redirect('admin/personalList');
                        }
	
	      }
        
	}
        
         public function deleteTransportList()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        $table = 'customer_job_transport_relation';
                        $obj            = new admin_model;
                        $result         = $obj->deleteList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Job has been deleted successfully');
				redirect('admin/transportList');
                        }
	
	      }
        
	}
        
        
        public function providerRating()
	{
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Provider Rating List','pageTitle'=>'Provider Rating List');
            $this->db->set("isRead",1);
            $this->db->update("provider_rating");
            $data['providerRating'] = $obj->getProviderRating();
            //pre($data['providerRating']);die;
            $this->load->view('providerRating',$data);	
            }else{
			 redirect('admin','refresh');
		}
	}
        
        public function customerRating()
	{		
            if(!empty($this->session->userdata('session_name')))
{
        	
            $obj = new admin_model();                
            $data=array('title'=>'Customer Rating List','pageTitle'=>'Customer Rating List');
            $this->db->set("isRead",1);
            $this->db->update("customer_rating");
            $data['providerRating'] = $obj->getCustomerRating();
           // pre($data['providerRating']);die;
            $this->load->view('customerRating',$data);	
            }else{
			 redirect('admin','refresh');
		}
	}
        
        
         public function deleteProviderRating()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        $table = 'provider_rating';
                        $obj            = new admin_model;
                        $result         = $obj->deleteList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Rating has been deleted successfully');
				redirect('admin/providerRating');
                        }
	
	      }
        
	}
        
        
         public function deleteCustomerRating()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        $table = 'provider_rating';
                        $obj            = new admin_model;
                        $result         = $obj->deleteList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Rating has been deleted successfully');
				redirect('admin/customerRating');
                        }
	
	      }
        
	}
        
        public function customerQuotesPayment()
	{		
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Customer Payment List','pageTitle'=>'Customer Payment List');
            $this->db->set("isRead",1);
            $this->db->update("customer_payment_details");
            $data['setting'] = $obj->getQuotesPayment();
           // pre($data['setting']);die;
            $this->load->view('customerQuotesPayment',$data);	
            }else{
			 redirect('admin','refresh');
		}
	}
        
         public function deleteCustomerQuotesPayment()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        $table = 'customer_payment_details';
                        $obj            = new admin_model;
                        $result         = $obj->deleteList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Payment has been deleted successfully');
				redirect('admin/customerQuotesPayment');
                        }
	
	      }
        
	}
        
       public function quotesPayment()
	{		
           if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Quotes List','pageTitle'=>'Quotes List');
            $data['setting'] = $obj->getQuotePaymentList();
            //pre($data['setting']);die;
            $this->load->view('quotesPayment',$data);
             }else{
			 redirect('admin','refresh');
		}
	} 
        
         public function deleteQuotesPayment()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        $table = 'quotations';
                        $obj            = new admin_model;
                        $result         = $obj->deleteList($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Quote has been deleted successfully');
				redirect('admin/quotesPayment');
                        }
	
	      }
        
        }
        
    public function invoicesPayment()
	{
        if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Invoice List','pageTitle'=>'Invoice List');
            $data['setting'] = $obj->getInvoicesPaymentList();
            //pre($data['setting']);die;
            $this->load->view('invoicesPayment',$data);		
             }else{
			 redirect('admin','refresh');
		}
	} 
               
     public function deleteInvoicesPayment()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        $table = 'invoice';
                        $obj            = new admin_model;
                        $result         = $obj->deleteList($table,$id);
			if($result==TRUE)
			{
                         	$this->session->set_flashdata('success_message','Invoice has been deleted successfully');
				redirect('admin/invoicesPayment');
                        }	
	         }
        
        }
        
        
        
        public function calloutChargePayment()
	{		
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Callout Payment List','pageTitle'=>'Callout Payment List');
            $data['setting'] = $obj->getCalloutChargePayment();
           // pre($data['setting']);die;
            $this->load->view('calloutChargePayment',$data);
            }else{
			 redirect('admin','refresh');
		}
	}
        
        
        
          
        public function providerAccounts()
	{	//die('fsdf');		
            if(!empty($this->session->userdata('session_name')))
{
        
            $obj = new admin_model();                
            $data=array('title'=>'Provider Accounts  List','pageTitle'=>'Provider Accounts List');
            $data['setting'] = $obj->getProviderAccountsPayment();
          //  pre($data['setting']);die;
            $this->load->view('providerAccounts',$data);
            }else{
			 redirect('admin','refresh');
		}
	}
        
         public function updateProviderAccounts()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        //echo $id;die;
                        $table = 'job_status';
                        $obj            = new admin_model;
                        $result         = $obj->updateList($table,$id);
			if($result==TRUE)
			{
                         	$this->session->set_flashdata('success_message','Provider account has been updated successfully');
				redirect('admin/providerAccounts');
                        }	
	         }
        
        }
        
        public function updateProviderAccountsUnpaid()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        //echo $id;die;
                        $table = 'job_status';
                        $obj            = new admin_model;
                        $result         = $obj->updateListUnpaid($table,$id);
			if($result==TRUE)
			{
                         	$this->session->set_flashdata('success_message','Provider account has been updated successfully');
				redirect('admin/providerAccounts');
                        }	
	         }
        
        }
        
         public function updateProviderListAccept()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        //echo $id;die;
                        $table = 'providers';
                        $obj            = new admin_model;
                        $result         = $obj->updateProviderListAccept($table,$id);
			if($result==TRUE)
			{
                         	$this->session->set_flashdata('success_message','Provider has been updated successfully');
				redirect('admin/providersDetails');
                        }	
	         }
        
        }
        
        public function updateProviderListReject()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        //echo $id;die;
                        $table = 'providers';
                        $obj            = new admin_model;
                        $result         = $obj->updateProviderListReject($table,$id);
			if($result==TRUE)
			{
                         	$this->session->set_flashdata('success_message','Provider has been updated successfully');
				redirect('admin/providersDetails');
                        }	
	         }
        
        }
        
         public function deleteProviderAccounts()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        $table = 'job_status';
                        $obj            = new admin_model;
                        $result         = $obj->deleteList($table,$id);
			if($result==TRUE)
			{
                         	$this->session->set_flashdata('success_message','Provider account has been deleted successfully');
				redirect('admin/providerAccounts');
                        }	
	         }
        
        }
          
        public function totalRevenue()
	{ 
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();
            $final =array('title'=>'Total Revenue List','pageTitle'=>'Total Revenue List');
            
            $date = date("Y-m-d");
            $monday= date('Y/m/d',strtotime('last monday'));
            $sunday= date('Y/m/d',strtotime('next sunday'));
            
            $this->db->select("SUM(additional_payment_amount + callout_payment_amount + quote_payment_amount) AS total");
            $this->db->where("date(quote_payment_date)",$date);
            $dayResults = $this->db->get('customer_payment_details')->result_array();
           // day
           
            
            //week
            $this->db->select("SUM(additional_payment_amount + callout_payment_amount + quote_payment_amount) AS total");           
            $this->db->where('date(quote_payment_date) >=', $monday);
            $this->db->where('date(quote_payment_date) <=', $sunday);
            $weekResults = $this->db->get('customer_payment_details')->result_array();
            //week
            
            
            //month
            $this->db->select("SUM(additional_payment_amount + callout_payment_amount + quote_payment_amount) AS total");           
            $this->db->like('date(quote_payment_date)',date('Y-m'));
            $monthResults = $this->db->get('customer_payment_details')->result_array();
            //month
            
            
            $this->db->select("SUM(additional_payment_amount + callout_payment_amount + quote_payment_amount) AS total");           
            $this->db->like('date(quote_payment_date)',date('Y'));
            $yearResults = $this->db->get('customer_payment_details')->result_array();
            //echo $this->db->last_query();
            //pre($results);die();
            
            $final['day']   = $dayResults;
            $final['week']  = $weekResults;
            $final['month'] = $monthResults;
            $final['year']  = $yearResults;
            
            $this->load->view('totalRevenue',$final);
            }else{
			 redirect('admin','refresh');
		}
            //pre($final);die();
        }
       

        
         public function totalCallout()
	{ 
             if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();
            $final =array('title'=>'Total Callout Payment List','pageTitle'=>'Total Callout Payment List');
            
            $date = date("Y-m-d");
            $monday= date('Y/m/d',strtotime('last monday'));
            $sunday= date('Y/m/d',strtotime('next sunday'));
            
            // day
            $this->db->select("SUM(callout_payment_amount) AS total");
            $this->db->where("date(callout_payment_date)",$date);
            $dayResults = $this->db->get('customer_payment_details')->result_array();
            //  pre($dayResults);
           // echo $this->db->last_query($dayResults);die;
          
           // day
           
            
            //week
            $this->db->select("SUM(callout_payment_amount) AS total");           
            $this->db->where('date(callout_payment_date) >=', $monday);
            $this->db->where('date(callout_payment_date) <=', $sunday);
            $weekResults = $this->db->get('customer_payment_details')->result_array();
            //week
            
            
            //month
            $this->db->select("SUM(callout_payment_amount) AS total");           
            $this->db->like('date(callout_payment_date)',date('Y-m'));
            $monthResults = $this->db->get('customer_payment_details')->result_array();
            //month
            
            $this->db->select("SUM(callout_payment_amount) AS total"); 
            $this->db->like('date(callout_payment_date)',date('Y'));
            $yearResults = $this->db->get('customer_payment_details')->result_array();
            //echo $this->db->last_query();
            //pre($results);die();
            
            $final['day']   = $dayResults;
            $final['week']  = $weekResults;
            $final['month'] = $monthResults;
            $final['year']  = $yearResults;
            
            $this->load->view('totalCallout',$final);
            }else{
			 redirect('admin','refresh');
		}
            //pre($final);die();
        }
         
         public function totalCommission()
	{ 
             if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();
            $final =array('title'=>'Total Commission List','pageTitle'=>'Total Commission List');
            
            $date = date("Y-m-d");
            $monday= date('Y/m/d',strtotime('last monday'));
            $sunday= date('Y/m/d',strtotime('next sunday'));
            
            // day
            $this->db->select("SUM(additional_payment_amount + callout_payment_amount + quote_payment_amount) AS total");
            $this->db->where("date(quote_payment_date)",$date);
            $dayResults = $this->db->get('customer_payment_details')->result_array();   
            // pre($dayResults);
            //echo $this->db->last_query($dayResults);die;
           // day
           
            
            //week
            $this->db->select("SUM(additional_payment_amount + callout_payment_amount + quote_payment_amount) AS total");           
            $this->db->where('date(quote_payment_date) >=', $monday);
            $this->db->where('date(quote_payment_date) <=', $sunday);
            $weekResults = $this->db->get('customer_payment_details')->result_array();
            //week
            
            
            //month
            $this->db->select("SUM(additional_payment_amount + callout_payment_amount + quote_payment_amount) AS total");           
            $this->db->like('date(quote_payment_date)',date('Y-m'));
            $monthResults = $this->db->get('customer_payment_details')->result_array();
            //month
            
            
            $this->db->select("SUM(additional_payment_amount + callout_payment_amount + quote_payment_amount) AS total");           
            $this->db->like('date(quote_payment_date)',date('Y'));
            $yearResults = $this->db->get('customer_payment_details')->result_array();
            //echo $this->db->last_query();
            //pre($results);die();
            
            $totalCommisionDayResults     = [($dayResults[0]['total']*7)/100];
            $totalCommisionWeekResults    = [($weekResults[0]['total']*7)/100];
            $totalCommisionMonthResults   = [($monthResults[0]['total']*7)/100];
            $totalCommisionYearResults    = [($yearResults[0]['total']*7)/100];
            
            
//            $totalCommisionDayResults     = [($dayResults['total']*7)/100];
//            $totalCommisionWeekResults    = [($weekResults['total']*7)/100];
//            $totalCommisionMonthResults   = [($monthResults['total']*7)/100];
//            $totalCommisionYearResults    = [($yearResults['total']*7)/100];

            
            $final['day']   = $totalCommisionDayResults;
            $final['week']  = $totalCommisionWeekResults;
            $final['month'] = $totalCommisionMonthResults;
            $final['year']  = $totalCommisionYearResults;
           //pre($final); die();
            $this->load->view('totalCommission',$final);
              }else{
			 redirect('admin','refresh');
		}
            //pre($final);die();
        }
       
        
        public function promoCode()
	{
         //   die('rahul');
            if(!empty($this->session->userdata('session_name')))
{
		$table='promocode';
		if(isset($_GET['zxcvbnm']))
		{
			$userId =  base64_decode($_GET['zxcvbnm']);
                       // pre($userId);
			$data=array('title'=>'Edit Promo Code ','pageTitle'=>'Edit Promo Code Details','key'=>'1');
			//$data['setting'] = $this->db->select('*')->from($table)->where(array('id'=>1))->get()->row();
			$data['setting'] = $this->db->select('*')->from($table)->where(array('id'=>$userId))->get()->row();
                       // pre($data);die('edit');
		}else{
                  //  $data['key'] = 0;
                    $data=array('title'=>'Add PromoCode ','pageTitle'=>'Add Promo CodeDetails','key'=>'0');
		    //$data['status'] = $this->db->select('*')->from($table)->get()->row();
                    
                }
		if($_POST)
		{
                $this->form_validation->set_rules('name','Promo Code','required|max_length[12]');    
              //  $this->form_validation->set_rules('vat','vat','required|numeric');    
               // $this->form_validation->set_rules('maximum_quote','Maximum Quote','required|numeric');    
		if($this->form_validation->run()==TRUE)
		{
			$name                    = $this->input->post('name');
			$promo_desc              = $this->input->post('promo_desc');
//			$amount                  = $this->input->post('amount');
			$status                  = $this->input->post('status');
			
                        
                        
                        if(!isset($_GET['zxcvbnm']))
			{
				$insertArray = array(
                                                    'name'=>$name,
                                                    'promo_desc'=>$promo_desc,
//                                                    'amount'=>$amount,
                                                    'status'=>0
                                                  
                                                    );
                                    $result = $this->db->insert($table,$insertArray);
                                  
                                    if($result ==TRUE)
                                    {
                                            $this->session->set_flashdata('success_message','Promo code  has been added successfully');
                                            redirect(base_url('index.php/admin/promoLists'));
                                            
                                    }
			}
                        elseif(isset($_GET['zxcvbnm']))
			{
				$updateArray = array(
						    'name'=>$name,
                                                    'promo_desc'=>$promo_desc,
//                                                    'amount'=>$amount,
                                                    'status'=>$status
					            );
				$this->db->where(array('id'=>$userId));
				$result = $this->db->update($table,$updateArray);
				if($result ==TRUE)
				{
					$this->session->set_flashdata('success_message','promoCode has been updated  successfully');
					redirect(base_url('index.php/admin/promoLists'));
				}
			}									
		}
	}
	$this->load->view('promoCode',$data);
          }else{
			 redirect('admin','refresh');
		}
	}
        
        
        public function promoLists()
	{			
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Promo Code List','pageTitle'=>'Promo Code  List');
            $data['setting'] = $obj->getPromoList();
            //pre($data['setting']);die('none');
            $this->load->view('promoLists',$data);
             }else{
			 redirect('admin','refresh');
		}
	}      
        
        
        
        
        public function deletePromoCode()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                       // echo $id; die('deletePromoCode');
                        $table = 'promocode';
                        $obj            = new admin_model;
                        $result         = $obj->deletePromoCode($table,$id);
			if($result==TRUE)
			{
				
				$this->session->set_flashdata('success_message','Promo Code has been deleted successfully');
				redirect('admin/promoLists');
                        }
	
	      }
        
	}   
        
        
         public function deletePending()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                       // echo $id; die('deletePromoCode');
                        $table = 'customer_job';
                        $obj            = new admin_model;
                        $result         = $obj->deletePendingJobs($table,$id);
			if($result==TRUE)
			{
				$this->session->set_flashdata('success_message','Pending Jobs has been deleted successfully');
				redirect('admin/pending');
                        }
	      }
   	}   
        
        
        public function revenue()
	{ 
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();
            $final =array('title'=>'Total Revenue List','pageTitle'=>'Total Revenue List');   
            if($_POST)
		{
			$this->form_validation->set_rules('date','Date','trim|required');
			$this->form_validation->set_rules('date1','Date','trim|required');

			if($this->form_validation->run()==TRUE)
			{
                            $fromDatePre     =    $this->input->post('date');
                            $toDatePre       =    $this->input->post('date1');
                            $fromDate        =    $fromDatePre." "."00:00:01";
                            $toDate          =    $toDatePre." "."23:59:59";    
                                
                        $this->db->select("SUM(additional_payment_amount + callout_payment_amount + quote_payment_amount) AS total");
                        $this->db->where('quote_payment_date BETWEEN "'.$fromDate . '" and "'. $toDate.'"');
                        $dayResults = $this->db->get('customer_payment_details')->result_array();

                        $final['fromDate'] = $fromDatePre;
                        $final['toDate']   = $toDatePre;
                        if($dayResults[0]['total'] == ''){
                            $dayResults[0]['total'] = 0;
                        }
                        $final['search'] = $dayResults[0]['total'];
                        
                        }
                }            
            $this->load->view('revenue',$final);
               }else{
			 redirect('admin','refresh');
		}

        }
       
        
        
        
      public function callout()
	{ 
          if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();
            $final =array('title'=>'Total Callout Payment','pageTitle'=>'Total Callout Payment');
            
            if($_POST)
		{
                        $this->form_validation->set_rules('date','Date','trim|required');
			$this->form_validation->set_rules('date1','Date','trim|required');

			if($this->form_validation->run()==TRUE)
			{
                        $fromDatePre		= $this->input->post('date');
                        $toDatePre 		= $this->input->post('date1');
                                
                        $fromDate         =  $fromDatePre." "."00:00:01";
                        $toDate           =  $toDatePre." "."23:59:59";
                        $date['fromDate'] =  $fromDate;
                        $date['toDate']   =  $toDate;
               
                        $table  = 'customer_payment_details' ;  
                        $obj = new admin_model();
                        $result = $obj->getAllCallout($table, $date);
                    
                        $final['fromDate']   = $fromDatePre;
                        $final['toDate']     = $toDatePre;
                        if($result[0]['total']){
                        $final['search']     = $result[0]['total'];
                        }else{
                        $final['search']     = "N/A";    
                        }
                        
                        }
                }            
            $this->load->view('callout',$final);
             }else{
			 redirect('admin','refresh');
		}
        }
       
        public function commission() { 
            if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();
            $final =array('title'=>'Total Commission ','pageTitle'=>'Total Commission ');
            
            if($_POST)
		{
                    $this->form_validation->set_rules('date','Date','trim|required');
                    $this->form_validation->set_rules('date1','Date','trim|required');

                    if($this->form_validation->run()==TRUE)
                    {
                    $fromDatePre		= $this->input->post('date');
                    $toDatePre 		        = $this->input->post('date1');

                    $fromDate           =  $fromDatePre." "."00:00:01";
                    $toDate             =  $toDatePre." "."23:59:59";
                    $date['fromDate']   =  $fromDate;
                    $date['toDate']     =  $toDate;
                    
                    $table  = 'customer_payment_details' ;  
                    $obj = new admin_model();

                    $this->db->select("SUM(additional_payment_amount + callout_payment_amount + quote_payment_amount) AS total");
                    $this->db->where('quote_payment_date BETWEEN "'.$fromDate . '" and "'. $toDate.'"');
                    $TotalPayResults = $this->db->get('customer_payment_details')->result_array();
                    
                    if(isset($TotalPayResults[0]['total'])){
                        
                    }else{
                        $TotalPayResults[0]['total'] = 0;
                    }
                    
                    $getAllCallout = $obj->getAllCallout($table,$date);
                  
                    if(isset($getAllCallout[0]['total'])){
                        
                    }else{
                        $getAllCallout[0]['total'] = 0;
                    }
                    
                    $commision           =  $this->db->get('setting')->row_array();
                    $Total_Minus_Callout =  $TotalPayResults[0]['total'] - $getAllCallout[0]['total'];
                    $totalCommision      =  [($Total_Minus_Callout * $commision['commision'])/100];
                    $final['fromDate']   =  $fromDatePre;
                    $final['toDate']     =  $toDatePre;
                    $final['search']     =  $totalCommision[0];

                    }
                }            
            $this->load->view('commission',$final);
             }else{
			 redirect('admin','refresh');
		}
        }            
        
        
          public function SubCategoryList()
	{	
              if(!empty($this->session->userdata('session_name')))
{
            $obj = new admin_model();                
            $data=array('title'=>'Sub Category List','pageTitle'=>'Sub Category List');
            $table = 'sub_category';
            $data['setting'] = $obj->getSubCategoryList($table);
            //pre($data['setting']);die;
            $this->load->view('SubCategoryList',$data);	
             }else{
			 redirect('admin','refresh');
		}
	}
        
        
         public function deleteSubcategoryList()
	{		
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                      //  echo $id; die('sub_category');
                        $table = 'sub_category';
                        $obj            = new admin_model;
                        $result         = $obj->deleteSubCategory($table,$id);
			if($result==TRUE)
			{
				$this->session->set_flashdata('success_message','Sub Category has been deleted successfully');
				redirect('admin/SubCategoryList');
                        }
	      }
   	}  
        
        public function updateSubCategoryList()
	{		//echo 	$_GET['lat']."<br>";
		if(isset($_GET['zxcvbnm']))
		{
			$id =  base64_decode($_GET['zxcvbnm']);
                        //echo $id;die;
                        $table = 'sub_category';
                        $obj            = new admin_model;
                        $result         = $obj->updateSubCategoryList($table,$id);
                      
                      //  pre($result);die;
			if($result==TRUE)
			{
                         	$this->session->set_flashdata('success_message','Sub Category has been updated successfully');
				redirect('admin/subPersonal');
                        }	
	}
        
        }
        
        
        
        
        
        
        public function uploadPDF(){
            
        if(!empty($this->session->userdata('session_name'))){
                 $table='pdf_version';     
                 $data=array('title'=>'Upload Catalogue','pageTitle'=>'Upload Catalogue','key'=>'0');   
                      
        if($_FILES)
        {   
            $obj                            = new Admin_model;   
            $filename                       = $_FILES['pdf']['name'];            
	    $ext                            = pathinfo($filename, PATHINFO_EXTENSION);
            
            
            if($filename == ""){
                $this->session->set_flashdata('error_message','Upload PDF Field Is Required');
                redirect(base_url('index.php/admin/uploadPDF'));
            }
             if($ext != "pdf" and $ext != "PDF"){
                $this->session->set_flashdata('error_message','Only Valid PDF File Can Be Upload');
                redirect(base_url('index.php/admin/uploadPDF'));
            }
            
           if($_FILES['pdf']['error'] == 0) {               
            $pdf = '';
            if (in_array($ext, array('PDF','pdf'))) 
            {
            $filename = str_replace(' ', '', $filename);    
            $pdf = time().$filename;
            move_uploaded_file($_FILES['pdf']['tmp_name'], getcwd().'/uploads/'.$pdf);
            }	
            } else {
               
                $this->session->set_flashdata('error_message','Please Try Again Later');
                redirect(base_url('index.php/admin/uploadPDF'));
           }
            $obj                            = new Admin_model;           
            $this->db->select('pdf_version.version as version');
            $this->db->order_by("id", "desc");
            $this->db->limit(1,0);
            $version= $this->db->get('pdf_version')->row_array();
            if(!empty($version)){
            $version_imp = implode($version);
            $New_version = $version_imp + 1; 
            }
            else{
                $New_version = 1;
            }
                                 $insertArray = array(
                                 'pdf'          =>$pdf,
                                 'version'      =>$New_version,
                                 'created_on'   =>date('Y-m-d H:i:s')    
                                 );
                                 
                         //pre($insertArray);die('nothing');
                          $table='pdf_version';        
                         $result = $this->db->insert($table,$insertArray);                   

                         $LastId = $this->db->insert_id();
                        // $IdForDelete = $LastId - 1;
                         
                        $this->db->where("id<",$LastId);
                         $deleted = $this->db->delete($table);
                         
                    //     echo $this->db->last_query($deleted);die;
                           $table='pdf_version';
			$data['setting'] = $obj->getListUploadPDF($table);
                      //  pre($data);die;
//                         if($deleted ==TRUE) {
//                         $this->session->set_flashdata('success_message','PDf added sucessfully');
//                         redirect(base_url('index.php/admin/uploadPDF'));
//                         }						
	              
        }
		$this->load->view('uploadPDF',$data);
                 }else{
			 redirect('admin','refresh');
		}

                }
        
        
        public function listUploadPDF()
	{	
         if(!empty($this->session->userdata('session_name')))
         {
            $obj = new admin_model();                
            $data=array('title'=>'List Catalogue ','pageTitle'=>'List Catalogue');
            $table = 'pdf_version';
            $data['setting'] = $obj->getListUploadPDF($table);
            //pre($data['setting']);die;
            $this->load->view('listUploadPDF',$data);	
             }else{
			 redirect('admin','refresh');
		}
	}
        
        
        
        
       public function addRole(){
            
        if(!empty($this->session->userdata('session_name'))){      
        $data = array('title'=>'Add Role','pageTitle'=>'Add Role');                       
        if($_POST) {  
            
            $this->form_validation->set_rules('role_name','Role','trim|required');

            if($this->form_validation->run()==TRUE){
            $role_name		       = $this->input->post('role_name');
            $modules 		        = $this->input->post('modules');
   
 
             foreach ($modules as $str){
                //  pre($str);die;
                 if($str == 1){
                     
                     $final[] = "role management";
                 }
                 if($str == 2){
                     
                     $final[] = "user management";
                 }
                 if($str == 3){
                     
                     $final[] = "catalogue management";
                 }
                 if($str == 4){
                     
                     $final[] = "complaint management";
                 }
                 if($str == 5){
                     
                     $final[] = "sales management";
                 }
                    if($str == 6){
                     
                     $final[] = "performance management";
                 }
                  if($str == 7){
                     
                     $final[] = "master management";
                 }
                 
             }
             

        if(!empty($comma_seprated)){
              $comma_seprated =  implode(',',$final);      //  pre($ect);die;
         }else{
             $comma_seprated= 'N/A';
         }
        
        if(!empty($modules)){
            $modules= implode(',',$modules);
         }else{
             $modules= '0';
         }
         
 
                    $insertArray = array(
                    'role_name'          =>$role_name,
                    'module_name'        =>$modules,
                    'seprated'           =>$comma_seprated,
                    'created_on'         =>date('Y-m-d')    
                    ); 
                  
                    $table='role';   
                   //   pre($insertArray);die('nothing');
                    $result = $this->db->insert($table,$insertArray);    
                    
                    if($result ==TRUE) {
                       $this->session->set_flashdata('success_message','Role added sucessfully');
                       redirect(base_url('index.php/admin/listRole'));                         }		
                      
                     }
         
                }
		$this->load->view('addRole',$data);
                 }else{
			 redirect('admin','refresh');
		}

                }
        
        
        public function listRole()
	{	
         if(!empty($this->session->userdata('session_name')))
         {
            $obj = new admin_model();                
            $data=array('title'=>'List Role ','pageTitle'=>'List Role');
            $table = 'role';
            $data['setting'] = $obj->getListRole($table);
           //pre($data['setting']);die;
            $this->load->view('listRole',$data);	
             }else{
			 redirect('admin','refresh');
		}
	}
              
        
         public function addUserOrignal(){
            
            if(!empty($this->session->userdata('session_name')))
            {
            $sessionName = $this->session->userdata('session_name');                         
            $data=array('title'=>'Add User','pageTitle'=>'Add User','key'=>'0'); 
                            
            $data['user_roles'] = $this->db->get("role")->result();
            $data['states'] = $this->db->get("demo_state")->result();

		if($_POST)  {             
                 //   pre($_POST);die;

                        $this->form_validation->set_rules('username','username','required|is_unique[admin.username]');
                        $this->form_validation->set_rules('role','Role','required');
			$this->form_validation->set_rules('password','Password','trim|required');
                        $this->form_validation->set_rules('email','Email','required|valid_email|is_unique[admin.email]');
                        $this->form_validation->set_rules('mobile', 'Mobile Number ', 'required|regex_match[/^[0-9]{10}$/]|is_unique[admin.mobile]');
			
			if($this->form_validation->run()==TRUE){
                        $username 		        = $this->input->post('username');
                        $password 		        = $this->input->post('password');
                        $email 	                        = $this->input->post('email');
                        $mobile      	                = $this->input->post('mobile');
                        $first_name	                = $this->input->post('first_name');
                        $last_name                      = $this->input->post('last_name');
                        $role                           = $this->input->post('role');
                        $stateId                        = $this->input->post('state');
                        $cityId                         = $this->input->post('city');
                        $created                        = date('Y-m-d H:i:s');

                       $type = $this->db->select('role_name')->from('role')->where(array('id'=>$role))->get()->row_array();           
                        $role_name =   $type['role_name'];
                        $role_name = explode(',',$role_name);

                        $state = $this->db->select('name')->from('demo_state')->where(array('id'=>$stateId))->get()->row_array();           
                        $state_name =   $state['name'];
                        $state_name = explode(',',$state_name);

                        $city = $this->db->select('name')->from('demo_cities')->where(array('id'=>$cityId))->get()->row_array();           
                        $city_name =   $city['name'];
                        $city_name = explode(',',$city_name);
                                    
                        
                        $obj            = new admin_model;
                        
                                $insertArray = array(
                                'username'                  =>$username,
                                'password'                  =>$password,
                                'email'                     =>$email,
                                'mobile'                     =>$mobile,
                                'first_name'                 =>$first_name,
                                'last_name'                  =>$last_name,			                          			                          
                                'user_type'                  =>$role,
                                'user_type_name'             =>$role_name[0],                                   			                          
                                'reletion'                   =>$state_name[0],			                          
                                'dealer'                     =>$city_name[0],			                          
                                'reletionId'                 =>$stateId,			                          
                                'dealerId'                   =>$cityId,			                          
                                'status'                     =>'1',			                          
                                'created_at'                 =>$created,
                                 );
                                
                            //  pre($insertArray);
                                $table='admin';
                                $result = $this->db->insert($table,$insertArray);
                                //pre($result); die;

                                if($result ==TRUE)
                                {
                                        $this->session->set_flashdata('success_message','User added sucessfully');
                                        redirect(base_url('index.php/admin/addUser'));

                                }      
                      
			}
		}
		$this->load->view('addUser',$data);
                }else{
			 redirect('admin','refresh');
		}
	}
                
        
        
       public function myformAjaxOrignal($id) { 
       $result = $this->db->where("state_id",$id)->get("demo_cities")->result();
       echo json_encode($result);

   }
   
       public function addUser(){
            
            if(!empty($this->session->userdata('session_name')))
            {
            $sessionName = $this->session->userdata('session_name');                         
            $data=array('title'=>'Add User','pageTitle'=>'Add User','key'=>'0'); 
                            
            $data['user_roles'] = $this->db->get("role")->result();
            $data['states'] = $this->db->where("user_type",'3')->get("admin")->result();
            
       //     pre($data['states']);die('user_type');

		if($_POST)  {             
                 //   pre($_POST);die;

                        $this->form_validation->set_rules('username','username','required|is_unique[admin.username]');
                        $this->form_validation->set_rules('role','Role','required');
			$this->form_validation->set_rules('password','Password','trim|required');
                        $this->form_validation->set_rules('email','Email','required|valid_email|is_unique[admin.email]');
                        $this->form_validation->set_rules('mobile', 'Mobile Number ', 'required|regex_match[/^[0-9]{10}$/]|is_unique[admin.mobile]');
			
			if($this->form_validation->run()==TRUE){
                        $username 		        = $this->input->post('username');
                        $password 		        = $this->input->post('password');
                        $email 	                        = $this->input->post('email');
                        $mobile      	                = $this->input->post('mobile');
                        $first_name	                = $this->input->post('first_name');
                        $last_name                      = $this->input->post('last_name');
                        $role                           = $this->input->post('role');
                        $stateId                        = $this->input->post('state');
                        $cityId                         = $this->input->post('city');
                        $created                        = date('Y-m-d H:i:s');

                       $type = $this->db->select('role_name')->from('role')->where(array('id'=>$role))->get()->row_array();           
                        $role_name =   $type['role_name'];
                        $role_name = explode(',',$role_name);

                        $state = $this->db->select('name')->from('demo_state')->where(array('id'=>$stateId))->get()->row_array();           
                        $state_name =   $state['name'];
                        $state_name = explode(',',$state_name);

                        $city = $this->db->select('name')->from('demo_cities')->where(array('id'=>$cityId))->get()->row_array();           
                        $city_name =   $city['name'];
                        $city_name = explode(',',$city_name);
                                    
                        
                        $obj            = new admin_model;
                        
                                $insertArray = array(
                                'username'                  =>$username,
                                'password'                  =>$password,
                                'email'                     =>$email,
                                'mobile'                     =>$mobile,
                                'first_name'                 =>$first_name,
                                'last_name'                  =>$last_name,			                          			                          
                                'user_type'                  =>$role,
                                'user_type_name'             =>$role_name[0],                                   			                          
                                'reletion'                   =>$state_name[0],			                          
                                'dealer'                     =>$city_name[0],			                          
                                'reletionId'                 =>$stateId,			                          
                                'dealerId'                   =>$cityId,			                          
                                'status'                     =>'1',			                          
                                'created_at'                 =>$created,
                                 );
                                
                            //  pre($insertArray);
                                $table='admin';
                                $result = $this->db->insert($table,$insertArray);
                                //pre($result); die;

                                if($result ==TRUE)
                                {
                                        $this->session->set_flashdata('success_message','User added sucessfully');
                                        redirect(base_url('index.php/admin/addUser'));

                                }      
                      
			}
		}
		$this->load->view('addUser',$data);
                }else{
			 redirect('admin','refresh');
		}
	}
    public function myformAjax($id) { 
      // $result = $this->db->where("userid",$id)->get("admin")->result();
        $result = $this->db->where("state_id",$id)->get("demo_cities")->result();
       echo json_encode($result);

   }
   
   
   
   
}
