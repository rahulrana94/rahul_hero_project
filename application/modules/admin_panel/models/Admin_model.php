<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();   
		$this->conn_id='';
    }
 function isUserExist($username,$password){
        $this->db->where('username', $username); 
        $this->db->where('password', $password); 
        $result = $this->db->get('admin')->row();   
        if(!empty($result)){
            return $result;
        }
        else {
            return false;
        }
    }
    public function insertData($table,$data)
    {
        //pre($table);
       // pre($data);die;
        $result = $this->db->insert($table,$data);
        return $result;
    }
	
    public function userLogin($table, $username, $password)
    {  
        $sql="SELECT * FROM $table WHERE (username=? or email=?) and password=?";
        $res=$this->db->query($sql, array($username,$username,md5($password)))->row();
		return $res;
    }
	
	public function escape_str($str, $like = FALSE)
	{
		if(is_array($str))
		{
			foreach ($str as $key => $val)
			{
				$str[$key] = $this->escape_str($val, $like);
			}
			return $str;
		}
		$str = is_object($this->conn_id) ? mysql_real_escape_string($str, $this->conn_id) : addslashes($str);
		  if ($like === TRUE)
		  {
		   return str_replace(array($this->_like_escape_chr, '%', '_'),
			  array($this->_like_escape_chr.$this->_like_escape_chr, $this->_like_escape_chr.'%', $this->_like_escape_chr.'_'),
			  $str);
		  }
		return $str;
	 }
	
    public function getAllRecords($table)
    { 

	$query = $this->db->get($table);
        return $query->result();
    }
    
    public function getSubCatRecords($table,$subcat)
    { 
        $this->db->select('name');
        $this->db->where('sb_id',$subcat);
	$query = $this->db->get($table)->row_array();
        return $query['name'];
    }
    
    
     public function isIdExist($SubcategoryId,$providersId)
    {      
        $this->db->where('business_subtype',$SubcategoryId);
        $this->db->where('prov_id',$providersId);
	$query = $this->db->get('providers_subcat_relation')->row_array();
        if(empty($query))
        {
            return $SubcategoryId;
        }
       else { return 0;}
        //echo $this->db->last_query(); die;
        
    }
    
    
    public function insertOneRow($table, $data)
    {    
        $this->db->insert($table, $data);
        $lastid = $this->db->insert_id();
        return $lastid; 
    }

    public function insertMultipleRows($table, $data)
    {  
        return $this->db->insert_batch($table, $data);
    }
	
    public function getAllRecordByCondition($table, $data,$order_by=null)
    {  	
        $this->db->order_by($order_by,'desc');
        $query = $this->db->get_where($table, $data);
        return $query->result();
    }
    
    public function getAllRecord($table,$order_by)
    { 
        $this->db->order_by($order_by,'desc');
        $query = $this->db->get($table);
        return $query->result();
    }
   
    public function getRecordById($table,$data,$where)
    {  	
        $query = $this->db->get_where($table, $data,$where);
        return $query->result();
    }
    
    public function updateRecordById($table,$data,$where)
    {
        return $this->db->update($table,$data,$where);
    }
    
    public function deleteRecordById($table,$where)
    {
        $this->db->delete($table,$where);
	return $this->db->affected_rows();
    }
    
    public function deleteRecordWhereIn($table,$colum,$wherein)
    {
        $this->db->where($colum, $wherein);
        $this->db->delete($table);
	return $this->db->affected_rows();
    }	
	
	public function joinThreeTable($table1,$table2,$table3,$column1,$column2,$column3,$orderby)
	{
		$this->$db->join($table2,$table2.$column2=$table1.$column1,'LEFT');
		$this->db->join($table3,$table3.$column3 = $table1.$column1,'LEFT');
		$this->db->where($table3,$column3);
		$this->db->order_by($orderby,'ASC');
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->result_array();
		}else{
			return false;
		}
	}

         
    function getSettingList()
    {
        //pre($document); die();
        $this->db->select('*');
        $this->db->where('id', 1);
        $result = $this->db->get('setting')->result_array(); 
        //echo $this->db->last_query();
       // pre($result); die();
        if(!empty($result))
        {            
           return $result; 
        }
        else
        {
            return false;
        }
    }
    
       function getPaymentList()
    {
        /*$this->db->select('customer_job.create_date as create_date,customer_payment_details.quote_payment_amount as Payment ,providers.firstName as ProviderName,customers.name as CustomerName');
        $this->db->order_by("providers.firstName", "asc");
        $this->db->join('providers','providers.prov_id = customer_payment_details.prov_id');
        $this->db->join('customers','customers.cust_id = customer_payment_details.cust_id');
        $this->db->join('customer_job','customer_job.id = customer_payment_details.job_id');
        $result = $this->db->get('customer_payment_details')->result_array();
       */
        
        
        $this->db->select('final_quotation.*,customer_job.create_date as create_date,providers.firstName as ProviderName,customers.name as CustomerName');       
        $this->db->order_by("final_quotation.total", "desc");
        $this->db->join('providers','providers.prov_id = final_quotation.prov_id');
        $this->db->join('customer_job','customer_job.id = final_quotation.job_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');
        $result = $this->db->get('final_quotation')->result_array();
        
        
        
        
        
        if(!empty($result))
        {            
           return $result; 
        }
        else
        {
            return false;
        }
    }
       
       function getQuotesPayment()
    {
 
           
        $this->db->select('customer_payment_details.*,customer_job.main_cat_id,sub_cat_id,user_id,date,time,create_date,providers.firstName as Provider,providers.lastName as ProviderlastName,customers.name as customer,main_category.name as Category,main_category.id as main_cat_id');
        //$this->db->order_by("customer_payment_details.quote_payment_amount", "desc");
        $this->db->order_by("customer_payment_details.id", "desc");
        $this->db->join('providers','providers.prov_id = customer_payment_details.prov_id');
        $this->db->join('customers','customers.cust_id = customer_payment_details.cust_id');
        $this->db->join('customer_job','customer_job.id = customer_payment_details.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
      //  $this->db->join('sub_category','sub_category.sb_id = customer_job.sub_cat_id');
        $result = $this->db->get('customer_payment_details')->result_array();

        foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['job_id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['job_id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];
        }
        $result_modi[] = $x;          
        endforeach;

        return (!empty($result_modi)) ? $result_modi : array();    
    }
    
    
     function getCustomerPaymentList()
    {

      
  /*
        $this->db->select('customer_payment_details.*,customer_job.create_date as create_date,customer_payment_details.quote_payment_amount as Payment ,providers.firstName as ProviderName,customers.name as CustomerName');
       // $this->db->order_by("Payment", "desc");
        $this->db->order_by("providers.firstName", "asc");
        $this->db->join('providers','providers.prov_id = customer_payment_details.prov_id');
        $this->db->join('customers','customers.cust_id = customer_payment_details.cust_id');
        $this->db->join('customer_job','customer_job.id = customer_payment_details.job_id');
        $result = $this->db->get('customer_payment_details')->result_array();
        
   */
  
        $this->db->select('final_quotation.*,customer_job.create_date as create_date,providers.firstName as ProviderName,customers.name as CustomerName');       
        $this->db->order_by("final_quotation.total", "desc");
        $this->db->join('providers','providers.prov_id = final_quotation.prov_id');
        $this->db->join('customer_job','customer_job.id = final_quotation.job_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');

        $result = $this->db->get('final_quotation')->result_array();
        
        if(!empty($result))
        {            
           return $result; 
        }
        else
        {
            return false;
        }
    }
    
    
    
        function getQuotedList()
    {
        //pre($document); die();
        $this->db->select('*');
        $this->db->where('id', 1);
        $result = $this->db->get('setting')->result_array(); 
        //echo $this->db->last_query();
       // pre($result); die();
        if(!empty($result))
        {            
           return $result; 
        }
        else
        {
            return false;
        }
    }
     
function getTomorrowData($document) {
        //pre($document);die;
        $tomorrow_date=date('Y/m/d',strtotime("+1 days"));   
        $result = $this->db->where('date',$tomorrow_date );
        $this->db->order_by("date", "desc");
        $result = $this->db->get('quotations')->result_array();  
        //pre($result);die;  
        if (!empty($result)) {           
        return $result;
        }  
        else {
        return false;
        }            
    }    
    
function getTodayData($document)  {

$this->db->select('quotations.prov_id, count( * ) as value');
$this->db->where("date(create_date)",$document['date']);
//$this->db->group_by("prov_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = quotations.prov_id');
$result = $this->db->get('quotations')->row_array();

if (!empty($result)) {           
return $result;
}  
else {
return false;
}            
    }
    

    
    
function getWeekData($document) {
//pre($document);die;
$monday= date('Y/m/d',strtotime('last monday'));
$sunday= date('Y/m/d',strtotime('next sunday'));
$this->db->select('quotations.prov_id, count( * ) as value');
$this->db->where('date(create_date) >=', $monday);
$this->db->where('date(create_date) <=', $sunday);
//$this->db->group_by("prov_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = quotations.prov_id');
$result = $this->db->get('quotations')->row_array();
//pre($result);
//echo $this->db->last_query($result);die;

if (!empty($result)) {           
return $result;
}  
else {
return false;
}            
    }
    
function getMonthData($document){             
$this->db->select('quotations.prov_id, count( * ) as value');
$this->db->like('date(create_date)',date('Y-m'));
//$this->db->group_by("prov_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = quotations.prov_id');
$result = $this->db->get('quotations')->row_array();
//pre($result);
//echo $this->db->last_query($result);die;
if (!empty($result)) {           
return $result;
}  
else {
return false;
} 
    }
    
function getYearData($document){         
$this->db->select('quotations.prov_id, count( * ) as value');
$this->db->like('date(create_date)',date('Y'));
//$this->db->group_by("prov_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = quotations.prov_id');
$result = $this->db->get('quotations')->row_array();
//pre($result);
//echo $this->db->last_query($result);die;
if (!empty($result)) {           
return $result;
}  
else {
return false;
} 
    }
    
    
function getJobsTodayData($document)  {

$this->db->select('customer_job.user_id, count( * ) as value');
$this->db->where("date(create_date)",$document['date']);
$this->db->where('customer_job.status','5');
//$this->db->group_by("user_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = customer_job.user_id');
$result = $this->db->get('customer_job')->row_array();
//pre($result);die('blank ');
if (!empty($result)) {           
return $result;
}  
else {
return false;
}            
    }
    

    
    
function getJobsWeekData($document) {
//pre($document);die;
 $monday= date('Y/m/d',strtotime('last monday'));
 $sunday= date('Y/m/d',strtotime('next sunday'));
//echo $monday ; 
//echo $sunday ; die;
$this->db->select('customer_job.user_id, count( * ) as value');
$this->db->where('date(create_date) >=', $monday);
$this->db->where('date(create_date) <=', $sunday);
$this->db->where('customer_job.status','5');
//$this->db->group_by("user_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = customer_job.user_id');
$result = $this->db->get('customer_job')->row_array();
//pre($result);
//echo $this->db->last_query($result);die;

if (!empty($result)) {           
return $result;
}  
else {
return false;
}            
    }
    
function getJobsMonthData($document){             
$this->db->select('customer_job.user_id, count( * ) as value');
$this->db->like('date(create_date)',date('Y-m'));
$this->db->where('customer_job.status','5');
//$this->db->group_by("user_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = customer_job.user_id');
$result = $this->db->get('customer_job')->row_array();
//pre($result);
//echo $this->db->last_query($result);die;
if (!empty($result)) {           
return $result;
}  
else {
return false;
} 
    }
    
function getJobsYearData($document){         
$this->db->select('customer_job.user_id, count( * ) as value');
$this->db->like('date(create_date)',date('Y'));
$this->db->where('customer_job.status','5');
//$this->db->group_by("user_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = customer_job.user_id');
$result = $this->db->get('customer_job')->row_array();
//pre($result);
//echo $this->db->last_query($result);die;
if (!empty($result)) {           
return $result;
}  
else {
return false;
} 
    }
        
  
        
function getTodayjobsPosted($document)  {

$this->db->select('customer_job.user_id, count( * ) as value');
$this->db->where("date(create_date)",$document['date']);
$this->db->where('customer_job.status','0');
//$this->db->group_by("user_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = customer_job.user_id');
$result = $this->db->get('customer_job')->row_array();

if (!empty($result)) {           
return $result;
}  
else {
return false;
}            
    }
    

    
    
function getWeekjobsPosted($document) {
//pre($document);die;
$monday= date('Y/m/d',strtotime('last monday'));
$sunday= date('Y/m/d',strtotime('next sunday'));
$this->db->select('customer_job.user_id, count( * ) as value');
$this->db->where('date(create_date) >=', $monday);
$this->db->where('date(create_date) <=', $sunday);
$this->db->where('customer_job.status','0');
//$this->db->group_by("user_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = customer_job.user_id');
$result = $this->db->get('customer_job')->row_array();
//pre($result);
//echo $this->db->last_query($result);die;

if (!empty($result)) {           
return $result;
}  
else {
return false;
}            
    }
    
function getMonthjobsPosted($document){             
$this->db->select('customer_job.user_id, count( * ) as value');
$this->db->like('date(create_date)',date('Y-m'));
$this->db->where('customer_job.status','0');
//$this->db->group_by("user_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = customer_job.user_id');
$result = $this->db->get('customer_job')->row_array();
//pre($result);
//echo $this->db->last_query($result);die;
if (!empty($result)) {           
return $result;
}  
else {
return false;
} 
    }
    
function getYearjobsPosted($document){         
$this->db->select('customer_job.user_id, count( * ) as value');
$this->db->like('date(create_date)',date('Y'));
$this->db->where('customer_job.status','0');
//$this->db->group_by("user_id");
$this->db->order_by("value", "desc");
$this->db->limit(1);
//$this->db->join('providers','providers.prov_id = customer_job.user_id');
$result = $this->db->get('customer_job')->row_array();
//pre($result);
//echo $this->db->last_query($result);die;
if (!empty($result)) {           
return $result;
}  
else {
return false;
} 
    }
    

    
         function getQuoteRecievedList()
    {
        //pre($document); die();
        $this->db->select('quotations.*,customers.name as CustomerName,providers.firstName as ProviderName');
        $this->db->where("quotations.status", "0");
      //  $this->db->group_by("quotations.job_id");
       // $this->db->order_by("value", "desc");
        $this->db->order_by("quotation","desc");
        $this->db->join('customer_job','customer_job.id = quotations.job_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');
        $this->db->join('providers','providers.prov_id = quotations.prov_id');
        $result = $this->db->get('quotations')->result_array();
        //pre($result);
        //echo $this->db->last_query($result);die;
       // pre($result); die();
        if(!empty($result))
        {            
           return $result; 
        }
        else
        {
            return false;
        }
    }
     
    
    
  //  function getAvailableJobsList_04Feb2017_CurrentDate()
    function getAvailableJobsList_wait()
    {
        $date = date("Y-m-d");
        $prov_data = $this->db->select("business_address_lat,business_address_long,callout_radius,business_subtype")
                       // ->where("p1.prov_id", $user_id)
                        ->join("providers_subcat_relation as p2", "p1.prov_id=p2.prov_id")
                        ->get("providers as p1")->result_array();
        $sub_prov = $this->db->select("business_subtype")
                       // ->where("p1.prov_id", $user_id)
                        ->get("providers_subcat_relation as p1")->result_array();
        $sub_id = array();
        
        foreach ($sub_prov as $x):
            $sub_id[] = $x['business_subtype'];
        endforeach;
        
        $prov_data['sub_cat'] = implode(",", array_values($sub_id));  
        $result = $this->db->query("select t1.*, m1.name as main_category_name,m3.name as customer,m3.mobile as mobile from
                (SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_household_relation c2 on c1.id=c2.job_id "
            . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_outdoor_relation c2 on c1.id=c2.job_id "
            . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_personal_relation c2 on c1.id=c2.job_id "
            . "UNION SELECT c1.*,c2.pickup_location_lat as location_lat,c2.pickup_location_long as location_long FROM `customer_job` c1 inner join customer_job_transport_relation c2 on c1.id=c2.job_id)t1"
            . " inner join main_category as m1 on m1.id = t1.main_cat_id"
           // . " inner join sub_category as m2 on m2.sb_id = t1.sub_cat_id"
            . " inner join customers as m3 on m3.cust_id = t1.user_id"
            . " where t1.date='" . $date . "' and t1.time>='" . date('H:i:s') . "' and status=0  and t1.id not in (select job_id from job_request) ORDER BY t1.id DESC "
           // . " where t1.date='" . $date . "' and t1.time>='" . date('H:i:s') . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request) ORDER BY t1.id DESC "

                        )->result_array();    
           //echo $this->db->last_query();             
          // pre($result);die;   
              
          foreach ($result as $x) :
            $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
            $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
            $x['sub_category_name'] = $x['sub_category_name']['name'];
              if($x['sub_cat_id']==0) {
          //$x['transport'] =  $this->db->get('sub_category_transport')->row_array();
            $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
            $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
            $x['location'] =$x['location']['pickup_location'];
                  
                  
              }
          
           $result_modi[] = $x;          
           endforeach;
                  
           
       return (!empty($result_modi)) ? $result_modi : array();
        }
           
             
             
             
//        if(!empty($result))
//        {            
//           return $result; 
//        }
//        else
//        {
//            return false;
//        }
    //}
    
        
         function getAvailableJobsList_withoutDate()
    {
        $date = date("Y-m-d");
        $prov_data = $this->db->select("business_address_lat,business_address_long,callout_radius,business_subtype")
                       // ->where("p1.prov_id", $user_id)
                        ->join("providers_subcat_relation as p2", "p1.prov_id=p2.prov_id")
                        ->get("providers as p1")->result_array();
        $sub_prov = $this->db->select("business_subtype")
                       // ->where("p1.prov_id", $user_id)
                        ->get("providers_subcat_relation as p1")->result_array();
        $sub_id = array();
        
        foreach ($sub_prov as $x):
            $sub_id[] = $x['business_subtype'];
        endforeach;
        
        $prov_data['sub_cat'] = implode(",", array_values($sub_id));  
        $result = $this->db->query("select t1.*, m1.name as main_category_name,m3.name as customer,m3.mobile as mobile from
                (SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_household_relation c2 on c1.id=c2.job_id "
            . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_outdoor_relation c2 on c1.id=c2.job_id "
            . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_personal_relation c2 on c1.id=c2.job_id "
            . "UNION SELECT c1.*,c2.pickup_location_lat as location_lat,c2.pickup_location_long as location_long FROM `customer_job` c1 inner join customer_job_transport_relation c2 on c1.id=c2.job_id)t1"
            . " inner join main_category as m1 on m1.id = t1.main_cat_id"
           // . " inner join sub_category as m2 on m2.sb_id = t1.sub_cat_id"
            . " inner join customers as m3 on m3.cust_id = t1.user_id"
            . " where status=0  and t1.id not in (select job_id from job_request) ORDER BY t1.id DESC "
           // . " where t1.date='" . $date . "' and t1.time>='" . date('H:i:s') . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request) ORDER BY t1.id DESC "

                        )->result_array();    
           //echo $this->db->last_query();             
          // pre($result);die;   
              
          foreach ($result as $x) :
            $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
            $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
            $x['sub_category_name'] = $x['sub_category_name']['name'];
              if($x['sub_cat_id']==0) {
          //$x['transport'] =  $this->db->get('sub_category_transport')->row_array();
            $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
            $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
            $x['location'] =$x['location']['pickup_location'];
                  
                  
              }
          
           $result_modi[] = $x;          
           endforeach;
                  
           
       return (!empty($result_modi)) ? $result_modi : array();
        }
           
        
    function getAvailableJobsList()
    {        
       $date  = date("Y-m-d"); 
       $dated = date('Y-m-d H:i:s');
       $time1 = explode(" ",$dated);
       $time= $time1[1];


       // $date = date("Y-m-d");
      //  $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.main_cat_id,customer_job.status,main_category.name as main_category_name");
         $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name,customers.name as customer,customers.mobile as mobile");
         $this->db->where('customer_job.status', 0);
         $this->db->where("customer_job.date>=", $date);
         //$this->db->where("customer_job.time>=", $time);      
        // $this->db->or_where("customer_job.date>=", $date);      
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job')->result_array();
      
       //  echo $this->db->last_query($result);   
        // pre($result); die(); 
        if($result){
            foreach($result as $res){
                if($res['date']==date('Y-m-d')){
                    if($res['time']>date('H:i:s')){
                        $final[] = $res;
                    }
                }else{
                    $final[] = $res;
                }
            }
            if(!empty($final)){
            $result = $final;    
            }else{
            $result = array();
            }
        }
         $result_modi = array();
        if (!empty($result)) {
            

            
            foreach ($result as $x) :
            $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
            $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
            $x['sub_category_name'] = $x['sub_category_name']['name'];
            if($x['sub_cat_id']==0) {
          
            $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
            $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
            
            $x['location'] =$x['location']['pickup_location'];      
          }
          
           $result_modi[] = $x;          
           endforeach; 
            
            
            
        }
        return (!empty($result_modi)) ? $result_modi : array();
        

        
        
    }
    
        
    
        
        
        
    function getAcceptedJobsList()
    {
             
        $date = date("Y-m-d");
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name,customers.name as CustomerName,customers.mobile as mobile,quotations.prov_id as ProviderId,providers.firstName as ProviderName,providers.lastName as ProviderLastName");
        $this->db->where('customer_job.date', $date);
        $this->db->where('customer_job.status', 3);
        $this->db->order_by("customer_job.date", "desc");
       // $this->db->where("customer_job.id in (select job_id from job_request where prov_id='" . $user_id . "' and status='1' union select job_id from quotations where prov_id='" . $user_id . "' and status ='1')");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
//        $this->db->join('sub_category', 'sub_category.sb_id = customer_job.sub_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $this->db->join('quotations', 'quotations.job_id = customer_job.id');
        $this->db->join('providers', 'providers.prov_id = quotations.prov_id');
        $result = $this->db->get('customer_job')->result_array();
        
       //echo $this->db->last_query($result);   
     //   pre($result); die();   
             
         $result_modi = array();
        if (!empty($result)) {
            
           foreach ($result as $x) :
            $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
         //   $x['provider_name'] = $this->db->select('firstName')->where("prov_id", $x['ProviderId'])->get('providers')->row_array();
             
           $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
           //pre($x['cat']); die('dui'); 
           $x['sub_category_name'] = $x['sub_category_name']['name'];
             
              if($x['sub_cat_id']==0) {
             $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
            $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
       
             $x['location'] =$x['location']['pickup_location'];
          }
         // pre($x);die;
           $result_modi[] = $x;          
           endforeach; 
            
            
        }
        return (!empty($result_modi)) ? $result_modi : array();   
        
        
        
//        if(!empty($result))
//        {            
//           return $result; 
//        }
//        else
//        {
//            return false;
//        }
    }
    
    
    
    function getRequestedJobsList() { 
 
        $this->db->select("providers.firstName as firstName,providers.lastName as lastName,job_request.prov_id as provId,customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name,customers.name as CustomerName,customers.mobile as mobile");
        //  $date = date("Y-m-d");  
        //  $this->db->where('customer_job.status', 0);
        //  $this->db->where("customer_job.date>", $date);
       
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $this->db->join('job_request', 'job_request.job_id = customer_job.id');
        $this->db->join('providers', 'providers.prov_id = job_request.prov_id');
        $result = $this->db->get('customer_job')->result_array();
        
         $result_modi = array();
         if (!empty($result)) {
       
        foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];      
        }
        $result_modi[] = $x;          
        endforeach; 
    }
    return (!empty($result_modi)) ? $result_modi : array();
     
    }
    
    
    
    
    
        function getRequestedJobsList04April2017()
    {
        //pre($document); die();
//        $this->db->select('customers.name as CustomerName,providers.firstName as ProviderName,job_status.status as Status');
//        $this->db->where("job_status.status", "2");
//        $this->db->order_by("customers.name", "desc");
//        $this->db->join('providers','providers.prov_id = job_status.prov_id');
//        $this->db->join('customers','customers.cust_id = job_status.cust_id');
//        $result = $this->db->get('job_status')->result_array();
        //pre($result);
       // echo $this->db->last_query($result);die;
       // pre($result); die();
        
        $date = date("Y-m-d");
      //  $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.main_cat_id,customer_job.status,main_category.name as main_category_name");
         $this->db->select("providers.firstName as firstName,providers.lastName as lastName,job_request.prov_id as provId,customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name,customers.name as CustomerName,customers.mobile as mobile");
         $this->db->where('customer_job.status', 0);
         $this->db->where("customer_job.date>", $date);
       
        $this->db->order_by("customer_job.date", "desc");
        //$this->db->where("customer_job.id in (select job_id from job_request where status='0') ");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $this->db->join('job_request', 'job_request.job_id = customer_job.id');
      //  $this->db->join('quotations', 'quotations.job_id = customer_job.id');
        $this->db->join('providers', 'providers.prov_id = job_request.prov_id');
        $result = $this->db->get('customer_job')->result_array();
      
        //echo $this->db->last_query($result);   
        //pre($result); die(); 
      
         $result_modi = array();
        if (!empty($result)) {
            
            /*
            
            
            foreach ($result as $x) :

//               $x['name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row()->name;
               $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
//               $prov_id = $this->db->select("prov_id,concat(firstName,' ',lastName) as prov_name,IF(profile_img='','',concat('" . base_url('providerImages') . "','/',profile_img)) as prov_profileImage")
//                                ->where("prov_id = (select prov_id from quotations where job_id = {$x['id']} and status =1)")
//                                ->or_where("prov_id = (select prov_id from job_request where job_id = {$x['id']} and status =1)")
//                                ->get('providers')->row_array();

//                $x = array_merge($x, $prov_id);
                if (!empty($x)) {
                $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                $x['time'] = date("h:i a", strtotime($x['time']));
                $result_modi[] = $x;
                }
        
            endforeach;         */
            
            
            foreach ($result as $x) :
            $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
              $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
          $x['sub_category_name'] = $x['sub_category_name']['name'];
              if($x['sub_cat_id']==0) {
          
            $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
            $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
            
            $x['location'] =$x['location']['pickup_location'];      
          }
          
           $result_modi[] = $x;          
           endforeach; 
            
            
            
        }
        return (!empty($result_modi)) ? $result_modi : array();
        
        
        
//        if(!empty($result))
//        {            
//           return $result; 
//        }
//        else
//        {
//            return false;
//        }
        
        
        
        
    }
    
    
    
    
    function get_sub_job_data($cat_id, $job_id) {
       // pre($cat_id)."&nbsp";
       // pre($job_id)."&nbsp";die;
        switch ($cat_id) {
            case 1:
                $table = 'customer_job_household_relation';
                break;
            case 2:
                $table = 'customer_job_outdoor_relation';
                break;
            case 3:
                $table = 'customer_job_personal_relation';
                break;
            case 4:
                $table = 'customer_job_transport_relation';
                break;
            default:
                break;
        }

        if (isset($table)) {
            $result = $this->db->where('job_id', $job_id)
                            ->get($table)->row_array();
           // unset($result['id']);
           // unset($result['job_id']);
        }
      //  pre($result); die('rana');
        return $result;
        
    }

    
         function getPendingJobsList() {

        $date = date("Y-m-d");
       // $date = '2017-02-16';
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name,customers.name as CustomerName,customers.mobile as mobile,providers.firstName as ProviderfirstName,providers.lastName as ProviderlastName");
        $this->db->where('customer_job.date<', $date);
        $this->db->where('customer_job.status', 3);
        $this->db->where('quotations.status', 1);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');       
        $this->db->join('quotations', 'quotations.job_id   = customer_job.id');        
        $this->db->join('providers', 'providers.prov_id   = quotations.prov_id');        
        $result = $this->db->get('customer_job')->result_array();  
        
        
        
        //echo $this->db->last_query();die;
        
        $result_modi = array();
        if (!empty($result))
        {        
        foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];     
        }        
        $result_modi[] = $x;          
        endforeach; 
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }
    
    
         function getCompletedJobsList() {

        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name,customers.name as CustomerName,invoice.total_amount as total_amount_paid,customers.mobile as mobile,providers.firstName as providerFirst,providers.lastName as providerlastName");    
        $this->db->where('customer_job.status', 5);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $this->db->join('invoice','invoice.job_id = customer_job.id'); 
        $this->db->join('providers', 'providers.prov_id = invoice.prov_id');
        $result = $this->db->get('customer_job')->result_array();     
  
        
        $result_modi = array();
        if (!empty($result))
        {        
        foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];     
        }        
        $result_modi[] = $x;          
        endforeach; 
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }
    
    
   function getCancelledJobsList()
    {
        //pre($document); die();
        $this->db->select('customers.*,customer_job.*,main_category.name as mainCategory,cancel_job_relation.created_on as cancleDate');
        //$this->db->order_by("customer_job.date", "desc");
        $this->db->order_by("cancel_job_relation.created_on", "desc");
        $this->db->join('cancel_job_relation','cancel_job_relation.job_id = customer_job.id');
         $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $this->db->join('customers','customers.cust_id = cancel_job_relation.user_id');
        $result = $this->db->get('customer_job')->result_array();
        
       // pre($result);die;

        $result_modi = array();
        if (!empty($result))
        {        
        foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];     
        }        
        $result_modi[] = $x;          
        endforeach; 
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }
    
    
   function getReschedulesJobsList()
    {
        //pre($document); die();
        $this->db->select('customers.name as CustomerName,customer_job.status as Status,customer_job.date as Jobdate,main_category.name as mainCategory,reschedule_job_relation.*');
      //  $this->db->where("customer_job.status", "0");
      //  $this->db->or_where("customer_job.status", "3");
        $this->db->order_by("reschedule_job_relation.date", "desc");
//        $this->db->join('providers','providers.prov_id = customer_job.user_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');
          $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $this->db->join('reschedule_job_relation','reschedule_job_relation.job_id = customer_job.id');
        $result = $this->db->get('customer_job')->result_array();
       // pre($result);
        //echo $this->db->last_query($result);die;
       // pre($result); die();
        if(!empty($result))
        {            
           return $result; 
        }
        else
        {
            return false;
        }
    }
    
    
    
    function getExpiredJobsListOLD()
    {
        
         $date=date('Y-m-d');
        //pre($document); die();
        $this->db->select('customers.name as CustomerName,customer_job.*,main_category.name as Category');
        $this->db->where("customer_job.status", "0");
        $this->db->where("customer_job.date<", $date);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job')->result_array();
       //pre($result); die;
       //echo $this->db->last_query($result);die;
       // pre($result); die();
        if(!empty($result))
        {            
           return $result; 
        }
        else
        {
            return false;
        }
    }
    function getExpiredJobsList()
    {
        
   /*      $date=date('Y-m-d');
        //pre($document); die();
        $this->db->select('customers.name as CustomerName,customer_job.*,main_category.name as Category');
        $this->db->where("customer_job.status", "0");
        $this->db->where("customer_job.date<", $date);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job')->result_array();
    */ 
        
        $date = date("Y-m-d");
      //  $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name,customers.name as CustomerName,customers.mobile as mobile,quotations.prov_id as ProviderId,providers.firstName as ProviderName,providers.lastName as ProviderLastName,TIMESTAMPDIFF( MINUTE ,CONCAT( date,  ' ', time ), '" . date('Y-m-d H:i:s') . "'  ) AS difference");
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name,customers.name as CustomerName,customers.mobile as mobile,TIMESTAMPDIFF( MINUTE ,CONCAT( date,  ' ', time ), '" . date('Y-m-d H:i:s') . "'  ) AS difference");
        $this->db->where("customer_job.status", "0");
        $this->db->where("customer_job.date<=", $date);
        $this->db->having("difference >=",0);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
     //  $this->db->join('quotations', 'quotations.job_id = customer_job.id');
     //   $this->db->join('providers', 'providers.prov_id = quotations.prov_id');
        $result = $this->db->get('customer_job')->result_array();
        
       // echo $this->db->last_query(); 
      // pre($result);die;
        
        $result_modi = array();
        if (!empty($result))
        {        
        foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];     
        }        
        $result_modi[] = $x;          
        endforeach; 
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }
    
    
    
     function getMissedCheckDatetimeOLD() {
        $date=date('Y-m-d');
        //pre($document); die();
        $this->db->select('customers.name as CustomerName,customer_job.*,main_category.name as Category');
        $this->db->where("customer_job.status", "3");
        $this->db->where("customer_job.date<", $date);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job')->result_array();
        
        if(!empty($result))
        {            
           return $result; 
        }
        else
        {
            return false;
        }
    }
  
 function getMissedCheckDatetime() {
         
/*      $date=date('Y-m-d');   
        pre($document); die();
        $this->db->select('customers.name as CustomerName,customers.mobile as mobile,customer_job.*,main_category.name as Category');
        $this->db->where("customer_job.status", "3");
        $this->db->where("customer_job.date<", $date);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job')->result_array();   
*/ 
     
        $date = date("Y-m-d");
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name,customers.name as CustomerName,customers.mobile as mobile,quotations.prov_id as ProviderId,providers.firstName as ProviderName,providers.lastName as ProviderLastName");
        $this->db->where("customer_job.status", "3");
        $this->db->where("customer_job.date<", $date);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $this->db->join('quotations', 'quotations.job_id = customer_job.id');
        $this->db->join('providers', 'providers.prov_id = quotations.prov_id');
        $result = $this->db->get('customer_job')->result_array();   
        
        
        
        $result_modi = array();
        if (!empty($result))
        {        
        foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];     
        }        
        $result_modi[] = $x;          
        endforeach; 
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }
    
     function get_check_datetime() {

       /* $result = $this->db->select("customer_job.id as CustomerJobId,customer_job.status as status,main_category.name as Category,user_id,date,time,customers.mobile as Mobile,customers.email as Email,customers.name as Customer,TIMESTAMPDIFF( MINUTE ,CONCAT( date,  ' ', time ), '" . date('Y-m-d H:i:s') . "'  ) AS difference")
       ->where("date",date('Y-m-d'))                    
       ->where("customer_job.status",'3')                    
       ->join('customers','customers.cust_id = customer_job.user_id')
       ->join('main_category','main_category.id = customer_job.main_cat_id')
       ->having("difference > ",30)
       ->get("customer_job")->result_array();
        */
         
        $date = date("Y-m-d");
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name,customers.name as Customer,customers.mobile as mobile,quotations.prov_id as ProviderId,providers.firstName as ProviderName,providers.lastName as ProviderLastName,TIMESTAMPDIFF( MINUTE ,CONCAT( date,  ' ', time ), '" . date('Y-m-d H:i:s') . "'  ) AS difference");
        //$this->db->select("customer_job.id as CustomerJobId,customer_job.status as status,main_category.name as Category,user_id,date,time,customers.mobile as Mobile,customers.email as Email,customers.name as Customer,TIMESTAMPDIFF( MINUTE ,CONCAT( date,  ' ', time ), '" . date('Y-m-d H:i:s') . "'  ) AS difference");
        $this->db->where("customer_job.status", "3");
        $this->db->where("customer_job.date", $date);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->having("difference >",30);
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $this->db->join('quotations', 'quotations.job_id = customer_job.id');
        $this->db->join('providers', 'providers.prov_id = quotations.prov_id');
        $result = $this->db->get('customer_job')->result_array();   
         
         
        $result_modi = array();
        if (!empty($result))
        {        
        foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];     
        }        
        $result_modi[] = $x;          
        endforeach; 
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }
    
    
    function get_check_datetimeOLD() {

        $result = $this->db->select("customer_job.id as CustomerJobId,customer_job.status as status,main_category.name as Category,user_id,date,time,customers.mobile as Mobile,customers.email as Email,customers.name as Customer,TIMESTAMPDIFF( MINUTE ,CONCAT( date,  ' ', time ), '" . date('Y-m-d H:i:s') . "'  ) AS difference")
       ->where("date",date('Y-m-d'))                    
       ->where("customer_job.status",'3')                    
       ->join('customers','customers.cust_id = customer_job.user_id')
       ->join('main_category','main_category.id = customer_job.main_cat_id')
       ->having("difference > ",30)
       ->get("customer_job")->result_array();
        if(!empty($result))
        {            
           return $result; 
        }
        else
        {
            return false;
        }
    }
    
    
    
        function get_check_datetime_05JAN2017() {
           // pre(date('Y-m-d'));die;
        //$job = $this->db->select("CONCAT(date,' ',time) as datetime")
        $job = $this->db->select("customer_job.id as CustomerJobId,customer_job.status as status,main_category.name as Category,sub_category.name as SubCategory,user_id,date,time,customers.mobile as Mobile,customers.email as Email,customers.name as Customer,TIMESTAMPDIFF( MINUTE ,CONCAT( date,  ' ', time ), '" . date('Y-m-d H:i:s') . "'  ) AS difference")
                        ->where("date",date('Y-m-d'))   
                        ->where("customer_job.status",'3')
                        ->or_where("customer_job.status",'4')                    
                        ->join('customers','customers.cust_id = customer_job.user_id')
                        ->join('main_category','main_category.id = customer_job.main_cat_id')
                        ->join('sub_category','sub_category.sb_id = customer_job.sub_cat_id')
                        ->having("difference > ",30)
                        ->get("customer_job")->result_array();
        
        //pre($job);
        //echo $this->db->last_query($job);die;
        if(!empty($job)){
        return $job;
        }
    }
    
   
    
    
    function getMissedCheckDatetime_05JAN2017() {
        $yesterday_date=date('Y-m-d',strtotime("-1 days"));
        $this->db->select("customer_job.id as CustomerJobId,customer_job.status as status,main_category.name as Category,sub_category.name as SubCategory,user_id,date,time,customers.mobile as Mobile,customers.email as Email,customers.name as Customer");
        $this->db->where("customer_job.date",$yesterday_date);
        $this->db->where("customer_job.status",'4');
        //$this->db->or_where("customer_job.status",'3');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $this->db->join('sub_category','sub_category.sb_id = customer_job.sub_cat_id') ;                    
        // $this->db->having("difference > ",1440)
        $job= $this->db->get("customer_job")->result_array();
        
        //pre($job);
        //echo $this->db->last_query($job);die;
        if(!empty($job)){
        return $job;
        }
    }
    
    
    function gethouseholdLocation()
    {       
        $this->db->select('customer_job.*,customer_job.id as CustomerJobID,customer_job_household_relation.*,customers.name as customersName,customer_job.sub_cat_id AS SubCategory,sub_category.name AS SubCategoryName,main_category.name as Category');
        $this->db->order_by("customer_job_household_relation.id", "desc");
        $this->db->join('customer_job','customer_job.id = customer_job_household_relation.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $this->db->join('sub_category','sub_category.sb_id = customer_job.sub_cat_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');  
        $result = $this->db->get('customer_job_household_relation')->result_array();
        if(!empty($result))  {      return $result;   }
        else   {    return false; }
    }
    
    function getoutdoorLocation()
    {         
        $this->db->select('customer_job.*,customer_job.id as CustomerJobID,customer_job_outdoor_relation.*,customers.name as customersName,customer_job.sub_cat_id AS SubCategory,sub_category.name AS SubCategoryName,main_category.name as Category');
        $this->db->order_by("customer_job_outdoor_relation.id", "desc");
        $this->db->join('customer_job','customer_job.id = customer_job_outdoor_relation.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $this->db->join('sub_category','sub_category.sb_id = customer_job.sub_cat_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');  
        $result = $this->db->get('customer_job_outdoor_relation')->result_array();
        if(!empty($result))  {   return $result;   }
        else   {    return false; }
    }
    function getpersonalLocation()
    {       
        $this->db->select('customer_job.*,customer_job.id as CustomerJobID,customer_job_personal_relation.*,customers.name as customersName,customer_job.sub_cat_id AS SubCategory,sub_category.name AS SubCategoryName,main_category.name as Category');
        $this->db->order_by("customer_job_personal_relation.id", "desc");
        $this->db->join('customer_job','customer_job.id = customer_job_personal_relation.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $this->db->join('sub_category','sub_category.sb_id = customer_job.sub_cat_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id'); 
        $result = $this->db->get('customer_job_personal_relation')->result_array();
        if(!empty($result))  {   return $result;   }
        else   {    return false; }
    }
    
    function gettransportLocation()
    {              
        $this->db->select('customer_job.*,customer_job.id as CustomerJobID,customer_job_transport_relation.*,customers.name as customersName,main_category.name as Category');
        $this->db->order_by("customer_job_transport_relation.id", "desc");
        $this->db->join('customer_job','customer_job.id = customer_job_transport_relation.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job_transport_relation')->result_array();
        if(!empty($result))  {   return $result;   }
        else   {    return false; }
    }
    
    function getPromoList() {    

    $result = $this->db->get('promocode')->result_array();
    if(!empty($result)) 
    {            
    return $result;
    }
    else  
    {
    return false;
    }
    }
    
    public function deletePromoCode($table,$id = null)
    {
    $result = $this->db->delete($table, array('id'=>$id));
    return $result;
    }
    
    public function deletePendingJobs($table,$id = null)
    {
    $result = $this->db->delete($table, array('id'=>$id));
    return $result;
    }
    public function deleteSubCategory($table,$id = null)
    {
    $result = $this->db->delete($table, array('sb_id'=>$id));
    return $result;
    }
    
    public function updateSubCategoryList($table,$id = null)
    {
    $this->db->where("sb_id",$id);
    $result = $this->db->update($table);
    if($result){
                 $this->db->where("sb_id",$id);
    	$query = $this->db->get($table)->result_array();
        return $query;
    }
        else{return FALSE;}
    
    //pre($result);
    //echo  $this->db->last_query();die;
    //return $result;
    }
    
     public function deleteList($table,$id = null)
    {
    $result = $this->db->delete($table, array('id'=>$id));
    return $result;
    }
    
    public function deleteProviderList($table,$id = null)
    {
    $result = $this->db->delete($table, array('prov_id'=>$id));
    return $result;
    }
    public function deleteCustomerList($table,$id = null)
    {
    $result = $this->db->delete($table, array('cust_id'=>$id));
    return $result;
    }
     public function updateList($table,$id = null)
    {
       //  pre($id);
        // pre($table);die;
              $this->db->where("id",$id);
              $this->db->set("status",10);
    $result = $this->db->update($table);
    //pre($result);
    //echo  $this->db->last_query();die;
    return $result;
    }
     public function updateListUnpaid($table,$id = null)
    {
              $this->db->where("id",$id);
              $this->db->set("status",9);
    $result = $this->db->update($table);
    //pre($result);
    //echo  $this->db->last_query();die;
    return $result;
    }
    
          public function updateProviderListAccept17March17($table,$id = null)
    {
        $this->db->where("prov_id",$id);
        $providers  = $this->db->get($table)->row_array();
        // pre($providers);die;
     
       // $id                        =$providers['prov_id']; 
        $id_or_passport            =$providers['id_or_passport']; 
        $bank_proof                =$providers['bank_proof']; 
        $certificates              =$providers['certificates']; 
      //  $firstName              =$providers['firstName']; 
         
     if(empty($id_or_passport) OR empty($bank_proof) OR empty($certificates)){
           $this->session->set_flashdata('success_message','Account cannot go LIVE until `Id or Passport` and `Bank Proof` and `Certificates` sections have documents ');
           redirect('admin/providersDetails');
    }  
    
    else{
	     $this->db->where("prov_id",$id);
              $this->db->set("status",'1');
              $result = $this->db->update($table);
             // pre($result);
              //echo $this->db->last_query();die;
              return $result;
    }			
	
    
    }
    
     public function updateProviderListReject17March17($table,$id = null)
    {
              $this->db->where("prov_id",$id);
              $this->db->set("status",'0');
    $result = $this->db->update($table);
   // pre($result);
   // echo  $this->db->last_query();die;
    return $result;
    }
    
        
     public function updateProviderListReject($table,$id = null)
    {
             // pre($table);
             // pre($id);die;
         $this->db->where("prov_id",$id);
              $this->db->set("status",'1');
    $result = $this->db->update($table);
   // pre($result);
   // echo  $this->db->last_query();die;
    return $result;
    }
    
    
     public function updateProviderListAccept($table,$id = null)
    {
        $this->db->where("prov_id",$id);
        $providers  = $this->db->get($table)->row_array();
        // pre($providers);die;
     
       // $id                        =$providers['prov_id']; 
        $id_or_passport            =$providers['id_or_passport']; 
        $bank_proof                =$providers['bank_proof']; 
        $certificates              =$providers['certificates']; 
      //  $firstName              =$providers['firstName']; 
         
     if(empty($id_or_passport) OR empty($bank_proof) OR empty($certificates)){
           $this->session->set_flashdata('success_message','Account cannot go LIVE until `Id or Passport` and `Bank Proof` and `Certificates` sections have documents ');
           redirect('admin/providersDetails');
    }  
    
    else{
	     $this->db->where("prov_id",$id);
              $this->db->set("status",'0');
              $result = $this->db->update($table);
             // pre($result);
              //echo $this->db->last_query();die;
              return $result;
    }			
	
    
    }
      
   
    
    function getProviderRating()
    {       
        $this->db->select('provider_rating.id,provider_rating.create_date as Date,customers.name as CustomerName,providers.firstName as ProviderName,provider_rating.rating as Rating,provider_rating.feedback as Feedback,main_category.name as CategoryName');
        $this->db->order_by("provider_rating.rating", "desc");
        $this->db->join('providers','providers.prov_id = provider_rating.prov_id');
        $this->db->join('customers','customers.cust_id = provider_rating.cust_id');
        $this->db->join('customer_job','customer_job.id = provider_rating.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $result = $this->db->get('provider_rating')->result_array();
        //pre($result); die;
        if(!empty($result))  {      return $result;   }
        else   {    return false; }
    }
    
    
    
    function getCustomerRating()
    {       
        $this->db->select('customer_rating.id,customer_rating.create_date as Date,providers.firstName as ProviderName,customer_rating.rating as Rating,customer_rating.feedback as Feedback,main_category.name as CategoryName,customers.name as CustomerName');
        $this->db->order_by("customer_rating.rating", "desc");
       // $this->db->where("customer_rating.isRead",0);
        $this->db->join('providers','providers.prov_id = customer_rating.prov_id');
        $this->db->join('customers','customers.cust_id = customer_rating.cust_id');
        $this->db->join('customer_job','customer_job.id = customer_rating.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $result = $this->db->get('customer_rating')->result_array();
        //pre($result); die;
        if(!empty($result))  {      return $result;   }
        else   {    return false; }
    }
    
    
         function getQuotePaymentList()
    {
        //pre($document); die();
        $this->db->select('customer_job.*,quotations.*,providers.firstName as Provider,customers.name as Customer,quotations.quotation as Quatation,quotations.status as Status,quotations.create_date as Date,main_category.name as Category');
        $this->db->order_by("quotations.id", "desc");
        $this->db->join('customer_job','customer_job.id = quotations.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        //$this->db->join('sub_category','sub_category.sb_id = customer_job.sub_cat_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');
        $this->db->join('providers','providers.prov_id = quotations.prov_id');
        $result = $this->db->get('quotations')->result_array();
       // pre($result);
       // echo $this->db->last_query($result);die;
       // pre($result); die();
//        if(!empty($result))
//        {            
//           return $result; 
//        }
//        else
//        {
//            return false;
//        }
        
         foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['job_id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['job_id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];
        }
        $result_modi[] = $x;          
        endforeach;

        return (!empty($result_modi)) ? $result_modi : array();  
        
        
        
    }
     
    
    
         function getInvoicesPaymentList()
    {
        //pre($document); die();
        $this->db->select('invoice.*,customer_job.*,providers.firstName as Provider,customers.name as Customer,main_category.name as Category,customer_job.date as date,final_quotation.total as finalQuatation');
//        $this->db->order_by("invoice.total_amount", "desc");
        $this->db->order_by("invoice.id", "desc");
        $this->db->join('customer_job','customer_job.id = invoice.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        //$this->db->join('sub_category','sub_category.sb_id = customer_job.sub_cat_id');
        $this->db->join('customers','customers.cust_id = customer_job.user_id');
        $this->db->join('providers','providers.prov_id = invoice.prov_id');
        $this->db->join('final_quotation','final_quotation.job_id = invoice.job_id');
        $result = $this->db->get('invoice')->result_array();
        //pre($result);die;
        //echo $this->db->last_query($result);die;
       // pre($result); die();
       foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['job_id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['job_id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];
        }
        $result_modi[] = $x;          
        endforeach;

        return (!empty($result_modi)) ? $result_modi : array();  
    }
    
    
           function getCalloutChargePayment()
    {
        //pre($document); die();
       // $this->db->select('customer_payment_details.*,providers.firstName as Provider,customers.name as customer,main_category.name as Category');
        $this->db->select('customer_payment_details.*,customer_job.*,providers.firstName as Provider,customers.name as customer,main_category.name as Category');
        $this->db->order_by("customer_payment_details.quote_payment_amount", "desc");
        //$this->db->where("callout_payment_amount", "48.00");
        $this->db->join('providers','providers.prov_id = customer_payment_details.prov_id');
        $this->db->join('customers','customers.cust_id = customer_payment_details.cust_id');
        $this->db->join('customer_job','customer_job.id = customer_payment_details.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
       // $this->db->join('sub_category','sub_category.sb_id = customer_job.sub_cat_id');
        $result = $this->db->get('customer_payment_details')->result_array();
       // pre($result);die;
        //echo $this->db->last_query($result);die;
       // pre($result); die();
        foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['job_id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['job_id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];
        }
        $result_modi[] = $x;          
        endforeach;

        return (!empty($result_modi)) ? $result_modi : array();  
    }
    
    
    
   function getProviderAccountsPayment()
    {
        //pre($document); die();
        
        $this->db->select('job_status.*,job_status.status as Job_status,customer_job.main_cat_id as main_cat_id,customer_job.sub_cat_id as sub_cat_id,providers.firstName as Provider,customers.name as customer,main_category.name as Category,customer_job.date as date');
//        $this->db->order_by("job_status.quote_payment_amount", "desc");
        $this->db->where("job_status.status","9");
        $this->db->or_where("job_status.status","10");
        $this->db->join('providers','providers.prov_id = job_status.prov_id');
        $this->db->join('customers','customers.cust_id = job_status.cust_id');
        $this->db->join('customer_job','customer_job.id = job_status.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
       // $this->db->join('sub_category','sub_category.sb_id = customer_job.sub_cat_id');
        $result = $this->db->get('job_status')->result_array();
       // pre($result);die;
        foreach ($result as $x) :
        $x['sub_category_name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
        $x['cat'] = $this->get_sub_job_data($x['main_cat_id'], $x['job_id']);
        $x['sub_category_name'] = $x['sub_category_name']['name'];
        if($x['sub_cat_id']==0) {
        $x['location'] = $this->db->select('pickup_location')->where("job_id", $x['job_id'])->get('customer_job_transport_relation')->row_array();
        $x['transport'] =  $this->db->get('sub_category_transport')->row_array();
        $x['location'] =$x['location']['pickup_location'];
        }
        $result_modi[] = $x;          
        endforeach;

        return (!empty($result_modi)) ? $result_modi : array();  
        
        
        
    }
    
   ///////////////////////////////////////////////////   Custom Helper Function /////////////
        function customer() {
        $this->db->where("isRead", "0");
        $result = $this->db->get('customers')->result_array();
        return $result;
    }
    
     function customerRating() {
        $this->db->where("isRead", "0");
        $result = $this->db->get('customer_rating')->result_array();
        return $result;
    }
    
    
    function providerRating() {
        $this->db->where("isRead", "0");
        $result = $this->db->get('provider_rating')->result_array();
        return $result;
    }
    
    function customerQuote() {
        $this->db->where("isRead", "0");
        $result = $this->db->get('quotations')->result_array();
        return $result;
    }
    
    
    function cancelled() {
        $this->db->where("isRead", "0");
        $result = $this->db->get('cancel_job_relation')->result_array();
        return $result;
    }
    
    
    function reschedules() {
        $this->db->where("isRead", "0");
        $result = $this->db->get('reschedule_job_relation')->result_array();
        //pre($result);die;
        return $result;
    }
    
    
    function expiredJobs() {
        
        $date = date('Y-m-d');       
        $this->db->where("customer_job.date<", $date);
        $this->db->where("isRead", "0");
        $this->db->where("status", "0");
        $result = $this->db->get('customer_job')->result_array();
        return $result;
    }
    
    function missedAppointment() {
        
        $date = date('Y-m-d');        
        $this->db->where("customer_job.date<", $date);
        $this->db->where("isRead", "0");
        $this->db->where("status", "3");
        $result = $this->db->get('customer_job')->result_array();
        return $result;
    }
    
        function lateAppointment() {
        
        $date = date('Y-m-d');
        $this->db->select("customer_job.*,TIMESTAMPDIFF( MINUTE ,CONCAT( date,  ' ', time ), '" . date('Y-m-d H:i:s') . "'  ) AS difference");    
        $this->db->where("customer_job.date", $date);
        $this->db->where("isRead", "0");                
        $this->db->where("customer_job.status",'3') ;
        $this->db->having("difference > ",30);
        $result = $this->db->get('customer_job')->result_array();
        return $result;
    }
    
     function payment() {
        $this->db->where("isRead", "0");
        $result = $this->db->get('customer_payment_details')->result_array();
       // pre($result);die('Nishu');
        return $result;
    }
       
     function getAllCallout($table,$date) {
        $this->db->select('SUM(quote_payment_amount) AS total,customer_payment_details.*,customer_job.*,customers.name as customer,main_category.name as Category');
        $this->db->order_by("customer_payment_details.created_on", "desc");
        $this->db->where_in("customer_job.main_cat_id",array(1,2));
        $this->db->where('quote_payment_date BETWEEN "'.$date['fromDate'] . '" and "'. $date['toDate'].'"');                       
        $this->db->join('customers','customers.cust_id = customer_payment_details.cust_id');
        $this->db->join('customer_job','customer_job.id = customer_payment_details.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $result = $this->db->get('customer_payment_details')->row_array(); 
       if($result){
        $this->db->select('customer_payment_details.*,customer_job.*,customers.name as customer,main_category.name as Category');
        $this->db->order_by("customer_payment_details.created_on", "desc");
        $this->db->where_in("customer_job.main_cat_id",array(1,2));
        $this->db->where('quote_payment_date BETWEEN "'.$date['fromDate'] . '" and "'. $date['toDate'].'"');                       
        $this->db->join('customers','customers.cust_id = customer_payment_details.cust_id');
        $this->db->join('customer_job','customer_job.id = customer_payment_details.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $result1 = $this->db->get('customer_payment_details')->result_array(); 
        if($result1){
            $result1[0]['total'] = $result['total'];
            return $result1;
        }
           
       }else{
           return false;
       }
    }
    
    public function getSubCategoryList($table){ //deleteSubcategoryList
	$query = $this->db->get($table)->result_array();
        return $query;
    }
    
    
    
    public function getListUploadPDF($table){ //deleteSubcategoryList
	$query = $this->db->get($table)->result_array();
        if(!empty($query)){
            
        return $query;
        }
        else{
            return FALSE;
        }
    }
    
    
    
    public function getListRole($table){ 

        $query = $this->db->get($table)->result_array();
        if(!empty($query)){
            
        return $query;
        }
        else{
            return FALSE;
        }
    }
 }

