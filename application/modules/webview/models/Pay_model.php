<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pay_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function register_token($data,$id) {
        $url = "https://oppwa.com/v1/registrations";
        $data = "authentication.userId=8a82941856a2ae3a0156a2d65bdb0081" .
                "&authentication.password=Q792M3K877" .
                "&authentication.entityId=8a82941856a2ae3a0156a2d8fe990091" .
                "&paymentBrand=" .
                "&card.number=" . $data['card_number'] .
                "&card.holder=" . $data['holder_name'] .
                "&card.expiryMonth=" . $data['exp_month'] .
                "&card.expiryYear=" . $data['exp_year'] .
                "&card.cvv=" . $data['card_cvv'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        curl_close($ch);
        $pay_data = json_decode($responseData, true);
        if(isset($pay_data['id'])){
            $this->db->set("token",$pay_data['id'])->where("cust_id",$id)->update("customers");
            return true;
        }
        else{
            return false;
        }
    }

}
