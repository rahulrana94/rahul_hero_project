<!DOCTYPE html>
<html >
    <head>


        <title>Signup Form</title>



        <link rel="stylesheet" href="<?php echo base_url("assets") ?>/css/style.css">

        <link rel="stylesheet" href="<?php echo base_url("assets") ?>/css/bootstrap.min.css">



    </head>

    <body>

        <div class="center">

            <div class="col-md-12 col-xs-12 col-sm-12 signup-form">

                <div class="col-md-12 col-sm-12 col-xs-12 text-center signup-heading">
                    <h4>Pay Now</h4>
                    
                </div>  

                <form name="form1" id="form1" method="post" action="">
                    <!--input type="hidden" name="cust_id" value="<?php //echo $cust_id;   ?>"-->
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php echo form_error('brand'); ?>
                        <select class="signup-form-select" id="dropdown" name="brand">
                            <option value="">Please select a brand</option>
                            <option value="VISA">VISA</option>
                            <option value="MASTER">MASTER</option>
                            <option value="ELO">ELO</option>
                            <option value="JCB">JCB</option>
                            <option value="AMEX">AMEX</option>
                            <option value="VPAY">VPAY</option>
                            <option value="AXESS">AXESS</option>
                            <option value="BONUS">BONUS</option>
                            <option value="CABAL">CABAL</option>
                            <option value="WORLD">WORLD</option>
                            <option value="DINERS">DINERS</option>
                            <option value="BITCOIN">BITCOIN</option>
                            <option value="DANKORT">DANKORT</option>
                            <option value="MAESTRO">MAESTRO</option>
                            <option value="MAXIMUM">MAXIMUM</option>
                            <option value="AFTERPAY">AFTERPAY</option>
                            <option value="ASYACARD">ASYACARD</option>
                            <option value="DISCOVER">DISCOVER</option>
                            <option value="POSTEPAY">POSTEPAY</option>
                            <option value="SERVIRED">SERVIRED</option>
                            <option value="HIPERCARD">HIPERCARD</option>
                            <option value="VISADEBIT">VISADEBIT</option>
                            <option value="CARDFINANS">CARDFINANS</option>
                            <option value="CARTEBLEUE">CARTEBLEUE</option>
                            <option value="VISAELECTRON">VISAELECTRON</option>
                            <option value="CARTEBANCAIRE">CARTEBANCAIRE</option>
                            <option value="KLARNA_INVOICE">KLARNA_INVOICE</option>
                            <option value="DIRECTDEBIT_SEPA">DIRECTDEBIT_SEPA</option>
                        </select>
                        
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php echo form_error('card_number'); ?>
                        <input type="text" class="signup-form-input" placeholder="Card Number" name="card_number" maxlength="16">
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php echo form_error('holder_name'); ?>
                        <input type="text" class="signup-form-input" placeholder="Card Holder" name="holder_name">
                    </div>



                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php echo form_error('exp_month'); ?>
                        <select class="signup-form-select" id="dropdown" name="exp_month">
                            <option value="">Card Expiry Month</option>
                            <?php for ($i = 1; $i <= 12; $i++): ?>
                                <option value="<?php echo sprintf("%02d", $i); ?>"><?php echo sprintf("%02d", $i); ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>


                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php echo form_error('exp_year'); ?>
                        <select class="signup-form-select" id="dropdown" name="exp_year">
                            <option value="">Card Year</option>
                            <?php for ($i = date('Y'); $i < 2031; $i++): ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php echo form_error('card_cvv'); ?>
                        <input type="text" class="signup-form-input" placeholder="Card CCV" name="card_cvv" minlength="3" maxlength="4">
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-offset-4 col-md-4">
                            <input type="submit" class="signup-form-submit" value="Next" name="submit">
                        </div>
                    </div>
                </form>
            </div>


        </div>

    </body>

</html>