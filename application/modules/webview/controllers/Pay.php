<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pay extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation', 'uploads');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('html');
        $this->load->model('Pay_model');
    }

    public function tokenization() {
        $id = $this->uri->segment(2);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('brand', 'Brand Name', 'required');
        $this->form_validation->set_rules('card_number', 'Card Number', 'required|numeric');
        $this->form_validation->set_rules('holder_name', 'Card Holder Name', 'required');
        $this->form_validation->set_rules('exp_month', 'Expiry Month', 'required');
        $this->form_validation->set_rules('exp_year', 'Expiry Year', 'required');
        $this->form_validation->set_rules('card_cvv', 'Card CVV', 'required|numeric');
        if (isset($_POST['submit'])) {
            if ($this->input->post('submit') && $this->form_validation->run() == FALSE) :
                $this->load->view('tokenization');
            else :
                $obj = new pay_model;
                $getData = $obj->register_token($_POST, $id);
                pre($getData);
                die;
            endif;
        }
        else {
            $this->load->view('tokenization');
        }
    }

}
