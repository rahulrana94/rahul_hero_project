<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Provider_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

//customerImages
    function makeBase64Img($image) {

        $data = str_replace(" ", "+", $image);
        $data = base64_decode($data);
        $im = imagecreatefromstring($data);
        $fileName = rand(5, 115) . time() . ".png";
        $imageName = "profileImages/" . $fileName;


        if ($im !== false) {

            imagepng($im, $imageName);

            imagedestroy($im);
        } else {
            echo 'An error occurred.';
        }

        return $fileName;
    }

    function check_datetime($data) {
//$job = $this->db->select("CONCAT(date,' ',time) as datetime")
        $job = $this->db->select("TIMESTAMPDIFF( MINUTE ,  '" . date('Y-m-d H:i:s') . "' , CONCAT( date,  ' ', time ) ) AS difference")
                        ->where("id", $data['job_id'])
                        ->having("difference <= ", 120)
                        ->having("difference > ", 0)
                        ->get("customer_job")->num_rows();
        return $job > 0 ? true : false;
    }

    
    function check_date($data) { 
          //  pre($data);
        $job =  $this->db->select('*')
                     ->where("id", $data['job_id'])
                     ->where("date", date('Y-m-d'))
                     ->get("customer_job")->num_rows();
        //pre($job); die();
        return $job > 0 ? true : false;
    }
    
    function createProfile($document) {
        $sub_type = explode(",", $document['business_subtype']);
        unset($document['business_subtype']);
        $query = $this->db->insert('providers', $document);
        $id = $this->db->insert_id();
        if ($id > 0) {
            foreach ($sub_type as $sub) {
                $subofsub = explode(':', $sub);
                $data['business_subtype'] = $subofsub['0'];
                $data['business_subtype_name'] = $subofsub['1'];
                $data['prov_id'] = $id;
                $query = $this->db->insert('providers_subcat_relation', $data);
            }
        }
        $this->db->where('prov_id', $id);
        $details = $this->db->get('providers')->row_array();
        if ($details) {
            $this->db->where('prov_id', $id);
            $details_sub1 = $this->db->get('providers_subcat_relation')->result_array();
            foreach ($details_sub1 as $details_sub) {
                $result[] = $details_sub['business_subtype'] . ":" . $details_sub['business_subtype_name'];
            }
            $details['business_subtype'] = implode(',', $result);
            return $details;
        } else {
            return false;
        }
    }

    function isUserExist($document) {
        $this->db->where('email', $document['email']);
        $result = $this->db->get('providers')->row_array();
        if (!empty($result)) {
            return $result['prov_id'];
        } else {
            return false;
        }
    }

    function updateProvider($document) {
        $this->db->where('email', $document['email']);
//$this->db->where('password', $document['password']);               
        $query = $this->db->update('providers', $document);

        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

    function providerLogin($document) {
        $this->db->where('email', $document['email']);
        $this->db->where('password', $document['password']);
//$this->db->where('verified', $document['verified']);
        $result = $this->db->get('providers')->row_array();

        if ($result) {
            $this->db->where('prov_id', $result['prov_id']);
            $details_sub1 = $this->db->get('providers_subcat_relation')->result_array();
            foreach ($details_sub1 as $details_sub) {
                $result1[] = $details_sub['business_subtype'] . ":" . $details_sub['business_subtype_name'];
            }
            $result['business_subtype'] = implode(',', $result1);
            return $result;
        } else {
            return false;
        }
    }

    function passwordRecovery($document) {
        $this->db->where('email', $document['email']);
        $result = $this->db->get('providers')->row_array();
        $password = $result['password'];
        if (!empty($result)) {
            return $password;
        } else {
            return false;
        }
    }

    function get_provider_info($user_id = "") {
        $this->db->where('prov_id', $user_id);
        $result = $this->db->get('providers')->row_array();
        if ($result) {
            $this->db->where('prov_id', $result['prov_id']);
            $details_sub1 = $this->db->get('providers_subcat_relation')->result_array();
            foreach ($details_sub1 as $details_sub) {
                $result1[] = $details_sub['business_subtype'] . ":" . $details_sub['business_subtype_name'];
            }
            $result['business_subtype'] = implode(',', $result1);
            return $result;
        } else {
            return false;
        }
    }

    public function update_provider_profile($data, $primary_key) {
        $data = array_filter($data);
        $this->db->where('prov_id', $primary_key);
        $query = $this->db->update('providers', $data);
        return ($query) ? true : false;
    }

    public function check_provider_status($id) {
        $status = $this->db->select("prov_id")
                        ->where(array("prov_id" => $id, "status" => 1))
                        ->get("providers")->num_rows();
        return $status > 0 ? TRUE : FALSE;
    }

    function get_jobDetails_for_provider($data) {
        $this->db->where('customer_job.id', $data['job_id']);
        $result = $this->db->get('customer_job')->row_array();
        $result_modi = array();
        if (!empty($result)) {


            $this->db->select("name as main_category_name");
            $this->db->where("id", $result['main_cat_id']);
            $result['main_category_name'] = $this->db->get("main_category")->row()->main_category_name;

            $job_status = $this->db->select("status")
                            ->where("job_id", $data['job_id'])
                            ->get("job_status")->row()->status;
            $result['job_status'] = $job_status != NULL ? $job_status : "";
            $result['quotation'] = $this->db->select("quotation")->where(array("job_id" => $data['job_id'], "prov_id" => $data['prov_id']))->get("quotations")->row()->quotation;
            $result['quotation'] = ($result['quotation'] != NULL) ? $result['quotation'] : "";

            $result['callout_charge'] = $this->db->select("callout_charge")->where("id", 1)->get("setting")->row()->callout_charge;
            $result['vat'] = $this->db->select("vat")->where("id", 1)->get("setting")->row()->vat;

            $this->db->select("name");
            $this->db->where("sb_id", $result['sub_cat_id']);
            $result['name'] = $this->db->get("sub_category")->row_array()['name'];

            $result1 = $this->get_sub_job_data($result['main_cat_id'], $result['id']);
            if (!empty($result1)) {

                $result1['img_url'] = base_url('transport/');
                $result_modi = array_merge($result, $result1);
                $result_modi['date'] = str_replace("-", "/", date("d-M-Y", strtotime($result_modi['date'])));
                $result_modi['time'] = date("h:i a", strtotime($result_modi['time']));
            }
        }

        return (!empty($result_modi)) ? $result_modi : array();
    }

    function get_sub_job_data($cat_id, $job_id) {
        switch ($cat_id) {
            case 1:
                $table = 'customer_job_household_relation';
                break;
            case 2:
                $table = 'customer_job_outdoor_relation';
                break;
            case 3:
                $table = 'customer_job_personal_relation';
                break;
            case 4:
                $table = 'customer_job_transport_relation';
                break;
            default:
                break;
        }

        if (isset($table)) {
            $result = $this->db->where('job_id', $job_id)
                            ->get($table)->row_array();
            unset($result['id']);
            unset($result['job_id']);
        }
        return $result;
    }

################# Get Job Quaotaion for An Provider

    function get_job_quotation($id, $user_id) {
        $quote_result = $this->db->select("quotation")
                        ->where('job_id', $id)
                        ->where('prov_id', $user_id)
                        ->get('quotations')->row()->quotation;
        return $quote_result != null ? $quote_result : "";
    }

################# Get Job Quaotaion for An Provider
################# Get Subcategory Name By ID

    function get_subcat_name($id) {
        $result = $this->db->select('name')
                        ->where("sb_id", $id)
                        ->get('sub_category')->row()->name;
        return $result;
    }



    function date_based_available_jobs_for_provider31_May_2017($user_id, $date1) {
	//echo $user_id; die();
        $date = date("Y-m-d", strtotime($date1));
        $prov_data = $this->db->select("business_address_lat,business_address_long,callout_radius,business_subtype")
                        ->where("p1.prov_id", $user_id)
                        ->join("providers_subcat_relation as p2", "p1.prov_id=p2.prov_id")
                        ->get("providers as p1")->row_array();
        $sub_prov = $this->db->select("business_subtype")
                        ->where("p1.prov_id", $user_id)
                        ->get("providers_subcat_relation as p1")->result_array();
        $sub_id = array();
        foreach ($sub_prov as $x):
            $sub_id[] = $x['business_subtype'];
        endforeach;
        $prov_data['sub_cat'] = implode(",", array_values($sub_id));
        
         //  $this->db->where('customer_job.status', 0);
        $immdiate = $this->db->get('customer_job')->result_array();
	//pre($immdiate); die();
        foreach ($immdiate as $immdiateCallout) {
       // pre($immdiate);die;
            if($immdiateCallout['imidiate_callout'] == 0) {
                
               /* 
                   $result = $this->db->query("select t1.*,TIMESTAMPDIFF( MINUTE ,  '" . date('Y-m-d H:i:s') . "' , CONCAT( date,  ' ', time ) ) AS difference, m1.name as main_category_name, SQRT(POW(69.1 * (location_lat - " . $prov_data['business_address_lat'] . "), 2) + POW(69.1 * (" . $prov_data['business_address_long'] . " - location_long) * COS(location_lat / 57.3), 2)) AS distance from
                        (SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_household_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_outdoor_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_personal_relation c2 on c1.id=c2.job_id "
                        . "UNION SELECT c1.*,c2.pickup_location_lat as location_lat,c2.pickup_location_long as location_long FROM `customer_job` c1 inner join customer_job_transport_relation c2 on c1.id=c2.job_id)t1"
                        . " inner join main_category as m1 on m1.id = t1.main_cat_id"
                        . " where t1.date='" . $date . "' and t1.time>='" . date('H:i:s') . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']} and `difference` >= 240"
                        )->result_array();
                */
                
               $result = $this->db->query("select t1.*, m1.name as main_category_name, SQRT(POW(69.1 * (location_lat - " . $prov_data['business_address_lat'] . "), 2) + POW(69.1 * (" . $prov_data['business_address_long'] . " - location_long) * COS(location_lat / 57.3), 2)) AS distance from
                (SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_household_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_outdoor_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_personal_relation c2 on c1.id=c2.job_id "
                        . "UNION SELECT c1.*,c2.pickup_location_lat as location_lat,c2.pickup_location_long as location_long FROM `customer_job` c1 inner join customer_job_transport_relation c2 on c1.id=c2.job_id)t1"
                        . " inner join main_category as m1 on m1.id = t1.main_cat_id"
                      //  . " where t1.date='" . $date . "' and t1.time>='" . date('H:i:s') . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']} "
                        . " where t1.date='" . $date . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']} "
                )->result_array();     
                             
               
                        
                        
            }
            
            else{
                  $result = $this->db->query("select t1.*, m1.name as main_category_name, SQRT(POW(69.1 * (location_lat - " . $prov_data['business_address_lat'] . "), 2) + POW(69.1 * (" . $prov_data['business_address_long'] . " - location_long) * COS(location_lat / 57.3), 2)) AS distance from
                (SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_household_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_outdoor_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_personal_relation c2 on c1.id=c2.job_id "
                        . "UNION SELECT c1.*,c2.pickup_location_lat as location_lat,c2.pickup_location_long as location_long FROM `customer_job` c1 inner join customer_job_transport_relation c2 on c1.id=c2.job_id)t1"
                        . " inner join main_category as m1 on m1.id = t1.main_cat_id"
                      //  . " where t1.date='" . $date . "' and t1.time>='" . date('H:i:s') . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']} "
                        . " where t1.date='" . $date . "'  and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']} "
                )->result_array();    
                        
         
                        
            }
            
        } 
 //pre($result);                    
//echo $this->db->last_query();die;
//pre($result);die;
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['quotation'] = $this->get_job_quotation($x['id'], $user_id);
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }

        return (!empty($result_modi)) ? $result_modi : array();
    }

 
    
    
#######################available jobs#####################################    
#######################available jobs##################################### 
#######################available jobs#####################################
      
  //          31 -May - 2017               // Rahul Rana
    
    
  function date_based_available_jobs_for_provider($user_id, $date1) {
	//echo $user_id; die();
        $date = date("Y-m-d", strtotime($date1));
        $prov_data = $this->db->select("business_address_lat,business_address_long,callout_radius,business_subtype")
                        ->where("p1.prov_id", $user_id)
                        ->join("providers_subcat_relation as p2", "p1.prov_id=p2.prov_id")
                        ->get("providers as p1")->row_array();
        $sub_prov = $this->db->select("business_subtype")
                        ->where("p1.prov_id", $user_id)
                        ->get("providers_subcat_relation as p1")->result_array();
        $sub_id = array();
        foreach ($sub_prov as $x):
            $sub_id[] = $x['business_subtype'];
        endforeach;
        $prov_data['sub_cat'] = implode(",", array_values($sub_id));
               $result = $this->db->query("select t1.*, m1.name as main_category_name, SQRT(POW(69.1 * (location_lat - " . $prov_data['business_address_lat'] . "), 2) + POW(69.1 * (" . $prov_data['business_address_long'] . " - location_long) * COS(location_lat / 57.3), 2)) AS distance from
                (SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_household_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_outdoor_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_personal_relation c2 on c1.id=c2.job_id "
                        . "UNION SELECT c1.*,c2.pickup_location_lat as location_lat,c2.pickup_location_long as location_long FROM `customer_job` c1 inner join customer_job_transport_relation c2 on c1.id=c2.job_id)t1"
                        . " inner join main_category as m1 on m1.id = t1.main_cat_id"
                      //  . " where t1.date='" . $date . "' and t1.time>='" . date('H:i:s') . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']} "
                        . " where t1.date='" . $date . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']} "
                )->result_array();     
                        
                        
            if($result){
            foreach($result as $res){             
                if($res['date']==date('Y-m-d')){
                    if($res['time']>date('H:i:s')){
                        $final[] = $res;
                    }
                }else{
                    $final[] = $res;
                }
            }
            if(!empty($final)){
            $result = $final;  
            }else{
            $result = array();
            }
        } 
        
        
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['quotation'] = $this->get_job_quotation($x['id'], $user_id);
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }

        return (!empty($result_modi)) ? $result_modi : array();
    }                 
#######################accepted jobs#####################################

    function date_based_accepted_jobs_for_provider($user_id, $date1, $staus) {
        $date = date("Y-m-d", strtotime($date1));
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name");
        $this->db->where('customer_job.date', $date);
        $this->db->where('customer_job.status', 3);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->where("customer_job.id in (select job_id from job_request where prov_id='" . $user_id . "' and status='1' union select job_id from quotations where prov_id='" . $user_id . "' and status ='1')");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $result = $this->db->get('customer_job')->result_array();
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);

                $job_status = $this->db->select("status")
                                ->where("job_id", $x['id'])
                                ->get("job_status")->row()->status;
                $x['job_status'] = $job_status != NULL ? $job_status : "";

                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }

#######################accepted jobs#####################################

    
    
    
####################### Future accepted jobs  #####################################

  //  1-June-2017    Rahul Rana
    
    
   function date_based_accepted_jobs_for_provider_future($user_id) {
       
       // $date = date("Y-m-d", strtotime($date1));
        $date = date("Y-m-d");
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name");
        $this->db->where('customer_job.date>', $date);
        $this->db->where('customer_job.status', 3);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->where("customer_job.id in (select job_id from job_request where prov_id='" . $user_id . "' and status='1' union select job_id from quotations where prov_id='" . $user_id . "' and status ='1')");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        
        $result = $this->db->get('customer_job')->result_array();
       //   echo $this->db->last_query($result);die('rahul');
         // pre($result);die('checkup');
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);

                $job_status = $this->db->select("status")
                                ->where("job_id", $x['id'])
                                ->get("job_status")->row()->status;
                $x['job_status'] = $job_status != NULL ? $job_status : "";

                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
               // pre($result_modi);die('rahul');
            }
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }
 


#######################started_jobs#####################################


  
   function date_based_started_jobs_for_provider($user_id, $date1, $staus) {
        $date = date("Y-m-d", strtotime($date1));
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name");
        $this->db->where('customer_job.date<', $date);
        $this->db->where('customer_job.status', 3);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->where("customer_job.id in (select job_id from job_request where prov_id='" . $user_id . "' and status='1' union select job_id from quotations where prov_id='" . $user_id . "' and status ='1')");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        
        $result = $this->db->get('customer_job')->result_array();
          //echo $this->db->last_query($result);
         // pre($result);die('checkup');
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);

                $job_status = $this->db->select("status")
                                ->where("job_id", $x['id'])
                                ->get("job_status")->row()->status;
                $x['job_status'] = $job_status != NULL ? $job_status : "";

                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
               // pre($result_modi);die('rahul');
            }
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }
 

#######################requested jobs#####################################   
    
    function date_based_requested_jobs_for_provider($user_id, $date1) {
        $date = date("Y-m-d", strtotime($date1));
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.main_cat_id,customer_job.status,main_category.name as main_category_name");
        $this->db->select("customers.name as cust_name,IF(customers.profile_img='','',CONCAT('" . base_url('customerImages') . "','/',profile_img)) as profile_img");
        $this->db->where('customer_job.date >=', date("Y-m-d"));
        $this->db->where('customer_job.status', 0);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->where("customer_job.id in (select job_id from job_request where prov_id='" . $user_id . "' and status='0') ");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job')->result_array(); //echo $this->db->last_query();die;
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
//if (in_array($x['main_cat_id'], array(3, 4))):
                $x['quotation'] = $this->db->select("quotation")
                                ->where("job_id", $x['id'])
                                ->get("quotations")->row()->quotation;
                $x['quotation'] = $x['quotation'] != NULL ? $x['quotation'] : "";
//endif;

                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }

        return (!empty($result_modi)) ? $result_modi : array();
    }

#######################requested jobs#####################################
#######################Completed jobs#####################################

    function date_based_completed_jobs_for_provider($user_id, $date1) {
        $date = date("Y-m-d", strtotime($date1));
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.main_cat_id,customer_job.status,main_category.name as main_category_name");
        $this->db->select("customers.name as cust_name,IF(customers.profile_img='','',CONCAT('" . base_url('customerImages') . "','/',profile_img)) as profile_img");
//$this->db->where('customer_job.date', $date);
        $this->db->where('customer_job.status', 5);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->where("customer_job.id in (select job_id from job_request where prov_id='" . $user_id . "' and status='1' union select job_id from quotations where prov_id='" . $user_id . "' and status ='1')");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job')->result_array();
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }

    
    
    
    
    
#######################Completed jobs#####################################

    
    
/////////////////////////////////////////   
    
    
####################### All Jobs According to Date

    function date_based_jobs_for_provider($user_id, $date1) {
        $result['available']      = $this->date_based_available_jobs_for_provider($user_id, $date1);
        $result['accepted']       = $this->date_based_accepted_jobs_for_provider($user_id, $date1);
        $result['acceptedFuture'] = $this->date_based_accepted_jobs_for_provider_future($user_id, $date1);
        $result['started']        = $this->date_based_started_jobs_for_provider($user_id, $date1);
        $result['requested']      = $this->date_based_requested_jobs_for_provider($user_id, $date1);
        $result['completed']      = $this->date_based_completed_jobs_for_provider($user_id, $date1);

        return (!empty($result)) ? $result : array();
    }

      function user_based_jobs_for_provider($user_id) {
    //  $result['available']       = $this->user_based_available_jobs_for_provider($user_id);
        $result['accepted']        = $this->user_based_accepted_jobs_for_provider($user_id);
        $result['acceptedFuture']  = $this->date_based_accepted_jobs_for_provider_future($user_id);
        $result['started']         = $this->user_based_started_jobs_for_provider($user_id);

      //  $result['requested'] = $this->user_based_requested_jobs_for_provider($user_id);
      //  $result['completed'] = $this->user_based_completed_jobs_for_provider($user_id);

        return (!empty($result)) ? $result : array();
    }  
    
####################### All Jobs According to Date

    function get_accepted_jobs_for_provider($user_id = "") {

        $this->db->select("customer_job.*,main_category.name as main_category_name");
        $this->db->where('customer_job.sub_cat_id in (select business_subtype from providers_subcat_relation where prov_id=' . $user_id . ' )');
        $this->db->where('customer_job.status', 3);
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $result = $this->db->get('customer_job')->result_array();


        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {

                $quote_result = $this->db->select("quotation")
                                ->where('job_id', $x['id'])
                                ->where('prov_id', $user_id)
                                ->get('quotations')->row_array();
                $x['quotation'] = $quote_result['quotation'] != null ? $quote_result['quotation'] : '';

                $sub_name = $this->db->select('name')
                                ->where("sb_id", $x['sub_cat_id'])
                                ->get('sub_category')->row()->name;
                $x['name'] = $sub_name != NULL ? $sub_name : "";

                $x['callout_charge'] = $this->db->select("callout_charge")->where("id", 1)->get("setting")->row()->callout_charge;
                $x['vat'] = $this->db->select("vat")->where("id", 1)->get("setting")->row()->vat;

                $result_request_check = $this->db->select("id")
                                ->where('job_id', $x['id'])
                                ->where('prov_id', $user_id)
                                ->get('job_request')->num_rows();
                if ($result_request_check > 0) {
                    $x['status'] = 4;
                }

                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }

        return (!empty($result_modi)) ? $result_modi : array();
    }

    function get_completed_jobs_for_provider($user_id = "") {

        $this->db->select("customer_job.*,main_category.name as main_category_name");
        $this->db->where('customer_job.sub_cat_id in (select business_subtype from providers_subcat_relation where prov_id=' . $user_id . ' )');
        $this->db->where('customer_job.status', 5);
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $result = $this->db->get('customer_job')->result_array();


        $result_modi = array();
        if (!empty($result)) {

            foreach ($result as $x) {

                $quote_result = $this->db->select("quotation")
                                ->where('job_id', $x['id'])
                                ->where('prov_id', $user_id)
                                ->get('quotations')->row_array();
                $x['quotation'] = $quote_result['quotation'] != null ? $quote_result['quotation'] : '';

                $sub_name = $this->db->select('name')
                                ->where("sb_id", $x['sub_cat_id'])
                                ->get('sub_category')->row_array();
                $x['name'] = $sub_name['name'];


                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }

        return (!empty($result_modi)) ? $result_modi : array();
    }

    function accepted_job_details($job_id) {

        $this->db->select("customer_job.*,customers.name as customer_name,customers.profile_img as profileImage,concat(customers.countryCode,customers.mobile) as mobile");
        $this->db->where('customer_job.id', $job_id);
        $this->db->where('customer_job.status', 3);
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job')->row_array();
// $result_modi = array();
        if (!empty($result)) {
            $result['profileImage'] = ($result['profileImage'] != "") ? base_url('customerImages') . "/" . $result['profileImage'] : "";

//foreach ($result as $x) {

            $sub_name = $this->db->select('name')
                            ->where("sb_id", $result['sub_cat_id'])
                            ->get('sub_category')->row_array();
            $result['name'] = $sub_name['name'];

            $quote_result = $this->db->select("quotation")
                            ->where('job_id', $job_id)
                            ->where('status', 1)
                            ->get('quotations')->row_array();
            $result['quotation'] = $quote_result['quotation'] != null ? $quote_result['quotation'] : '';

            $job_status = $this->db->select("status")
                            ->where("job_id", $job_id)
                            ->get("job_status")->row()->status;
            $result['job_status'] = $job_status != NULL ? $job_status : "";

            $sub_name = $this->db->select('name')
                            ->where("id", $result['main_cat_id'])
                            ->get('main_category')->row_array();
            $result['main_category_name'] = $sub_name['name'];

            if (in_array($result['main_cat_id'], array(1, 2))) {
                $result['applied_vat'] = $this->db->select("vat")->where('job_id', $job_id)->get('final_quotation')->row()->vat;
            
                 if(empty($result['applied_vat'])){
                $result['applied_vat'] = "" ;
            }
      
            } else {
                $result['applied_vat'] = $this->db->select("vat")->where('job_id', $job_id)->get('customer_payment_details')->row()->vat;

                if(empty($result['applied_vat'])){
                $result['applied_vat'] = "" ;
            } 
                
            }

            $result['avg_rating'] = $this->db->select_avg('rating', 'rating')
                            ->where('cust_id', $result['user_id'])
                            ->where("job_id in (select id from customer_job where sub_cat_id=" . $result['sub_cat_id'] . ")")
                            ->get('customer_rating')->row()->rating;
            $result['avg_rating'] = $result['avg_rating'] != NULL ? intval($result['avg_rating']) : 0;

            $result['review_count'] = $this->db->select("id")
                            ->where('cust_id', $result['user_id'])
                            ->where("job_id in (select id from customer_job where sub_cat_id=" . $result['sub_cat_id'] . ")")
                            ->get('customer_rating')->num_rows();

            $setting = $this->db->select("vat,callout_charge")->where("id", 1)->get("setting")->row_array();
            if (empty($setting)) {
                $final_quote = array("vat" => "", "callout_charge" => "");
            }
            $result = array_merge($result, $setting);
           
            $result['additional_desc'] = $this->db->select("additional_desc")->where('job_id', $job_id)->get('final_quotation')->row()->additional_desc;
             if(empty($result['additional_desc'])){
                $result['additional_desc'] = "" ;
            }
            
            $result['final_quotation'] = $this->db->select("total")->where('job_id', $job_id)->get('final_quotation')->row()->total;
            if(empty($result['final_quotation'])){
                $result['final_quotation'] = "" ;
            }
            
            $invoice_data = $this->db->select("addi_job_desc,addi_charges,total_amount")
                            ->where("job_id", $job_id)
                            ->get("invoice")->row_array();
            if (empty($invoice_data)) {
                $invoice_data = array("addi_job_desc" => "", "addi_charges" => "", "total_amount" => "");
            }
            $result = array_merge($result, $invoice_data);

            $reschedule = $this->db->where(array("job_id" => $job_id, "usertype" => 1))->get("reschedule_job_relation")->row_array();
            if (isset($reschedule) && !empty($reschedule)):
                $reschedule['date'] = str_replace("-", "/", date("d-M-Y", strtotime($reschedule['date'])));
                $reschedule['time'] = date("h:i a", strtotime($reschedule['time']));
            endif;
            $result['reschedule'] = $reschedule != NULL ? $reschedule : "";
//pre($result);die;

            $result1 = $this->get_sub_job_data($result['main_cat_id'], $result['id']);
            if (!empty($result1)) {
                $result1['img_url'] = base_url('transport') . "/";
                $result['date'] = str_replace("-", "/", date("d-M-Y", strtotime($result['date'])));
                $result['time'] = date("h:i a", strtotime($result['time']));
                $result_modi = array_merge($result, $result1);
            }
// }
        } else {
            $this->db->select("status");
            $this->db->where('id', $job_id);
            $status = $this->db->get('customer_job')->row_array();
        }

        return (!empty($result_modi)) ? $result_modi : $status;
    }

    function get_quotation($id = "") {
        $this->db->where('id', $id);
        $result = $this->db->get('quotations')->row_array();
        return (!empty($result)) ? $result : array();
    }

    function cancel_job($document) {
        $doc = array();
        $doc['id'] = $document['id'];
        $doc['status'] = 3;
        $this->db->where('id', $document['job_id']);
        $query1 = $this->db->update('customer_job', $doc);
        $query2 = $this->db->insert('cancel_job_relation', $document);

        return ($query1 && $query2) ? true : false;
    }

    function send_quotations($document) {
        $document['create_date'] = date("Y-m-d H:i:s");
        $query = $this->db->insert('quotations', $document);
        $id = $this->db->insert_id();
        if ($id > 0):
            $immi_job = $this->db->select("imidiate_callout")
                            ->where("id", $document['job_id'])
                            ->get("customer_job")->row();
     //       if ($immi_job->imidiate_callout != 1) {              // 24 - March -17
                $main_cat = $this->db->select("main_cat_id")
                                ->where("id", $document['job_id'])
                                ->get("customer_job")->row()->main_cat_id;

                $sub_cat_id = $this->db->select("sub_cat_id")
                                ->where("id", $document['job_id'])
                                ->get("customer_job")->row()->sub_cat_id;
                
                $provider_name = $this->db->select("CONCAT(firstName,' ',lastName) as name")
                                ->where("prov_id", $document['prov_id'])
                                ->get("providers")->row()->name;
                $customer = $this->db->select("name,device_type,device_token")
                                ->where("cust_id in (select user_id from customer_job where id={$document['job_id']})")
                                ->get("customers")->row_array();
                                
                
                 $sub_cat = $this->db->select("name")->where("sb_id",$sub_cat_id)->get("sub_category")->row()->name;
                 if(!empty($sub_cat)){
                 $subCategory = $sub_cat ;                
                 }
                 else{
                  $subCategory = "N/A" ;
                 }
                //  pre($sub_cat);die;
                $message1 = "Hi {$customer['name']} , " . ucfirst($provider_name) . " has sent you a quotation for your job.";
                $message['job_id'] = $document['job_id'];
                $message['prov_id'] = $document['prov_id'];
                $message['main_cat_id'] = $main_cat;
                $message['sub_cat'] = $subCategory;
                $message['notification_type'] = 7;
                $message['message'] = $message1;
                $key = 2;

                generatePush(strtolower($customer['device_type']), $customer['device_token'], $message ,$key);
      //      }           // 24 -march -17
            return $id;
        else:
            return false;
        endif;
    }

    function rate_the_customer($document) {
        $query = $this->db->insert('customer_rating', $document);
        $id = $this->db->insert_id();
        $this->db->where('id', $id);
        $details = $this->db->get('customer_rating')->row_array();
        return !empty($details) ? $details : false;
    }

    function start_job($data) {
        $da['job_id']  = $data['job_id'];
        $da['cust_id'] = $data['cust_id'];
        $da['prov_id'] = $data['prov_id'];
        $da['status']  = 0;
       
        $query = $this->db->insert("job_status", $da);
        $prov_data = array();
        $CI = & get_instance();
        $CI->load->model('customers_model');
        $immid_job = $this->db->select("id")->where("imidiate_callout", 1)->get("customer_job")->result_array();
	$job_id = end($immid_job);	
	//pre($immid_job); //die();
	//pre( end($immid_job)); die();
       // foreach ($immid_job as $id):

            $val1 = $CI->customers_model->get_immidiate_callout_provider_by_jobid_available($job_id['id']);
            $avail = $val1 != NULL ? $val1 : 0;
            $val2 = $CI->customers_model->get_immidiate_callout_provider_by_jobid_busy($job_id['id']);
            $busy = $val2 != NULL ? $val2 : 0;
            $prov_data["jobData_" . $id['id']] = array("busy" => "$busy", "calling" => "$avail");

        //endforeach;
##############################curl firebase##################
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bazinga-fe8be.firebaseio.com/push_thread.json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => json_encode($prov_data),
        ));

        $response = curl_exec($curl);
	//pre($response); die();
        curl_close($curl);

##############################curl firebase##################

        $this->db->select("customer_job.id,customer_job.main_cat_id,customer_job.sub_cat_id,customers.cust_id,customers.name as customer_name,customers.device_type,customers.device_token");
        $this->db->where('customer_job.id', $data['job_id']);
        $this->db->where('customer_job.status', 3);
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job')->row_array();
      //  pre($result);die;
        if (!empty($result)):
            $main_cat = $this->db->select("name")->where("id", $result['main_cat_id'])->get("main_category")->row()->name;
            $sub_cat = $this->db->select("name")->where("sb_id", $result['sub_cat_id'])->get("sub_category")->row()->name;
            $message1 = "Hi {$result['customer_name']} , Your job has been started.";
            $message['job_id'] = $result['id'];
            $message['main_cat_id'] = $result['main_cat_id'];
            $message['sub_cat_name'] = $sub_cat;
            $message['main_cat_name'] = $main_cat;
            $message['cust_id'] = $result['cust_id'];
            $message['notification_type'] = 1;
            $message['message'] = $message1;
            $key = 2 ;

            generatePush(strtolower($result['device_type']), $result['device_token'], $message, $key);
        endif;
        return !empty($result) ? true : false;
    }

    function update_job_status($data) {
        $query = $this->db->set("status", $data['status'])
                ->where("job_id", $data['job_id'])
                ->update("job_status");

        $this->db->select("customer_job.id,customer_job.main_cat_id,customer_job.sub_cat_id");
        $this->db->where('customer_job.id', $data['job_id']);
        $result = $this->db->get('customer_job')->row_array();

        $main_cat = $this->db->select("name")->where("id", $result['main_cat_id'])->get("main_category")->row()->name;
        $sub_cat = $this->db->select("name")->where("sb_id", $result['sub_cat_id'])->get("sub_category")->row()->name;
        if ($data['status'] == 1):
            $message['notification_type'] = 21;
            $message1 = "Pick up confirm by the provider.";
        elseif ($data['status'] == 2):
            $message['notification_type'] = 22;
            $message1 = "Provider has reached your job location";
        elseif ($data['status'] == 3):
            $message['notification_type'] = 23;
            $message1 = "Please pay to provider callout charges.";
        elseif ($data['status'] == 4):
//            $message['notification_type'] = 24;
//            $message1 = "Customer has paid callout charges.";
        elseif ($data['status'] == 5):
            $message['notification_type'] = 25;
            $message['callout_charges'] = $this->db->select("callout_charge")->where("id", 1)->get("setting")->row()->callout_charge;
            $message1 = "Please accept final quotation for job.";
        elseif ($data['status'] == 6):
//            $message['notification_type'] = 26;
//            $message1 = "Final quote has beeen accepted by customer.";
        elseif ($data['status'] == 7):
            $message['notification_type'] = 27;
            $message['final_quote'] = $this->db->select("total")->where('job_id', $data['job_id'])->get('final_quotation')->row()->total;
            $message1 = "Job completed by provider, Please wait for final invoice.";
//        elseif ($data['status'] == 8):
//            $message['notification_type'] = 28;
//            $message1 = "Invoice sent by the provider";
        elseif ($data['status'] == 9):
            $message['notification_type'] = 29;
            $this->db->set("status", 5)
                    ->where("id", $data['job_id'])
                    ->update("customer_job");
            $message1 = "Final payment paid by the Customer";
        endif;

####################################
        $this->db->select("customers.cust_id,customers.name as customer_name,customers.device_type,customers.device_token");
        $this->db->where('job_status.job_id', $data['job_id']);
        $this->db->join('job_status', 'customers.cust_id = job_status.cust_id');
        $result1 = $this->db->get('customers')->row_array();
        if (!empty($result1)):

            $message['job_id'] = $data['job_id'];
            $message['main_cat_id'] = $result['main_cat_id'];
            $message['sub_cat_name'] = $sub_cat;
            $message['main_cat_name'] = $main_cat;
            $message['cust_id'] = $result1['cust_id'];
            $message['message'] = $message1;
            $key = 2 ;
            
            generatePush(strtolower($result1['device_type']), $result1['device_token'], $message, $key);
        endif;
####################################


        if (!empty($query)):
        //  $result2 = $this->db->where("job_id", $data['job_id'])->get("job_status")->row_array();
            $this->db->select('job_status.*,main_category.id as CategoryId,main_category.name as Category');
            $this->db->where("job_status.job_id", $data['job_id']);
            $this->db->where("job_status.status", $data['status']);
            $this->db->join('customer_job','customer_job.id = job_status.job_id');
            $this->db->join('main_category','main_category.id = customer_job.main_cat_id');     
            $result = $this->db->get('job_status')->row_array();
            
           // pre($result);die;
            
            return $result;
        else:
            return false;
        endif;
    }

    
       function job_invoice($data) {
        //pre($data);die;
        $query = $this->db->insert("invoice", $data);
        $id = $this->db->insert_id();
        if ($id > 0):

            $prov_data = array();
            $CI = & get_instance();
            $CI->load->model('customers_model');
            $immid_job = $this->db->select("id")->where("imidiate_callout", 1)->get("customer_job")->result_array();
            foreach ($immid_job as $cid):
                $job_id = end($immid_job);

                $val1 = $CI->customers_model->get_immidiate_callout_provider_by_jobid_available($job_id['id']);
                $avail = $val1 != NULL ? $val1 : 0;
                $val2 = $CI->customers_model->get_immidiate_callout_provider_by_jobid_busy($job_id['id']);
                $busy = $val2 != NULL ? $val2 : 0;
                $prov_data["jobData_" . $job_id['id']] = array("busy" => "$busy", "calling" => "$avail");

            endforeach;
##############################curl firebase##################
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://bazinga-fe8be.firebaseio.com/push_thread.json",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => json_encode($prov_data),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

##############################curl firebase##################
            $query2 = $this->db->set("status", 8)
                    ->where("job_id", $data['job_id'])
                    ->update("job_status");
            $result = $this->db->where("id", $id)
                            ->get("invoice")->row_array();

            $this->db->select("customer_job.id,customer_job.main_cat_id,customer_job.sub_cat_id");
            $this->db->where('customer_job.id', $data['job_id']);
            $result2 = $this->db->get('customer_job')->row_array();
            $main_cat = $this->db->select("name")->where("id", $result2['main_cat_id'])->get("main_category")->row()->name;
            $sub_cat = $this->db->select("name")->where("sb_id", $result2['sub_cat_id'])->get("sub_category")->row()->name;
            $this->db->select("customers.cust_id,customers.name as customer_name,customers.email as email,customers.device_type,customers.device_token");
            $this->db->where('job_status.job_id', $data['job_id']);
            $this->db->join('job_status', 'customers.cust_id = job_status.cust_id');
            $result1 = $this->db->get('customers')->row_array();
            //echo $this->db->last_query();
            //pre($result1);
            //($data);
            //($result2);die('hey');
            
              
            if (!empty($result1)):
                $message['notification_type'] = 28;
                $message1 = "Invoice sent by the provider";
                $message['job_id'] = $data['job_id'];
                $message['main_cat_id'] = $result2['main_cat_id'];
                $message['sub_cat_name'] = $sub_cat;
                $message['main_cat_name'] = $main_cat;
                $message['cust_id'] = $result1['cust_id'];
                $message['message'] = $message1;
                $key = 2;
            
                generatePush(strtolower($result1['device_type']), $result1['device_token'], $message, $key);
            
                if (!empty($result1['email'])) { 
        $to = $result1['email'];                   
        $msg  =  "Your Invoice has been genrated,Invoice details are given below.".PHP_EOL;
       // $msg .=  PHP_EOL;
        $msg .=  "Time is :".$data['time'].PHP_EOL;
        $msg .= "Date is :".$data['date'].PHP_EOL;
        $msg .= "Budget is :".$data['budget'].PHP_EOL;
        $msg .= "Description is :".$data['job_desc'].PHP_EOL;
        $msg .= "Additional charges is :".$data['addi_charges'].PHP_EOL;
        $msg .= "Total Amount is :".$data['total_amount'].PHP_EOL;
        $msg .=  PHP_EOL;
        $msg .= "Regards".PHP_EOL;
        $msg .= "Bazinga Services";
       
        $sub = "Invoice from Bazinga Services";
     // $headers = 'From: Bazinga <support@BazingaServices.com>' . "\r\n";
        $headers = 'From: Bazinga <admin@bazingaservice.com>' . "\r\n";
        @$sentmail = mail($to, $sub, $msg, $headers);
        if (!empty($sentmail)) {
        //$data = array();
        //$isSuccess = true;
        $message = "Invoice was sent successfully to this Email Id.";
       // $data = $updateOTP;
        } 
        else {
        $isSuccess = false;
        $message = "sorry, Invoice could not sent.";
        }
        }  
             return ($query2) ? $result : false;    
                endif;
           
        else:
            return false;
        endif;
            
        
        
        
          
    }
    
    
    
    function job_invoice24MARCH17($data) {
        //pre($data);die;
        $query = $this->db->insert("invoice", $data);
        $id = $this->db->insert_id();
        if ($id > 0):

            $prov_data = array();
            $CI = & get_instance();
            $CI->load->model('customers_model');
            $immid_job = $this->db->select("id")->where("imidiate_callout", 1)->get("customer_job")->result_array();
            foreach ($immid_job as $cid):
                $job_id = end($immid_job);

                $val1 = $CI->customers_model->get_immidiate_callout_provider_by_jobid_available($job_id['id']);
                $avail = $val1 != NULL ? $val1 : 0;
                $val2 = $CI->customers_model->get_immidiate_callout_provider_by_jobid_busy($job_id['id']);
                $busy = $val2 != NULL ? $val2 : 0;
                $prov_data["jobData_" . $job_id['id']] = array("busy" => "$busy", "calling" => "$avail");

            endforeach;
##############################curl firebase##################
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://bazinga-fe8be.firebaseio.com/push_thread.json",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => json_encode($prov_data),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

##############################curl firebase##################
            $query2 = $this->db->set("status", 8)
                    ->where("job_id", $data['job_id'])
                    ->update("job_status");
            $result = $this->db->where("id", $id)
                            ->get("invoice")->row_array();

            $this->db->select("customer_job.id,customer_job.main_cat_id,customer_job.sub_cat_id");
            $this->db->where('customer_job.id', $data['job_id']);
            $result2 = $this->db->get('customer_job')->row_array();
            $main_cat = $this->db->select("name")->where("id", $result2['main_cat_id'])->get("main_category")->row()->name;
            $sub_cat = $this->db->select("name")->where("sb_id", $result2['sub_cat_id'])->get("sub_category")->row()->name;
            $this->db->select("customers.cust_id,customers.name as customer_name,customers.email as email,customers.device_type,customers.device_token");
            $this->db->where('job_status.job_id', $data['job_id']);
            $this->db->join('job_status', 'customers.cust_id = job_status.cust_id');
            $result1 = $this->db->get('customers')->row_array();
            //echo $this->db->last_query();
            //pre($result1);
            //($data);
            //($result2);die('hey');
            
        if (!empty($result1['email'])) { 
        $to = $result1['email'];                   
        $msg  =  "Your Invoice has been genrated,Invoice details are given below.".PHP_EOL;
       // $msg .=  PHP_EOL;
        $msg .=  "Time is :".$data['time'].PHP_EOL;
        $msg .= "Date is :".$data['date'].PHP_EOL;
        $msg .= "Budget is :".$data['budget'].PHP_EOL;
        $msg .= "Description is :".$data['job_desc'].PHP_EOL;
        $msg .= "Additional charges is :".$data['addi_charges'].PHP_EOL;
        $msg .= "Total Amount is :".$data['total_amount'].PHP_EOL;
        $msg .=  PHP_EOL;
        $msg .= "Regards".PHP_EOL;
        $msg .= "Bazinga Services";
       
        $sub = "Invoice from Bazinga Services";
        $headers = 'From: Bazinga <support@BazingaServices.com>' . "\r\n";
        @$sentmail = mail($to, $sub, $msg, $headers);
        if (!empty($sentmail)) {
        //$data = array();
        //$isSuccess = true;
        $message = "Invoice was sent successfully to this Email Id.";
       // $data = $updateOTP;
        } 
        else {
        $isSuccess = false;
        $message = "sorry, Invoice could not sent.";
        }
        }   
            
            if (!empty($result1)):
                $message['notification_type'] = 28;
                $message1 = "Invoice sent by the provider";
                $message['job_id'] = $data['job_id'];
                $message['main_cat_id'] = $result2['main_cat_id'];
                $message['sub_cat_name'] = $sub_cat;
                $message['main_cat_name'] = $main_cat;
                $message['cust_id'] = $result1['cust_id'];
                $message['message'] = $message1;
                $key = 2;
            
                generatePush(strtolower($result1['device_type']), $result1['device_token'], $message, $key);
            endif;
            return ($query2) ? $result : false;
        else:
            return false;
        endif;
    }

    function check_job_requested($doc) {
        $check = $this->db->select('id')->where(array('status' => 3, 'id' => $doc['job_id']))->get('customer_job')->num_rows();
        return $check > 0 ? false : true;
    }

    function action_job_request($document) {
        $flag = 0;
        $message = array();
        $message['job_id'] = $document['job_id'];
        $message['prov_id'] = $document['prov_id'];
        $message['callout_charges'] = $this->db->select("callout_charge")->where("id", 1)->get("setting")->row()->callout_charge;
        $provider_name = $this->db->select("CONCAT(firstName,' ',lastName) as name")
                        ->where("prov_id", $document['prov_id'])
                        ->get("providers")->row()->name;

        $customer = $this->db->select("name,device_type,device_token")
                        ->where("cust_id in (select cust_id from job_request where job_id={$document['job_id']}   and prov_id={$document['prov_id']})")
                        ->get("customers")->row_array();
        if ($document['status'] == 1):
            $query1 = $this->db->set('status', 1)
                    ->where(array('prov_id' => $document['prov_id'], 'job_id' => $document['job_id']))
                    ->update("job_request");
            $query2 = $this->db->set('status', 3)
                    ->where(array('id' => $document['job_id']))
                    ->update("customer_job");

            if ($query1 && $query2):
                $flag = 1;
                $message1 = "Hi {$customer['name']} , Your job request has been accepted by " . ucfirst($provider_name) . ".";

                $message['notification_type'] = 3;
                $message['message'] = $message1;
                $key = 2;

                generatePush(strtolower($customer['device_type']), $customer['device_token'], $message ,$key);

            endif;
        else:
            $query = $this->db->where(array('prov_id' => $document['prov_id'], 'job_id' => $document['job_id']))
                    ->delete("job_request");
            if ($query):
                $flag = 1;

                $message1 = "Hi {$customer['name']} , Your job request has been rejected by " . ucfirst($provider_name) . ".";


                $message['notification_type'] = 4;
                $message['message'] = $message1;
                $key = 2;

                generatePush(strtolower($customer['device_type']), $customer['device_token'], $message, $key);
            endif;

        endif;
        return $flag == 1 ? true : false;
    }

    function action_job_quotaton($document) {
        $flag = 0;
        $jobdata = $this->db->where("id", $document['job_id'])->get("customer_job")->row_array();

        $provider_name = $this->db->select("CONCAT(firstName,' ',lastName) as name,device_type,device_token")
                        ->where("prov_id", $document['prov_id'])
                        ->get("providers")->row_array();

        if ($document['status'] == 1):
            $query1 = $this->db->set('status', 1)
                    ->where(array('prov_id' => $document['prov_id'], 'job_id' => $document['job_id']))
                    ->update("quotations");
            $query2 = $this->db->set('status', 3)
                    ->where(array('id' => $document['job_id']))
                    ->update("customer_job");
            $quote = $this->db->select("quotation")
                            ->where(array('prov_id' => $document['prov_id'], 'job_id' => $document['job_id']))
                            ->get("quotations")->row()->quotation;
            $quote = ($quote == NULL || $quote == "") ? 0 : $quote;

            if ($query1 && $query2):
                $flag = 1;


                $message1 = "Hi {$provider_name['name']} , Your job quotation has been accepted.";

                $message['job_id'] = $document['job_id'];
                $message['main_cat_id'] = $jobdata['main_cat_id'];
                $message['quotation'] = $quote;
                $message['prov_id'] = $document['prov_id'];
                $message['notification_type'] = 5;
                $message['message'] = $message1;
                $key = 2;

                generatePush(strtolower($provider_name['device_type']), $provider_name['device_token'], $message, $key);

            endif;
        else:
            $query = $this->db->where(array('prov_id' => $document['prov_id'], 'job_id' => $document['job_id']))
                    ->delete("quotations");
            if ($query):
                $flag = 1;

                $message1 = "Hi {$provider_name['name']} , Your job quotation has been rejected";

                $message['job_id'] = $document['job_id'];
                $message['main_cat_id'] = $jobdata['main_cat_id'];
                $message['prov_id'] = $document['prov_id'];
                $message['notification_type'] = 6;
                $message['message'] = $message1;
                $key = 2;

                generatePush(strtolower($provider_name['device_type']), $provider_name['device_token'], $message, $key);
            endif;

        endif;
        return $flag == 1 ? true : false;
    }

    function send_final_quotation($doc) {
        $final = $this->db->insert("final_quotation", $doc);
        $id = $this->db->insert_id();
        if ($id > 0):
            $data = array("job_id" => $doc['job_id'], "status" => 5);
            $this->update_job_status($data);
            return true;
        else:
            return false;
        endif;
    }

    function get_provider_completed_jobdetail($id) {
        $result = $this->db->select('customers.cust_id,customer_job.id,customer_job.main_cat_id,customer_job.sub_cat_id,customer_job.time,customer_job.date')
                        ->select("customers.name as cust_name,IF(profile_img='','',concat('" . base_url('customerImages') . "','/',profile_img)) as cust_profileImage")
                        ->select("CONCAT(customers.countryCode,customers.mobile) as mobile")
                        ->where('id', $id)
                        ->where('status', 5)
                        ->join('customers', 'customers.cust_id=customer_job.user_id')
                        ->get('customer_job')->row_array();
        if (!empty($result)) {
            $result1 = $this->get_sub_job_data($result['main_cat_id'], $result['id']);
            $result = array_merge($result, $result1);
            $result['main_cat'] = $this->db->select("name")
                            ->where("id", $result['main_cat_id'])
                            ->get("main_category")->row()->name;
            $result['sub_cat'] = $this->db->select("name")
                            ->where("sb_id", $result['sub_cat_id'])
                            ->get("sub_category")->row()->name;

            $quote_result = $this->db->select("prov_id,addi_job_desc,addi_charges,time,total_amount")
                            ->where('job_id', $id)
                            ->get('invoice')->row_array();
            $result['timeinhours'] = $quote_result['time'];
            $result['final_quotation'] = $quote_result['total_amount'];
            $result['description'] = $quote_result['addi_job_desc'];
            $result['addi_charges'] = $quote_result['addi_charges'];
            $prov_id = $this->db->select("prov_id,concat(firstName,' ',lastName) as prov_name,IF(profile_img='','',concat('" . base_url('providerImages') . "','/',profile_img)) as prov_profileImage")
                            ->where("prov_id = {$quote_result['prov_id']}")
                            ->get('providers')->row_array();
            if (empty($prov_id)):
                $prov_id = array("prov_name" => "", "prov_profileImage" => "");
            endif;

            $result = array_merge($result, $prov_id); //pre($this->db->last_query());die;
            $feedback = $this->db->select("rating,feedback,create_date")
                            ->where("job_id", $id)
                            ->where("prov_id", $result['prov_id'])
                            ->where("cust_id", $result['cust_id'])
                            ->get("provider_rating")->row_array();

            if (empty($feedback)):
                $feedback = array("rating" => "", "feedback" => "", "create_date" => "");
            endif;
            $result = array_merge($result, $feedback);
            
            $rating_done = $this->db->select("id")
                            ->where("job_id", $id)
                            ->get("customer_rating")->num_rows();
            $result['customer_rating_done'] = $rating_done > 0 ? 1 : 0;
            
             $provider_rating_done = $this->db->select("id")
                            ->where("job_id", $id)
                            ->get("provider_rating")->num_rows();
            $result['provider_rating_done'] = $provider_rating_done > 0 ? 1 : 0;
            if (!empty($result)):
                $result['date'] = str_replace("-", "/", date("d-M-Y", strtotime($result['date'])));
                $result['time'] = date("h:i a", strtotime($result['time']));
            endif;
        }
        return (!empty($result)) ? $result : false;
    }

    function get_provider_all_available_jobs($user_id) {
        $prov_data = $this->db->select("business_address_lat,business_address_long,callout_radius,business_subtype")
                        ->where("p1.prov_id", $user_id)
                        ->join("providers_subcat_relation as p2", "p1.prov_id=p2.prov_id")
                        ->get("providers as p1")->row_array();
        $sub_prov = $this->db->select("business_subtype")
                        ->where("p1.prov_id", $user_id)
                        ->get("providers_subcat_relation as p1")->result_array();
        $sub_id = array();
        foreach ($sub_prov as $x):
            $sub_id[] = $x['business_subtype'];
        endforeach;
        $prov_data['sub_cat'] = implode(",", array_values($sub_id));

        $date = date('Y-m-d');
        
      /*          $result = $this->db->query("select t1.*,TIMESTAMPDIFF( MINUTE ,  '" . date('Y-m-d H:i:s') . "' , CONCAT( date,  ' ', time ) ) AS difference, m1.name as main_category_name,SQRT(POW(69.1 * (location_lat - " . $prov_data['business_address_lat'] . "), 2) +   POW(69.1 * (" . $prov_data['business_address_long'] . " - location_long) * COS(location_lat / 57.3), 2)) AS distance from 
                (SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_household_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_outdoor_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_personal_relation c2 on c1.id=c2.job_id "
                        . "UNION SELECT c1.*,c2.pickup_location_lat as location_lat,c2.pickup_location_long as location_long FROM `customer_job` c1 inner join customer_job_transport_relation c2 on c1.id=c2.job_id) t1"
                        . " inner join main_category as m1 on m1.id = t1.main_cat_id"
                       // . " where concat(date,' ',time) >= '" . date("Y-m-d H:i:s") . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']}"
                         . " where t1.date>='" . $date . "' and t1.time>='" . date('H:i:s') . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']} and `difference` >= 240"
                )->result_array();
        
       08-05-2017  */
        
        $result = $this->db->query("select t1.*, m1.name as main_category_name,SQRT(POW(69.1 * (location_lat - " . $prov_data['business_address_lat'] . "), 2) +   POW(69.1 * (" . $prov_data['business_address_long'] . " - location_long) * COS(location_lat / 57.3), 2)) AS distance from 
                (SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_household_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_outdoor_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_personal_relation c2 on c1.id=c2.job_id "
                        . "UNION SELECT c1.*,c2.pickup_location_lat as location_lat,c2.pickup_location_long as location_long FROM `customer_job` c1 inner join customer_job_transport_relation c2 on c1.id=c2.job_id) t1"
                        . " inner join main_category as m1 on m1.id = t1.main_cat_id"
                       // . " where concat(date,' ',time) >= '" . date("Y-m-d H:i:s") . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']}"
                         . " where t1.date>='" . $date . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']}")->result_array();

                         
            if($result){
            foreach($result as $res){             
                if($res['date']==date('Y-m-d')){
                    if($res['time']>date('H:i:s')){
                        $final[] = $res;
                    }
                }else{
                    $final[] = $res;
                }
            }
            if(!empty($final)){
            $result = $final;  
            }else{
            $result = array();
            }
        }        
                         
                         
//        $result = $this->db->query("select t1.*,TIMESTAMPDIFF( MINUTE ,  '" . date('Y-m-d H:i:s') . "' , CONCAT( date,  ' ', time ) ) AS difference, m1.name as main_category_name, SQRT(POW(69.1 * (location_lat - " . $prov_data['business_address_lat'] . "), 2) + POW(69.1 * (" . $prov_data['business_address_long'] . " - location_long) * COS(location_lat / 57.3), 2)) AS distance from
//                (SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_household_relation c2 on c1.id=c2.job_id "
//                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_outdoor_relation c2 on c1.id=c2.job_id "
//                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_personal_relation c2 on c1.id=c2.job_id "
//                        . "UNION SELECT c1.*,c2.pickup_location_lat as location_lat,c2.pickup_location_long as location_long FROM `customer_job` c1 inner join customer_job_transport_relation c2 on c1.id=c2.job_id)t1"
//                        . " inner join main_category as m1 on m1.id = t1.main_cat_id"
//                        . " where t1.date='" . $date . "' and t1.time>='" . date('H:i:s') . "' and status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']} and `difference` >= 240"
//                )->result_array();               
                        
                        
                        
          //      echo $this->db->last_query();die;
             //    pre($result);die;
                        
                        
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['quotation'] = $this->get_job_quotation($x['id'], $user_id);
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }

        return (!empty($result_modi)) ? $result_modi : array();
    }

    function re_schedule_job($document) {
        $document['job_id'] = $document['id'];
        unset($document['id']);
        $status = $this->db->select("sub_cat_id,main_cat_id,status,user_id")->where('id', $document['id'])->get("customer_job")->row();
        $document['date'] = date("Y-m-d", strtotime(str_replace('/', '-', $document['date'])));
        $document['time'] = date("H:i:s", strtotime($document['time']));
        $document['usertype'] = 2;
        $query = $this->db->insert("reschedule_job_relation", $document);
       // echo $this->db->last_query();
        //pre($query);die('rescheduled');
        if ($query):

            $customer = $this->db->select("name,device_type,device_token")
                            ->where("cust_id", $status->user_id)
                            ->get("customers")->row_array();
            $main_cat = $this->db->select("name")
                            ->where("id", $result['main_cat_id'])
                            ->get("main_category")->row()->name;
            $sub_cat = $this->db->select("name")
                            ->where("sb_id", $result['sub_cat_id'])
                            ->get("sub_category")->row()->name;
            if ($customer != '' && $customer != NULL) {
                $message1 = "Hi {$customer['name']} , Your job has been rescheduled by provider.";
                $message['job_id'] = $document['job_id'];
                $message['main_cat_id'] = $status->main_cat_id;
                $message['main_cat_name'] = $main_cat;
                $message['sub_cat_name'] = $sub_cat;
                $message['date'] = str_replace("-", "/", date("d-M-Y", strtotime($document['date'])));
                $message['time'] = date("h:i a", strtotime($document['time']));
                $message['notification_type'] = 54;
                $message['message'] = $message1;
                $key = 2;

                generatePush(strtolower($customer['device_type']), $customer['device_token'], $message, $key);
            }

        endif;
//$id = $document['id'];
        return ($query) ? true : false;
    }

    function set_reschedule_actionByProvider($data) {
        $usr_id = $this->db->select("user_id")->where("id", $data['job_id'])->get("customer_job")->row_array();
        $usr = $this->db->select("name,device_type,device_token")->where("cust_id", $usr_id['user_id'])->get("customers")->row_array();
        $job = $this->db->where("job_id", $data['job_id'])->get("reschedule_job_relation")->row_array();
        if ($data['status'] == 1):
            $data1['date'] = $job['date'];
            $data1['time'] = $job['time'];
            $query = $this->db->update('customer_job', $data1, array('id' => $data['job_id']));

            $message1 = "Hi {$usr['name']} , Your reschedule request has been accepted.";
            $message['notification_type'] = 611;
        else:
            $data1['date'] = $job['date'];
            $data1['time'] = $job['time'];
            $data1['status'] = 0;
            $query = $this->db->update('customer_job', $data1, array('id' => $data['job_id']));

            $this->db->where(array("job_id" => $data['job_id'], "status" => 1))->delete("job_request");
            $this->db->where(array("job_id" => $data['job_id'], "status" => 1))->delete("quotations");
            $message1 = "Hi {$usr['name']} , Your reschedule request has been rejected.";
            $message['notification_type'] = 612;
        endif;
        $this->db->where("job_id", $data['job_id'])->delete("reschedule_job_relation");
        $message['job_id'] = $data['job_id'];
        $message['message'] = $message1;
        $key = 2;

        generatePush(strtolower($usr['device_type']), $usr['device_token'], $message, $key);
        return ($query) ? true : false;
    }
    
    
    
    ####################### Provider Id based   (11-Jan-2017)   #######################
    
   
#######################available jobs#####################################  
    function user_based_available_jobs_for_provider($user_id) {
      //  $date = date("Y-m-d", strtotime($date1));
        $prov_data = $this->db->select("business_address_lat,business_address_long,callout_radius,business_subtype")
                        ->where("p1.prov_id", $user_id)
                        ->join("providers_subcat_relation as p2", "p1.prov_id=p2.prov_id")
                        ->get("providers as p1")->row_array();
        $sub_prov = $this->db->select("business_subtype")
                        ->where("p1.prov_id", $user_id)
                        ->get("providers_subcat_relation as p1")->result_array();
        $sub_id = array();
        foreach ($sub_prov as $x):
            $sub_id[] = $x['business_subtype'];
        endforeach;
        $prov_data['sub_cat'] = implode(",", array_values($sub_id));

        $result = $this->db->query("select t1.*, m1.name as main_category_name, SQRT(POW(69.1 * (location_lat - " . $prov_data['business_address_lat'] . "), 2) + POW(69.1 * (" . $prov_data['business_address_long'] . " - location_long) * COS(location_lat / 57.3), 2)) AS distance from
                (SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_household_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_outdoor_relation c2 on c1.id=c2.job_id "
                        . "union SELECT c1.*,c2.location_lat,c2.location_long FROM `customer_job` c1 inner join customer_job_personal_relation c2 on c1.id=c2.job_id "
                        . "UNION SELECT c1.*,c2.pickup_location_lat as location_lat,c2.pickup_location_long as location_long FROM `customer_job` c1 inner join customer_job_transport_relation c2 on c1.id=c2.job_id)t1"
                        . " inner join main_category as m1 on m1.id = t1.main_cat_id"
                        . " where   status=0 and sub_cat_id in ({$prov_data['sub_cat']}) and t1.id not in (select job_id from job_request where prov_id={$user_id}) having distance < {$prov_data['callout_radius']} "
                )->result_array();
// pre($result);                    
//echo $this->db->last_query();die;
//pre($result);die;
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['quotation'] = $this->get_job_quotation($x['id'], $user_id);
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }

        return (!empty($result_modi)) ? $result_modi : array();
    } 
    
####################### accepted jobs ##################################### 
    function user_based_accepted_jobs_for_provider($user_id) {
       // $date = date("Y-m-d", strtotime($date1));
        $date = date("Y-m-d");
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name");
        $this->db->where('customer_job.date', $date);
        $this->db->where('customer_job.status', 3);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->where("customer_job.id in (select job_id from job_request where prov_id='" . $user_id . "' and status='1' union select job_id from quotations where prov_id='" . $user_id . "' and status ='1')");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $result = $this->db->get('customer_job')->result_array();
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);

                $job_status = $this->db->select("status")
                                ->where("job_id", $x['id'])
                                ->get("job_status")->row()->status;
                $x['job_status'] = $job_status != NULL ? $job_status : "";

                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }
    
####################### started jobs #####################################  
   function user_based_started_jobs_for_provider($user_id) {
//      $date = date("Y-m-d", strtotime($date1));
        $date = date("Y-m-d");
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name");
        $this->db->where('customer_job.date<', $date);
        $this->db->where('customer_job.status', 3);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->where("customer_job.id in (select job_id from job_request where prov_id='" . $user_id . "' and status='1' union select job_id from quotations where prov_id='" . $user_id . "' and status ='1')");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        
        $result = $this->db->get('customer_job')->result_array();
          //echo $this->db->last_query($result);
         // pre($result);die('checkup');
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);

                $job_status = $this->db->select("status")
                                ->where("job_id", $x['id'])
                                ->get("job_status")->row()->status;
                $x['job_status'] = $job_status != NULL ? $job_status : "";

                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
               // pre($result_modi);die('rahul');
            }
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }
 
####################### requested jobs #####################################      
    function user_based_requested_jobs_for_provider($user_id) {
        //$date = date("Y-m-d", strtotime($date1));
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.main_cat_id,customer_job.status,main_category.name as main_category_name");
        $this->db->select("customers.name as cust_name,IF(customers.profile_img='','',CONCAT('" . base_url('customerImages') . "','/',profile_img)) as profile_img");
      //  $this->db->where('customer_job.date >=', date("Y-m-d"));
        $this->db->where('customer_job.status', 0);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->where("customer_job.id in (select job_id from job_request where prov_id='" . $user_id . "' and status='0') ");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job')->result_array(); //echo $this->db->last_query();die;
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
//if (in_array($x['main_cat_id'], array(3, 4))):
                $x['quotation'] = $this->db->select("quotation")
                                ->where("job_id", $x['id'])
                                ->get("quotations")->row()->quotation;
                $x['quotation'] = $x['quotation'] != NULL ? $x['quotation'] : "";
//endif;

                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }

        return (!empty($result_modi)) ? $result_modi : array();
    }
    
####################### completed jobs #####################################   
    function user_based_completed_jobs_for_provider($user_id) {
        //  $date = date("Y-m-d", strtotime($date1));
        $this->db->select("customer_job.id,customer_job.sub_cat_id,customer_job.date,customer_job.time,customer_job.main_cat_id,customer_job.status,main_category.name as main_category_name");
        $this->db->select("customers.name as cust_name,IF(customers.profile_img='','',CONCAT('" . base_url('customerImages') . "','/',profile_img)) as profile_img");
        //$this->db->where('customer_job.date', $date);
        $this->db->where('customer_job.status', 5);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->where("customer_job.id in (select job_id from job_request where prov_id='" . $user_id . "' and status='1' union select job_id from quotations where prov_id='" . $user_id . "' and status ='1')");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $result = $this->db->get('customer_job')->result_array();
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) {
                $x['name'] = $this->get_subcat_name($x['sub_cat_id']);
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);
                if (!empty($result1)) {
                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            }
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }
    
  ####################### End of Provider Id based   (11-Jan-2017)    #######################

    
    function setCommisionData($document) {
        
        $this->db->select("customer_payment_details.id as paymentId,main_category.id as catId,customers.name as customer,CONCAT(providers.firstName, ' ', providers.lastName) AS provider,customer_payment_details.additional_payment_amount,customer_payment_details.callout_payment_amount,customer_payment_details.quote_payment_amount,customer_payment_details.quote_payment_date");             
        $this->db->where('customer_payment_details.prov_id', $document['prov_id']);
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('customer_job', 'customer_job.id = customer_payment_details.job_id');
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $this->db->join('providers', 'providers.prov_id = customer_payment_details.prov_id');
        $TotalPayResults = $this->db->get('customer_payment_details')->result_array();
        
        foreach ($TotalPayResults as $key=> $res){ 
            
        
            if($res['catId']== 1 OR $res['catId']== 2){
              $variable      =   $res['quote_payment_amount'];
              $TotalPayResults[$key]['quote_payment_amount']   = $res['callout_payment_amount'] ;
              $TotalPayResults[$key]['callout_payment_amount'] = $variable ;
            }  
            $commision = $this->db->query("select commision FROM  setting")->row_array();
           $TotalPayResults[$key]['commision'] =$commision['commision'];
      
        }
       // $TotalPayResults[$key]['commision'] = $this->db->query("select commision FROM  setting")->row_array();
          //pre($commision);die;
       // pre($TotalPayResults[$key]['commision']);die;
                     
        if(!empty($TotalPayResults))
        {               
           return $TotalPayResults; 
        }
        else
        {
            return false;
        }
    }
    
    
    
     function getAllCallout($document) {
        $this->db->select('SUM(quote_payment_amount) AS total,customer_payment_details.*,customer_job.*,customers.name as customer,main_category.name as Category');
        $this->db->order_by("customer_payment_details.created_on", "desc");
        $this->db->where_in("customer_job.main_cat_id",array(1,2)); 
        $this->db->where('customer_payment_details.prov_id', $document['prov_id']);
        $this->db->join('customers','customers.cust_id = customer_payment_details.cust_id');
        $this->db->join('customer_job','customer_job.id = customer_payment_details.job_id');
        $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
        $result = $this->db->get('customer_payment_details')->row_array(); 
       if($result){
            $this->db->select('customer_payment_details.*,customer_job.*,customers.name as customer,main_category.name as Category');
            $this->db->order_by("customer_payment_details.created_on", "desc");
            $this->db->where_in("customer_job.main_cat_id",array(1,2)); 
            $this->db->where('customer_payment_details.prov_id', $document['prov_id']);
            $this->db->join('customers','customers.cust_id = customer_payment_details.cust_id');
            $this->db->join('customer_job','customer_job.id = customer_payment_details.job_id');
            $this->db->join('main_category','main_category.id = customer_job.main_cat_id');
            $result1 = $this->db->get('customer_payment_details')->result_array(); 
            if($result1){
                $result1[0]['total'] = $result['total'];
                return $result1;
            }
           
       }else{
           return false;
       }
    }
    
     
    function setCommisionDataYes($document) {
      //  pre($document);die;
        $this->db->select("invoice.total_amount as PaidAmount,invoice.created_on as PaidDate,customer_job.id,customer_job.sub_cat_id,customer_job.date as JobDate,customer_job.time,customer_job.status,customer_job.main_cat_id,main_category.name as main_category_name,customers.name as CustomerName,invoice.total_amount as total_amount_paid,customers.mobile as mobile,providers.firstName as providerFirst,providers.lastName as providerlastName");
        $this->db->order_by("customer_job.date", "desc");
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $this->db->join('customers', 'customers.cust_id = customer_job.user_id');
        $this->db->where('invoice.prov_id', $document['prov_id']);
        $this->db->join('invoice','invoice.job_id = customer_job.id'); 
        $this->db->join('providers', 'providers.prov_id = invoice.prov_id');
        $result = $this->db->get('customer_job')->result_array(); 

        
        $this->db->select("SUM(additional_payment_amount + callout_payment_amount + quote_payment_amount) AS total");             
        $this->db->where('customer_payment_details.prov_id', $document['prov_id']);
        $TotalPayResults = $this->db->get('customer_payment_details')->result_array();
       
        if(isset($TotalPayResults[0]['total'])){
          }else{
        $TotalPayResults[0]['total'] = 0;
          }
          
        $getAllCallout = $this->getAllCallout($document);
        
        if(isset($getAllCallout[0]['total'])){
          }else{
        $getAllCallout[0]['total'] = 0;
          }
        
        $Total_Minus_Callout =  $TotalPayResults[0]['total'] - $getAllCallout[0]['total'];         
        if(!empty($result))  {
        $result[]['grandTotal'] =$TotalPayResults[0]['total'];     
        }      
           
        if(!empty($result))  {
             $result[]['TotalCallout'] =$getAllCallout[0]['total'];     
         } 

        if(!empty($result))  {
            $result[]['totalWithoutCallout'] =$Total_Minus_Callout;

        }                        
        $this->db->select("commision");
        $commision =   $this->db->get("setting")->row_array();

        if(!empty($result))  {
        $result[]['commision'] = $commision['commision'];

         }                                
        if(!empty($result))
        {            
           return $result; 
        }
        else
        {
            return false;
        }
    }
    
    
}
