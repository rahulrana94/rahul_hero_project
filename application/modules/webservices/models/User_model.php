<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class User_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();   
    }



    function isUserExist($document)
    {
        $this->db->where('email', $document['email']); 
        $result = $this->db->get('users')->row_array();       
        if (!empty($result))
        {
            return $result['userId'];
        }
        else
        {
            return false;
        }
    }
    
    
    
    function sendInviteMail($document)
    {
        $this->db->where('userId', $document['userId']); 
        $result = $this->db->get('users')->row_array();       
        $sender = $result['firstName'];
        
        
        $to = $document['email'];
        $msg = "Your friend $sender has invited you on SpiritBox Mobile Application.";
        $sub = "Invitation To Install Application Spirit Box";
        $headers = 'From: Spirit Box <sumit.chauhan@fourthscreenlabs.com>' . "\r\n";

        @$sentmail = mail($to,$sub,$msg,$headers);
        if(!empty($sentmail))
        {
            return $sender;
        }
        else
        {
            return false;
        }
    }
    
    
    function isLeftPersonExist($document)
    {
        $this->db->where('name', $document['name']); 
        $this->db->where('birthDate', $document['birthDate']); 
        $this->db->where('deathDate', $document['deathDate']); 
        $this->db->where('graveLocation', $document['graveLocation']);
        $result = $this->db->get('left_person')->row_array();       
        if (!empty($result))
        {
            return $result['id'];
        }
        else
        {
            return false;
        }
    }

    function makeBase64Img($image){

         $data = str_replace(" ","+",$image);
         $data = base64_decode($data);
         $im = imagecreatefromstring($data);
         $fileName = rand(5, 115).time().".png";
         $imageName = "profileImages/".$fileName; 
               

         if ($im !== false) {

          imagepng($im,$imageName);

          imagedestroy($im);

         }else {
          echo 'An error occurred.';
         } 
         
         return $fileName;
        }

        
         function makeBase64ChatImg($image){

         $data = str_replace(" ","+",$image);
         $data = base64_decode($data);
         $im = imagecreatefromstring($data);
         $fileName = rand(5, 115).time().".png";
         $imageName = "chatImages/".$fileName; 
               

         if ($im !== false) {

          imagepng($im,$imageName);

          imagedestroy($im);

         }else {
          echo 'An error occurred.';
         } 
         
         return $fileName;
        }

    function userSignUp($document)
    {
        $query  = $this->db->insert('users', $document);
        $id = $this->db->insert_id();
        $this->db->where('userId', $id); 
        $details=$this->db->get('users')->row_array();
        if($details)
        {
            return $details;
        }
        else
        {
            return false;
        }
    }
    
    function editProfile($document)
    {
        $this->db->select('users.*,spirit_left_person.id' ); 
        $this->db->where('userId', $document['userId']);       
        $query = $this->db->update('users', $document);       
        $this->db->where('users.userId', $document['userId']);         
        $this->db->join('spirit_left_person','users.userId=spirit_left_person.userId','left');  
        $result = $this->db->get('users')->row_array();
        if($query)
        {          
           return $result ;
        }
        else
        {
           return false; 
        }
              
    }
    
     
    function editLeftPersonProfile($document)
    {
         $this->db->select('*' );
        $this->db->where('id', $document['id']);
       
        $query = $this->db->update('spirit_left_person', $document);
        $this->db->where('spirit_left_person.id', $document['id']);         
       // $this->db->join('memories','users.userId=memories.userId'); 
        $result1 = $this->db->get('spirit_left_person')->row_array();
        
        $group_id = $document['id'];
        
        //
        $this->db->select('users.firstName,users.userId,users.deviceToken,users.deviceType' );               
        $this->db->join('users','users.userId=family.userId'); 
     
        $this->db->where('id',$document['id']);
        $result = $this->db->get('family')->result_array();
      // print_r($result);
        $count1 = count($result);
        
        //echo $count1; die();
        for ($i=0; $i<$count1; $i++)
        {
            if($result[$i]['userId']!=$document['userId'])
            {
              
                $this->db->select('firstName,deviceToken,deviceType');         
                $this->db->where('userId',$result[$i]['userId']);  
                $this->db->where_not_in('userId',$document['userId']);
                $result2 = $this->db->get('users')->result_array();
                
                $name     =  $result[$i]['firstName'];
                $deviceToken = $result[$i]['deviceToken'];
                $deviceType  =  $result[$i]['deviceType'];
               // print_r($i);
                $message1 = "Hi $name , left person profile has been edited in your group.";
                $document1['from_id']  = $document['userId']; 
                $document1['message'] = $message1;
                $document1['to_id']   = $result[$i]['userId'];
                $document1['id']      = $document['id'];
                $document['to_id']    = $result[$i]['userId'];

              //  print_r($document1);die();
                $this->db->insert('notification',$document1 );      
                $id2 = $this->db->insert_id();

                $message['group_id']  =$group_id;
                $message['to_id']     =$result[$i]['userId'];       
                $message['message1']  =$message1;   
                $document['message']  = $message1;
                generatePush($deviceType,$deviceToken,$message);
            }
        }
        
        if($query)
        {
           return $result1;
        }
        else
        {
           return false; 
        }
              
    }
    
    
    function leftPersonAdd($document)
    {
        //$this->db->insert('spirit_left_person', $document);
        $this->db->insert('spirit_left_person', $document);
        $id = $this->db->insert_id();
        
       // $document['userId'] = $user_id;
        //echo $user_id; die();
        $document1['id']=$id;
        $document1['userId']=$document['userId'];
        $this->db->insert('family', $document1);
        
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }


    function createProfile($document)
    {
        $this->db->insert('users', $document);
        $id = $this->db->insert_id();
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }

    function updateProfile($document)
    { 
        $this->db->where('email', $document['email']); 
        //$this->db->where('userId', $document['userId']); 
        $query  = $this->db->update('users', $document);
        if (!empty($query))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    

    function UserVerifyCode($document)
    {
        
        $this->db->where('email', $document['email']); 
        $result = $this->db->get('users')->row_array();

        if(!empty($result))
        {
            if($result['verificationCode']==$document['verificationCode'])
            {                    
                return 1;
            }
            else
            {
                return 2;
            }
        }
        else
        {
            return 3;
        }
    }

    function getProfile($id)
    { 
        $this->db->where('userId', $id); 
        $details=$this->db->get('users')->row_array();
        if($details['profileImage']!="")
        {
            $details['profileImage'] =  "http://fourthscreenlabs.com/amit/livepubhub/profileImages/".$user['profileImage'];
        }
        return $details;
    }

    function userLogin($document)
    {
        $this->db->select('users.*,spirit_left_person.id' ); 
        $this->db->where('users.email', $document['email']); 
        $this->db->where('users.password', $document['password']);         
        $this->db->join('spirit_left_person','users.userId=spirit_left_person.userId','left'); 
        
        $result = $this->db->get('users')->row_array();  
        
       // $this->db->where('email', $document['email']);
       // $result = $this->db->get('users')->row_array();
        if(!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    
    function userMemoriId($document)
    {
        $this->db->where('email', $document['email']); 
        //$this->db->join('password', $document['password']); 
        $this->db->join('users','users.userId=memories.userId');
        $result = $this->db->get('memories')->row_array();  
        if(!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }


    function changePassword($document)
    {
        $this->db->where('email', $document['email']); 
        $query = $this->db->update('users', $document);
        if(!empty($query))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    
      function userChat($document)
    {
        $this->db->insert('chat', $document);
        $id = $this->db->insert_id();
        $group_id = $document['id'];
        $this->db->select('users.firstName,users.userId,users.deviceToken,users.deviceType' );               
        $this->db->join('users','users.userId=family.userId');     
        $this->db->where('id',$document['id']);
        $result = $this->db->get('family')->result_array();
        $count1 = count($result);
      
        for ($i=0; $i<$count1; $i++)
        {
            if($result[$i]['userId']!=$document['userId'])
            {
   
                $this->db->select('firstName,deviceToken,deviceType');         
                $this->db->where('userId',$result[$i]['userId']); 
                //$this->db->where('userId !=',$document['userId']); 
                $result2              = $this->db->get('users')->result_array();
               // print_r($result2);
                $name                 =  $result[$i]['firstName'];
                $deviceToken          =  $result[$i]['deviceToken'];
                $deviceType           =  $result[$i]['deviceType'];
               // print_r($i);
                $message1             = "Hi $name , you have new message.";

                $document1['message2']= $message1;
                $document1['to_id']   = $result[$i]['userId'];
                $document1['id']      = $document['id'];


                $document2['to_id']     = $result[$i]['userId'];
                $document2['from_id']   =  $document['userId'];
                $document2['id']        =  $document['id'];
                $document2['message']   =  $message1;
                $document2['created_at']=  $document['created_at'];

                $document['message']   = $message1; 
                $this->db->insert('notification',$document2 );

                $id1 = $this->db->insert_id();


                $message['group_id']=$group_id;
                $message['to_id']=$result[$i]['userId'];

                $message['message1']=$message1;

                $document['message'] = $message1;
                generatePush($deviceType,$deviceToken,$message);
            }
        
        }

        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }
    
      function userChatDisplay($document)
    {
        $this->db->select("chat.id,chat.userId,chat.message,chat.created_at,users.firstName,concat('".base_url("profileImages").'/'."',live_users.profileImage) as profileImage");         
        $this->db->where('id',$document['id']);
        $this->db->join('users','users.userId=chat.userId','left'); 
        
        $result = $this->db->get('chat')->result_array(); 
        
         
        if(!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    
     
      function notificationDisplay($document)
    {
         $result3 =array(); 
        $this->db->select("notification.*,concat('".base_url("profileImages").'/'."',live_users.profileImage) as profileImage");                 
        $this->db->join('users','users.userId=notification.from_id','left');
        $this->db->where('to_id',$document['to_id']);     
        $result = $this->db->get('notification')->result_array();
        $count = count($result);
 
         
        if(!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    
    
     
      function groupDisplay($document)
    {
        $result3 =array();
        $this->db->select("family.id");         
        $this->db->where('family.userId',$document['userId']);
        $this->db->join('users','users.userId=family.userId','left'); 
        $result = $this->db->get('family')->result_array(); 
        $count = count($result);
       // echo $count; die();
         for ($i=0; $i<$count; $i++)
        {
        $this->db->select('name,id,graveImage');         
        $this->db->where('id',$result[$i]['id']);  
        $result2 = $this->db->get('spirit_left_person')->row_array();   
        $result2['graveImage'] = base_url()."profileImages/".$result2['graveImage'];
           array_push($result3,$result2);
        }
         
        if(!empty($result2))
        {
            return $result3;
        }
        else
        {
            return false;
        }
    }

    function leftPersonExist($document)
    {
        //$this->db->where('userId', $document['userId']); 
        $this->db->where('id', $document['id']); 
       
        $result = $this->db->get('spirit_left_person')->row_array();       
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    
    
      
      function inviteUsers($document)
    { 
    $existing = array();
    $notexisting = array();
    $frnd = array();
    $result10 = array();
      
          $contacts = $document['phone'];
          $userContact = explode(',',$contacts);
          $count = count($userContact);
          $county = $count-1;
          
          
          foreach($userContact as $cc)
          {
            //echo $cc."/";
            $this->db->select("userId,firstName,profileImage");
            $this->db->where('phone',$cc);
            $result = $this->db->get('users')->row_array();
            if($result)
            { 
                 $result['profileImage'] = base_url()."profileImages/".$result['profileImage'];
                $id = $result['userId'];
                $userId = $document['userId'];
                //$this->db->select('users.firstName,users.userId');
                //$this->db->join('users','users.userId=family.userId');
                $this->db->where('id',$document['id']);
                $this->db->where('userId',$id) ;
                $result2 = $this->db->get('family')->row_array();
                if($result2){
                     $result['phone'] =$cc; 
                     $result['code'] =3;
                    array_push($result10, $result);
                   
                }
                else{
                    $result['phone'] =$cc;
                    $result['code'] =2;
                array_push($result10, $result);
              
                }
                
 
            }
            else { 
                $result['phone'] =$cc; 
                $result['code'] =1; 
                array_push($result10, $result);  } 
          }
         
           
           
   
        if(!empty($result10))
        {
            return $result10;
        }
        else
        {
            return false;
        }
    }
    
    
 


   
      function pushNotification($document)
    {
          //Add Notification
        $this->db->insert('notification', $document);
        $id = $this->db->insert_id();
          //Send push message
        $this->db->select('deviceToken,deviceType');         
        $this->db->where('userId',$document['userId']);      
        $result1 = $this->db->get('users')->row_array(); 
        
        $deviceToken = $result1['deviceToken'];
        $deviceType =  $result1['deviceType'];
      
       
        generatePush($deviceType,$deviceToken,$document['message']);
        
        
        if($result1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    


      function pushFriendRequest($document)
    {
        $this->db->select('firstName');         
        $this->db->where('userId',$document['from_id']);      
        $result2 = $this->db->get('users')->row_array();
        $from_name     =  $result2['firstName'];
          
        $this->db->select('deviceToken,deviceType,firstName');         
        $this->db->where('userId',$document['to_id']);      
        $result1 = $this->db->get('users')->row_array(); 
        $group_id    =  $document['id'];
        $deviceToken =  $result1['deviceToken'];
        $deviceType  =  $result1['deviceType'];
        $to_name     =  $result1['firstName'];
   
      
        $message1 = "Hi $to_name , $from_name would like to add you in the group.";
       
        
        $message['group_id']=$group_id;
        $message['to_name']=$to_name;
        $message['from_name']=$from_name;
        $message['message5']=$message1;
   
        $document['message'] = $message1;
       
        
        $this->db->insert('notification',$document );
        $id = $this->db->insert_id();
       
        generatePush($deviceType,$deviceToken,$message);
        
     
        if($result1)
        {          
            return $message1;
        }
        else
        {
            return false;
        }
    }
    
    
    
    function acceptFriendRequest($document)
    {
        $this->db->insert('family', $document);
        //$this->db->insert('spirit_left_person', $document);
        $id = $this->db->insert_id();
       // $q = $this->db->last_query();
        //echo $q; 
        //die();
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }

    
    function birthdayPush($tday)
    {
        
        
        $this->db->select('*' );
        $this->db->where('birthDate', $tday);
       
       
        
        $result1 = $this->db->get('spirit_left_person')->result_array();
        
        
        $count = count($result1);
        
        for($k=0; $k<$count; $k++ )
        {
            $group_id = $result1[$k]['id'];
            $leftPerson = $result1[$k]['name'];
            
        
            $this->db->select('users.userId' );               
            $this->db->join('users','users.userId=family.userId'); 

            $this->db->where('id',$result1[$k]['id']);
            $result = $this->db->get('family')->result_array();
            //print_r($result); die();
            $count1 = count($result) ;
            
            
            for ($i=0; $i<$count1; $i++)
            {
               // echo $result[$i]['userId']."n";
                
                    $this->db->select('firstName,deviceToken,deviceType');         
                    $this->db->where('userId',$result[$i]['userId']);  
                    //$this->db->where_not_in('userId',$document['userId']);
                    $result2     = $this->db->get('users')->row_array();
                    //print_r($result2); //die();
                    ///*
                    $name                   =  $result2['firstName'];
                    $deviceToken            = $result2['deviceToken'];
                    $deviceType             =  $result2['deviceType'];
                   // print_r($i);
                    $message1               = "Hi $name , today is birthday of your beloved family member $leftPerson.";
                    //$document1['from_id']  = $document['userId']; 
                    $document1['message']   = $message1;
                    $document1['id']        =$group_id;
                    $document1['to_id']     = $result[$i]['userId'];
                    //$document1['id']      = $document['id'];
                    //$document['to_id']    = $result[$i]['userId'];

                  //  print_r($document1);die();
                    $this->db->insert('notification',$document1 );      
                    $id2 = $this->db->insert_id();

                    $message['group_id']  =$group_id;
                    $message['to_id']     =$result[$i]['userId'];       
                    $message['message1']  =$message1;   
                    $document['message']  = $message1;
                    generatePush($deviceType,$deviceToken,$message);
               //*/
            }
            //die();
        }    
        
        if($result2)
        {
           return $result2;
        }
        else
        {
           return false; 
        }
              
    }
    
    
     function deathPush($tday)
    {
        $this->db->select('*' );
        $this->db->where('deathDate', $tday);
        $result1 = $this->db->get('spirit_left_person')->result_array();     
        $count = count($result1);
        
        for($k=0; $k<$count; $k++ )
        {
            $group_id = $result1[$k]['id'];
            $leftPerson = $result1[$k]['name'];                  
            $this->db->select('users.userId' );               
            $this->db->join('users','users.userId=family.userId'); 
            $this->db->where('id',$result1[$k]['id']);
            $result = $this->db->get('family')->result_array();          
            $count1 = count($result) ;
            
            
            for ($i=0; $i<$count1; $i++)
            {
                              
                    $this->db->select('firstName,deviceToken,deviceType');         
                    $this->db->where('userId',$result[$i]['userId']);  
                 
                    $result2     = $this->db->get('users')->row_array();
             
                    $name                   =  $result2['firstName'];
                    $deviceToken            =  $result2['deviceToken'];
                    $deviceType             =  $result2['deviceType'];
                                               
                    $message1               = "Hi $name , today is death anniversary of your beloved family member $leftPerson.";                  
                    $document1['message']   = $message1;
                    $document1['id']        =$group_id;
                    $document1['to_id']     = $result[$i]['userId'];
      
                    $this->db->insert('notification',$document1 );      
                    $id2 = $this->db->insert_id();

                    $message['group_id']  =$group_id;
                    $message['to_id']     =$result[$i]['userId'];       
                    $message['message1']  =$message1;   
                    $document['message']  = $message1;
                    generatePush($deviceType,$deviceToken,$message);
               //*/
            }
        
        }    
        
        if($result2)
        {
           return $result2;
        }
        else
        {
           return false; 
        }
    }
              
   
    
    
    
    
        }
    

