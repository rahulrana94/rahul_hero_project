<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function userLogin($json) {

        $document['loginType'] = $json['loginType'];
        $device['deviceType']  = $json['deviceType'];
        $device['deviceToken'] = $json['deviceToken'];

        if ($json['loginType'] == 1) {
            $document['socialId'] = $json['socialId'];
            $this->db->where('socialId', $document['socialId']);
        }
        if ($json['loginType'] == 2) {
            $document['socialId'] = $json['socialId'];
            $this->db->where('socialId', $document['socialId']);
        }
        if ($json['loginType'] == 0) {
            $document['email'] = $json['email'];
            $document['password'] = $json['password'];
            $this->db->where('email', $document['email']);
            $this->db->where('password', $document['password']);
        }
        $profile = $this->db->get('users')->row_array();
//pre($profile); die();
//if(empty($profile))
//{return false;}
//else{$profile['profileImage'] = base_url()."profileImage/".$profile['profileImage'];
//$created = 1000*strtotime($profile['created']);
//$profile['created'] = $created;
//$this->db->where('userId', $profile['userId']);
//$query = $this->db->update('users', $device);
//$paymentStatus = $this->checkPaymentStatus($profile);
//$profile['paymentStatus'] = $paymentStatus;return $profile;}}
    }

    function basicLogin($json) {
        
        $device['deviceType']     = $json['deviceType'];
        $device['deviceToken']    = $json['deviceToken'];
        $document['email']        = $json['email'];
        $document['password']     = $json['password'];
        
        $this->db->where('email', $document['email']);
        $this->db->where('password', $document['password']);
        $profile = $this->db->get('admin')->row_array();
        if (empty($profile)) {
            return false;
        } else {
            $profile['profileImage'] = base_url() . "profileImages/" . $profile['profileImage'];
            $this->db->where('userId', $profile['id']);
            $query = $this->db->update('admin', $device);
            return $profile;
        }
    }

}
