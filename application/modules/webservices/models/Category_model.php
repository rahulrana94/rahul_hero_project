<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Category_model extends CI_Model{
    function __construct(){
        parent::__construct();   
    }

	/*
	* get sub category w.r.t. key  {parent_id}
	*/
	function get_subcategories($main_category = ""){
        $this->db->select('sub_category.*, main_category.name as main_category_name');
        $this->db->join('main_category', 'main_category.id = sub_category.main_category');
        $result = $this->db->get_where('sub_category', array('main_category' => $main_category))->result_array();      
        return (count($result)>0)?$result:array(); 
    }

     function searchSubCategory($search = "")
    {
        $this->db->select('sub_category.*,concat("'.base_url("subcategoryimages")."/".'",sub_category.icon_img) as icon_img, main_category.name as main_category_name');
        $this->db->like('sub_category.name', $search, "after"); 
        $this->db->join('main_category', 'main_category.id = sub_category.main_category');
        $result = $this->db->get('sub_category')->result_array();  
        return (!empty($result)) ? $result : false; 
    }
}
    

