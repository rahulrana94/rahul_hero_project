<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Customers_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function createProfile($document) {
        $query = $this->db->insert('customers', $document);
        $id = $this->db->insert_id();
        $this->db->where('cust_id', $id);
        $details = $this->db->get('customers')->row_array();
        if ($details) {
            return $details;
        } else {
            return false;
        }
    }

    function isUserExist($document) {
        $this->db->where('email', $document['email']);
        $result = $this->db->get('customers')->row_array();
        if (!empty($result)) {
            //return $result['cust_id'];
            return $result;
        } else {
            return false;
        }
    }

    /*
     * get customer profile info with key {cust_id}
     */

    function get_customer_info($user_id = "") {
        $this->db->where('cust_id', $user_id);
        $result = $this->db->get('customers')->row_array();
        return (!empty($result)) ? $result : array();
    }

    function updateOTP($document) {
        $this->db->where('mobile', $document['mobile']);
        $query = $this->db->update('customers', $document);
        
    
        if ($query) {  
                  $this->db->where('mobile', $document['mobile']);
                  $result = $this->db->get('customers')->row_array();
            return $result;
        } else {
            return false;
        }
    }

    function isUserPhoneExist($document) {


        $this->db->where('mobile', $document['mobile']);
        $result = $this->db->get('customers')->row_array();
        if (!empty($result)) {
            return $result['cust_id'];
        } else {
            return false;
        }
    }

    function isUserExistSocially($document) {
        $this->db->where('social_id', $document['social_id']);
        $result = $this->db->get('customers')->row_array();
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function makeBase64Img($image) {

        $data = str_replace(" ", "+", $image);
        $data = base64_decode($data);
        $im = imagecreatefromstring($data);
        $fileName = rand(5, 115) . time() . ".png";
        $imageName = "customerImages/" . $fileName;


        if ($im !== false) {

            imagepng($im, $imageName);

            imagedestroy($im);
        } else {
            echo 'An error occurred.';
        }

        return $fileName;
    }

    function userSignUp($document) {
        $query = $this->db->insert('customers', $document);
        $id = $this->db->insert_id();
        $this->db->where('cust_id', $id);
        $details = $this->db->get('customers')->row_array();
        if ($details) {
            return $details;
        } else {
            return false;
        }
    }

    function customerLogin($document) {
        $this->db->select("*,IF(profile_img='','',CONCAT('" . base_url("customerImages") . "','/',profile_img)) as profile_img");
        $this->db->where('email', $document['email']);
        $this->db->where('password', $document['password']);
        //$this->db->where('verified', $document['verified']);
        $result = $this->db->get('customers')->row_array();

        //$notVerified = "User Not Verified";
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    function customerSignIn($document) {
        $this->db->where('social_id', $document['social_id']);
        $this->db->where('login_type', $document['login_type']);
        //$this->db->where('verified', $document['verified']);
        $result = $this->db->get('customers')->row_array();

        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function updateCustomer($document) {
        $this->db->where('email', $document['email']);
        //$this->db->where('password', $document['password']);               
        $query = $this->db->update('customers', $document);

        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

    function updateCustomerByPhone($document) {
        $this->db->where('mobile', $document['mobile']);
        //$this->db->where('password', $document['password']);               
        $query = $this->db->update('customers', $document);

        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

    function updateCustomerSocially($document) {
        $this->db->where('social_id', $document['social_id']);
        // $this->db->where('login_type', $document['login_type']);               
        $query = $this->db->update('customers', $document);

        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

    function passwordRecovery($document) {

        $this->db->where('email', $document['email']);
        $result = $this->db->get('customers')->row_array();
        $password = $result['password'];
        if (!empty($result)) {
            return $password;
        } else {
            return false;
        }
    }

    function verifyPhone($document) {
        $to = $document['mobile'];
        $this->db->where('mobile', $document['mobile']);
        $result = $this->db->get('customers')->row_array();
        $msg = $result['otp'];
        //echo $code; die();
        if (!empty($result)) {

            function sendSMSTWILIO($to, $msg) {
                require_once('twilio-php/Services/Twilio.php');
                $account_sid = 'ACfde42acd3bd91661b6fd0256c2ce4965';
                $auth_token = 'c9eea2ef65ea2ef6095e1a92ae5a033f';
                $client = new Services_Twilio($account_sid, $auth_token);

                $client->account->messages->create(array(
                    'To' => $to,
                    'From' => "+27875506558",
                    'Body' => $msg,
                ));
            }

            return true;
        } else {
            return false;
        }
    }

    function verifyPhone2($document) {
        $this->db->where('mobile', $document['mobile']);
        $this->db->where('otp', $document['otp']);
        $result = $this->db->get('customers')->row_array();
        $msg = $result['otp'];
        //echo $code; die();
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    /* update customer profile with primary key {cust_id} */

    public function update_customer_profile($data, $primary_key) {
        $this->db->where('cust_id', $primary_key);
        $query = $this->db->update('customers', $data);
        return ($query) ? true : false;
    }

    public function checkPromoCode($data) {
        $check = $this->db->select("id")
                        ->where(array("user_id" => $data['user_id'], "promocode" => $data['promocode']))
                        ->get("customer_job")->num_rows();
        return $check > 0 ? true : false;
    }

    public function createCustomerJobPosting_16Feb2017($data) {
        $image = array();
        $data1['user_id'] = $data['user_id'];
        unset($data['user_id']);
        $data1['main_cat_id'] = $data['main_cat_id'];
        unset($data['main_cat_id']);
        $data1['sub_cat_id'] = $data['sub_cat_id'];
        unset($data['sub_cat_id']);
        $data1['promocode'] = $data['promocode'];
        unset($data['promocode']);
        $data1['imidiate_callout'] = $data['imidiate_callout'];
        unset($data['imidiate_callout']);
        $data1['date'] = date("Y-m-d", strtotime(str_replace('/', '-', $data['date'])));
        unset($data['date']);
        $data1['time'] = date("H:i:s", strtotime($data['time']));
        unset($data['time']);
        $data1['create_date'] = date('Y-m-d H:i:s');

        $query = $this->db->insert('customer_job', $data1);
        
        $id = $this->db->insert_id();
        if (isset($id) && $id > 0) {
            $data['job_id'] = $id;

            switch ($data1['main_cat_id']) {
                case 1:
                    $query = $this->db->insert('customer_job_household_relation', $data);
                    break;
                case 2:
                    $query = $this->db->insert('customer_job_outdoor_relation', $data);
                    break;
                case 3:
                    $query = $this->db->insert('customer_job_personal_relation', $data);
                    break;
                case 4:
                    for ($i = 1; $i <= $data['items_to_move']; $i++) {
                        $image[] = $this->uploadfile('image_' . $i);
                    }
                    $data['image'] = implode(",", $image);
                    $query = $this->db->insert('customer_job_transport_relation', $data);
                    break;
                default:
                    return false;
                    break;
            }
            $id1 = $this->db->insert_id();
            if (isset($id1) && $id1 > 0):
                $result = $this->get_customer_jobdetail_by_id($id);
                if ($data1['imidiate_callout'] == 1):
                    $pros_data = $this->get_immidiate_callout_provider_by_jobid($id);
                    $result['total'] = count($pros_data);
                    $val1 = $this->get_immidiate_callout_provider_by_jobid_available($id);
                    $result['available'] = $val1 != NULL ? $val1 : 0;
                    $val2 = $this->get_immidiate_callout_provider_by_jobid_busy($id);
                    $result['busy'] = $val2 != NULL ? $val2 : 0;
                    foreach ($pros_data as $prov):
                        $message1 = "A new job started for immidiate callout";
                        $message['job_id'] = $id;
                        $message['main_cat_id'] = $data1['main_cat_id'];
                        $message['prov_id'] = $prov['prov_id'];
                        $message['notification_type'] = 10;
                        $message['message'] = $message1;
                        $key =1;
                        generatePush(strtolower($prov['device_type']), $prov['device_token'], $message,$key);
                    endforeach;
                    return $result;
                else:
                    return $result;
                endif;
            else:
                return false;
            endif;
        } else {
            return false;
        }
    }

    
 

    

    public function createCustomerJobPosting($data) {
       // pre($data); die();
        $image = array();
        $data1['user_id'] = $data['user_id'];
        unset($data['user_id']);
        $data1['main_cat_id'] = $data['main_cat_id'];
        unset($data['main_cat_id']);
        $data1['sub_cat_id'] = $data['sub_cat_id'];
        unset($data['sub_cat_id']);
        $data1['promocode'] = $data['promocode'];
        unset($data['promocode']);
        $data1['imidiate_callout'] = $data['imidiate_callout'];
        //unset($data['imidiate_callout']);
        $data1['date'] = date("Y-m-d", strtotime(str_replace('/', '-', $data['date'])));
        //unset($data['date']);
        $data1['time'] = date("H:i:s", strtotime($data['time']));
        //unset($data['date']);
        $data1['create_date'] = date('Y-m-d H:i:s');

        //$this->checkTime($data);
        //$data1['time'] == date('H:i:s');
        
        $ts1 = strtotime(str_replace('/', '-', $data1['time']));
        $ts2 = strtotime(str_replace('/', '-', date('H:i:s')));
        $diff = abs($ts1 - $ts2) / 3600;
        $hrs = explode('.', $diff);
        //pre($hrs[0]); die();
        
   // if($data1['imidiate_callout'] == 0 && $hrs[0]>3){ 
    if($data1['imidiate_callout'] == 0){ 
        //die('udi');
        $func = $this->calloutcheckdata($data1,$data);
        return $func;
    }else if($data1['imidiate_callout'] == 1) {
        $func = $this->calloutcheckdata($data1,$data);
        return $func;
        }else{
            return 0;
        }
    }
    
  
public function calloutcheckdata24March2017($data1,$data) {
    
    //pre($data1);die("ufgu");
    unset($data['imidiate_callout']);
    unset($data['date']);
    unset($data['time']);
    $query = $this->db->insert('customer_job', $data1);
    
        $id = $this->db->insert_id();
        if (isset($id) && $id > 0) {
            $data['job_id'] = $id;

            switch ($data1['main_cat_id']) {
                case 1:
                    $query = $this->db->insert('customer_job_household_relation', $data);
                    break;
                case 2:
                    $query = $this->db->insert('customer_job_outdoor_relation', $data);
                    break;
                case 3:
                    $query = $this->db->insert('customer_job_personal_relation', $data);
                    break;
                case 4:
                    for ($i = 1; $i <= $data['items_to_move']; $i++) {
                        $image[] = $this->uploadfile('image_' . $i);
                    }
                    $data['image'] = implode(",", $image);
                    $query = $this->db->insert('customer_job_transport_relation', $data);
                    break;
                default:
                    return false;
                    break;
            }
            $id1 = $this->db->insert_id();
            if (isset($id1) && $id1 > 0):
                $result = $this->get_customer_jobdetail_by_id($id);
                if ($data1['imidiate_callout'] == 1):
                    $pros_data = $this->get_immidiate_callout_provider_by_jobid($id);
                    $result['total'] = count($pros_data);
                    $val1 = $this->get_immidiate_callout_provider_by_jobid_available($id);
                    $result['available'] = $val1 != NULL ? $val1 : 0;
                    $val2 = $this->get_immidiate_callout_provider_by_jobid_busy($id);
                    $result['busy'] = $val2 != NULL ? $val2 : 0;
                    foreach ($pros_data as $prov):
                        $message1 = "A new job started for immidiate callout";
                        $message['job_id'] = $id;
                        $message['main_cat_id'] = $data1['main_cat_id'];
                        $message['prov_id'] = $prov['prov_id'];
                        $message['notification_type'] = 10;
                        $message['message'] = $message1;
                        $key =1;
                        generatePush(strtolower($prov['device_type']), $prov['device_token'], $message,$key);
                    endforeach;
                    return $result;
                else:
                    return $result;
                endif;
            else:
                return false;
            endif;
        }else {
            return false;
        } 
        
}
    public function calloutcheckdata($data1,$data) {
    
    //pre($data1);die("ufgu");
    unset($data['imidiate_callout']);
    unset($data['date']);
    unset($data['time']);
    $query = $this->db->insert('customer_job', $data1);
    
        $id = $this->db->insert_id();
        if (isset($id) && $id > 0) {
            $data['job_id'] = $id;

            switch ($data1['main_cat_id']) {
                case 1:
                    $query = $this->db->insert('customer_job_household_relation', $data);
                    break;
                case 2:
                    $query = $this->db->insert('customer_job_outdoor_relation', $data);
                    break;
                case 3:
                    $query = $this->db->insert('customer_job_personal_relation', $data);
                    break;
                case 4:
                    for ($i = 1; $i <= $data['items_to_move']; $i++) {
                        $image[] = $this->uploadfile('image_' . $i);
                    }
                    $data['image'] = implode(",", $image);
                    $query = $this->db->insert('customer_job_transport_relation', $data);
                    break;
                default:
                    return false;
                    break;
            }
            $id1 = $this->db->insert_id();
            if (isset($id1) && $id1 > 0):
                $result = $this->get_customer_jobdetail_by_id($id);
                if ($data1['imidiate_callout'] == 1):
                    $pros_data = $this->get_immidiate_callout_provider_by_jobid($id);
                    $result['total'] = count($pros_data);
                    $val1 = $this->get_immidiate_callout_provider_by_jobid_available($id);
                    $result['available'] = $val1 != NULL ? $val1 : 0;
                    $val2 = $this->get_immidiate_callout_provider_by_jobid_busy($id);
                    $result['busy'] = $val2 != NULL ? $val2 : 0;
                    foreach ($pros_data as $prov):
                        $message1 = "A new job started for immidiate callout";
                        $message['job_id'] = $id;
                        $message['main_cat_id'] = $data1['main_cat_id'];
                        $message['prov_id'] = $prov['prov_id'];
                        $message['notification_type'] = 10;
                        $message['message'] = $message1;
                        $key =1;
                        generatePush(strtolower($prov['device_type']), $prov['device_token'], $message,$key);
                    endforeach;
                    return $result;
                    
                else:
                    $pros_data = $this->get_immidiate_callout_provider_by_jobid($id);
                    $result['total'] = count($pros_data);
                    $val1 = $this->get_immidiate_callout_provider_by_jobid_available($id);
                    $result['available'] = $val1 != NULL ? $val1 : 0;
                    $val2 = $this->get_immidiate_callout_provider_by_jobid_busy($id);
                    $result['busy'] = $val2 != NULL ? $val2 : 0;
                    foreach ($pros_data as $prov):
                        $message1 = "A new job started for scheduled job";
                        $message['job_id'] = $id;
                        $message['main_cat_id'] = $data1['main_cat_id'];
                        $message['prov_id'] = $prov['prov_id'];
                        $message['notification_type'] = 10;
                        $message['message'] = $message1;
                        $key =1;
                        generatePush(strtolower($prov['device_type']), $prov['device_token'], $message,$key);
                    endforeach;
                    return $result;
                
                endif;
            else:
                return false;
            endif;
        }else {
            return false;
        } 
        
}
    
    
    function get_immidiate_callout_provider_by_jobid_available($job_id) {
	//$job_id = "310";
        $job_details = $this->get_customer_jobdetail_by_id($job_id);
        $lat = $job_details['main_cat_id'] == 4 ? $job_details['pickup_location_lat'] : $job_details['location_lat'];
        $long = $job_details['main_cat_id'] == 4 ? $job_details['pickup_location_long'] : $job_details['location_long'];
//echo $lat."<br>";
//	echo $long; die();
        $this->db->select("status");
        $this->db->select("SQRT(POW(69.1 * (business_address_lat - " . $lat . "), 2) +   POW(69.1 * (" . $long . " - business_address_long) * COS(business_address_lat / 57.3), 2)) AS distance");
	$this->db->where('providers_subcat_relation.business_subtype', $job_details['sub_cat_id']);
        $this->db->where('immidiate_callout_radius >=', 'distance');
        $this->db->where('providers.status', 1);
        $this->db->where('providers.prov_id not in (select prov_id from job_status where status !=6)');
        $this->db->join('providers_subcat_relation', 'providers_subcat_relation.prov_id = providers.prov_id');
        $result = $this->db->get('providers')->num_rows(); 
         //pre($result);die;
        //echo $this->db->last_query();die;


        return (!empty($result)) ? $result : array();
    }

    function get_immidiate_callout_provider_by_jobid_busy($job_id) {
	//$job_id = "310";
        $job_details = $this->get_customer_jobdetail_by_id($job_id);
        $lat = $job_details['main_cat_id'] == 4 ? $job_details['pickup_location_lat'] : $job_details['location_lat'];
        $long = $job_details['main_cat_id'] == 4 ? $job_details['pickup_location_long'] : $job_details['location_long'];
	//echo $lat."<br>";
	//echo $long; die();
        $this->db->select(" status");
        $this->db->select(" SQRT(POW(69.1 * (business_address_lat - " . $lat . "), 2) +   POW(69.1 * (" . $long . " - business_address_long) * COS(business_address_lat / 57.3), 2)) AS distance ");
        $this->db->where('providers_subcat_relation.business_subtype', $job_details['sub_cat_id']);
        $this->db->where('immidiate_callout_radius >=', 'distance');
        $this->db->where('providers.status', 1);
        $this->db->where('providers.prov_id in (select prov_id from job_status where status !=6)');
        $this->db->join('providers_subcat_relation', 'providers_subcat_relation.prov_id = providers.prov_id');
        $result = $this->db->get('providers')->num_rows(); //echo $this->db->last_query();die;
	//pre($result);//die;
        //echo $this->db->last_query();die;


        return (!empty($result)) ? $result : array();
    }

    function get_immidiate_callout_provider_by_jobid($job_id) {

        //pre($job_id);die;
        
        $job_details = $this->get_customer_jobdetail_by_id($job_id);
        $lat = $job_details['main_cat_id'] == 4 ? $job_details['pickup_location_lat'] : $job_details['location_lat'];
        $long = $job_details['main_cat_id'] == 4 ? $job_details['pickup_location_long'] : $job_details['location_long'];
        $this->db->select(" providers.prov_id,concat(firstName,' ',lastName) as name");
        $this->db->select(" device_type , device_token");
        $this->db->select(" SQRT(POW(69.1 * (business_address_lat - " . $lat . "), 2) +   POW(69.1 * (" . $long . " - business_address_long) * COS(business_address_lat / 57.3), 2)) AS distance ");
        $this->db->where('providers_subcat_relation.business_subtype', $job_details['sub_cat_id']);
        $this->db->where('immidiate_callout_radius >=', ' distance');
        $this->db->where('providers.status', 1);
        $this->db->join('providers_subcat_relation', 'providers_subcat_relation.prov_id = providers.prov_id');
        $result = $this->db->get('providers')->result_array(); //echo $this->db->last_query();die;

        //pre($result);die;
        return (!empty($result)) ? $result : array();
    }

    function uploadfile($key) {
        // pre($_FILES);
        //die();
        $files = $_FILES[$key];
        //   /home/content/site/folders/filename.php                                    
        if ($files['error'] == 0) {
            $name = "transport/"; //.'img-' . date('Ymd') . date("h:i:sa");
            $file_name = rand(0, 100) . time() . basename($_FILES[$key]["name"]);
            $target_file = $name . $file_name;

            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file)) {
                $imgs = array('url' => '/' . $name, 'name' => $files['name']);
            }
        }
        return $file_name;
    }

    function get_sub_job_data($cat_id, $job_id) {
     //   pre($cat_id);
       // pre($job_id);die;
        switch ($cat_id) {
            case 1:
                $table = 'customer_job_household_relation';
                break;
            case 2:
                $table = 'customer_job_outdoor_relation';
                break;
            case 3:
                $table = 'customer_job_personal_relation';
                break;
            case 4:
                $table = 'customer_job_transport_relation';
                break;
            default:
                break;
        }

        if (isset($table)) {
            $result = $this->db->where('job_id', $job_id)
                            ->get($table)->row_array();
          //  echo $this->db->last_query($result);
           // pre($result);die;
            unset($result['id']);
            unset($result['job_id']);
        }
        return $result;
    }

    function get_customer_posted_jobs($user_id) {

        $result = $this->db->select("customer_job.*,main_category.name as main_category_name")
                        ->where('customer_job.user_id', $user_id)
                        ->where('customer_job.status', 0)
                        ->order_by('customer_job.date','desc')
                        ->order_by('customer_job.time','desc')
                        ->where("customer_job.id not in (select job_id from job_request where cust_id={$user_id} and status=1)")
                        ->join('main_category', 'main_category.id = customer_job.main_cat_id')
                        ->get('customer_job')->result_array();

        $result_modi = array();

        if (!empty($result)) {

            foreach ($result as $x) :

                $x['quotation_count'] = $this->db->select('id')
                                ->where('job_id', $x['id'])
                                ->get('quotations')->num_rows();

                $sub_name = $this->db->select('name')
                                ->where("sb_id", $x['sub_cat_id'])
                                ->get('sub_category')->row_array();
                $x['name'] = $sub_name['name'];

                $hours = (time() - strtotime($x['create_date'])) / (60 * 60);
                $x['is24Completed'] = ($hours >= 24) ? 1 : 0;

                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);

                if (!empty($result1)) {

                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }

            endforeach;
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }

    function get_customer_accepted_jobs($user_id) {
        $this->db->select("customer_job.*,main_category.name as main_category_name");
        $this->db->where('customer_job.user_id', $user_id);
        $this->db->where('customer_job.status', 3);
        $this->db->order_by('customer_job.date','desc');
        $this->db->order_by('customer_job.time','desc');
        $this->db->join('main_category', 'main_category.id = customer_job.main_cat_id');
        $result = $this->db->get('customer_job')->result_array();
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) :
                $x['quotation_count'] = $this->db->select('id')
                                ->where('job_id', $x['id'])
                                ->get('quotations')->num_rows();
                $job_status = $this->db->select("status")
                                ->where("job_id", $x['id'])
                                ->get("job_status")->row()->status;
                $x['job_status'] = $job_status != NULL ? $job_status : "";

                $sub_name = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row_array();
                $x['name'] = $sub_name['name'];
                $hours = (time() - strtotime($x['create_date'])) / (60 * 60);
                $x['is24Completed'] = ($hours >= 24) ? 1 : 0;
                $result1 = $this->get_sub_job_data($x['main_cat_id'], $x['id']);

                if (!empty($result1)) {

                    $result1['img_url'] = base_url('transport/');
                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = array_merge($x, $result1);
                }
            endforeach;
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }

    function get_customer_completed_jobs($user_id) {
        $result = $this->db->select("customer_job.id,customer_job.main_cat_id,customer_job.sub_cat_id,customer_job.date,customer_job.time,main_category.name as main_category_name")
                        ->where('customer_job.user_id', $user_id)
                        ->where('customer_job.status', 5)
                        ->order_by('customer_job.date','desc')
                        ->order_by('customer_job.time','desc')
                        ->join('main_category', 'main_category.id = customer_job.main_cat_id')
                        ->get('customer_job')->result_array();
        
        //pre($result);die;
        $result_modi = array();
        if (!empty($result)) {
            foreach ($result as $x) :

                $x['name'] = $this->db->select('name')->where("sb_id", $x['sub_cat_id'])->get('sub_category')->row()->name;
                $prov_id = $this->db->select("prov_id,concat(firstName,' ',lastName) as prov_name,IF(profile_img='','',concat('" . base_url('providerImages') . "','/',profile_img)) as prov_profileImage")
                                ->where("prov_id = (select prov_id from quotations where job_id = {$x['id']} and status =1)")
                                ->or_where("prov_id = (select prov_id from job_request where job_id = {$x['id']} and status =1)")
                                ->get('providers')->row_array();

                $x = array_merge($x, $prov_id);
                
             //  pre($x);die('rana');  
                if (!empty($x)) {

                    $x['date'] = str_replace("-", "/", date("d-M-Y", strtotime($x['date'])));
                    $x['time'] = date("h:i a", strtotime($x['time']));
                    $result_modi[] = $x;
                }
             //   pre($result_modi);die('rana_rahul'); 
            endforeach;
            
           //   pre($result_modi);die('rana_rahul'); 
        }
        return (!empty($result_modi)) ? $result_modi : array();
    }

    function get_customer_jobdetail_by_id02_03_17($job_id) {
        $this->db->where('id', $job_id);
        $result = $this->db->get('customer_job')->row_array();
        $result_modi = array();
        if (!empty($result)) {
            $result['provider_quotations'] = $this->db->select("quotations.quotation,providers.prov_id,concat(firstName,' ',lastName) as name,IF(profile_img='','',concat('" . base_url('providerImages') . "','/',profile_img)) as profileImage")
                            ->where('quotations.job_id', $result['id'])
                            ->join('providers', 'providers.prov_id=quotations.prov_id')
                            ->get('quotations')->result_array();

            $this->db->select("name as main_category_name");
            $this->db->where("id", $result['main_cat_id']);
            $result['main_category_name'] = $this->db->get("main_category")->row_array()['main_category_name'];

            $this->db->select("name");
            $this->db->where("sb_id", $result['sub_cat_id']);
            $result['name'] = $this->db->get("sub_category")->row_array()['name'];

            $hours = (time() - strtotime($result['create_date'])) / (60 * 60);
            $result['is24Completed'] = ($hours >= 24) ? 1 : 0;

            $result1 = $this->get_sub_job_data($result['main_cat_id'], $result['id']);
            if (!empty($result1)) {

                $result1['img_url'] = base_url('transport/');
                $result_modi = array_merge($result, $result1);
                $result_modi['date'] = str_replace("-", "/", date("d-M-Y", strtotime($result_modi['date'])));
                $result_modi['time'] = date("h:i a", strtotime($result_modi['time']));
            }
        }

        return (!empty($result_modi)) ? $result_modi : array();
    }

     function get_customer_jobdetail_by_id($job_id) {
	//echo $job_id; die();
        $this->db->where('id', $job_id);
        $result = $this->db->get('customer_job')->row_array();
	//pre($result); die();
        $result_modi = array();
        if (!empty($result)) {
            $result['provider_quotations'] = $this->db->select("quotations.quotation,providers.prov_id,concat(firstName,' ',lastName) as name,IF(profile_img='','',concat('" . base_url('providerImages') . "','/',profile_img)) as profileImage")
                            ->where('quotations.job_id', $result['id'])
                            ->join('providers', 'providers.prov_id=quotations.prov_id')
                            ->get('quotations')->result_array();

            $this->db->select("name as main_category_name");
            $this->db->where("id", $result['main_cat_id']);
            $result['main_category_name'] = $this->db->get("main_category")->row_array()['main_category_name'];

            $this->db->select("name");
            $this->db->where("sb_id", $result['sub_cat_id']);
            $result['name'] = $this->db->get("sub_category")->row_array()['name'];

            $hours = (time() - strtotime($result['create_date'])) / (60 * 60);
            $result['is24Completed'] = ($hours >= 24) ? 1 : 0;

            $result1 = $this->get_sub_job_data($result['main_cat_id'], $result['id']);
            if (!empty($result1)) {

                $result1['img_url'] = base_url('transport/');
                $result_modi = array_merge($result, $result1);
                $result_modi['date'] = str_replace("-", "/", date("d-M-Y", strtotime($result_modi['date'])));
                $result_modi['time'] = date("h:i a", strtotime($result_modi['time']));
            }
        }

        return (!empty($result_modi)) ? $result_modi : array();
    }

    
    
    
    function get_provider_by_jobid($job_id) {
        $result = array();
        $job_details = $this->get_customer_jobdetail_by_id($job_id);
        $lat = $job_details['main_cat_id'] == 4 ? $job_details['pickup_location_lat'] : $job_details['location_lat'];
        $long = $job_details['main_cat_id'] == 4 ? $job_details['pickup_location_long'] : $job_details['location_long'];
        $this->db->select(" providers.prov_id,concat(firstName,' ',lastName) as name,IF(profile_img='','',concat('" . base_url('providerImages') . "','/',profile_img)) as profileImage ");
        $this->db->select(" providers.business_category as main_cat_name , device_type , device_token,providers.status");
        $this->db->select(" providers_subcat_relation.business_subtype,providers_subcat_relation.business_subtype_name ");
        $this->db->select(" SQRT(POW(69.1 * (business_address_lat - " . $lat . "), 2) +   POW(69.1 * (" . $long . " - business_address_long) * COS(business_address_lat / 57.3), 2)) AS distance ");
        $this->db->where('providers_subcat_relation.business_subtype', $job_details['sub_cat_id']);
        $this->db->where('callout_radius >=', ' distance');
        $this->db->where('providers.status', 1);
        $this->db->where("providers.prov_id not in(select prov_id from job_request where job_id={$job_id}) ");
        $this->db->join('providers_subcat_relation', 'providers_subcat_relation.prov_id = providers.prov_id');
        $result1 = $this->db->get('providers')->result_array(); //echo $this->db->last_query();die;
        if (!empty($result1)):
            foreach ($result1 as $rs):
                $rs['avg_rating'] = $this->db->select_avg('rating', 'rating')
                                ->where('prov_id', $rs['prov_id'])
                                ->where("job_id in (select id from customer_job where sub_cat_id=" . $rs['business_subtype'] . ")")
                                ->get('provider_rating')->row()->rating;
                $rs['avg_rating'] = $rs['avg_rating'] != NULL ? intval($rs['avg_rating']) : 0;
                $rs["review"] = $this->db->select("provider_rating.id")
                                ->where('prov_id', $rs['prov_id'])
                                ->where("job_id in (select id from customer_job where sub_cat_id=" . $rs['business_subtype'] . ")")
                                ->get('provider_rating')->num_rows();
                $result[] = $rs;
            endforeach;
        endif;

        return (!empty($result)) ? $result : array();
    }

    function get_provider_details($id, $job_id ,$promocode) {
        $job_sub_cat = $this->db->select("sub_cat_id,user_id,main_cat_id")
                        ->where('id', $job_id)
                        ->get('customer_job')->row_array();

        $job_quotation = $this->db->select("quotation")
                        ->where('job_id', $job_id)
                        ->where('prov_id', $id)
                        ->get('quotations')->row_array();
        
        if (!empty($job_sub_cat)):
            $result = $this->db->select("providers.prov_id,concat(firstName,' ',lastName) as name,providers.desc_box as description,providers.business_type as business_type,IF(profile_img='','',concat('" . base_url('providerImages') . "','/',profile_img)) as profileImage ")
                            ->select(" providers.business_category as main_cat_name ")
                            ->select(" providers_subcat_relation.business_subtype,IF(providers_subcat_relation.business_subtype=NULL,'',providers_subcat_relation.business_subtype ) as business_subtype")
                            ->where('providers_subcat_relation.prov_id', $id)
                            ->where('providers_subcat_relation.business_subtype', $job_sub_cat['sub_cat_id'])
                            ->join('providers_subcat_relation', 'providers_subcat_relation.prov_id = providers.prov_id')
                            ->get('providers')->row_array();
            if (!empty($result)):
                if (isset($job_sub_cat['user_id']) && $job_sub_cat['user_id'] != "") {
                    $token = $this->db->select("payment_token")
                                    ->where("cust_id", $job_sub_cat['user_id'])
                                    ->get("customers")->row_array();
                }
                $result['payment_token'] = isset($token['payment_token']) ? $token['payment_token'] : "";
                $result['main_cat_id'] = isset($job_sub_cat['main_cat_id']) ? $job_sub_cat['main_cat_id'] : "";
                $result['vat'] = $this->db->select("vat")->where("id", 1)->get("setting")->row()->vat;
                $result['quotation'] = $job_quotation['quotation'] != NULL ? $job_quotation['quotation'] : 0;
                
                //$result['promocode'] = $this->db->select("name,status")->where("status", 1)->get("promocode")->result_array();
                //echo $promocode;
                //pre($result['promocode']); //die();
               // foreach ($result['promocode'] as $promo){                   
                   // if($promo['name'] == $promocode){
                        //echo "udi";
                        //$result['callout_charge'] = 0;
                        //break;
                   // }else{
                       // $result['callout_charge'] = $this->db->select("callout_charge")->where("id", 1)->get("setting")->row_array();
                    //}
               // }
               // pre( $result['promocode'][1]);die;
                $result['callout_charge'] = $this->db->select("callout_charge")->where("id", 1)->get("setting")->row_array();
                $this->db->select("name,status");
                $this->db->where("status", 1);
                $this->db->where("name",$promocode );
                $exist = $this->db->get("promocode")->row_array();                
                if($exist){
                    $result['callout_charge'] = 0;
                }                
                $result['avg_rating'] = $this->db->select_avg('rating', 'rating')
                                ->where('prov_id', $id)
                                ->where("job_id in (select id from customer_job where sub_cat_id=" . $job_sub_cat['sub_cat_id'] . ")")
                                ->get('provider_rating')->row()->rating;
                $result['avg_rating'] = $result['avg_rating'] != NULL ? intval($result['avg_rating']) : 0;
                $result["review"] = $this->db->select("provider_rating.*")
                                ->select("customers.name,IF(customers.profile_img='','',CONCAT('" . base_url('customerImages') . "','/',profile_img)) as profile_img")
                                ->join("customers", "provider_rating.cust_id=customers.cust_id")
                                ->where('prov_id', $id)
                                ->where("job_id in (select id from customer_job where sub_cat_id=" . $job_sub_cat['sub_cat_id'] . ")")
                                ->get('provider_rating')->result_array();
            endif;
        endif;
        return (!empty($result)) ? $result : array();
    }

    function sent_job_request($document) {
        
      $doc['date']                 = $document['date'];
      unset($document['date']);
      $doc['time']                 = $document['time'];
      unset($document['time']);
      $doc['create_date']          = $document['create_date'];
      unset($document['create_date']);
       
        //pre($doc);die;
        $this->db->insert('job_request', $document);
        $id = $this->db->insert_id();
        if ($id > 0):
            $main_cat = $this->db->select("main_cat_id")
                            ->where("id", $document['job_id'])
                            ->get("customer_job")->row()->main_cat_id;
            
            $this->db->where("id", $document['job_id']);                   
            $result =  $this->db->update('customer_job', $doc);
            
            
           // echo $this->db->last_query();
           // pre($doc);die;

            $provider = $this->db->select("CONCAT(firstName,' ',lastName) as name,device_type,device_token")
                            ->where("prov_id", $document['prov_id'])
                            ->get("providers")->row_array();

            $message1 = "Hi {$provider['name']} , You are requested to new job.";
            $message['job_id'] = $document['job_id'];
            $message['prov_id'] = $document['prov_id'];
            $message['main_cat_id'] = $main_cat;
            $message['notification_type'] = 2;
            $message['message'] = $message1;
            $key=1;
            generatePush(strtolower($provider['device_type']), $provider['device_token'], $message,$key);
        endif;
        $this->db->where('id', $id);
        $details = $this->db->get('job_request')->row_array();
        return !empty($details) ? $details : false;
    }

    function acceptedJobDetailsCustomer($id) {
        $result = $this->db->select('customers.cust_id,customer_job.id,customer_job.main_cat_id,customer_job.sub_cat_id,customers.payment_token')
                        ->select("customers.name as cust_name,IF(profile_img='','',concat('" . base_url('customerImages') . "','/',profile_img)) as cust_profileImage")
                        ->select("customer_job.*")
                        ->where('id', $id)                 
                        ->where_in('status', array('5','3','0'))
                        ->join('customers', 'customers.cust_id=customer_job.user_id')
                        ->get('customer_job')->row_array();
     //  echo $this->db->last_query($result);
       //pre($result);die;
        
        if (!empty($result)) {
            $result1 = $this->get_sub_job_data($result['main_cat_id'], $result['id']);
            $result = array_merge($result, $result1);
     
            
        // for get initial data i am updating  quotations   status  // 27 -june -2017
        if (!empty($result)) {           
                 $query = $this->db->set("status", 1)->where("job_id", $id)->update("quotations");
        }
        //    pre($query);die('rahul');
            $quote_result = $this->db->select("quotation")
                            ->where('job_id', $id)
                         //   ->where('status', 1)
                            ->get('quotations')->row_array();
            $result['quotation'] = $quote_result['quotation'] != null ? $quote_result['quotation'] : '';

            $job_status = $this->db->select("status")
                            ->where("job_id", $id)
                            ->get("job_status")->row()->status;
            $result['job_status'] = $job_status != NULL ? $job_status : "";
            $reschedule = $this->db->where(array("job_id" => $id, "usertype" => 2))->get("reschedule_job_relation")->row_array();
            if (isset($reschedule) && !empty($reschedule)):
                $reschedule['date'] = str_replace("-", "/", date("d-M-Y", strtotime($reschedule['date'])));
                $reschedule['time'] = date("h:i a", strtotime($reschedule['time']));
            endif;
            $result['reschedule'] = $reschedule != NULL ? $reschedule : "";
            $prov_id = $this->db->select("prov_id,concat(firstName,' ',lastName) as prov_name,IF(profile_img='','',concat('" . base_url('providerImages') . "','/',profile_img)) as prov_profileImage")
                            ->select('concat(landlineCode,business_phone) as phone,concat(countryCode,mobile) as mobile,email,vehicle_make_yr,vehicle_regn,business_address_lat,business_address_long')
                            ->where("prov_id in (select prov_id from quotations where job_id = {$result['id']} and status =1)")
                            ->or_where("prov_id in (select prov_id from job_request where job_id = {$result['id']} and status =1)")
                            ->get('providers')->row_array();

            $result = array_merge($result, $prov_id); //pre($this->db->last_query());die;

            $t_amount = $this->db->select("total_amount")
                            ->where("job_id", $id)
                            ->get("invoice")->row()->total_amount;
            $result['total_amount'] = isset($t_amount) ? $t_amount : 0;
            
            $addtional_amount = $this->db->select("addi_charges")
                            ->where("job_id", $id)
                            ->get("invoice")->row()->addi_charges;
            $result['addtional_amount'] = isset($addtional_amount) ? $addtional_amount : 0;

            $setting = $this->db->select("vat,callout_charge")->where("id", 1)->get("setting")->row_array();
            if (empty($setting)) {
                $final_quote = array("vat" => "", "callout_charge" => 0);
            }
            $result = array_merge($result, $setting);
            $result['final_quote'] = $this->db->select("total")->where('job_id', $id)->get('final_quotation')->row()->total;
           // if(empty($result['final_quote']) and $result['final_quote'] == ""){
           // $result['final_quote'] =  0;
            //}
            $result['timeTaken'] = $this->db->select("time")->where('job_id', $id)->get('final_quotation')->row()->time;
             if(empty($result['timeTaken'])) {
            $result['timeTaken'] = isset($result['timeTaken']) ? $result['timeTaken'] : 0;
             }
             
            $result['materialPrice'] = $this->db->select("materialPrice")->where('job_id', $id)->get('final_quotation')->row()->materialPrice;
             if(empty($result['materialPrice'])) {
            $result['materialPrice'] = isset($result['materialPrice']) ? $result['materialPrice'] : 0;
             } 
             
            $result['additional_desc'] = $this->db->select("additional_desc")->where('job_id', $id)->get('final_quotation')->row()->additional_desc;
            if(empty($result['additional_desc'])) {
            $result['additional_desc'] = isset($result['additional_desc']) ? $result['additional_desc'] : "";
             }
            $result['additional_desc'] = isset($result['additional_desc']) ? $result['additional_desc'] : 0;
            
            $result['final_quote'] = $result['final_quote'] != NULL ? $result['final_quote'] : 0;
            if (in_array($result['main_cat_id'], array(1, 2))) {
                $result['applied_vat'] = $this->db->select("vat")->where('job_id', $id)->get('final_quotation')->row()->vat;
            if(empty($result['applied_vat']))
                $result['applied_vat'] = isset($result['applied_vat']) ? $result['applied_vat'] : 0;
                }
        }
        return ($result) ? $result : false;
    }

    function rate_the_provider($document) {
        $query = $this->db->insert('provider_rating', $document);
        $id = $this->db->insert_id();
        $this->db->where('id', $id);
        $details = $this->db->get('provider_rating')->row_array();
        return !empty($details) ? $details : false;
    }

    function get_customer_completed_jobdetail($id) {
        $result = $this->db->select('customers.cust_id,customer_job.id,customer_job.main_cat_id,customer_job.sub_cat_id,customer_job.time,customer_job.date')
                        ->select("customers.name as cust_name,IF(profile_img='','',concat('" . base_url('customerImages') . "','/',profile_img)) as cust_profileImage")
                        ->where('id', $id)
                        ->where('status', 5)
                        ->join('customers', 'customers.cust_id=customer_job.user_id')
                        ->get('customer_job')->row_array();
        if (!empty($result)) {
            $result12 = $this->get_sub_job_data($result['main_cat_id'], $result['id']);
            // $result = array_merge($result, $result1);
            $result['main_cat'] = $this->db->select("name")
                            ->where("id", $result['main_cat_id'])
                            ->get("main_category")->row()->name;
            $result['sub_cat'] = $this->db->select("name")
                            ->where("sb_id", $result['sub_cat_id'])
                            ->get("sub_category")->row()->name;

            $customer_payment_details = $this->db->select("additional_payment_amount")
                                       ->where('job_id', $id)
                                       ->get('customer_payment_details')->row_array();
               //pre($customer_payment_details);die;
               
               if ($customer_payment_details['additional_payment_amount'] != ""){
               $result['additional_payment_amount'] = $customer_payment_details['additional_payment_amount'];
               }
               else{
                   $result['additional_payment_amount'] = '0';
               }
      
            
            $quote_result = $this->db->select("prov_id,addi_job_desc,time,total_amount")
                            ->where('job_id', $id)
                            ->get('invoice')->row_array();
            $result['final_quotation'] = $quote_result['total_amount'];
            $result['description'] = $result12['job_description'];

            $result['timeinhours'] = $this->db->select("time")
                            ->where('job_id', $id)
                            ->get('final_quotation')->row()->time;

            $prov_id = $this->db->select("prov_id,concat(firstName,' ',lastName) as prov_name,IF(profile_img='','',concat('" . base_url('providerImages') . "','/',profile_img)) as prov_profileImage,CONCAT(countryCode,mobile) as mobile,CONCAT(landlineCode,business_phone) as business_phone")
                            ->where("prov_id = {$quote_result['prov_id']}")
                            ->get('providers')->row_array();
            if (empty($prov_id)):
                $prov_id = array("prov_name" => "", "prov_profileImage" => "");
            endif;
            $result = array_merge($result, $prov_id); //pre($this->db->last_query());die;
            $rating_done = $this->db->select("id")
                            ->where("job_id", $id)
                            ->get("provider_rating")->num_rows();
            $result['provider_rating_done'] = $rating_done > 0 ? 1 : 0;
            $feedback = $this->db->select("rating,feedback,create_date")
                            ->where("job_id", $id)
                            ->where("prov_id", $result['prov_id'])
                            ->where("cust_id", $result['cust_id'])
                            ->get("customer_rating")->row_array();
            if (empty($feedback)):
                $feedback = array("rating" => "", "feedback" => "", "create_date" => "");
            endif;
            $result = array_merge($result, $feedback);
            if (!empty($result)):
                $result['date'] = str_replace("-", "/", date("d-M-Y", strtotime($result['date'])));
                $result['time'] = date("h:i a", strtotime($result['time']));
            endif;
        }
        return (!empty($result)) ? $result : false;
    }

    function get_final_quotation($id) {
        $details = $this->db->where("job_id", $id)->get("final_quotation")->row_array();
        if (!empty($details)):

            $job = $this->db->select("main_cat_id,sub_cat_id")
                            ->where("id", $id)
                            ->get("customer_job")->row_array();
            $details['main_cat_name'] = $job['main_cat_id'];
            $details['main_cat'] = $this->db->select("name")
                            ->where("id", $job['main_cat_id'])
                            ->get("main_category")->row()->name;

            $details['sub_cat_id'] = $job['sub_cat_id'];
            $details['sub_cat_name'] = $this->db->select("name")
                            ->where("sb_id", $job['sub_cat_id'])
                            ->get("sub_category")->row()->name;
            $details['sub_cat_name'] = $details['sub_cat_name'] == NULL ? "" : $details['sub_cat_name'];

            $details['job_status'] = $this->db->select("status")
                            ->where("id", $id)
                            ->get("job_status")->row()->status;
            $details['job_status'] = $details['job_status'] == NULL ? "" : $details['job_status'];
        endif;

        return !empty($details) ? $details : false;
    }

    function set_action_final_quotation($data) {
        $provider = $this->db->select("CONCAT(firstName,' ',lastName) as name,device_type,device_token")
                        ->where("prov_id", $data['prov_id'])
                        ->get("providers")->row_array();
        if ($data['status'] == 1):
            $query = $this->db->set("status", 1)->where("job_id", $data['job_id'])->update("final_quotation");
            $query12 = $this->db->set("status", 6)->where("job_id", $data['job_id'])->update("job_status");
            $message1 = "Hi {$provider['name']} , Your final quotaton has been accepted.";
            $message['notification_type'] = 311;
        else:
            $prov_data = array();
            $CI = & get_instance();
            $CI->load->model('customers_model');
            $immid_job = $this->db->select("id")->where("imidiate_callout", 1)->get("customer_job")->result_array();
            foreach ($immid_job as $id):

                $val1 = $CI->customers_model->get_immidiate_callout_provider_by_jobid_available($id['id']);
                $avail = $val1 != NULL ? $val1 : 0;
                $val2 = $CI->customers_model->get_immidiate_callout_provider_by_jobid_busy($id['id']);
                $busy = $val2 != NULL ? $val2 : 0;
                $prov_data["jobData_" . $id['id']] = array("busy" => "$busy", "calling" => "$avail");

            endforeach;
            ##############################curl firebase##################
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://bazinga-fe8be.firebaseio.com/push_thread.json",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => json_encode($prov_data),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            ##############################curl firebase##################
            $this->db->delete('final_quotation', array('job_id' => $data['job_id'], 'prov_id' => $data['prov_id']));
            $this->db->delete('quotations', array('job_id' => $data['job_id'], 'prov_id' => $data['prov_id']));
            $this->db->delete('job_request', array('job_id' => $data['job_id'], 'prov_id' => $data['prov_id']));
            $this->db->delete('job_status', array('job_id' => $data['job_id'], 'prov_id' => $data['prov_id']));
            $query = $this->db->set("status", 1)->where("id", $data['job_id'])->update("customer_job");
            $message1 = "Hi {$provider['name']} , Your final quotaton has been rejected.";
            $message['notification_type'] = 312;
        endif;
        $main_cat_id = $this->db->select("main_cat_id")->where("id", $data['job_id'])->get("customer_job")->row()->main_cat_id;
        $message['job_id'] = $data['job_id'];
        $message['main_cat_id'] = $main_cat_id;
        $message['prov_id'] = $data['prov_id'];
        $message['message'] = $message1;
        $key =1;
        generatePush(strtolower($provider['device_type']), $provider['device_token'], $message,$key);
        return ($query) ? true : false;
    }

    function cancel_job($document) {
        $job = $this->db->select("main_cat_id,status")->where('id', $document['job_id'])->get("customer_job")->row();
        $status = $job->status;
        $doc = array();
        $document['user_type'] = 1;
        $doc['id'] = $document['job_id'];
        $doc['status'] = 1;
        $this->db->where('id', $document['job_id']);
        $query1 = $this->db->update('customer_job', $doc); //pre($document);die;
        $query2 = $this->db->insert('cancel_job_relation', $document);
        $this->db->where(array("job_id" => $document['job_id'], "status" => 1))->delete("job_request");
        $this->db->where(array("job_id" => $document['job_id'], "status" => 1))->delete("quotations");

        if ($status == 3) {
            $provider = $this->db->select("CONCAT(firstName,' ',lastName) as name,device_type,device_token")
                            ->where("prov_id IN(select prov_id from job_request where job_id='" . $document['job_id'] . "' and status='1' union select job_id from quotations where job_id='" . $document['job_id'] . "' and status ='1')")
                            ->get("providers")->row_array();
            if ($provider != '' && $provider != NULL) {
                $message1 = "Hi {$provider['name']} , Your job has been cancelled by customer.";
                $message['job_id'] = $document['job_id'];
                $message['main_cat_id'] = $job->main_cat_id;
                $message['reason'] = $document['reason'];
                $message['notification_type'] = 52;
                $message['message'] = $message1;
                $key =1;
            
                generatePush(strtolower($provider['device_type']), $provider['device_token'], $message,$key);
            }
        }
        return ($query1 && $query2) ? true : false;
    }

    function re_schedule_job($document) {
       // pre($document);die;
        $status = $this->db->select("main_cat_id,status")->where('id', $document['id'])->get("customer_job")->row();
      // pre($status);die;
        $document['date'] = date("Y-m-d", strtotime(str_replace('/', '-', $document['date'])));
        $document['time'] = date("H:i:s", strtotime($document['time']));
        if ($status->status == 0) {
            $this->db->where('id', $document['id']);
            $query = $this->db->update('customer_job', $document);
        }

//        $pro = $this->db->select("*")->where('job_id', $document['id'])->get("job_status")->row();
        $pro = $this->db->select("*")->where('job_id', $document['id'])->get("quotations")->row();
         //  echo $this->db->last_query();die;
            //  pre($pro);die;
        if ($status->status == 3) {
            //die('udi');
                   
            $document['usertype'] = 1;
            $document['job_id'] = $document['id'];
            unset($document['id']);
            $query = $this->db->insert("reschedule_job_relation", $document);
            
           
            
            $provider = $this->db->select("CONCAT(firstName,' ',lastName) as name,device_type,device_token")
                            //->where("prov_id IN(select prov_id from job_request where job_id='" . $document['job_id'] . "' and status='1' union select job_id from quotations where job_id='" . $document['job_id'] . "' and status ='1')")   
                           ->where('prov_id',$pro->prov_id)
                    ->get("providers")->row_array();
         //  pre($provider); die();
            if ($provider != '' && $provider != NULL) {
                $message1 = "Hi {$provider['name']} , Your job has been rescheduled by customer.";
                $message['job_id'] = $document['job_id'];
                $message['main_cat_id'] = $status->main_cat_id;
                //$message['main_cat_name'] = $main_cat;
                //$message['sub_cat_name'] = $sub_cat;
                $message['date'] = str_replace("-", "/", date("d-M-Y", strtotime($document['date'])));
                $message['time'] = date("h:i a", strtotime($document['time']));
                $message['notification_type'] = 53;
                $message['message'] = $message1;
                $key = 1 ;
                //echo $key; die();
                generatePush(strtolower($provider['device_type']), $provider['device_token'], $message,$key);
            }
        }

        //$id = $document['id'];
        return ($query) ? true : false;
    }

    function set_reschedule_actionByCustomer($data) {

        $provider = $this->db->select("CONCAT(firstName,' ',lastName) as name,device_type,device_token")
                        ->where("prov_id IN(select prov_id from job_request where job_id='" . $document['id'] . "' and status='1' union select job_id from quotations where job_id='" . $document['id'] . "' and status ='1')")
                        ->get("providers")->row_array();

        if ($data['status'] == 1):
            $job = $this->db->where("job_id", $data['job_id'])->get("reschedule_job_relation")->row_array();
            $data1['date'] = $job['date'];
            $data1['time'] = $job['time'];
            $query = $this->db->update('customer_job', $data1, array('id' => $data['job_id']));
            $this->db->where("job_id", $data['job_id'])->delete("reschedule_job_relation");
            $message1 = "Hi {$provider['name']} , Your reschedule request has been accepted.";
            $message['notification_type'] = 621;
        else:
            $query = $this->db->set("status", 0)->where('id', $data['job_id'])->update('customer_job');
            $this->db->where("job_id", $data['job_id'])->delete("reschedule_job_relation");
            $this->db->where(array("job_id" => $data['job_id'], "status" => 1))->delete("job_request");
            $this->db->where(array("job_id" => $data['job_id'], "status" => 1))->delete("quotations");
            $message1 = "Hi {$provider['name']} , Your reschedule request has been rejected.";
            $message['notification_type'] = 622;
        endif;

        $message['job_id'] = $data['job_id'];
        $message['message'] = $message1;
        $key = 1 ;

        generatePush(strtolower($provider['device_type']), $provider['device_token'], $message,$key);
        return ($query) ? true : false;
    }

    function finish_immidiate_callout($id) {
        $this->db->set('imidiate_callout', 0);
        $this->db->where('id', $id);
        $this->db->where('status', "3");
        $query = $this->db->update('customer_job');
        if ($query):
            $count = $this->db->select("id")->where("job_id", $id)->get("quotations")->num_rows();
            return $count;
        else:
            return false;
        endif;
    }

    function save_payment_details($data) {
        if (isset($data) && !empty($data)) {
            if (isset($data['payment_token'])) {
                $cust_id = $this->db->select("user_id")
                                ->where("id", $data['job_id'])
                                ->get("customer_job")->row()->user_id;
                if (isset($cust_id)) {
                    $this->db->set("payment_token", $data['payment_token'])
                            ->where("cust_id", $cust_id)
                            ->update("customers");
                }
                unset($data['payment_token']);
            }
            $prov_id = $data['prov_id'];
            unset($data['prov_id']);
            $data['create_date'] = date("Y-m-d H:i:s");
            $this->db->insert("customer_payment_details", $data);
            $id = $this->db->insert_id();
            if ($id > 0) {
                $this->db->set('status', 1)
                        ->where(array('prov_id' => $prov_id, 'job_id' => $data['job_id']))
                        ->update("quotations");
                $this->db->set('status', 3)
                        ->where(array('id' => $data['job_id']))
                        ->update("customer_job");
                return true;
            }
        }
        return false;
    }

    function update_payment_details($data) {
        if (isset($data['payment_token'])) {
            $cust_id = $this->db->select("user_id")
                            ->where("id", $data['job_id'])
                            ->get("customer_job")->row()->user_id;
            if (isset($cust_id)) {
                $this->db->set("payment_token", $data['payment_token'])
                        ->where("cust_id", $cust_id)
                        ->update("customers");
            }
            unset($data['payment_token']);
        }
        if (isset($data) && !empty($data)) {
            $query = $this->db->where("job_id", $data['job_id'])
                    ->update("customer_payment_details", $data);
            if (isset($data['additional_payment_amount']) && $data['additional_payment_amount'] != "") {
                $this->db->set("status", 5)
                        ->where("id", $data['job_id'])
                        ->update("customer_job");
                $this->db->set("status", 9)
                        ->where("job_id", $data['job_id'])
                        ->update("job_status");
            } else {
                $this->db->set("status", 4)
                        ->where("job_id", $data['job_id'])
                        ->update("job_status");
            }
            if ($query) {
                return true;
            }
        }
        return false;
    }

    public function check_payment_status($data) {
        $this->db->select("id");
        $this->db->where("job_id", $data['job_id']);
        if ($data['transaction_type'] == 1):
            $this->db->where("quote_payment_transaction_id!=", "");
        elseif ($data['transaction_type'] == 2):
            $this->db->where("callout_payment_transaction_id!=", "");
        elseif ($data['transaction_type'] == 3):
            $this->db->where("additional_payment_transaction_id!=", "");
        endif;
        $query_result = $this->db->get("customer_payment_details")->num_rows();
        return $query_result > 0 ? true : false;
    }

    public function savePaymentData($jdata, $pdata) {

        $flag = 0;
        $this->db->set("payment_token", $jdata['token'])
                ->where("cust_id", $jdata['cust_id'])
                ->update("customers");

        if ($jdata['transaction_type'] == 1):
            $data1 = array("job_id" => $jdata['job_id'], "cust_id" => $jdata['cust_id'], "prov_id" => $jdata['prov_id'], "quote_payment_transaction_id" => $pdata['id'], "quote_payment_amount" => $pdata['amount'], "quote_payment_date" => date('Y-m-d H:i:s'), "vat" => $jdata['vat']);
            $data1['qpti_merchant_id'] = $pdata['merchantTransactionId'];

            $this->db->insert("customer_payment_details", $data1);
            $id = $this->db->insert_id();
            if ($id > 0) :
                $flag = 1;
                $this->db->set('status', 1)
                        ->where(array('prov_id' => $jdata['prov_id'], 'job_id' => $jdata['job_id']))
                        ->update("quotations");
                $this->db->set('status', 3)
                        ->where(array('id' => $jdata['job_id']))
                        ->update("customer_job");
                ############################################################
                $jobdata = $this->db->where("id", $jdata['job_id'])->get("customer_job")->row_array();

                $provider_name = $this->db->select("CONCAT(firstName,' ',lastName) as name,device_type,device_token")
                                ->where("prov_id", $jdata['prov_id'])
                                ->get("providers")->row_array();

                $quote = $this->db->select("quotation")
                                ->where(array('prov_id' => $jdata['prov_id'], 'job_id' => $jdata['job_id']))
                                ->get("quotations")->row()->quotation;
                $quote = ($quote == NULL || $quote == "") ? 0 : $quote;

                $message1 = "Hi {$provider_name['name']} , Your job quotation has been accepted.";
                $message['job_id'] = $jdata['job_id'];
                $message['main_cat_id'] = $jobdata['main_cat_id'];
                $message['quotation'] = $quote;
                $message['prov_id'] = $jdata['prov_id'];
                $message['notification_type'] = 5;
                $message['message'] = $message1;
                $key=1;
            
                generatePush(strtolower($provider_name['device_type']), $provider_name['device_token'], $message,$key);


            ############################################################
            endif;
        elseif ($jdata['transaction_type'] == 2):
            $data2 = array("job_id" => $jdata['job_id'], "callout_payment_transaction_id" => $pdata['id'], "callout_payment_amount" => $pdata['amount'], "callout_payment_date" => date('Y-m-d H:i:s'));
            $data2['cpti_merchant_id'] = $pdata['merchantTransactionId'];
            $query = $this->db->where("job_id", $jdata['job_id'])
                    ->update("customer_payment_details", $data2);
            if ($query):
                $flag = 1;
                $this->db->set("status", 4)
                        ->where("job_id", $jdata['job_id'])
                        ->update("job_status");
                $msg = "Customer has paid callout charges.";
                $this->sendPushNotification($jdata['job_id'], $jdata['prov_id'], $msg, 4, 24);
            endif;

            elseif ($jdata['transaction_type'] == 3):

            $data3 = array("job_id" => $jdata['job_id'], "additional_payment_transaction_id" => $pdata['id'], "additional_payment_amount" => $pdata['amount'], "additional_payment_date" => date('Y-m-d H:i:s'), "vat" => $jdata['vat']);
            $data3['apti_merchant_id'] = $pdata['merchantTransactionId'];
            $query = $this->db->where("job_id", $jdata['job_id'])
                    ->update("customer_payment_details", $data3);
            if ($query):
                $flag = 1;
                $this->db->set("status", 5)
                         ->where("id", $jdata['job_id'])
                         ->update("customer_job");
                $this->db->set("status", 9)
                         ->where("job_id", $jdata['job_id'])
                         ->update("job_status");
                $msg = "Final payment paid by the Customer";
                $this->sendPushNotification($jdata['job_id'], $jdata['prov_id'], $msg, 9, 29);
            endif;
            endif;
            return ($flag == 1) ? true : false;
    }

    public function sendPushNotification($job_id, $prov_id, $msg, $status, $type) {
        $result2 = $this->db->select("main_cat_id")
                        ->where('customer_job.id', $job_id)
                        ->get('customer_job')->row_array();
        $result1 = $this->db->select("device_type,device_token")
                        ->where("prov_id", $prov_id)
                        ->get("providers")->row_array();
        if (!empty($result1)):
            $message['notification_type'] = $type;
            $message1 = $msg;
            $message['job_id'] = $job_id;
            $message['main_cat_id'] = $result2['main_cat_id'];
            $message['prov_id'] = $prov_id;
            $message['job_status'] = $status;
            $message['message'] = $message1;
            $key=1;
            generatePush(strtolower($result1['device_type']), $result1['device_token'], $message,$key);
        endif;
    }

    public function get_customer_payment_history($cdata) {
        $payments = $this->db->select("t1.*,t2.main_cat_id,t2.sub_cat_id,t2.status,t3.name as main_category_name")
                        ->select("(t1.quote_payment_amount+t1.callout_payment_amount+t1.additional_payment_amount) as total")
                        ->select("CONCAT(t4.firstName,' ',t4.lastName) as provider_name,IF(profile_img='','',concat('" . base_url('providerImages') . "','/',profile_img)) as prov_profileImage,t4.desc_box as description")
                        ->where("t1.cust_id", $cdata['cust_id'])
                        ->order_by("t1.quote_payment_date","desc")
                        ->join("customer_job as t2", "t1.job_id=t2.id")
                        ->join("main_category as t3", "t2.main_cat_id=t3.id")
                        ->join("providers as t4", "t1.prov_id=t4.prov_id")
                        ->get("customer_payment_details as t1")->result_array();
     //  pre($payments);die;
        if (!empty($payments)):
            $i = 0;
            foreach ($payments as $p):
                $sub_category_name = $this->db->select('name')->where("sb_id", $p['sub_cat_id'])->get('sub_category')->row()->name;
                $payments[$i]['sub_category_name'] = $sub_category_name != NULL ? $sub_category_name : "";
                
                $payments[$i]['job_data'] = $this->get_sub_job_data($p['main_cat_id'], $p['job_id']);
                
//              if(empty($payments[$i]['sub_category_name'])) {
//               $payments[$i]['location'] = $this->db->select('pickup_location')->where("job_id", $p['job_id'])->get('customer_job_transport_relation')->row()->pickup_location;
//               $payments[$i]['job_description'] = $this->db->select('job_description')->where("job_id", $p['job_id'])->get('customer_job_transport_relation')->row()->job_description;
//                                 //->get('sub_category_transport')->row()->pickup_location;
//              // $x['location'] =$x['location']['pickup_location'];
//                }
              
                
                $payments[$i]['review_count'] = $this->db->select("id")
                                ->where("prov_id", $p['prov_id'])
                                ->get("provider_rating")->num_rows();
                $p_rating = $this->db->select_avg('rating', 'rating')
                                ->where('prov_id', $p['prov_id'])
                                ->get('provider_rating')->row()->rating;
                $payments[$i]['avg_rating'] = $p_rating != NULL ? $p_rating : "";
                $ad_desc = $this->db->select("addi_job_desc")
                                ->where('job_id', $p['job_id'])
                                ->get('invoice')->row_array();
                $payments[$i]['additional_description'] = $ad_desc != NULL ? $ad_desc['addi_job_desc'] : "";
                $i++;
            endforeach;
            return $payments;
        else:
            return false;
        endif;
    }

    public function getPromoCodeUserCalloutPayment($json) {

        $data = array("job_id" => $json['job_id'], "callout_payment_transaction_id" => "", "callout_payment_amount" => 0, "callout_payment_date" => date('Y-m-d H:i:s'));
       
        $query = $this->db->where("job_id", $json['job_id'])
        ->update("customer_payment_details", $data);
        
        if ($query){             
        $this->db->set("status", 4);
        $this->db->where("job_id", $json['job_id']);
        $result= $this->db->update("job_status");    
        }
        
        if(!empty($result))
        {
        return $result;
        }
        else
        {
        return FALSE;
        }

    }
    
    
     public function getAllPdfDetails() {
       // $this->db->select('pdf_version.version,created_on as Registration_Date,pdf as Pdf_Link');
       // $data= $this->db->get("pdf_version")->row_array(); 
        $this->db->select('pdf_version.version as version,pdf_version.pdf as PDF_Name,pdf as Pdf_Link,created_on as Registration_Date');
        $this->db->order_by("id", "desc");
        $this->db->limit(1,0);
        $data = $this->db->get('pdf_version')->row_array();
         
        if(!empty($data))
        {
        $data['Pdf_Link'] = base_url() . "uploads/" . $data['Pdf_Link'];    
        return $data;
        }
        else
        {
        return FALSE;
        }
        }
    
    
        
        
     public function versionCheck($json) {
         
        $this->db->select('pdf_version.version as version,pdf_version.pdf as PDF_Name,pdf as Pdf_Link,created_on as Registration_Date');
        $this->db->order_by("id", "desc");
        $this->db->limit(1,0);
        $data = $this->db->get('pdf_version')->row_array(); 
       //  echo  $this->db->last_query($result);
       //  pre($result);die('query');
      
        if(!empty($data))
        {   
           if($data['version'] > $json['version']){
             $data['check'] = '1';
             
        } elseif($data['version'] < $json['version']){
             $data['check'] = '2';
             
        }else{
            $data['check'] = '3';
        }
        $data['Pdf_Link'] = base_url() . "uploads/" . $data['Pdf_Link'];    
        return $data;
        }
        else
        {
        return FALSE;
        }
        }
        
        
        
}
