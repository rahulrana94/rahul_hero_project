<?php

class Pushnotification_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function send_push_notification($document) {
        $flag = 0;
        if ($document['type'] == 1):
            $flag = 1;
            $provider = $this->db->select("name,device_type,device_token")
                            ->where("cust_id", $document['cust_id'])
                            ->get("customers")->row_array();

            $message['cust_id'] = $document['cust_id'];
            $message['prov_id'] = $document['prov_id'];
            $message['name'] = $document['name'];
            $message['notification_type'] = 51;
            $message['message'] = $document['msg'];

            generatePush(strtolower($provider['device_type']), $provider['device_token'], $message);

        elseif ($document['type'] == 2):
            $flag = 1;

            $provider = $this->db->select("concat(firstName,' ',lastName) as name,device_type,device_token")
                            ->where("prov_id", $document['prov_id'])
                            ->get("providers")->row_array();

            $message['prov_id'] = $document['prov_id'];
            $message['cust_id'] = $document['cust_id'];
            $message['name'] = $document['name'];
            $message['notification_type'] = 51;
            $message['message'] = $document['msg'];

            generatePush(strtolower($provider['device_type']), $provider['device_token'], $message);

        else:
            $flag = 0;
        endif;
        return $flag == 1 ? true : false;
    }

    function get_webhook_request($document) {
        $document1['response'] = $document;
        $query = $this->db->insert("webhook_response", $document1);
        $id = $this->db->insert_id();
        return ($id > 0) ? true : false;
    }

}
