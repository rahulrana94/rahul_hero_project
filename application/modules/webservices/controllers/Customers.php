<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MX_Controller {

    function __construct() {
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
        parent::__construct();
        $this->load->model('customers_model');
        $this->load->library('email');
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }

    function sendOTP($document) {
        if (strlen($document['mobile']) == 11):
            $document['mobile'] = substr($document['mobile'], 1, 10);
        endif;
        if ($document['countryCode'] != "") {
            $document['mobile'] = $document['countryCode'] . $document['mobile'];
        }
        $username = urlencode("Bazinga");
        $password = urlencode("IQCOQGODNNVCcV");
        $api_id = urlencode("3598107");
        $to = urlencode($document['mobile']);
        $message = urlencode("Thanks for using Bazinga! your OTP is " . $document['otp']);

        $curl = curl_init("https://api.clickatell.com/http/sendmsg"
                . "?user=$username&password=$password&api_id=$api_id&to=$to&text=$message");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        $page = curl_exec($curl);

        if (curl_errno($curl)) { // check for execution errors
            echo 'Scraper error: ' . curl_error($curl);
        }
        curl_close($curl);
        $get_result = explode(":", $page);
        if ($data['0'] == "ID" && $data['2'] != "") {
            return true;
        } else {
            return false;
        }
    }

    
    
    
    
    public function createCustomerProfile() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['name'] = $json['name'];
            $document['email'] = $json['email'];
            $document['mobile'] = $json['mobile'];
            $document['password'] = $json['password'];
            $document['profile_img'] = $json['profile_img'];
            $document['home_address'] = $json['home_address'];
            $document['buss_address'] = $json['buss_address'];
            $document['social_id'] = $json['social_id'];
            $document['login_type'] = $json['login_type'];
            $document['device_type'] = $json['device_type'];
            $document['device_token'] = $json['device_token'];
            $document['otp'] = rand(1000, 9999);

            $profileImage = $json['profile_img'];

            $isSuccess = false;
            $obj = new Customers_model;

            if ($profileImage == NULL or $profileImage == "") {
                $document['profile_img'] = "";
            } else {
                $document['profile_img'] = $obj->makeBase64Img($profileImage);
            }


            $isUserExist = $obj->isUserExist($document);

            if (!$isUserExist) {
                $createProfile = $obj->createProfile($document);

                if ($createProfile) {
                    $isSuccess = true;
                    $message = "Successful sign up";
                    if (empty($createProfile['profileImage'])) {

                        $createProfile['profile_img'] = "";
                    } else {
                        $createProfile['profile_img'] = base_url() . 'customerImages/' . $createProfile['profile_img'];
                    }
                    $data = $createProfile;
                } else {
                    $isSuccess = false;
                    $message = "User could not register";
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "User already registered";
                $data = $isUserExist;
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("createCustomerProfile") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function customerSignUp() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['name']            = $_REQUEST['name'];
            $document['email']           = $_REQUEST['email'];
            $document['countryCode']     = $_REQUEST['countryCode'];
            $document['mobile']          = $_REQUEST['mobile'];
            $document['password']        = $_REQUEST['password'];
            $document['social_id']       = $_REQUEST['social_id'];
            $document['login_type']      = $_REQUEST['login_type'];
            $document['device_type']     = $_REQUEST['device_type'];
            $document['device_token']    = $_REQUEST['device_token'];
            $document['otp']             = rand(1000, 9999);
            $loginType                   = $_REQUEST['login_type'];



            if (isset($_FILES['profile_img']) and $_FILES['profile_img'] != "") {
                $key = "profile_img";
                $document['profile_img'] = $this->uploadfile($key);
            } else {
                $document['profile_img'] = "";
            }

            $isSuccess = false;
            $obj = new Customers_model;
            if ($loginType == 0) {
                $isUserExist = $obj->isUserExist($document);

                if (!$isUserExist) {
                    $userSignUp = $obj->userSignUp($document);

                    if ($userSignUp) {
                        $sendOTP = $this->sendOTP($document);

                        $isSuccess = true;
                        $message = "Successful sign up";
                        if ($userSignUp['profile_img'] != "") {
                            $userSignUp['profile_img'] = base_url() . 'customerImages/' . $userSignUp['profile_img'];
                        } else {
                            $userSignUp['profile_img'] = "";
                        }

                        $data = $userSignUp;
                    } else {
                        $isSuccess = false;
                        $message = "User could not register";
                        $data = array();
                    }
                } else {
                    $isSuccess = false;
                    $message = "User already registered";
                    $data = array();
                }
            } else {
                $isUserExist = $obj->isUserExistSocially($document);

                if (!$isUserExist) {
                    $sendOTP = $this->sendOTP($document);
                    $userSignUp = $obj->userSignUp($document);

                    if ($userSignUp) {
                        $isSuccess = true;
                        $message = "Successful sign up";
                        if ($userSignUp['profile_img'] != "") {
                            $userSignUp['profile_img'] = base_url() . 'customerImages/' . $userSignUp['profile_img'];
                        } else {
                            $userSignUp['profile_img'] = "";
                        }

                        $data = $userSignUp;
                    } else {
                        $isSuccess = false;
                        $message = "User could not register";
                        $data = array();
                    }
                } else {
                    $isSuccess = false;
                    $message = "User already registered";
                    $data = array();
                }
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("customerSignUp") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function resendOTP() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['countryCode'] = $json['countryCode'];
            $document['mobile'] = $json['mobile'];
            $document['otp'] = rand(1000, 9999);

            $isSuccess = false;
            $obj = new Customers_model;

            $isUserExist = $obj->isUserPhoneExist($document);

            if ($isUserExist) {
                $sendOTP = $this->sendOTP($document);
                $updateOTP = $obj->updateOTP($document);

                $isSuccess = true;
                $message = "OTP resend successfully";
              //  $data = array();
                $data = $updateOTP;
            } else {
                $isSuccess = false;
                $message = "User not registered";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("resendOTP") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function customerLogin() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();

            $document['email'] = $json['email'];
            $document['password'] = $json['password'];
            $document['social_id'] = $json['social_id'];
            $document['login_type'] = $json['login_type'];
            $document['device_type'] = $json['device_type'];
            $document['device_token'] = $json['device_token'];
            $document['verified'] = 1;
            $loginType = $json['login_type'];

            $isSuccess = false;

            $obj = new Customers_model;

            if ($loginType == 0) {
                $isUserExist = $obj->isUserExist($document);

                if ($isUserExist) {
                    $user = $obj->customerLogin($document);
                    if ($user) {
                        $mobile = array();
                        $mobile['phone'] = $user['mobile'];
                        $isSuccess = true;
//                        if ($user['profile_img'] != "") {
//                            $user['profile_img'] = base_url() . 'customerImages/' . $user['profile_img'];
//                        } else {
//                            $user['profile_img'] = "";
//                        }

                        if ($user['verified'] != 0) {
                            //pre($user); die();
                            $results = $obj->updateCustomer($document);
                            // $userMemoriId                    = $obj->userMemoriId($document); 
                            unset($user['otp']);


                            $message = "Successful login!";
                            $messageCode = "1";
                            $data = $user;
                        } else {
                            $message = "Phone not verified yet";
                            $messageCode = "4";
                            $data = $user;
                        }
                    } else {
                        $isSuccess = false;
                        $message = "Wrong password";
                        $messageCode = "2";
                        $data = array();
                    }
                } else {
                    $isSuccess = false;
                    $message = "User does not exist";
                    $messageCode = "3";
                    $data = array();
                }
            } else {
                $isUserExist = $obj->isUserExistSocially($document);

                if ($isUserExist) {
                    $user = $obj->customerSignIn($document);
                    if ($user) {
                        $mobile = array();
                        $mobile['phone'] = $user['mobile'];
                        $isSuccess = true;
                        if ($user['verified'] != 0) {
                            $results = $obj->updateCustomerSocially($document);

                            unset($user['code']);

                            $isSuccess = true;
                            $message = "Successful login!";
                            $messageCode = "1";
                            if ($user['profile_img'] != "") {
                                $user['profile_img'] = base_url() . 'customerImages/' . $user['profile_img'];
                            } else {
                                $user['profile_img'] = "";
                            }
                            $data = $user;
                        } else {
                            $message = "Phone not verified yet";
                            $messageCode = "4";
                            $data = $user;
                        }
                    } else {
                        $isSuccess = false;
                        $message = "Wrong password!";
                        $messageCode = "2";
                        $data = array();
                    }
                } else {
                    $isSuccess = false;
                    $message = "User does not exist!";
                    $messageCode = "3";
                    $data = array();
                }
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $messageCode = "";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "messageCode" => $messageCode, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("customerLogin") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function passwordRecovery() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['email'] = $json['email'];

           // pre($document);
            $isSuccess = false;
            $obj = new Customers_model;
            $isUserExist = $obj->isUserExist($document);
           // pre($isUserExist);die('isExist');
            if ($isUserExist) {
                if ($isUserExist['login_type'] == 0) {
                    $passwordRecovery = $obj->passwordRecovery($document);

                   // pre($passwordRecovery);die('rahul');
                    
                    $to = $document['email'];
                    $msg = "Please use this for Login " . $passwordRecovery;
                    $sub = "Password Recovery from Bazinga App";
                    // $headers = 'From: Bazinga App <sumit.chauhan@fourthscreenlabs.com>' . "\r\n";

                    $headers .= "Organization: Sender Organization\r\n";
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
                    $headers .= "X-Priority: 3\r\n";
                    $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;
                   // $headers .= 'From: Bazinga App <noreply@bazinga.com>' . "\r\n";
                    $headers .= 'From: Bazinga App <admin@bazingaservice.com>' . "\r\n";

                    @$sentmail = mail($to, $sub, $msg, $headers);
                    
                    //pre(@$sentmail);die;
                    if (!empty($sentmail)) {
                        $isSuccess = true;
                        $message = "Password Sent Successfully";
                        $data = array();
                    } else {
                        $isSuccess = false;
                        $message = "Password Not Sent Successfully";
                        $data = array();
                    }
                } else {
                    $isSuccess = false;
                    $message = "You can not retrieve password of your social account";
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "User does not exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }

        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));

        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("passwordRecovery") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function verifyPhone() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['mobile'] = $json['mobile'];
            // $document['verified']           = 1;

            $isSuccess = false;
            $obj = new Customers_model;

            $verifyPhone = $obj->verifyPhone($document);
            if ($verifyPhone) {
                //$updateCustomer             = $obj->updateCustomer($document);
                $isSuccess = true;
                $message = "Otp sent successfully";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Otp not sent successfully";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("verifyPhone") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function verifyPhone2() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['mobile'] = $json['mobile'];
            $document['otp'] = $json['otp'];
            $document['verified'] = 1;
            $isSuccess = false;
            $obj = new Customers_model;

            $verifyPhone2 = $obj->verifyPhone2($document);
            if ($verifyPhone2) {
                $updateCustomer = $obj->updateCustomerByPhone($document);
                $isSuccess = true;
                $message = "Phone Verified";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Phone Verification Failed";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("verifyPhone2") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    function uploadfile($key) {
        // pre($_FILES);
        //die();
        $files = $_FILES[$key];
        //   /home/content/site/folders/filename.php                                    
        if ($files['error'] == 0) {
            $name = "customerImages/"; //.'img-' . date('Ymd') . date("h:i:sa");
            //$name = "/home/fourtfsa/public_html/amit/motary/carImages/";//.'img-' . date('Ymd') . date("h:i:sa");
            $file_name = time() . basename($_FILES[$key]["name"]);
            $target_file = $name . $file_name;

            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file)) {
                $imgs = array('url' => '/' . $name, 'name' => $files['name']);
            }
        }
        return $file_name;
    }

    /*
     * Get customer profile with key {user_id}
     */

    public function get_customer_profile() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['cust_id'])) {
                $obj = new Customers_model;
                $data = $obj->get_customer_info($json['cust_id']);

                if (count($data) > 0 && $data['profile_img'] != "") {
                    $data['profile_img'] = base_url() . 'customerImages/' . $data['profile_img'];
                }

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "User record found";
                } else {
                    $message = "No record found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("get_customer_profile") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    /* for update profile */

    public function update_customer_profile() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        if ($_REQUEST) {
            $record = array();
            $cust_id = $_REQUEST['cust_id'];
            $record['name'] = $_REQUEST['name'];
            $record['email'] = $_REQUEST['email'];
            $record['countryCode'] = $_REQUEST['countryCode'];
            $record['mobile'] = $_REQUEST['mobile'];
            $record['password'] = $_REQUEST['password'];
            $record['home_address'] = $_REQUEST['home_address'];
            $record['buss_address'] = $_REQUEST['buss_address'];
            //$record['profile_img'] = "";
            if (isset($_FILES['profile_img']) && $_FILES['profile_img'] != "") {
                $record['profile_img'] = $this->uploadfile('profile_img');
            }

            /* initialize model */
            $cutomers_model = new Customers_model;
            /* update customer profile */
            $update = $cutomers_model->update_customer_profile($record, $cust_id);

            $message = "Customer id is not valid";
            if ($update && $cutomers_model->get_customer_info($cust_id)) {
                $isSuccess = true;
                $message = "Profile updated successfully";
                $data = $cutomers_model->get_customer_info($cust_id);
                $data['profile_img'] = ($data ['profile_img'] != "") ? base_url() . 'customerImages/' . $data ['profile_img'] : '';
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("update_customer_profile") . PHP_EOL;
        $log = $log . "Request " . $_REQUEST . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function createCustomerJob_16feb2017() {
        $isSuccess = false;
        $message = 'Invalid Input';
        $data = array();
        if (isset($_REQUEST)) {
            $cutomers_model = new Customers_model;
            if ($_REQUEST['promocode'] != '') {
                $promocode = $cutomers_model->checkPromoCode($_REQUEST);
            }
            if ($promocode) {
                $message = 'Promocode has been used already.';
            } else {
                $update = $cutomers_model->createCustomerJobPosting($_REQUEST);
                $message = "Job could not be posted.";
                if ($update) {
                    $isSuccess = true;
                    $message = "Job has been posted.";
                    $data = $update;
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("createCustomerJob") . PHP_EOL;
        $log = $log . "Request " . $_REQUEST . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    
        public function createCustomerJob() { 
        $json = file_get_contents('php://input');
        $isSuccess = false;
        $message = 'Invalid Input';
        $data = array();
        
        //pre($_REQUEST); die('uu');
        if (isset($_REQUEST)) {
            $cutomers_model = new Customers_model;
            if ($_REQUEST['promocode'] != '') {
                $promocode = $cutomers_model->checkPromoCode($_REQUEST);
            }
            if ($promocode) {
                $message = 'Promocode has been used already.';
            } else {
                $update = $cutomers_model->createCustomerJobPosting($_REQUEST);
                if($update == 0){
                $message = "Job could be posted after four hours";
                }else{
                $message = "Job could not be posted.";
                }
                if ($update) {
                    $isSuccess = true;
                    $message = "Job has been posted.";
                    $data = $update;
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("createCustomerJob") . PHP_EOL;
        $log = $log . "Request " . $_REQUEST . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }
    
 
    public function getCustomerPostedJobs() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['cust_id'])) {
                $obj = new Customers_model;
                $data = $obj->get_customer_posted_jobs($json['cust_id']);

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Customer job record found";
                } else {
                    $message = "No Customer job record found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getCustomerPostedJobs") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function getCustomerAcceptedJobs() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['cust_id'])) {
                $obj = new Customers_model;
                $data = $obj->get_customer_accepted_jobs($json['cust_id']);

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Customer job record found";
                } else {
                    $message = "No Customer job record found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getCustomerAcceptedJobs") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function getCustomerCompletedJobs() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['cust_id'])) {
                $obj = new Customers_model;
                $data = $obj->get_customer_completed_jobs($json['cust_id']);
//pre($data);die('rahul');
                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Customer job record found";
                } else {
                    $message = "No Customer job record found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getCustomerCompletedJobs") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function getProvidersByJobID() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['job_id'])) {
                $obj = new Customers_model;
                $data = $obj->get_provider_by_jobid($json['job_id']);

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Provider found";
                } else {
                    $message = "No provider found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getProvidersByJobID") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function getProvidersDetails() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['prov_id']) && $json['job_id']) {
                $obj = new Customers_model;
                $data = $obj->get_provider_details($json['prov_id'], $json['job_id'],$json['promocode']);

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Provider found";
                } else {
                    $message = "No provider found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getProvidersDetails") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function getCustomerJobdetailById() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['job_id'])) {
                $obj = new Customers_model;
                $data = $obj->get_customer_jobdetail_by_id($json['job_id']);

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Job Details found";
                } else {
                    $message = "No Job Details found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getCustomerJobdetailById") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    
   
    public function sentJobRequestToProvider() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json)) {
                $obj = new Customers_model;
                $data = $obj->sent_job_request($json);

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Request send successfully!";
                } else {
                    $message = "Requested failed!";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("sentJobRequestToProvider") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function cancelJob() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json)) {
                $document = array();
                $document['job_id'] = $json['job_id'];
                $document['user_id'] = $json['user_id'];
                $document['user_type'] = 1;
                $document['reason'] = $json['reason'];
                $obj = new Customers_model;
                $res = $obj->cancel_job($document);

                if ($res) {
                    $isSuccess = true;
                    $message = "Job cancelled successfully!";
                } else {
                    $message = "Job cancel failed!";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("cancelJob") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function reScheduleJobByCustomer() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Customers_model;
            $update = $cutomers_model->re_schedule_job($json);
            $message = "Job rescheduled failed!";
            if ($update) {
                $isSuccess = true;
                $message = "Job rescheduled successfully";
                //$data = $cutomers_model->get_customer_jobdetail_by_id($update);
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("reScheduleJobByCustomer") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function acceptedJobDetailsCustomer() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();
        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Customers_model;
            $data = $cutomers_model->acceptedJobDetailsCustomer($json['job_id']);
            $message = "No data found!";
            if ($data) {
                $isSuccess = true;
                $message = "Data found successfully!";
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("acceptedJobDetailsCustomer") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function rateTheProvider() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();
        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Customers_model;
            $data = $cutomers_model->rate_the_provider($json);
            $message = "Provider could not be rated.";
            if ($data) {
                $isSuccess = true;
                $message = "Provider has been rated!";
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("rateTheProvider") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function getCustomerCompletedJobdetails() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['job_id'])) {
                $obj = new Customers_model;
                $data = $obj->get_customer_completed_jobdetail($json['job_id']);

                if ($data) {
                    $isSuccess = true;
                    $message = "Job Details found";
                } else {
                    $message = "No Job Details found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getCustomerCompletedJobdetails") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function getFinalQuotation() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['job_id'])) {
                $obj = new Customers_model;
                $data = $obj->get_final_quotation($json['job_id']);

                if ($data) {
                    $isSuccess = true;
                    $message = "Details found";
                } else {
                    $message = "No Details found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getFinalQuotation") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function setActionFinalQuotation() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json)) {
                $obj = new Customers_model;
                $data = $obj->set_action_final_quotation($json);

                if ($data) {
                    $isSuccess = true;
                    if ($json['status'] == 1):
                        $message = "Final quotaion has been accepted";
                    else:
                        $message = "Final quotaion has been rejected";
                    endif;
                } else {
                    if ($json['status'] == 1):
                        $message = "Final quotation could not be accepted.";
                    else:
                        $message = "Final quotation could not be rejected.";
                    endif;
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("setActionFinalQuotation") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function setRescheduleActionByCustomer() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json)) {
                $obj = new Customers_model;
                $data = $obj->set_reschedule_actionByCustomer($json);

                if ($data) {
                    $isSuccess = true;
                    $message = "Action successful";
                } else {
                    $message = "Action Failed";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("setRescheduleActionByCustomer") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function finishImmidiateCallout() {
        $json = file_get_contents('php://input');
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Customers_model;
            $update = $cutomers_model->finish_immidiate_callout($json['job_id']);
            $message = "Immidiate callout could not be finished.";
            //if (is_numeric($update)) {
            if ($update > 0) {
                $isSuccess = true;
                $message = "Immidiate callout finished.";
                $data = $update;
            }
            else{
                $isSuccess = false;
                $message = "Immidiate callout could not be finished.";
                $data = array();
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("finishImmidiateCallout") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function saveInitialPaymentDetails() {
        $json = file_get_contents('php://input');
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Customers_model;
            $update = $cutomers_model->save_payment_details($json);
            $message = "Payment-details could not be saved.";
            if ($update) {
                $isSuccess = true;
                $message = "Payment-details has been saved.";
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("saveInitialPaymentDetails") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function updatePaymentDetails() {
        $json = file_get_contents('php://input');
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Customers_model;
            $update = $cutomers_model->update_payment_details($json);
            $message = "Payment-details could not be updated.";
            if ($update) {
                $isSuccess = true;
                $message = "Payment-details has been updated.";
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("updatePaymentDetails") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function hitCurl($data) {
        $url = "https://oppwa.com/v1/registrations/{$data['token']}/payments";
        $data = "authentication.userId=8a8394c457d1dbc30157e18ef2564fb4" .
                "&authentication.password=tPqScCjs6r" .
                "&authentication.entityId=8a8394c457d1dbc30157e190c50f4fd2" .
                "&amount={$data['amount']}" .
                "&currency=ZAR" .
                "&paymentType=DB".
                "&merchantTransactionId={$data['merchantTransactionId']}";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true); // this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        curl_close($ch);
	//write response to file 
	$myfile = fopen("/var/www/html/liveBazinga/log.txt", "w+") or die("Unable to open file!");
//	$myfile = fopen("/var/www/html/bazinga/log.txt", "w+") or die("Unable to open file!");
	fwrite($myfile, $responseData);
	fclose($myfile);

        return $responseData;
    }

    public function makePaymentbyToken() {
        $json = file_get_contents('php://input');
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();
        
        if (is_json($json)) {
            $json = json_decode($json, true);

            $cutomers_model = new Customers_model;
            $check_status = $cutomers_model->check_payment_status($json);
            if ($check_status) {
                $isSuccess = true;
                $message = "Payment already has been made for this scenario.";
            } else {
                /* send data to curl !!*/
                $merchantTransactionId = md5($json['token'].date('Ymdhis'));
                $request_data = array("token" => $json['token'], "amount" => $json['amount'] , "merchantTransactionId" => $merchantTransactionId);
                $response_data = $this->hitCurl($request_data);
               //  pre($response_data);die('rana');
                $response_data = json_decode($response_data, true);
                if (isset($response_data) && !empty($response_data) && ($response_data['result']['code'] == '000.100.110' || $response_data['result']['code'] == '000.000.000')) {
                    $result = $cutomers_model->savePaymentData($json, $response_data);
                    
                   
                    if ($result) {
                        $isSuccess = true;
                        $message = $response_data['result']['description'];
                    } else {
                        $message = "Payment could not be completed successfully.";
                    }
                } else {
                    $message = $response_data['result']['description'];
                }
            }
        }

        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("makePaymentbyToken") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL;
        $log .= "RAW response ".json_encode($response_data). PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function customerPaymentHistory() {
        $json = file_get_contents('php://input');
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();
        //keys = array("cust_id");
        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Customers_model;
            $payment_data = $cutomers_model->get_customer_payment_history($json);
            if (!empty($payment_data)) {
                $isSuccess = true;
                $message = "Payment details found.";
                $data = $payment_data;
            } else {
                $message = "Payment details could not be found.";
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("customerPaymentHistory") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Customer.txt', $log, FILE_APPEND);
    }

    public function demoPush()
    {
        $json = file_get_contents('php://input');
        if(is_json($json))
        {
            $json                       = json_decode($json,true);
           // $obj                        = new user_model;
            //$id                         = $json['id'];
            //$push['pushType']           = $json['type'];
            //$push['pushMessage']        = "Almadarsa demo push";


            //$exist                      = $obj->isUserIdExist($json);
            //$deviceType                 = 1;
            $deviceType                 =  "ios";
            $deviceToken                 = "7752528adf2d03bb3904c53fecbf4d9141d5eeb77b91893270c0f3ac00ceb8ba";
          //  $deviceToken                = "b17445ae49a1df3ad07d61229b49a856621d1fddfcbe27dfc826a09444831912"; //ios
          // $deviceToken                ="f9inVBuFBp8:APA91bGK_-Cns_3ljeI5lmv9Kmxc1So_ufraQ8DhPjgkIw639OMWNEMND8FNd-9vh158P7c8odN8bwcvoiunzrBRoRh2OEFytlXCb5rdOSPbHONbCfYMZDWKou8X76xREpGhUigH1A3_";
            $msg                        = "hello";
            $key                        = 1;
            $generatePush               = generatePush($deviceType,$deviceToken,$msg,$key);
            //echo json_encode($msg);
            //teapre($msg);
            //echo $deviceToken;
            //echo $deviceType;
            echo "push sent";
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invalid Json Input";
            $data           = array();
        }
    }
    
    
    public function getPromoCodeUserCalloutPayment() {
        
        $json           = file_get_contents('php://input');
        $isSuccess      = false;
        $message        = 'Invalid Json Input';
        $data           = array();

        if (is_json($json))
        {
        $json           = json_decode($json, true);
        if (isset($json['job_id']))
        {
        $obj            = new Customers_model;
        $data           = $obj->getPromoCodeUserCalloutPayment($json);

        if (!empty($data)) {
        $isSuccess      = true;
        $message        = "Callout payment details found.";
        $data           = array();
        } 
        else {
        $message        = "Callout payment details could not be found.";
             }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        }
    }
      
    //__________________________________HERO________________________17sept17______BY__Rahul Rana



    public function getPdfDetails() {
        
        $json           = file_get_contents('php://input');
        $isSuccess      = false;
        $message        = 'Invalid Json Input';
        $data           = array();

        if (is_json($json))
        {
        $json           = json_decode($json, true);
        $obj            = new Customers_model;
        $data           = $obj->getAllPdfDetails();

        if (!empty($data)) {
        $isSuccess      = true;
        $message        = "Result display successfully";
        $data           = $data;
        } 
        else {
        $message        = "No result found.";
             }   
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getPdfDetails") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/getPdfDetails.txt', $log, FILE_APPEND);
    }

    
    

     public function pdfVersionCheck() {
        
        $json           = file_get_contents('php://input');
        $isSuccess      = false;
        $message        = 'Invalid Json Input';
        $data           = array();
        if (is_json($json)){
        $json           = json_decode($json, true);
        $obj            = new Customers_model;
        $result          = $obj->versionCheck($json);
        //pre($result);die;       
        if(!empty($result)){

        if($result['check']=='1'){
        $isSuccess      = true;
        $message        = "Your PDF version is smaller than existing.";
        unset($result['check']);
        $data           = $result;
        } elseif ($result['check']=='2') {
        $isSuccess      = true;
        $message        = "Your PDF version is grater than existing.";
        unset($result['check']);
        $data           = $result;
        } else {
        $isSuccess      = true;
        $message        = "Your PDF version equal to existing.";
        unset($result['check']);
        $data           = $result;
        }
  
        }
        
        else {
        $isSuccess      = FALSE;
        $message        = "No Record Found!!";
        $data           = array();
        } 
        
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("pdfVersionCheck") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/pdfVersionCheck.txt', $log, FILE_APPEND);
    }





    
}
