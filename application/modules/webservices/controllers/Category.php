<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller 
{
  
    function __construct()
    {
        parent::__construct();
		$this->load->model('category_model');
                 error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }
   
	public function get_sub_category(){
	
		$json = file_get_contents('php://input'); 
		
		$isSuccess = false; 
		$message = 'Invalid Json Input';
		$data = array();
		
	 	if(is_json($json))
	 	{
		 	$json = json_decode($json,true);

			$all = array();
			foreach(explode(',',$json['main_category']) as $cat )
			{
				$obj      = new Category_model;
				$all[$cat] = $obj->get_subcategories($cat);
				$with_images_url = array();
				foreach($all[$cat] as $d)
				{
					$d['icon_img']     = ($d['icon_img'] != "")?base_url().'subcategoryimages/'.$d['icon_img']:'';
					$with_images_url[] = $d;
				}
				$all[$cat] = $with_images_url;
			}
			$data           = $all;
			$isSuccess      = true;
			$message        = "Sub category found";

		}
		
		echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
	
	}


	public function searchSubCategory(){
	
		$json = file_get_contents('php://input'); 
		
		$isSuccess = false; 
		$message = 'Invalid Json Input';
		$data = array();
		
	 	if(is_json($json))
	 	{
		 	$json = json_decode($json,true);
			if(isset($json['search']))
			{	
				$obj             = new Category_model;
				$data            = $obj->searchSubCategory($json['search']);
			}
			if($data)
			{
				$with_images_url = array();				
				$data            = $data;
				$isSuccess       = true;
                $message         = "Sub category found";
			}
			else
			{
				$isSuccess       = false;
                $message         = "No sub categories found";
                $data            = array();
			}
		}
		
		echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
	}

}
