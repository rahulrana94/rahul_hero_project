<?php

class PushNotification extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('pushnotification_model');
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }

    public function sendPushNotification() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);            
                $obj = new Pushnotification_model;
                $data = $obj->send_push_notification($json);

                if ($data) {
                    $isSuccess = true;
                    $message = "Notification Sent";
                } else {
                    $message = "Notification Could not be sent";
                }
            
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }
    
    public function getWebHookRequest() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            //$json = json_decode($json, true);            
                $obj = new Pushnotification_model;
                $data1 = $obj->get_webhook_request($json);

                if ($data1) {
                    $isSuccess = true;
                    $message = "Request successful.";
                } else {
                    $message = "Request failed.";
                }
            
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

}
