<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

    function __construct() {
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->library('email');
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }

    public function userLogin() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            
            $document  = array();
            $data      = array();
            $isSuccess = false;
            $obj       = new Login_model;
            
            $userLogin = $obj->userLogin($json);
            
            if (empty($userLogin)) {
                $message = "Invalid Credentials";
            } else {
                $isSuccess = true;
                $message = "Login Successfully";
                $data = $userLogin;
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }



/////////////////////////////////////////////////////////////////////////////


public function basicLogin() {
    $json = file_get_contents('php://input');
    if (is_json($json)) {
        $json = json_decode($json, true);
        $document = array();
        $data = array();
        $isSuccess = false;
        $obj = new login_model;
        $userLogin = $obj->basicLogin($json);
        if (empty($userLogin)) {
            $message = "Invalid Credentials";
        } else {
            $isSuccess = true;
            $message = "Login Successfully";
            $data = $userLogin;
        }
    } else {
        $isSuccess = false;
        $message = "Invaid Json Input";
        $data = array();
    }
    echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
}



}



?>