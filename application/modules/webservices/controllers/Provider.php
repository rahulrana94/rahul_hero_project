<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Provider extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('provider_model');
        $this->load->model('customers_model');
        $this->load->library('email');
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }

    public function createProviderProfile() {
        if ($_REQUEST) {

            //$json                           = json_decode($json,true);
            $document = array();
            $document['firstName'] = $_REQUEST['firstName'];
            $document['lastName'] = $_REQUEST['lastName'];
            $document['email'] = $_REQUEST['email'];
            $document['password'] = $_REQUEST['password'];
            $document['countryCode'] = $_REQUEST['countryCode'];
            $document['mobile'] = $_REQUEST['mobile'];
            $document['landlineCode'] = $_REQUEST['landlineCode'];
            $document['home_address'] = $_REQUEST['home_address'];
            $document['fb_link'] = $_REQUEST['fb_link'];
            $document['lnkdin'] = $_REQUEST['lnkdin'];
            $document['business_name'] = $_REQUEST['business_name'];
            $document['business_subtype'] = $_REQUEST['business_subtype'];
            $document['business_size'] = $_REQUEST['business_size'];
            $document['desc_box'] = $_REQUEST['desc_box'];
            $document['business_type'] = $_REQUEST['business_type'];
            $document['business_address'] = $_REQUEST['business_address'];
            $document['business_address_lat'] = $_REQUEST['business_address_lat'];
            $document['business_address_long'] = $_REQUEST['business_address_long'];
            $document['business_phone'] = $_REQUEST['business_phone'];
            $document['business_category'] = $_REQUEST['business_category'];
            $document['Vat_no'] = $_REQUEST['Vat_no'];
            $document['vehicle_make_yr'] = $_REQUEST['vehicle_make_yr'];
            $document['vehicle_regn'] = $_REQUEST['vehicle_regn'];
            $document['verified'] = 0;
            $document['device_type'] = $_REQUEST['device_type'];
            $document['device_token'] = $_REQUEST['device_token'];
            $document['callout_radius'] = $_REQUEST['callout_radius'];
            $document['immidiate_callout_radius'] = $_REQUEST['immidiate_callout_radius'];
            $document['created_on'] = date('Y-m-d H:i:s');
            $document['code'] = rand(1000, 9999);


            if (isset($_FILES['profile_img']) and $_FILES['profile_img'] != "") {
                $key = "profile_img";
                $document['profile_img'] = $this->uploadfile($key);
            }
            if (isset($_FILES['id_or_passport']) and $_FILES['id_or_passport'] != "") {
                $key = "id_or_passport";
                $document['id_or_passport'] = $this->uploadfile($key);
            }
            if (isset($_FILES['bank_proof']) and $_FILES['bank_proof'] != "") {
                $key = "bank_proof";
                $document['bank_proof'] = $this->uploadfile($key);
            }
            if (isset($_FILES['certificates']) and $_FILES['certificates'] != "") {
                $key = "certificates";
                $document['certificates'] = $this->uploadfile($key);
            }
            if (isset($_FILES['driving_licence']) and $_FILES['driving_licence'] != "") {
                $key = "driving_licence";
                $document['driving_licence'] = $this->uploadfile($key);
            }
            if (isset($_FILES['vehicle_regn_doc']) and $_FILES['vehicle_regn_doc'] != "") {
                $key = "vehicle_regn_doc";
                $document['vehicle_regn_doc'] = $this->uploadfile($key);
            }

            $isSuccess = false;
            $obj = new Provider_model;

            $isUserExist = $obj->isUserExist($document);
            if (!$isUserExist) {
                $createProfile = $obj->createProfile($document);
                if ($createProfile) {
                    if ($createProfile['profile_img'] != "") {
                        $createProfile['profile_img'] = base_url() . "providerImages/" . $createProfile['profile_img'];
                    }
                    if ($createProfile['id_or_passport'] != "") {
                        $createProfile['id_or_passport'] = base_url() . "providerImages/" . $createProfile['id_or_passport'];
                    }
                    if ($createProfile['bank_proof'] != "") {
                        $createProfile['bank_proof'] = base_url() . "providerImages/" . $createProfile['bank_proof'];
                    }
                    if ($createProfile['certificates'] != "") {
                        $createProfile['certificates'] = base_url() . "providerImages/" . $createProfile['certificates'];
                    }
                    if ($createProfile['driving_licence'] != "") {
                        $createProfile['driving_licence'] = base_url() . "providerImages/" . $createProfile['driving_licence'];
                    }
                    if ($createProfile['vehicle_regn_doc'] != "") {
                        $createProfile['vehicle_regn_doc'] = base_url() . "providerImages/" . $createProfile['vehicle_regn_doc'];
                    }



                    $isSuccess = true;
                    $message = "Successful sign up";
                    $data = $createProfile;
                } else {
                    $isSuccess = false;
                    $message = "Profile not created successfully";
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "Profile already exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invalid Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("createProviderProfile") . PHP_EOL;
        $log = $log . "Request " . $_REQUEST . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function updateProviderDetails() {

            $isSuccess = false;
            $message = 'Invalid Input';
            $data = array();

            if ($_REQUEST) {
            $document = array();
            $prov_id = $_REQUEST['prov_id'];
            $document['firstName']         = $_REQUEST['firstName'];
            $document['lastName']          = $_REQUEST['lastName'];
            $document['email']             = $_REQUEST['email'];
            $document['password']          = $_REQUEST['password'];
            $document['countryCode']       = $_REQUEST['countryCode'];
            $document['mobile']            = $_REQUEST['mobile'];
            $document['landlineCode']      = $_REQUEST['landlineCode'];
            $document['notification_status'] = $_REQUEST['notification_status'];
            $document['home_address']      = $_REQUEST['home_address'];
            $document['fb_link']           = $_REQUEST['fb_link'];
            $document['lnkdin']            = $_REQUEST['lnkdin'];
            $document['business_name']     = $_REQUEST['business_name'];
            $document['business_size']     = $_REQUEST['business_size'];
            $document['desc_box']          = $_REQUEST['desc_box'];
            $document['business_type']     = $_REQUEST['business_type'];
            $document['business_address']  = $_REQUEST['business_address'];
            $document['business_address_lat']  = $_REQUEST['business_address_lat'];
            $document['business_address_long'] = $_REQUEST['business_address_long'];
            $document['business_phone']    = $_REQUEST['business_phone'];
            $document['business_category'] = $_REQUEST['business_category'];
            $document['Vat_no']            = $_REQUEST['Vat_no'];
            $document['vehicle_make_yr']   = $_REQUEST['vehicle_make_yr'];
            $document['vehicle_regn']      = $_REQUEST['vehicle_regn'];
            $document['device_type']       = $_REQUEST['device_type'];
            $document['device_token']      = $_REQUEST['device_token'];
            $document['callout_radius']    = $_REQUEST['callout_radius'];
            $document['immidiate_callout_radius'] = $_REQUEST['immidiate_callout_radius'];

            //$record['profile_img'] = "";
            if (isset($_FILES['profile_img']) and $_FILES['profile_img'] != "") {
                $key = "profile_img";
                $document['profile_img'] = $this->uploadfile($key);
            }
            if (isset($_FILES['id_or_passport']) and $_FILES['id_or_passport'] != "") {
                $key = "id_or_passport";
                $document['id_or_passport'] = $this->uploadfile($key);
            }
            if (isset($_FILES['bank_proof']) and $_FILES['bank_proof'] != "") {
                $key = "bank_proof";
                $document['bank_proof'] = $this->uploadfile($key);
            }
            if (isset($_FILES['certificates']) and $_FILES['certificates'] != "") {
                $key = "certificates";
                $document['certificates'] = $this->uploadfile($key);
            }
            if (isset($_FILES['driving_licence']) and $_FILES['driving_licence'] != "") {
                $key = "driving_licence";
                $document['driving_licence'] = $this->uploadfile($key);
            }
            if (isset($_FILES['vehicle_regn_doc']) and $_FILES['vehicle_regn_doc'] != "") {
                $key = "vehicle_regn_doc";
                $document['vehicle_regn_doc'] = $this->uploadfile($key);
            }

            /* initialize model */
            $cutomers_model = new Provider_model;
            /* update customer profile */
            $update = $cutomers_model->update_provider_profile($document, $prov_id);

            $message = "Provider id is not valid";
            if ($update && $cutomers_model->get_provider_info($prov_id)) {
                $isSuccess = true;
                $message = "Profile updated successfully";
                $data = $cutomers_model->get_provider_info($prov_id);

                if ($data['profile_img'] != "") {
                    $data['profile_img'] = base_url() . "providerImages/" . $data['profile_img'];
                }
                if ($createProfile['id_or_passport'] != "") {
                    $data['id_or_passport'] = base_url() . "providerImages/" . $data['id_or_passport'];
                }
                if ($data['bank_proof'] != "") {
                    $data['bank_proof'] = base_url() . "providerImages/" . $data['bank_proof'];
                }
                if ($data['certificates'] != "") {
                    $data['certificates'] = base_url() . "providerImages/" . $data['certificates'];
                }
                if ($data['driving_licence'] != "") {
                    $data['driving_licence'] = base_url() . "providerImages/" . $data['driving_licence'];
                }
                if ($data['vehicle_regn_doc'] != "") {
                    $data['vehicle_regn_doc'] = base_url() . "providerImages/" . $data['vehicle_regn_doc'];
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("updateProviderDetails") . PHP_EOL;
        $log = $log . "Request " . $_REQUEST . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    //login--------------------------------------   
    public function providerLogin() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['email'] = $json['email'];
            $document['password'] = $json['password'];
            $document['device_type'] = $json['device_type'];
            $document['device_token'] = $json['device_token'];
            //   $document['verified']           = 1;
            // $loginType                      = $json['login_type'];

            $isSuccess = false;

            $obj = new Provider_model;


            $isUserExist = $obj->isUserExist($document);

            if ($isUserExist) {
                $user = $obj->providerLogin($document);
                if ($user) {
                    $results = $obj->updateProvider($document);


                    $isSuccess = true;
                    $message = "Login Successful!";
                    $messageCode = "1";
                    if ($user['profile_img'] != "") {
                        if ($user['profile_img'] != "") {
                            $user['profile_img'] = base_url() . "providerImages/" . $user['profile_img'];
                        }
                        if ($user['id_or_passport'] != "") {
                            $user['id_or_passport'] = base_url() . "providerImages/" . $user['id_or_passport'];
                        }
                        if ($user['bank_proof'] != "") {
                            $user['bank_proof'] = base_url() . "providerImages/" . $user['bank_proof'];
                        }
                        if ($user['certificates'] != "") {
                            $user['certificates'] = base_url() . "providerImages/" . $user['certificates'];
                        }
                        if ($user['driving_licence'] != "") {
                            $user['driving_licence'] = base_url() . "providerImages/" . $user['driving_licence'];
                        }
                        if ($user['vehicle_regn_doc'] != "") {
                            $user['vehicle_regn_doc'] = base_url() . "providerImages/" . $user['vehicle_regn_doc'];
                        }
                    }
                    $data = $user;
                } else {
                    $isSuccess = false;
                    $message = "Wrong password!";
                    $messageCode = "2";
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "User doesn't exist!";
                $messageCode = "3";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $messageCode = "";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "messageCode" => $messageCode, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("providerLogin") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function passwordRecovery() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['email'] = $json['email'];


            $isSuccess = false;
            $obj = new Provider_model;
            $isUserExist = $obj->isUserExist($document);
            if ($isUserExist) {
                $passwordRecovery = $obj->passwordRecovery($document);

                $to = $document['email'];
                $msg = "Your recovered password is " . $passwordRecovery;
                $sub = "Password Recovery Mail From Bazinga App";
              //  $headers = 'From: Bazinga App <sumit.chauhan@fourthscreenlabs.com>' . "\r\n";
                $headers = 'From: Bazinga App <admin@bazingaservice.com>' . "\r\n";

                @$sentmail = mail($to, $sub, $msg, $headers);
                if (!empty($sentmail)) {
                    $isSuccess = true;
                    $message = "Password sent successfully";
                    $data = array();
                } else {
                    $isSuccess = false;
                    $message = "Password could not send";
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "User does not exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("passwordRecovery2") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    //----------------------File Upload------------------//
    function uploadfile($key) {
        $files = $_FILES[$key];
        //   /home/content/site/folders/filename.php                                    
        if ($files['error'] == 0) {
            $name = "providerImages/"; //.'img-' . date('Ymd') . date("h:i:sa");
            //$name = "/home/fourtfsa/public_html/amit/motary/carImages/";//.'img-' . date('Ymd') . date("h:i:sa");
            $file_name = time() . basename($_FILES[$key]["name"]);
            $target_file = $name . $file_name;

            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file)) {
                $imgs = array('url' => '/' . $name, 'name' => $files['name']);
            }
        }
        return $file_name;
    }

    public function getProviderDetails() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['prov_id'])) {
                $obj = new Provider_model;
                $data = $obj->get_provider_info($json['prov_id']);
                if ($data['profile_img'] != "") {
                    $data['profile_img'] = base_url() . "providerImages/" . $data['profile_img'];
                }
                if ($data['id_or_passport'] != "") {
                  //  die('udi');
                    $data['id_or_passport'] = base_url() . "providerImages/" . $data['id_or_passport'];
                }
                if ($data['bank_proof'] != "") {
                    $data['bank_proof'] = base_url() . "providerImages/" . $data['bank_proof'];
                }
                if ($data['certificates'] != "") {
                    $data['certificates'] = base_url() . "providerImages/" . $data['certificates'];
                }
                if ($data['driving_licence'] != "") {
                    $data['driving_licence'] = base_url() . "providerImages/" . $data['driving_licence'];
                }
                if ($data['vehicle_regn_doc'] != "") {
                    $data['vehicle_regn_doc'] = base_url() . "providerImages/" . $data['vehicle_regn_doc'];
                }
                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "User record found";
                } else {
                    $message = "No record found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getProviderDetails") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function dateBasedJobsForProvider() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['prov_id']) && isset($json['job_date'])) {
                $obj = new Provider_model;
                $data = $obj->date_based_jobs_for_provider($json['prov_id'], str_replace('/', '-', $json['job_date']));

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Jobs found";
                } else {
                    $message = "No job found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("dateBasedJobsForProvider") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    
    
    public function userBasedJobsForProvider() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['prov_id'])) {
                $obj = new Provider_model;          
                $data = $obj->user_based_jobs_for_provider($json['prov_id']);

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Jobs found";
                } else {
                    $message = "No job found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("userBasedJobsForProvider") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

   
    public function getAcceptedJobsForProvider() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['prov_id'])) {
                $obj = new Provider_model;
                $data = $obj->get_accepted_jobs_for_provider($json['prov_id']);

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Jobs found";
                } else {
                    $message = "No job found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getAcceptedJobsForProvider") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function getCompletedJobs() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['prov_id'])) {
                $obj = new Provider_model;
                $data = $obj->get_completed_jobs_for_provider($json['prov_id']);

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Jobs found";
                } else {
                    $message = "No job found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getCompletedJobs") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function jobDetailsProvider() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json)) {
                $obj = new Provider_model;
                $data = $obj->get_jobDetails_for_provider($json);

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Job details found";
                } else {
                    $message = "No details found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("jobDetailsProvider") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function acceptedJobDetailsProvider() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['job_id'])) {
                $obj = new Provider_model;
                $data = $obj->accepted_job_details($json['job_id']);
                if (isset($data['customer_name'])) {
                    $isSuccess = true;
                    $message = "Job detail found";
                } else {
                    $message = "No job detail found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("acceptedJobDetailsProvider") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function sendQuotations() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json)) {
                $obj = new Provider_model;
                $checkProvStatus = $obj->check_provider_status($json['prov_id']);
                if ($checkProvStatus) {
                    $data_id = $obj->send_quotations($json);
                    if ($data_id) {
                        $isSuccess = true;
                        $message = "Quotation sent successfully.";
                        $data = $obj->get_quotation($data_id);
                    } else {
                        $message = "You are not a verified provider.";
                    }
                } else {
                    $message = "No quotation sent successfully.";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("sendQuotations") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function cancelJob() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json)) {
                $document = array();
                $document['job_id'] = $json['job_id'];
                $document['user_id'] = $json['user_id'];
                $document['user_type'] = 2;
                $document['reason'] = $json['reason'];
                $obj = new Customers_model;
                $res = $obj->cancel_job($document);

                if ($res) {
                    $isSuccess = true;
                    $message = "Job cancelled successfully!";
                } else {
                    $message = "Job cancel failed!";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("cancelJob") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function rateTheCustomer() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();
        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Provider_model;
            $data = $cutomers_model->rate_the_customer($json);
            $message = "Customer could not be rated.";
            if ($data) {
                $isSuccess = true;
                $message = "Customer has been rated.";
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("rateTheCustomer") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function startJob() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();
        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Provider_model;
           
            $check = $cutomers_model->check_date($json);
             //if ($json['strt_date']==date) {
            if ($check) {
                $data = $cutomers_model->start_job($json);
                $message = "Job could not be started";
                if ($data) {
                    $isSuccess = true;
                    $message = "Job started!";
                }
            } else {
                $message = "You can't start job on this date.";
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("startJob") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function updateJobStatus() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();
        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Provider_model;
            $data = $cutomers_model->update_job_status($json);
            $message = "Job could not be updated!";
            if ($data) {
                $isSuccess = true;
                if ($json['status'] == 1):     
                    $message = "Pick up confirm for job.";
                elseif ($json['status'] == 2):
                    $message = "Job location reached.";
                elseif ($json['status'] == 3):
                    $message = "Callout request sent successfully.";
                elseif ($json['status'] == 4):
                    $message = "Accepted final quotation.";
                elseif ($json['status'] == 5):
                    $message = "Final quotation sent successfully.";
                elseif ($json['status'] == 6):
                    $message = "Final quotation accepted successfully.";
                elseif ($json['status'] == 7):
                    $message = "Job completed succesfully.";
//                elseif ($json['status'] == 8):
//                    $message = "Invoice sent successfully.";
                elseif ($json['status'] == 9):
                    $message = "Job invoice sent successfully.";
                endif;
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("updateJobStatus") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function jobInvoice() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();
        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Provider_model;
            $data = $cutomers_model->job_invoice($json);
            $message = "Job-invoice could not be created";
            if ($data) {
                $isSuccess = true;
                $message = "Job-invoice created successfully";
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("jobInvoice") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function actionJobRequest() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();
        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            $message = "Action failed!";
            $cutomers_model = new Provider_model;
            $data1 = $cutomers_model->check_job_requested($json);
            if ($data1):
                $data2 = $cutomers_model->action_job_request($json);
                if ($data1) :

                    $isSuccess = true;

                    $message = $json['status'] == 1 ? "Job request accepted successfully" : "Job request rejected";
                endif;
            else:
                $message = 'No longer available!';
            endif;
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("actionJobRequest") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function actionJobQuotation() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();
        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            $message = "Action failed!";
            $cutomers_model = new Provider_model;
            $data1 = $cutomers_model->check_job_requested($json);
            if ($data1):
                $data2 = $cutomers_model->action_job_quotaton($json);
                if ($data1) :

                    $isSuccess = true;

                    $message = $json['status'] == 1 ? "Job quotation accepted successfully" : "Job quotation rejected";
                endif;
            else:
                $message = 'No longer available!';
            endif;
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("actionJobQuotation") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function sendFinalQuotation() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();
        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Provider_model;
            $data = $cutomers_model->send_final_quotation($json);
            $message = "Job quotation sending failed!";
            if ($data) {
                $isSuccess = true;
                $message = "Job quotation sent successfully!";
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("sendFinalQuotation") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function getProviderCompletedJobdetails() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['job_id'])) {
                $obj = new Provider_model;
                $data = $obj->get_provider_completed_jobdetail($json['job_id']);

                if ($data) {
                    $isSuccess = true;
                    $message = "Job Details found";
                } else {
                    $message = "No Job Details found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getProviderCompletedJobdetails") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function getProviderAllAvailableJobs() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json['prov_id'])) {
                $obj = new Provider_model;
                $data = $obj->get_provider_all_available_jobs($json['prov_id']);

                if (count($data) > 0) {
                    $isSuccess = true;
                    $message = "Jobs found";
                } else {
                    $message = "No Job found";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("getProviderAllAvailableJobs") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function reScheduleJobByProvider() {
        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            $cutomers_model = new Provider_model;
            $update = $cutomers_model->re_schedule_job($json);
            $message = "Job rescheduled unsuccessful!";
            if ($update) {
                $isSuccess = true;
                $message = "Job rescheduled successfully";
                //$data = $cutomers_model->get_customer_jobdetail_by_id($update);
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("reScheduleJobByProvider") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function setRescheduleActionByProvider() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json)) {
                $obj = new Provider_model;
                $data = $obj->set_reschedule_actionByProvider($json);

                if ($data) {
                    $isSuccess = true;
                    $message = "Action successful";
                } else {
                    $message = "Action Failed";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("setRescheduleActionByProvider") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    public function setCommision() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json)) {
                $obj = new Provider_model;
                $data = $obj->setCommisionData($json);
             //
             //   
             //         pre($data);die;

                if ($data) {
                    $isSuccess = true;
                    $message = "Action successful";
                } else {
                    $message = "Action Failed";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("setCommision") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    
    
    public function setCommision1() {

        $json = file_get_contents('php://input');
        /* output param set/initialize */
        $isSuccess = false;
        $message = 'Invalid Json Input';
        $data = array();

        /* check if input is in json */
        if (is_json($json)) {
            $json = json_decode($json, true);
            if (isset($json)) {
                $obj = new Provider_model;
                $data = $obj->setCommisionDataYes($json);
             //
             //   
             //         pre($data);die;

                if ($data) {
                    $isSuccess = true;
                    $message = "Action successful";
                } else {
                    $message = "Action Failed";
                }
            }
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . site_url("setCommision") . PHP_EOL;
        $log = $log . "Request " . json_encode($json) . PHP_EOL;
        $log = $log . "Responce " . json_encode(array("Result" => $data)) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/Provider.txt', $log, FILE_APPEND);
    }

    
    
    
}
