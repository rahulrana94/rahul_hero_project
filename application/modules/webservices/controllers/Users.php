<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller 
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model'); 
        $this->load->library('email');
         error_reporting(E_ALL ^ (E_NOTICE | E_WARNING)); 
    }



    public function userSignUp()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['firstName']          = $json['firstName'];      
            $document['email']              = $json['email'];
            $document['phone']              = $json['phone'];
            $document['password']           = $json['password'];
            $document['profileImage']       = $json['profileImage'];
            $document['deviceToken']        = $json['deviceToken'];
            $document['deviceType']         = $json['deviceType'];
           
            $profileImage                   = $json['profileImage'];

            $isSuccess                      = false;
            $obj                            = new user_model;
            
            if($profileImage==NULL or $profileImage=="")
            {  
                $document['profileImage'] = "";  
            }
            else
            {  
                $document['profileImage']       = $obj->makeBase64Img($profileImage);
            }
                

            $isUserExist                       = $obj->isUserExist($document);
            
            if(!$isUserExist)
            {
                $userSignUp                    = $obj->userSignUp($document);
               
                if($userSignUp)
                {
                    $isSuccess                    = true;
                    $message                      = "User registered successfully";
                    if(empty($userSignUp['profileImage'])){
                       
                    $userSignUp['profileImage'] = "";
                        
                    }
                    else
                    {
                       $userSignUp['profileImage'] = "http://ec2-52-91-61-98.compute-1.amazonaws.com/spiritBox/profileImages/".$userSignUp['profileImage']; 
                    }
                    $data                         = $userSignUp;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "User could not register";
                    $data           = array();
                }
            }
            else
            {
                $isSuccess      = false;
                $message        = "User already registered";
                $data           = $isUserExist;
            } 
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }

    
    
    
    public function editProfile()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['userId']             = $json['userId']; 
            $document['firstName']          = $json['firstName'];      
            $document['email']              = $json['email'];
            $document['phone']              = $json['phone'];
            $document['password']           = $json['password'];
            $document['profileImage']       = $json['profileImage'];
           
            $profileImage                   = $json['profileImage'];


            $isSuccess                      = false;
            $obj                            = new user_model;
            
              
            if($profileImage==NULL or $profileImage=="")
            {  
                $document['profileImage'] = "";  
            }
            else
            {  
                $document['profileImage']       = $obj->makeBase64Img($profileImage);
            }
                
          
            $editProfile                    = $obj->editProfile($document);
                
            if($editProfile)
                {
                    $isSuccess      = true;
                    $message        = "Updation successful";
                    $editProfile['profileImage'] = "http://ec2-52-91-61-98.compute-1.amazonaws.com/spiritBox/profileImages/".$editProfile['profileImage'];
                    $data           = $editProfile;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Could not update";
                    $data           = array();
                }
            }
            
        
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }

    
      public function editLeftPersonProfile()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['id']                 = $json['id']; 
            $document['userId']             = $json['userId'];
            $document['name']               = $json['name'];      
            $document['relation']           = $json['relation'];
            $document['birthDate']          = $json['birthDate'];
            $document['deathDate']          = $json['deathDate'];
            $document['graveLocation']      = $json['graveLocation'];
            $document['graveImage']         = $json['graveImage'];
           
           $graveImage                      = $json['graveImage'];

            $isSuccess                      = false;
            $obj                            = new user_model;
            
              
            if($graveImage==NULL or $graveImage=="")
            {  
                $document['graveImage']     = "";  
            }
            else
            {  
                $document['graveImage']      = $obj->makeBase64Img($graveImage);
            }
                
          
            $editLeftPersonProfile           = $obj->editLeftPersonProfile($document);
                
            if($editLeftPersonProfile)
                {
                    $isSuccess      = true;
                    $message        = "Updation successful";
                    $editLeftPersonProfile['graveImage'] = "http://ec2-52-91-61-98.compute-1.amazonaws.com/spiritBox/profileImages/".$editLeftPersonProfile['graveImage'];
                    $data           = $editLeftPersonProfile;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Could not update";
                    $data           = array();
                }
            }
            
        
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }

    
    
    public function leftPersonProfile()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['userId']             = $json['userId'];
            $document['name']               = $json['name'];      
            $document['relation']           = $json['relation'];
            $document['birthDate']          = $json['birthDate'];
            $document['deathDate']          = $json['deathDate'];
            $document['graveLocation']      = $json['graveLocation'];
            $document['graveImage']         = $json['graveImage'];
           
           $graveImage                   = $json['graveImage'];

            $isSuccess                      = false;
            $obj                            = new user_model;

          
                
                if($graveImage==NULL or $graveImage=="")
                {  
                    $document['graveImage'] = "";  
                }
                else
                {  
                   $document['graveImage']       = $obj->makeBase64Img($graveImage);
                }
                
                $leftPersonAdd                    = $obj->leftPersonAdd($document);
                if($leftPersonAdd)
                {
                    $isSuccess      = true;
                    $message        = "Left person profile added  successfully";
                    $data           = $leftPersonAdd;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Left person profile  could not add";
                    $data           = array();
                }
           /* } 
            else
            {
                $isSuccess      = false;
                $message        = "Profile already exists";
                $data           = $isLeftPersonExist;
            } */
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }



    public function createProfileUser()
    {            
        $json                               = file_get_contents('php://input');     
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();

            $document['firstName']          = $json['firstName'];          
            $document['email']              = $json['email'];          
            $document['phone']              = $json['phone'];
            $document['website']            = $json['website'];
            $document['barType']            = $json['website'];
            $document['musicType']          = $json['musicType'];
            $document['musicGenre']         = $json['musicGenre'];
            $document['amenities']          = $json['amenities'];
            $document['foodType']           = $json['foodType'];
            $document['avgCrowd']           = $json['avgCrowd'];
           // $document['deviceToken']        = $json['deviceToken'];
           // $document['deviceType']         = $json['deviceType'];

            $profileImage                   = $json['profileImage'];

            $isSuccess                      = false;
            $obj                            = new user_model;

            $isUserExist                        = $obj->isUserExist($document);
            
            if(!$isUserExist)
            {

                if($profileImage==NULL or $profileImage=="")
                {  
                    $document['profileImage'] = "";  
                }
                else
                {  
                   $document['profileImage']       = $obj->makeBase64Img($profileImage);
                }

                $results                        = $obj->updateProfile($document);
                if($results=="true")
                { 
                    //$user           =   $obj->getProfile($results);
                    $isSuccess      = true;
                    $message        = "Profile created successfully";
                    //$data           = $user;
                    $data           = true;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Profile could not create";
                    $data           = array();
                }
            }
            else
            {
                $isSuccess      = false;
                $message        = "Profile already exist";
                $data           = $isUserExist;
            }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }


    public function forgetPasswordByOTP()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['email']              = $json['email'];
            $document['verificationCode']   = rand(1000,9999);

            $isSuccess                      = false;
            $obj                            = new user_model;
            $isUserExist                    = $obj->isUserExist($document);
            if($isUserExist)
            {
                $updateProfile                    = $obj->updateProfile($document);

                    $to = $document['email'];
                    $msg = "Your OTP is ".$document['verificationCode'];
                    $sub = "Your OTP from Spirit Box";
                    $headers = 'From: Spirit Box <sumit.chauhan@fourthscreenlabs.com>' . "\r\n";

                    @$sentmail = mail($to,$sub,$msg,$headers);
                if(!empty($sentmail))
                {
                    $isSuccess      = true;
                    $message        = "OTP sent successfully";
                    $data           = array();
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Otp not sent";
                    $data           = array();
                }
                
            }
            else
            {
                $isSuccess      = false;
                $message        = "User not exist";
                $data           = array();
            }         
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }

    public function UserVerifyCode()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();
            $document['email']              = $json['email'];
            $document['verificationCode']   = $json['otp'];
            $isSuccess                      = false;
            $message                        = "";
            $obj                            = new user_model;          
            $results                        = $obj->UserVerifyCode($document);             
            if($results==1)
            {
                $isSuccess      = true;
                $message        = "OTP verified successfully";
                $data           = $results;
            }
            elseif($results==2)
            {
                $isSuccess      = false;
                $message        = "OTP mismatched";
                $data           = array();
            }
            elseif($results==3)
            {
                $isSuccess      = false;
                $message        = "user not exist";
                $data           = array();
            }
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }


    public function changePassword()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['email']              = $json['email'];
            $document['password']           = $json['newPassword'];

            $isSuccess                      = false;
            $obj                            = new user_model;


            $isUserExist                    = $obj->isUserExist($document);

            if($isUserExist)
            {
                $results                    = $obj->changePassword($document);
                if(!empty($results))
                {  
                    $isSuccess      = true;
                    $message        = "Password changed successfully";
                    $data           = array();
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "password could not change";
                    $data           = array();
                }
            }
            else
            {
                $isSuccess      = false;
                $message        = "User not exist";
                $data           = array();
            }         
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }



    public function userLogin()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();        
            $document['email']              = $json['email'];
            $document['password']           = $json['password'];
            $document['deviceToken']        = $json['deviceToken'];
            $document['deviceType']         = $json['deviceType'];
            $isSuccess                      = false;
            $message                        = "";
            $obj                            = new user_model;   

            $isUserExist                    = $obj->isUserExist($document);

            if($isUserExist)
            {
                $user                    = $obj->userLogin($document); 
                if($user)
                {
                    $results                        = $obj->updateProfile($document);
                   // $userMemoriId                    = $obj->userMemoriId($document); 
                    unset($user['verificationCode']);
                   
                    $isSuccess      = true;
                    $message        = "Login successfully";
                    $messageCode    =  "1";
                    $user['profileImage'] = "http://ec2-52-91-61-98.compute-1.amazonaws.com/spiritBox/profileImages/".$user['profileImage'];
                    $data           = $user;
                    
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Wrong password!";
                    $messageCode    =  "2";
                    $data           = array();
                }
            }
            else
            {
                    $isSuccess      = false;
                    $message        = "User doesn't exist!";
                    $messageCode    =  "3";
                    $data           = array();
            }
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }

    
    public function userChat()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();        
            $document['id']                 = $json['groupId'];
            $document['userId']             = $json['userId'];
            $document['message']            = $json['message'];
            $document['created_at']         = $json['time'];
           // $document['chat_image']         = $json['chat_image'];
            //$chat_image                     = $json['chat_image'];
            $isSuccess                      = false;
           
            $obj                            = new user_model;   
  
          
                $userChat                    = $obj->userChat($document); 
                if($userChat)
                {
                   
                    $isSuccess      = true;
                    $message        = "Message Successful";
                    $messageCode    =  "1";
                   // $user['profileImage'] = "http://fourthscreenlabs.com/jai/spiritBox/profileImages/".$user['profileImage'];
                    $data           = $userChat;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Message Not Sent";
                    $messageCode    =  "2";
                    $data           = array();
                }
           
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }

    
     public function userChatDisplay()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();        
            $document['id']                 = $json['groupId'];
            //$document['userId']             = $json['userId'];
            $isSuccess                      = false;
           
            $obj                            = new user_model;   
         
          
                $userChatDisplay                    = $obj->userChatDisplay($document); 
                if($userChatDisplay)
                {
                   unset($userChatDisplay['chat_id']);
                    $isSuccess      = true;
                    $message        = "Chat Display Successful";
                    $messageCode    =  "1";
                   // $user['profileImage'] = "http://fourthscreenlabs.com/jai/spiritBox/profileImages/".$user['profileImage'];
                    $data           = $userChatDisplay;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Wrong Credentials";
                    $messageCode    =  "2";
                    $data           = array();
                }
           
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }

  public function groupDisplay()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();        
            $document['userId']             = $json['userId'];
            //$document['userId']             = $json['userId'];
            $isSuccess                      = false;
           
            $obj                            = new user_model;   
         
          
                $groupDisplay                    = $obj->groupDisplay($document); 
                if($groupDisplay)
                {
                   
                    $isSuccess      = true;
                    $message        = "Group Display Successful";
                    $messageCode    =  "1";
                   // $user['profileImage'] = "http://fourthscreenlabs.com/jai/spiritBox/profileImages/".$user['profileImage'];
                    $data           = $groupDisplay;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Wrong Credentials";
                    $messageCode    =  "2";
                    $data           = array();
                }
           
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }


   
  public function notificationDisplay()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();        
            $document['to_id']             = $json['userId'];
          
            $isSuccess                      = false;
           
            $obj                            = new user_model;   
         
          
                $notificationDisplay                    = $obj->notificationDisplay($document); 
                if($notificationDisplay)
                {
                   
                    $isSuccess      = true;
                    $message        = "Group Display Successful";
                    $messageCode    =  "1";
                   // $user['profileImage'] = "http://fourthscreenlabs.com/jai/spiritBox/profileImages/".$user['profileImage'];
                    $data           = $notificationDisplay;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "No Notifications";
                    $messageCode    =  "2";
                    $data           = array();
                }
           
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }


    
    public function leftPersonProfileDisplay()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();
            //$document['userId']             = $json['userId'];          
            $document['id']                 = $json['id'];        
            $isSuccess                      = false;
            $message                        = "";
            $obj                            = new user_model;   



                    $leftPersonExist                   = $obj->leftPersonExist($document); 
                  
                    if($leftPersonExist)
                    {
                        $leftPersonExist['graveImage'] = "http://ec2-52-91-61-98.compute-1.amazonaws.com/spiritBox/profileImages/".$leftPersonExist['graveImage'];
                        $isSuccess      = true;
                        $message        = "Left Person Profile Display Successful";
                        $messageCode    =  "1";
                        $data           = $leftPersonExist;
                       
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Left Person Profile Display Unsuccessful";
                        $messageCode    =  "2";
                        $data           = array();
                    }
      
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }

    
    public function pushNotification()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();        
            $document['userId']             = $json['userId'];
           // $document['deviceToken']        = $json['deviceToken'];
            $document['message']            = $json['message'];
            $isSuccess                      = false;
           
            $obj                            = new user_model;   
         
          
                $pushNotification                    = $obj->pushNotification($document); 
                if($pushNotification)
                {
                   
                    $isSuccess      = true;
                    $message        = "Push Message Sent Successfully";
                    $messageCode    =  "1";
                  
                    $data           = $pushNotification;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Push Message Not Sent";
                    $messageCode    =  "2";
                    $data           = array();
                }
           
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }

 
    
    public function pushFriendRequest()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();        
            $document['from_id']            = $json['from_id'];
            $document['to_id']              = $json['to_id'];
            $document['id']                 = $json['id'];
           
            $isSuccess                      = false;
           
            $obj                            = new user_model;   
         
          
                $pushFriendRequest                    = $obj->pushFriendRequest($document); 
                if($pushFriendRequest)
                {
                  // echo $pushFriendRequest; die();
                    $isSuccess      = true;
                    $message        = "Push Message Sent Successfully";
                    $messageCode    =  "1";
                  
                    $data           = $pushFriendRequest;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Push Message Not Sent";
                    $messageCode    =  "2";
                    $data           = array();
                }
           
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }

     
    public function acceptFriendRequest()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();        
            $document['userId']             = $json['userId'];
           // $document['deviceToken']        = $json['deviceToken'];
            $document['id']                 = $json['id'];
            $isSuccess                      = false;
           
            $obj                            = new user_model;   
         
          
                $acceptFriendRequest                    = $obj->acceptFriendRequest($document); 
                if($acceptFriendRequest)
                {
                   
                    $isSuccess      = true;
                    $message        = "Friend Request Accepted Successfully";
                    $messageCode    =  "1";
                  
                    $data           = $acceptFriendRequest;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Friend Request Not Accepted Successfully";
                    $messageCode    =  "2";
                    $data           = array();
                }
           
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }

    
    
    
    
    
    public function inviteUsers()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();        
            $document['phone']              = $json['phone'];
            $document['userId']             = $json['userId'];
            $document['id']                 = $json['id'];
            $isSuccess                      = false;
           
            $obj                            = new user_model;   
         
          
                $inviteUsers                    = $obj->inviteUsers($document); 
                if($inviteUsers)
                {
                   
                    $isSuccess      = true;
                    $message        = "Chat Display Successful";
                    $messageCode    =  "1";
                   // $inviteUsers['profileImage'] = "http://fourthscreenlabs.com/jai/spiritBox/profileImages/".$inviteUsers['profileImage'];
                    $data           = $inviteUsers;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Wrong Credentials";
                    $messageCode    =  "2";
                    $data           = array();
                }
           
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }

    
      
    public function sendInviteMail()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $document                       = array();        
            $document['email']              = $json['email'];
            $document['userId']             = $json['userId'];
           
            $isSuccess                      = false;
           
            $obj                            = new user_model;   
         
          
                $sendInviteMail                    = $obj->sendInviteMail($document); 
                if($sendInviteMail)
                {
                   
                    $isSuccess      = true;
                    $message        = "Email Sent Successfully";
                    $messageCode    =  "1";               
                    $data           = $sendInviteMail;
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Email Not Sent";
                    $messageCode    =  "2";
                    $data           = array();
                }
           
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }

        
    public function birthdayPush()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $tday = date('Y-m-d');
            $isSuccess                      = false;
           
            $obj                            = new user_model;   
         
          
                $birthdayPush                   = $obj->birthdayPush($tday); 
                if($birthdayPush)
                {
                   
                    $isSuccess      = true;
                    $message        = "Birthday push sent successfully";
                    $messageCode    =  "1";               
                    $data           = array();
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Birthday push not sent successfully";
                    $messageCode    =  "2";
                    $data           = array();
                }
           
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }

    
    
         
    public function deathPush()
    {             
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);       
            $tday                           = date('Y-m-d');
            $isSuccess                      = false;
           
            $obj                            = new user_model;   
         
          
                $deathPush                   = $obj->deathPush($tday); 
                if($deathPush)
                {
                   
                    $isSuccess      = true;
                    $message        = "Death anniversary push sent successfully";
                    $messageCode    =  "1";               
                    $data           = array();
                }
                else
                {
                    $isSuccess      = false;
                    $message        = "Death anniversary push not sent successfully";
                    $messageCode    =  "2";
                    $data           = array();
                }
           
        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $messageCode    =  "";
            $data           = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"messageCode"=>$messageCode,"Result"=>$data));
    }


    

}