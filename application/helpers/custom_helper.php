<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('pre'))
{
    function pre($array)
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }   
}

if ( ! function_exists('is_json'))
{
    function is_json($string,$return_data = false) 
    {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    } 
}

function customer(){
$CI =& get_instance();
$CI->load->model('Admin_model');

$membres = $CI->Admin_model->customer();
if(!empty($membres)){
return $membres;}
else{
    return array();
}
}
 

function providerRating(){
$CI =& get_instance();
$CI->load->model('Admin_model');

$membres = $CI->Admin_model->providerRating();
if(!empty($membres)){
return $membres;}
else{
    return array();
}
}
 
function customerRating(){
$CI =& get_instance();
$CI->load->model('Admin_model');

$membres = $CI->Admin_model->customerRating();
if(!empty($membres)){
return $membres;}
else{
    return array();
}
}

function customerQuote(){
$CI =& get_instance();
$CI->load->model('Admin_model');
$membres = $CI->Admin_model->customerQuote();
if(!empty($membres)){
return $membres;}
else{
    return array();
}
}

function cancelled(){
$CI =& get_instance();
$CI->load->model('Admin_model');
$membres = $CI->Admin_model->cancelled();
if(!empty($membres)){
return $membres;}
else{
    return array();
}
}


function reschedules(){
$CI =& get_instance();
$CI->load->model('Admin_model');
$membres = $CI->Admin_model->reschedules();
if(!empty($membres)){
return $membres;}
else{
    return array();
}
}


function expiredJobs(){
$CI =& get_instance();
$CI->load->model('Admin_model');
$membres = $CI->Admin_model->expiredJobs();
if(!empty($membres)){
return $membres;}
else{
    return array();
}
}

function missedAppointment(){
$CI =& get_instance();
$CI->load->model('Admin_model');
$membres = $CI->Admin_model->missedAppointment();
if(!empty($membres)){
return $membres;}
else{
    return array();
}
}

function lateAppointment(){
$CI =& get_instance();
$CI->load->model('Admin_model');
$membres = $CI->Admin_model->lateAppointment();
if(!empty($membres)){
return $membres;}
else{
    return array();
}
}


function payment(){
$CI =& get_instance();
$CI->load->model('Admin_model');
$membres = $CI->Admin_model->payment();
//pre($membres);die('custom');
if(!empty($membres)){
return $membres;}
else{
    return array();
}
}