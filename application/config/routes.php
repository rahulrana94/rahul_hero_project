<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller']       = 'welcome';
$route['404_override']             = '';
$route['translate_uri_dashes']     = FALSE;

//----------------------------13 Sept 2017  HERO  ----------------------------------//

$route['getPdfDetails']                           = 'webservices/customers/getPdfDetails';
$route['pdfVersionCheck']                         = 'webservices/customers/pdfVersionCheck';


// ___________________________ADMIN ___________________________________________________

$route['admin']                                   = 'admin_panel/admin';
$route['admin/dashboard']                         = 'admin_panel/admin/dashboard';
$route['admin/logout']                            = 'admin_panel/admin/logout';

// Catalogue Management
$route['admin/uploadPDF']                         = 'admin_panel/admin/uploadPDF';
$route['admin/listUploadPDF']                     = 'admin_panel/admin/listUploadPDF';

// Role Management
$route['admin/addRole']                           = 'admin_panel/admin/addRole';
$route['admin/listRole']                          = 'admin_panel/admin/listRole';

// User Management
$route['admin/addUser']                           = 'admin_panel/admin/addUser';
$route['admin/myformAjax/(:any)']                 = 'admin_panel/admin/myformAjax/$1';
    


















##################################Customer################################################################
$route['createCProfile']                   = 'webservices/customers/createCustomerProfile';
$route['customerSignUp']                   = 'webservices/customers/customerSignUp';
$route['customerLogin']                    = 'webservices/customers/customerLogin';
$route['passwordRecovery1']                = 'webservices/customers/passwordRecovery';
$route['verifyPhone']                      = 'webservices/customers/verifyPhone';
$route['verifyPhone2']                     = 'webservices/customers/verifyPhone2';
$route['resendOTP']                        = 'webservices/customers/resendOTP';
$route['get_customer_profile']             = 'webservices/customers/get_customer_profile';
$route['update_customer_profile']          = 'webservices/customers/update_customer_profile';
$route['createCustomerJob']                = 'webservices/customers/createCustomerJob';
$route['getCustomerPostedJobs']            = 'webservices/customers/getCustomerPostedJobs';
$route['getCustomerAcceptedJobs']          = 'webservices/customers/getCustomerAcceptedJobs';
$route['getCustomerCompletedJobs']         = 'webservices/customers/getCustomerCompletedJobs';
$route['getCustomerCompletedJobdetails']   = 'webservices/customers/getCustomerCompletedJobdetails';
$route['acceptedJobDetailsCustomer']       = 'webservices/customers/acceptedJobDetailsCustomer';
$route['getCustomerJobdetailById']         = 'webservices/customers/getCustomerJobdetailById';
$route['getProvidersByJobID']              = 'webservices/customers/getProvidersByJobID';
$route['getProvidersDetails']              = 'webservices/customers/getProvidersDetails';
///////////////////////////
$route['getPromoCodeUserCalloutPayment']   = 'webservices/customers/getPromoCodeUserCalloutPayment';
///////////////////////////////
$route['sentJobRequestToProvider']         = 'webservices/customers/sentJobRequestToProvider';
$route['rateTheProvider']                  = 'webservices/customers/rateTheProvider';
$route['getFinalQuotation']                = 'webservices/customers/getFinalQuotation';
$route['setActionFinalQuotation']          = 'webservices/customers/setActionFinalQuotation';
$route['reScheduleJobByCustomer']          = 'webservices/customers/reScheduleJobByCustomer';
$route['cancelJob1']                       = 'webservices/customers/cancelJob';
$route['setRescheduleActionByCustomer']    = 'webservices/customers/setRescheduleActionByCustomer';
$route['finishImmidiateCallout']           = 'webservices/customers/finishImmidiateCallout';
$route['saveInitialPaymentDetails']        = 'webservices/customers/saveInitialPaymentDetails';
$route['updatePaymentDetails']             = 'webservices/customers/updatePaymentDetails';
$route['makePaymentbyToken']               = 'webservices/customers/makePaymentbyToken';
$route['customerPaymentHistory']           = 'webservices/customers/customerPaymentHistory';
$route['demoPush']                         = 'webservices/customers/demoPush';

###################################Provider############################################################
$route['createProviderProfile']            = 'webservices/provider/createProviderProfile';
$route['providerLogin']                    = 'webservices/provider/providerLogin';
$route['getProviderDetails']               = 'webservices/provider/getProviderDetails';
$route['updateProviderDetails']            = 'webservices/provider/updateProviderDetails';
$route['passwordRecovery2']                = 'webservices/provider/passwordRecovery';
$route['dateBasedJobsForProvider']         = 'webservices/provider/dateBasedJobsForProvider';
$route['userBasedJobsForProvider']         = 'webservices/provider/userBasedJobsForProvider';
$route['getAcceptedJobsForProvider']       = 'webservices/provider/getAcceptedJobsForProvider';
$route['sendQuotations']                   = 'webservices/provider/sendQuotations';
$route['getCompletedJobs']                 = 'webservices/provider/getCompletedJobs';
$route['jobDetailsProvider']               = 'webservices/provider/jobDetailsProvider';
$route['acceptedJobDetailsProvider']       = 'webservices/provider/acceptedJobDetailsProvider';
//$route['cancelJob2']                     = 'webservices/provider/cancelJob';
$route['rateTheCustomer']                  = 'webservices/provider/rateTheCustomer';
$route['startJob']                         = 'webservices/provider/startJob';
$route['updateJobStatus']                  = 'webservices/provider/updateJobStatus';
$route['actionJobRequest']                 = 'webservices/provider/actionJobRequest';
$route['actionJobQuotation']               = 'webservices/provider/actionJobQuotation';
$route['jobInvoice']                       = 'webservices/provider/jobInvoice';
$route['sendFinalQuotation']               = 'webservices/provider/sendFinalQuotation';
$route['getProviderCompletedJobdetails']   = 'webservices/provider/getProviderCompletedJobdetails';
$route['getProviderAllAvailableJobs']      = 'webservices/provider/getProviderAllAvailableJobs';
$route['reScheduleJobByProvider']          = 'webservices/provider/reScheduleJobByProvider';
$route['setRescheduleActionByProvider']    = 'webservices/provider/setRescheduleActionByProvider';
$route['setCommision']                     = 'webservices/provider/setCommision';

####################################Category###########################################################
$route['get_sub_category']                 = 'webservices/category/get_sub_category';
$route['searchSubCategory']                = 'webservices/category/searchSubCategory';

#################Push Notification##############################
$route['sendPushNotification']             = 'webservices/PushNotification/sendPushNotification';
$route['getWebHookRequest']                = 'webservices/PushNotification/getWebHookRequest';

#################Web View Pages##############################
$route['tokenization/(:any)']              = 'webview/pay/tokenization';
$route['oneclickpay/(:any)']               = 'webview/pay/oneclickpay';


#################Admin Panel##############################

//   ------------------      Rahul Rana   `10-October-2016`   ---------------
$route['admin']                       = 'admin_panel/admin';
$route['admin/dashboard']             = 'admin_panel/admin/dashboard';
$route['admin/activation']            = 'admin_panel/admin/activation';
$route['admin/logout']                = 'admin_panel/admin/logout';

// job Management
$route['admin/household']             = 'admin_panel/admin/household';
$route['admin/outdoor']               = 'admin_panel/admin/outdoor';
$route['admin/personal']              = 'admin_panel/admin/personal';
$route['admin/transport']             = 'admin_panel/admin/transport';

//jobs List
$route['admin/householdList']         = 'admin_panel/admin/householdList';
$route['admin/outdoorList']           = 'admin_panel/admin/outdoorList';
$route['admin/personalList']          = 'admin_panel/admin/personalList';
$route['admin/transportList']         = 'admin_panel/admin/transportList';

//Delete List
$route['admin/deleteHouseholdList']   = 'admin_panel/admin/deleteHouseholdList';
$route['admin/deleteOutdoorList']     = 'admin_panel/admin/deleteOutdoorList';
$route['admin/deletePersonalList']    = 'admin_panel/admin/deletePersonalList';
$route['admin/deleteTransportList']   = 'admin_panel/admin/deleteTransportList';

//Qoute Management
$route['admin/setting']               = 'admin_panel/admin/setting';
$route['admin/settingList']           = 'admin_panel/admin/settingList';

//______________________   Rahul Rana   `08-November-2016` _____________________ 

// Provider Management
$route['admin/providersDetails']         = 'admin_panel/admin/providersDetails';
$route['admin/businessDetails']          = 'admin_panel/admin/businessDetails';
$route['admin/deleteBusinessDetails']    = 'admin_panel/admin/deleteBusinessDetails';
$route['admin/documentDetails']          = 'admin_panel/admin/documentDetails';
$route['admin/deleteDocumentDetails']    = 'admin_panel/admin/deleteDocumentDetails';
$route['admin/addProviders']             = 'admin_panel/admin/addProviders';
$route['admin/updateProviderListReject'] = 'admin_panel/admin/updateProviderListReject';
$route['admin/updateProviderListAccept'] = 'admin_panel/admin/updateProviderListAccept';
$route['admin/findProviderMap']          = 'admin_panel/admin/findProviderMap';
$route['admin/deleteProviderList']       = 'admin_panel/admin/deleteProviderList';


// Sub Category ManagementsubHousehold
$route['admin/subHousehold']            = 'admin_panel/admin/subHousehold';
$route['admin/subOutdoor']              = 'admin_panel/admin/subOutdoor';
$route['admin/subPersonal']             = 'admin_panel/admin/subPersonal';
$route['admin/subTransport']            = 'admin_panel/admin/subTransport';
$route['admin/SubCategoryList']         = 'admin_panel/admin/SubCategoryList';
$route['admin/deleteSubcategoryList']   = 'admin_panel/admin/deleteSubcategoryList';
$route['admin/updateSubCategoryList']   = 'admin_panel/admin/updateSubCategoryList';

// productivity management
$route['admin/jobquoted']               = 'admin_panel/admin/jobquoted';
$route['admin/jobsCompleted']           = 'admin_panel/admin/jobsCompleted';
$route['admin/paymentList']             = 'admin_panel/admin/paymentList';

// Customer List
$route['admin/customerList']            = 'admin_panel/admin/customerList';
$route['admin/deleteCustomerList']      = 'admin_panel/admin/deleteCustomerList';
$route['admin/jobsPosted']              = 'admin_panel/admin/jobsPosted';
$route['admin/quoteRecieved']           = 'admin_panel/admin/quoteRecieved';
$route['admin/customerPayment']         = 'admin_panel/admin/customerPayment';

// Jobs Status List
$route['admin/available']               = 'admin_panel/admin/available';
$route['admin/accepted']                = 'admin_panel/admin/accepted';
$route['admin/requested']               = 'admin_panel/admin/requested';
$route['admin/pending']                 = 'admin_panel/admin/pending';
$route['admin/completed']               = 'admin_panel/admin/completed';
$route['admin/cancelled']               = 'admin_panel/admin/cancelled';
$route['admin/reschedules']             = 'admin_panel/admin/reschedules';
$route['admin/expiredJobs']             = 'admin_panel/admin/expiredJobs';
$route['admin/lateAppointment']         = 'admin_panel/admin/lateAppointment';
$route['admin/missedAppointment']       = 'admin_panel/admin/missedAppointment';
  
//Location
$route['admin/householdLocation']       = 'admin_panel/admin/householdLocation';
$route['admin/outdoorLocation']         = 'admin_panel/admin/outdoorLocation';
$route['admin/personalLocation']        = 'admin_panel/admin/personalLocation';
$route['admin/transportLocation']       = 'admin_panel/admin/transportLocation';
$route['admin/findMap']                 = 'admin_panel/admin/findMap';

// Rating
$route['admin/providerRating']          = 'admin_panel/admin/providerRating';
$route['admin/customerRating']          = 'admin_panel/admin/customerRating';

// Delete Rating
$route['admin/deleteProviderRating']    = 'admin_panel/admin/deleteProviderRating';
$route['admin/deleteCustomerRating']    = 'admin_panel/admin/deleteCustomerRating';

//Payment
$route['admin/customerQuotesPayment']        = 'admin_panel/admin/customerQuotesPayment';
$route['admin/quotesPayment']                = 'admin_panel/admin/quotesPayment';
$route['admin/invoicesPayment']              = 'admin_panel/admin/invoicesPayment';
$route['admin/calloutChargePayment']         = 'admin_panel/admin/calloutChargePayment';
$route['admin/totalRevenue']                 = 'admin_panel/admin/totalRevenue';
$route['admin/totalCallout']                 = 'admin_panel/admin/totalCallout';
$route['admin/totalCommission']              = 'admin_panel/admin/totalCommission';
$route['admin/providerAccounts']             = 'admin_panel/admin/providerAccounts';

$route['admin/updateProviderAccounts']       = 'admin_panel/admin/updateProviderAccounts';
$route['admin/updateProviderAccountsUnpaid'] = 'admin_panel/admin/updateProviderAccountsUnpaid';
$route['admin/deleteProviderAccounts']       = 'admin_panel/admin/deleteProviderAccounts';
$route['admin/deleteCustomerQuotesPayment']  = 'admin_panel/admin/deleteCustomerQuotesPayment';
$route['admin/deleteQuotesPayment']          = 'admin_panel/admin/deleteQuotesPayment';
$route['admin/deleteInvoicesPayment']        = 'admin_panel/admin/deleteInvoicesPayment';

// PromoCode management
$route['admin/promoCode']                     = 'admin_panel/admin/promoCode';
$route['admin/promoLists']                    = 'admin_panel/admin/promoLists';
$route['admin/deletePromoCode']               = 'admin_panel/admin/deletePromoCode';

// delete Panding Jobs
$route['admin/deletePending']                 = 'admin_panel/admin/deletePending';

// ------------------------30 March 2017 -------------------------------

$route['admin/revenue']                  = 'admin_panel/admin/revenue';
$route['admin/callout']                  = 'admin_panel/admin/callout';
$route['admin/commission']               = 'admin_panel/admin/commission';



