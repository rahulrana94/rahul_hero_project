<?php

defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//-------------------------------------------------------------------//
##################################Customer################################################################
$route['createCProfile'] = 'webservices/customers/createCustomerProfile';
$route['customerSignUp'] = 'webservices/customers/customerSignUp';
$route['customerLogin'] = 'webservices/customers/customerLogin';
$route['passwordRecovery1'] = 'webservices/customers/passwordRecovery';
$route['verifyPhone'] = 'webservices/customers/verifyPhone';
$route['verifyPhone2'] = 'webservices/customers/verifyPhone2';
$route['resendOTP'] = 'webservices/customers/resendOTP';
$route['get_customer_profile'] = 'webservices/customers/get_customer_profile';
$route['update_customer_profile'] = 'webservices/customers/update_customer_profile';

$route['createCustomerJob'] = 'webservices/customers/createCustomerJob';

$route['getCustomerPostedJobs'] = 'webservices/customers/getCustomerPostedJobs';
$route['getCustomerAcceptedJobs'] = 'webservices/customers/getCustomerAcceptedJobs';
$route['getCustomerCompletedJobs'] = 'webservices/customers/getCustomerCompletedJobs';
$route['getCustomerCompletedJobdetails'] = 'webservices/customers/getCustomerCompletedJobdetails';

$route['acceptedJobDetailsCustomer'] = 'webservices/customers/acceptedJobDetailsCustomer';

$route['getCustomerJobdetailById'] = 'webservices/customers/getCustomerJobdetailById';

$route['getProvidersByJobID'] = 'webservices/customers/getProvidersByJobID';
$route['getProvidersDetails'] = 'webservices/customers/getProvidersDetails';
$route['sentJobRequestToProvider'] = 'webservices/customers/sentJobRequestToProvider';

$route['rateTheProvider'] = 'webservices/customers/rateTheProvider';
$route['getFinalQuotation'] = 'webservices/customers/getFinalQuotation';
$route['setActionFinalQuotation'] = 'webservices/customers/setActionFinalQuotation';
$route['reScheduleJobByCustomer'] = 'webservices/customers/reScheduleJobByCustomer';
$route['cancelJob1'] = 'webservices/customers/cancelJob';
$route['setRescheduleActionByCustomer'] = 'webservices/customers/setRescheduleActionByCustomer';

$route['finishImmidiateCallout'] = 'webservices/customers/finishImmidiateCallout';
$route['saveInitialPaymentDetails'] = 'webservices/customers/saveInitialPaymentDetails';
$route['updatePaymentDetails'] = 'webservices/customers/updatePaymentDetails';
$route['makePaymentbyToken'] = 'webservices/customers/makePaymentbyToken';
$route['customerPaymentHistory'] = 'webservices/customers/customerPaymentHistory';
###################################Provider############################################################
$route['createProviderProfile'] = 'webservices/provider/createProviderProfile';
$route['providerLogin'] = 'webservices/provider/providerLogin';
$route['getProviderDetails'] = 'webservices/provider/getProviderDetails';
$route['updateProviderDetails'] = 'webservices/provider/updateProviderDetails';
$route['passwordRecovery2'] = 'webservices/provider/passwordRecovery';

$route['dateBasedJobsForProvider'] = 'webservices/provider/dateBasedJobsForProvider';
$route['getAcceptedJobsForProvider'] = 'webservices/provider/getAcceptedJobsForProvider';
$route['sendQuotations'] = 'webservices/provider/sendQuotations';
$route['getCompletedJobs'] = 'webservices/provider/getCompletedJobs';
$route['jobDetailsProvider'] = 'webservices/provider/jobDetailsProvider';
$route['acceptedJobDetailsProvider'] = 'webservices/provider/acceptedJobDetailsProvider';
//$route['cancelJob2'] = 'webservices/provider/cancelJob';
$route['rateTheCustomer'] = 'webservices/provider/rateTheCustomer';
$route['startJob'] = 'webservices/provider/startJob';
$route['updateJobStatus'] = 'webservices/provider/updateJobStatus';
$route['actionJobRequest'] = 'webservices/provider/actionJobRequest';
$route['actionJobQuotation'] = 'webservices/provider/actionJobQuotation';
$route['jobInvoice'] = 'webservices/provider/jobInvoice';
$route['sendFinalQuotation'] = 'webservices/provider/sendFinalQuotation';
$route['getProviderCompletedJobdetails'] = 'webservices/provider/getProviderCompletedJobdetails';
$route['getProviderAllAvailableJobs'] = 'webservices/provider/getProviderAllAvailableJobs';
$route['reScheduleJobByProvider'] = 'webservices/provider/reScheduleJobByProvider';
$route['setRescheduleActionByProvider'] = 'webservices/provider/setRescheduleActionByProvider';

####################################Category###########################################################
$route['get_sub_category'] = 'webservices/category/get_sub_category';
$route['searchSubCategory'] = 'webservices/category/searchSubCategory';

#################Push Notification##############################
$route['sendPushNotification'] = 'webservices/PushNotification/sendPushNotification';
$route['getWebHookRequest'] = 'webservices/PushNotification/getWebHookRequest';

#################Web View Pages##############################
$route['tokenization/(:any)'] = 'webview/pay/tokenization';
$route['oneclickpay/(:any)'] = 'webview/pay/oneclickpay';
#################Admin Panel##############################

$route['admin']                       = 'admin_panel/admin';
$route['admin/dashboard']             = 'admin_panel/admin/dashboard';
$route['admin/providersDetails']      = 'admin_panel/admin/providersDetails';
$route['admin/activation']            = 'admin_panel/admin/activation';
$route['admin/logout']                = 'admin_panel/admin/logout';
$route['admin/household']             = 'admin_panel/admin/household';
$route['admin/outdoor']               = 'admin_panel/admin/outdoor';
$route['admin/personal']              = 'admin_panel/admin/personal';
$route['admin/transport']             = 'admin_panel/admin/transport';


$route['admin/setting']             = 'admin_panel/admin/setting';
$route['admin/settingList']             = 'admin_panel/admin/settingList';

